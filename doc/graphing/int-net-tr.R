#!/usr/bin/Rscript

# int-net-tr.R
# Simple R script to graph - Consumer Satisfied Interests / Network Timed out interests
# from nnnsim tracers
#
# Copyright (c) 2018 Jairo Eduardo Lopez
#
# int-net-tr.R is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# int-net-tr.R is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of              
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               
# GNU Affero Public License for more details.                                 
#                                                                             
# You should have received a copy of the GNU Affero Public License            
# along with int-net-tr.R.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
#

suppressPackageStartupMessages(library (ggplot2))
suppressPackageStartupMessages(library (scales))
suppressPackageStartupMessages(library (optparse))
suppressPackageStartupMessages(library (doBy))
suppressPackageStartupMessages(library (plyr))

# set some reasonable defaults for the options that are needed
option_list <- list (
  make_option(c("-f", "--file"), type="character", default="results/aggregate-trace.txt",
              help="File which holds the raw aggregate data.\n\t\t[Default \"%default\"]"),
  make_option(c("-c", "--consumer"), type="character", default="0",
              help="Consumer node data to graph. Can be a comma separated list.\n\t\tDefault \"%default\""),
  make_option(c("-p", "--producer"), type="character", default="1",
              help="Producer node data to ignore. Can be a comma separated list.\n\t\tDefault \"%default\""),
  make_option(c("-o", "--output"), type="character", default=".",
              help="Output directory for graphs.\n\t\t[Default \"%default\"]"),
  make_option(c("--txt"), action="store_true", default=FALSE,
              help="Flag to export data to space separated txt file\n\t\t[Default \"%default\"]"),
  make_option(c("-s", "--seq"), type="integer", default=100,
              help="Sets how apart the ticks are on graph.\n\t\t[Default \"%default\"]"),
  make_option(c("-t", "--title"), type="character", default="Scenario",
              help="Title for the graph")
)

# Load the parser
opt <- parse_args(OptionParser(option_list=option_list, description="Creates graphs from L3 Data Aggregate Tracer data"))

data <- read.table (opt$file, header=T)
data$Node <- factor (data$Node)
data$Kilobits <- data$Kilobytes * 8
data$Type <- factor (data$Type)

# Eliminate the information from local devices
intdata = subset (data, !grepl("dev=local", FaceDescr))

consumer_nodes <- unlist(strsplit(opt$consumer, ","))

producer_nodes <- unlist(strsplit(opt$producer, ","))

intdata.con <- subset (intdata, Node %in% consumer_nodes & Type %in% c("SatisfiedInterests"))

keep1 <- c("Time", "Type", "Packets")
consumer <- intdata.con[keep1]

intdata.net <- subset (intdata, !(Node %in% consumer_nodes) & !(Node %in% producer_nodes) & Type %in% c("TimedOutInterests"))

intdata.net.combined <- summaryBy (. ~ Time + Type, data=intdata.net, FUN=sum)

keep2 <- c("Time", "Type", "Packets.sum")
network <- intdata.net.combined[keep2]

colnames(network) <- c("Time", "Type", "Packets")

combinedlist <- list (consumer, network)

res <- do.call(rbind.fill, combinedlist)

# Sequence for ticks
tbreak = seq (0, ceiling(max(data$Time)), opt$seq)

# Set the theme for graph output
theme_set(theme_grey(base_size = 24) + 
            theme(axis.text = element_text(colour = "black")))

# Get the basename of the file
tmpname = strsplit(opt$file, "/")[[1]]
filename = tmpname[length(tmpname)]
# Get rid of the extension
noext = gsub("\\..*", "", filename)

grlabel = c("Consumer Satisfied Interests", "Network Timed Out Interests")

g.int <- ggplot(res, aes(x=Time, y=Packets)) +
  geom_line(aes (linetype=Type), size=0.5) +
  geom_point(aes (shape=Type), size=3) +
  scale_linetype_discrete(name="Labels", labels = grlabel) +
  scale_shape_discrete(name="Labels", labels = grlabel) +
  ggtitle ("Interests forwarded per second") +
  ylab ("Packets / Seconds") +
  xlab ("Simulation time [Seconds]") +
  scale_x_continuous (breaks=tbreak)

outInpng = ""
# The output png
outInpng = sprintf("%s/%s-ns.png", opt$output, noext)
if (opt$txt)
{
  outtxt = sprintf("%s/%s-ns.txt", opt$output, noext)
  write.table (res, file=outtxt)
}

png (outInpng, width=1024, height=768)
print (g.int)
x = dev.off ()