#!/bin/bash

# icc-graphing.sh
#
# Script for graphing 3N ICC scenario results.
# These scenarios alternates the position of the consumer and the producer
# Graphs PROC number of scenarios at the same time.
#
# Copyright (c) 2016 Jairo Eduardo Lopez
#
# icc-graphing.sh is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# icc-graphing.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#                                                                             
# You should have received a copy of the GNU Affero Public License
# along with icc-graphing.sh.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
#

SPEEDS="1.40 2.80 4.20 5.60 7.00 8.40 9.80 11.20"
NSLOG=ICCMobility
NSscenario=nnn-icc-mobility
NUMSEC=01-001-004
SCDE=${NUMSEC}.txt

GRHDIR=./src/nnnsim/doc/graphing

CONSUMER=0
PRODUCER=7

T1="3n"
T2="smart"
STR1="NDN Smart Flooding"
STR2="3N Architecture"
MAIN=.
DIR=${MAIN}/results
OUTPUT=$DIR/graphs
LOSS=0.5
SEQ=20
EXTRA="--shape"
PROC=3
GOODPUT=8.192

###############################################################################

CON=0
PROD=0
EXE=0

if [ ! -d $OUTPUT ]; then
    echo "Error: Please create ${OUTPUT}"
    exit 1
fi

speedstr=""
for i in $SPEEDS
do
    speed=${i//\./-}

    # Create comparison graphs
    # App Delay
    # Consumer Mobility
    FILE=$OUTPUT/$NSLOG-app-delays-$T2-con-$speed-$NUMSEC-comp-delay.png 
    COMP1=$DIR/$NSLOG-app-delays-$T1-con-$speed-$SCDE 
    COMP2=$DIR/$NSLOG-app-delays-$T2-con-$speed-$SCDE
    if [[ $COMP1 -nt $FILE ]] || [[ $COMP2 -nt $FILE ]]; then
        ($GRHDIR/app-data-compare.R -r -d -f $COMP2 -c $COMP1 --str1="$STR1" \
            --str2="$STR2" -t "Mobile Consumer" -s $SEQ --txt -o $OUTPUT) &
        echo "Creating Consumer mobility network delay $i"

        EXE=`expr $EXE + 1`
        CON=`expr $CON + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else
        echo "Delay graph for con-$speed has already been updated"
    fi

    # Producer Mobility
    FILE=$OUTPUT/$NSLOG-app-delays-$T2-prod-$speed-$NUMSEC-comp-delay.png 
    COMP1=$DIR/$NSLOG-app-delays-$T1-prod-$speed-$SCDE 
    COMP2=$DIR/$NSLOG-app-delays-$T2-prod-$speed-$SCDE
    if [[ $COMP1 -nt $FILE ]] || [[ $COMP2 -nt $FILE ]]; then
        ($GRHDIR/app-data-compare.R -r -d -f $COMP2 -c $COMP1 --str1="$STR1" \
            --str2="$STR2" -t "Mobile Producer" -s $SEQ --txt -o $OUTPUT ) &
        echo "Creating Producer mobility network delay $i"

        EXE=`expr $EXE + 1`
        PROD=`expr $PROD + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else
        echo "Delay graph for prod-$speed has already been updated"
    fi

    # Aggregate packets
    # Consumer Mobility
    FILE=$OUTPUT/$NSLOG-aggregate-trace-$T2-con-$speed-$NUMSEC-$CONSUMER-comp-sin.png 
    COMP1=$DIR/$NSLOG-aggregate-trace-$T1-con-$speed-$SCDE 
    COMP2=$DIR/$NSLOG-aggregate-trace-$T2-con-$speed-$SCDE 
    if [[ $COMP1 -nt $FILE ]] || [[ $COMP2 -nt $FILE ]]; then
        ($GRHDIR/int-tr-compare.R -e $CONSUMER -f $COMP2 -c $COMP1 \
            -t "Mobile consumer" --str1="$STR1" --str2="$STR2" -s $SEQ \
            --txt -o $OUTPUT) &
        echo "Creating Consumer mobility Interest success rate $i"

        EXE=`expr $EXE + 1`
        CON=`expr $CON + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else 
        echo "Interest success rate con-$speed has already been updated"
    fi

    # Producer Mobility
    FILE=$OUTPUT/$NSLOG-aggregate-trace-$T2-prod-$speed-$NUMSEC-$PRODUCER-comp-sin.png 
    COMP1=$DIR/$NSLOG-aggregate-trace-$T1-prod-$speed-$SCDE 
    COMP2=$DIR/$NSLOG-aggregate-trace-$T2-prod-$speed-$SCDE 
    if [[ $COMP1 -nt $FILE ]] || [[ $COMP2 -nt $FILE ]]; then

        ($GRHDIR/int-tr-compare.R -e $PRODUCER -f $COMP2 -c $COMP1 \
            -t "Consumer with Mobile Producer" -s $SEQ --str1="$STR1" \
            --str2="$STR2" --txt -o $OUTPUT) &
        echo "Creating Producer mobility Interest success rate $i"

        EXE=`expr $EXE + 1`
        PROD=`expr $PROD + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else
        echo "Interest success rate prod-$speed has already been updated"
    fi

    # Data rate
    # Consumer Mobility
    FILE=$OUTPUT/$NSLOG-rate-trace-$T2-con-$speed-$NUMSEC-$CONSUMER-comp-in.png 
    COMP1=$DIR/$NSLOG-rate-trace-$T1-con-$speed-$SCDE 
    COMP2=$DIR/$NSLOG-rate-trace-$T2-con-$speed-$SCDE 
    if [[ $COMP1 -nt $FILE ]] || [[ $COMP2 -nt $FILE ]]; then
        ($GRHDIR/rate-tr-icn-compare.R -e $CONSUMER \
        -f $COMP2 -c $COMP1 -t "Mobile Consumer" \
        -s $SEQ --str1="$STR1" --str2="$STR2" --txt -o $OUTPUT) &
        echo "Mobile Consumer Data rate $i"

        EXE=`expr $EXE + 1`
        CON=`expr $CON + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else
        echo "Interest success rate for con-$speed has already been updated"
    fi

    # Producer Mobility
    FILE=$OUTPUT/$NSLOG-rate-trace-$T2-prod-$speed-$NUMSEC-$PRODUCER-comp-in.png 
    COMP1=$DIR/$NSLOG-rate-trace-$T1-prod-$speed-$SCDE 
    COMP2=$DIR/$NSLOG-rate-trace-$T2-prod-$speed-$SCDE 
    if [[ $COMP1 -nt $FILE ]] || [[ $COMP2 -nt $FILE ]]; then
        ($GRHDIR/rate-tr-icn-compare.R -e $PRODUCER -f $COMP2 -c $COMP1 \
        -t "Consumer with Mobile Producer" -s $SEQ --str1="$STR1" \
        --str2="$STR2" --txt -o $OUTPUT) &
        echo "Consumer with Mobile Producer Data rate $i"

        EXE=`expr $EXE + 1`
        PROD=`expr $PROD + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else
        echo "Interest success rate for prod-$speed has already been updated"
    fi
done

speedstr=${SPEEDS// /,}

EXE=1
CON=1
PROD=1

if [ $EXE -ge 1 ]; then
    if [ $CON -ge 1 ]; then
        ($GRHDIR/speed-avg-rate.R -s $NSLOG --str1="$STR1" --str2="$STR2" \
            -m "$speedstr" --e1="$CONSUMER" --e2="$PRODUCER" -x $SCDE \
            -d $DIR -c --txt -o $OUTPUT $EXTRA) &
        echo "Data rate vs Speed graphs - Consumer"

        EXE=`expr $EXE + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi

	($GRHDIR/speed-avg-delay.R -s $NSLOG --str1="$STR1" --str2="$STR2" \
	    -m "$speedstr" -c --delay --lgraph --loss="$LOSS" --maxSeq --retx \
            -x $SCDE --ggraph --goodput="$GOODPUT" \
	    --e1="$CONSUMER" --e2="$PRODUCER" -d $DIR --txt \
            -o $OUTPUT $EXTRA ) &
	 echo "Max Seq, loss, retransmission, delay and goodput vs Speed graphs - Consumer"

	 EXE=`expr $EXE + 1`
	 if (( $EXE % $PROC == 0 )); then
	     echo "Already have $PROC processes running, waiting"
	     wait
	 fi
    else
        echo "Consumer summary graphs are up to date"
    fi

    if [ $PROD -ge 1 ]; then
        ($GRHDIR/speed-avg-rate.R -s $NSLOG --str1="$STR1" --str2="$STR2" \
            -m "$speedstr" --e1="$PRODUCER" --e2="$CONSUMER" -x $SCDE \
            -d $DIR -p --txt -o $OUTPUT $EXTRA) &
        echo "Data rate vs Speed graphs - Producer"

        EXE=`expr $EXE + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi

        ($GRHDIR/speed-avg-delay.R -s $NSLOG --str1="$STR1" --str2="$STR2" \
            -m "$speedstr" -p --delay --lgraph --loss="$LOSS" --maxSeq --retx \
            -x $SCDE --ggraph --goodput="$GOODPUT" \
            --e1="$PRODUCER" --e2="$CONSUMER" -d $DIR --txt \
            -o $OUTPUT $EXTRA ) &
        echo "Max Seq, loss, retransmission, delay and goodput vs Speed graphs - Producer"

        EXE=`expr $EXE + 1`
        if (( $EXE % $PROC == 0 )); then
            echo "Already have $PROC processes running, waiting"
            wait
        fi
    else
        echo "Producer summary graphs are up to date"
    fi
fi

wait
