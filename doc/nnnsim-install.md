# Install nnnsim with ns-3

The installation procedure seen below has been tested with Debian Jessie 8.6 and Ubuntu 16.04.1 using ns-3 3.26

If using Debian:

1. Install the following packages using apt-get

        # apt-get install git build-essential python python-dev libgcrypt20-dev libgtk2.0-dev libgsl0ldbl libgsl0-dev libsqlite3-0 libxml2 valgrind gccxml doxygen python-pygccxml python-pygoocanvas python-pygraphviz python-gtk2 sudo libboost1.55-all-dev

If using Ubuntu:

1. Install the following packages using apt-get

        # apt-get install git build-essential python python-dev libgcrypt20-dev libgtk2.0-dev libgsl2 libgsl-dev libsqlite3-0 libxml2 valgrind gccxml doxygen python-pygccxml python-pygoocanvas python-pygraphviz python-gtk2 sudo libboost1.58-all-dev

From here on in, the instructions are the same. If you do not wish to use nnnsim, you can skip steps 3 and 4. 

1. Download the [ns-allinone-3.26.tar.bz2](https://www.nsnam.org/release/ns-allinone-3.26.tar.bz2)

        $ wget https://www.nsnam.org/release/ns-allinone-3.26.tar.bz2

2. Decompress the file to the desired directory

        $ tar xjvf ns-allinone-3.26.tar.bz2

3. Clone the nnnsim repository within the ns-allinone-3.26/ns-3.26/src directory

        ns-allinone-3.26/ns-3.26/src $ git clone git@bitbucket.org:nnnsimdev/nnnsim.git

4. If you want to be able to use Point-To-Point NetDevices in your ns-3 scenarios, a patch is required.
    To apply the patch, you can use the following command line:

        ns-allinone-3.26/ns-3.26/src $ patch -p 0 < nnnsim/doc/patches/3n-icn-enabled-point-to-point.patch

5. Once patched, you can build ns-3 with nnnsim using the supplied build.py script in ns-allinone-3.26.
    nnnsim also includes examples which we highly recommend seeing before doing anything else.

    The following command line takes care of the configuration and compilation of ns-3

        ns-allinone-3.26 $ ./build.py --enable-examples
