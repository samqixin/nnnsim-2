/* -*- Mode:C++; c-file-style:"gnu"; -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-dual-wifi-test.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-dual-wifi-test.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-dual-wifi-test.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 *  Special thanks to University of Washington for initial templates
 */

// Standard C++ modules
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iterator>
#include <iostream>
#include <map>
#include <string>
#include <sys/time.h>
#include <vector>

// Random modules
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/variate_generator.hpp>

// ns3 modules
#include "ns3/config.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wifi-module.h"

// nnnSIM modules
#include "ns3/nnnsim-module.h"

using namespace ns3;
using namespace boost;
using namespace std;

namespace br = boost::random;

NS_LOG_COMPONENT_DEFINE ("DUALWifi");

int main (int argc, char *argv[])
{
  int lease = 300;                                 // 3N name lease
  int contentSize = -1;                            // Size of content to be retrieved
  int maxSeq = -1;                                 // Maximum number of Data packets to request
  int csSize = 10000000;                           // How big the Content Store should be
  double sec = 0.0;                                // Movement start
  double endTime = 30;                            // Number of seconds to run the simulation
  double grace = 5.0;                              // Number of seconds before and after simulation run time to get all information
  // 0.15 MB/s equals to 1.2 Mb/s which is the bitrate for
  // 480p livestream video according to Adobe
  double MBps = 0.01024;                           // MB/s data rate desired for applications
  double retxtime = 0.05;                          // How frequent Interest retransmission timeouts should be checked (seconds)
  bool producerMobile = false;                     // Tells to run the simulation with the provider using mobile functions
  bool consumerMobile = false;                     // Tells to run the simulation with consumer uses mobile functions
  bool useACK = false;                             // Flag for use of 3N ACK PDUs
  bool enablePoA = false;                          // Flag for when the layer shares PoA information
  bool use3N = true;                               // Flag for 3N

  // Variable for buffer
  char buffer[250];
  std::string ack;
  std::string u3n;

  CommandLine cmd;
  cmd.AddValue ("start", "Starting second", sec);
  cmd.AddValue ("csSize", "Number of Interests a Content Store can maintain", csSize);
  cmd.AddValue ("endTime", "How long the simulation will last (Seconds)", endTime);
  cmd.AddValue ("mbps", "Data transmission rate for App in MBps", MBps);
  cmd.AddValue ("size", "Content size in MB (-1 is for no limit)", contentSize);
  cmd.AddValue ("retx", "How frequent Interest retransmission timeouts should be checked in seconds", retxtime);
  cmd.AddValue ("lease", "Establishes the lease time for 3N names produced by edge nodes", lease);
  cmd.AddValue ("consumer", "Whether the consumer uses 3N mobile functions", consumerMobile);
  cmd.AddValue ("producer", "Whether the provider uses 3N mobile functions", producerMobile);
  cmd.AddValue ("useACK", "Makes the simulation use ACKP", useACK);
  cmd.AddValue ("enablePoA", "Uses PoA information throughout the layer", enablePoA);
  cmd.AddValue ("use3N", "Use of the 3N with a stack", use3N);
  cmd.Parse (argc,argv);

  uint32_t kbps = ceil (MBps * 8000);              // Rate

  if (useACK)
    {
      ack = "true";
    }
  else
    {
      ack = "false";
    }

  if (use3N)
    {
      u3n = "true";
    }
  else
    {
      u3n = "false";
    }

  NS_LOG_INFO ("------ Creating nodes ------");

  uint32_t consumerN = 1;
  uint32_t providerN = 1;
  uint32_t centerN = 1;
  uint32_t apN = 2;

  NS_LOG_INFO ("Creating consumers");
  NodeContainer consumerNodes;
  consumerNodes.Create(consumerN);

  std::vector<uint32_t> consumerNodeIds;

  // Save all the mobile Node IDs
  for (uint32_t i = 0; i < consumerN; i++)
    {
      consumerNodeIds.push_back(consumerNodes.Get (i)->GetId ());
    }

  NS_LOG_INFO ("Creating providers");
  NodeContainer providerNodes;
  providerNodes.Create(providerN);

  std::vector<uint32_t> providerNodeIds;

  // Save all the mobile Node IDs
  for (uint32_t i = 0; i < providerN; i++)
    {
      providerNodeIds.push_back(providerNodes.Get (i)->GetId ());
    }

  NS_LOG_INFO ("Creating central node");
  // Central Nodes
  NodeContainer centralNodes;
  centralNodes.Create (centerN);

  NS_LOG_INFO ("Creating AP nodes");
  // Central Nodes
  NodeContainer apNodes;
  apNodes.Create (apN);

  int displacement = 400;

  NS_LOG_INFO ("------ Placing Consumer node -------");

  MobilityHelper consumerStation;

  Ptr<ListPositionAllocator> initialConsumer = CreateObject<ListPositionAllocator> ();

  for (uint32_t i = 0; i < consumerN; i++)
    {
      Vector pos (0+(i*displacement/4), -300, 0.0);
      initialConsumer->Add (pos);
    }

  consumerStation.SetPositionAllocator(initialConsumer);
  consumerStation.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  consumerStation.Install(consumerNodes);

  NS_LOG_INFO ("------ Placing Provider node -------");

  MobilityHelper providerStation;

  Ptr<ListPositionAllocator> initialProvider = CreateObject<ListPositionAllocator> ();

  for (uint32_t i = 0; i < providerN; i++)
    {
      Vector pos (0+(i*displacement/4), 300, 0.0);
      initialProvider->Add (pos);
    }

  providerStation.SetPositionAllocator(initialProvider);
  providerStation.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  providerStation.Install(providerNodes);

  NS_LOG_INFO ("------ Placing Central node -------");

  MobilityHelper centralStation;

  Ptr<ListPositionAllocator> initialCenter = CreateObject<ListPositionAllocator> ();

  for (uint32_t i = 0; i < centerN; i++)
    {
      Vector pos (0+(i*displacement), 0, 0.0);
      initialCenter->Add (pos);
    }

  centralStation.SetPositionAllocator(initialCenter);
  centralStation.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  centralStation.Install(centralNodes);


  NS_LOG_INFO ("------ Placing AP nodes -------");

  MobilityHelper apStations;

  Ptr<ListPositionAllocator> initialAP = CreateObject<ListPositionAllocator> ();

  for (uint32_t i = 0; i < apN; i++)
    {
      Vector pos (0, (int)((i*displacement)-200), 0.0);
      initialAP->Add (pos);
    }

  apStations.SetPositionAllocator(initialAP);
  apStations.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  apStations.Install(apNodes);

  // PtP connections are 100Mbps with 1ms delay
  PointToPointHelper p2p_100mbps1ms;
  p2p_100mbps1ms.SetDeviceAttribute ("DataRate", StringValue ("100Mbps"));
  p2p_100mbps1ms.SetChannelAttribute ("Delay", StringValue ("1ms"));

  NS_LOG_INFO ("------ Connecting central nodes to AP nodes ------");

  NetDeviceContainer ptpApCentralDevices;

  for (uint32_t i = 0; i < apN ; i++)
    {
      ptpApCentralDevices.Add (p2p_100mbps1ms.Install (centralNodes.Get (0), apNodes.Get (i)));
    }

  NS_LOG_INFO ("------ Creating Wireless cards ------");

  // Use the Wifi Helper to define the wireless interfaces for APs
  WifiHelper wifi;
  wifi.SetRemoteStationManager ("ns3::ArfWifiManager");

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::ThreeLogDistancePropagationLossModel");
  wifiChannel.AddPropagationLoss("ns3::NakagamiPropagationLossModel");

  // All interfaces are placed on the same channel. Makes AP changes easy. Might
  // have to be reconsidered for multiple mobile nodes
  YansWifiPhyHelper wifiPhyHelper = YansWifiPhyHelper::Default ();
  wifiPhyHelper.SetChannel (wifiChannel.Create ());
  wifiPhyHelper.Set("TxPowerStart", DoubleValue(16.0206));
  wifiPhyHelper.Set("TxPowerEnd", DoubleValue(16.0206));

  // Add a simple no QoS based card to the Wifi interfaces
  WifiMacHelper wifiMacHelper;

  // Create SSIDs for all the APs
  std::vector<Ssid> ssidV;

  NS_LOG_INFO ("------ Creating ssids for wireless cards ------");

  for (uint32_t i = 0; i < apN; i++)
    {
      // Temporary string containing our SSID
      std::string ssidtmp("ap-" + boost::lexical_cast<std::string>(i));

      // Push the newly created SSID into a vector
      ssidV.push_back (Ssid (ssidtmp));
    }

  NS_LOG_INFO ("------ Assigning AP wireless cards ------");

  std::vector<NetDeviceContainer> wifiAPNetDevices;
  for (uint32_t i = 0; i < apN; i++)
    {
      wifiMacHelper.SetType ("ns3::ApWifiMac",
			     "Ssid", SsidValue (ssidV[i]),
			     "BeaconGeneration", BooleanValue (true),
			     "BeaconInterval", TimeValue (Seconds (0.102)));

      wifiAPNetDevices.push_back (wifi.Install (wifiPhyHelper, wifiMacHelper, apNodes.Get (i)));
    }

  // Create a Wifi station with a modified Station MAC.
  wifiMacHelper.SetType("ns3::StaWifiMac",
			"Ssid", SsidValue (ssidV[0]),
			"ActiveProbing", BooleanValue (true));

  NS_LOG_INFO ("------ Assigning Consumer wireless cards ------");
  NetDeviceContainer wifiConsumerNetDevices = wifi.Install (wifiPhyHelper, wifiMacHelper, consumerNodes);

  // Create a Wifi station with a modified Station MAC.
   wifiMacHelper.SetType("ns3::StaWifiMac",
 			"Ssid", SsidValue (ssidV[1]),
 			"ActiveProbing", BooleanValue (true));

  NS_LOG_INFO ("------ Assigning Provider wireless cards ------");
  NetDeviceContainer wifiProviderNetDevices = wifi.Install (wifiPhyHelper, wifiMacHelper, providerNodes);



  // What the ICN Data packet payload size is fixed to 1024 bytes
  uint32_t payLoadsize = 1024;

  // Give the content size, find out how many sequence numbers are necessary
  if (contentSize > 0)
    {
      maxSeq = 1 + (((contentSize*1000000) - 1) / payLoadsize);
    }

  // How many Interests/second a producer creates
  double intFreq = (MBps * 1000000) / payLoadsize;

  uint32_t window = ceil(2 * intFreq);

  sprintf(buffer, "%d", csSize);
  std::string cs (buffer);

  sprintf (buffer, "%ds", (int)endTime +35);
  std::string centralTimeStr (buffer);

  sprintf (buffer, "%ds", lease);
  std::string apTimeStr (buffer);

  sprintf(buffer, "%dms", (int)(retxtime*1000));

  sprintf(buffer, "%d", kbps);
  std::string str_rate (buffer);

  sprintf(buffer, "%d", window);
  std::string str_window (buffer);

  NS_LOG_INFO ("Frequency: " << intFreq);
  NS_LOG_INFO ("Rate: " << str_rate);
  NS_LOG_INFO ("Window: " << str_window);

  std::string retxch (buffer);
  NS_LOG_INFO ("Setting lease time to: " << apTimeStr);
  NS_LOG_INFO ("Setting retransmission times to: " << retxch);

  NS_LOG_INFO ("Setting Content Store size: " << cs);

  NS_LOG_INFO ("------ Installing Central 3N stack ------");
  nnn::NNNStackHelper CentralStack;
  // Set the Forwarding Strategy
  CentralStack.SetForwardingStrategy ("ns3::nnn::ICN3NSmartFlooding",
				      "3NLeasetime", apTimeStr,
				      "3NRetransmitTime", retxch,
				      "AskACKedPDUs", ack,
				      "ReturnACKedPDUs", ack,
				      "3NPDUWindowSize", str_window,
				      "3NRate", str_rate,
				      "GenerateFixed3Nname", "true");
  // Set the Content Store for the primary stack, Normal LRU ContentStore
  CentralStack.SetContentStore ("ns3::nnn::cs::Freshness::Lru", "MaxSize", cs);
  // Set the FIB default routes
  CentralStack.SetDefaultRoutes (true);
  CentralStack.SetPoASharing (enablePoA); // Enable use of PoAs throughout layer
  // Install the stack
  CentralStack.Install (centralNodes);



  NS_LOG_INFO ("------ Giving name to central node 3N stack ------");
  // Create the initial 3N name
  Ptr<nnn::NNNAddress> firstName = Create <nnn::NNNAddress> ("a");
  // Get the ForwardingStrategy object from the node
  Ptr<nnn::ForwardingStrategy> CentralFW = centralNodes.Get (0)->GetObject<nnn::ForwardingStrategy> ();
  // Give a 3N name for the first AP - ensure it is longer than the actual simulation
  CentralFW->SetNode3NName (firstName, Seconds (endTime + 35), true);


  NS_LOG_INFO ("------ Installing AP sector 3N stacks ------");
  nnn::NNNStackHelper APStack;
  // Set the Forwarding Strategy
  APStack.SetForwardingStrategy ("ns3::nnn::ICN3NSmartFlooding",
				 "3NLeasetime", apTimeStr,
				 "AskACKedPDUs", ack,
				 "ReturnACKedPDUs", ack,
				 "3NRetransmitTime", retxch,
				 "3NPDUWindowSize", str_window,
				 "3NRate", str_rate,
				 "AskFixed3Nname", "true");
  // Set the Content Store for the primary stack, Normal LRU ContentStore
  APStack.SetContentStore ("ns3::nnn::cs::Freshness::Lru", "MaxSize", cs);
  // Set the FIB default routes
  APStack.SetDefaultRoutes (true);
  APStack.SetPoASharing (enablePoA); // Enable use of PoAs throughout layer
  // Install the stack
  APStack.Install (apNodes);

  NS_LOG_INFO ("------ Enrolling AP sector 3N stacks ------");

  // Vector of all forwarding strategies for APs
  std::vector<Ptr<nnn::ForwardingStrategy> > APFWs;

  // Force Enrollment for the APs
  for (uint32_t i = 0; i < apNodes.GetN (); i++)
    {
      APFWs.push_back (apNodes.Get (i)->GetObject<nnn::ForwardingStrategy> ());
    }

  for (uint32_t i = 0; i < apNodes.GetN () ; i++)
    {
      Simulator::Schedule(Seconds (0), &nnn::ForwardingStrategy::Enroll, APFWs[i]);
    }

  NS_LOG_INFO ("------ Installing Mobile 3N stacks ------");
  // Stack for nodes that are mobile;
  nnn::NNNStackHelper mobileStack;
  // No Content Store for mobile stack
  mobileStack.SetContentStore ("ns3::nnn::cs::Nocache");
  // Do not produce 3N names for these nodes
  mobileStack.SetForwardingStrategy ("ns3::nnn::ICN3NSmartFlooding",
				     "Produce3Nnames", "false",
				     "AskACKedPDUs", ack,
				     "ReturnACKedPDUs", ack,
				     "3NPDUWindowSize", str_window,
				     "3NRate", str_rate,
				     "IsMobile", u3n,
				     "use3NName", u3n,
				     "3NRetransmitTime", retxch);
  // Set the FIB default routes
  mobileStack.SetDefaultRoutes (true);
  mobileStack.SetPoASharing (enablePoA); // Enable use of PoAs throughout layer
  // Install the stack

  NodeContainer mobileNodes;
  mobileNodes.Add (consumerNodes);
  mobileNodes.Add (providerNodes);
  mobileStack.Install (mobileNodes);

  NS_LOG_INFO ("------ Enrolling mobile 3N stacks ------");

  // Vector of all forwarding strategies for APs
  std::vector<Ptr<nnn::ForwardingStrategy> > mobileFWs;

  // Force Enrollment for the APs
  for (uint32_t i = 0; i < mobileNodes.GetN (); i++)
    {
      mobileFWs.push_back (mobileNodes.Get (i)->GetObject<nnn::ForwardingStrategy> ());
    }

  for (uint32_t i = 0; i < mobileNodes.GetN () ; i++)
    {
      Simulator::Schedule(Seconds (1), &nnn::ForwardingStrategy::Enroll, mobileFWs[i]);
    }

  NS_LOG_INFO ("------ Installing 3N Producer Application ------ ");
  NS_LOG_INFO ("Producer Payload size: " << payLoadsize);

  // Create the producer on the AP node - same as in ndnSIM
  nnn::AppHelper producerHelper ("ns3::nnn::ICNProducer");
  producerHelper.SetPrefix ("/waseda/sato");
  // Payload size is in bytes
  producerHelper.SetAttribute ("PayloadSize", UintegerValue(payLoadsize));
  producerHelper.SetAttribute("StopTime", TimeValue (Seconds(endTime+grace+1)));

  if (producerMobile)
    {
      producerHelper.SetAttribute ("IsMobile", BooleanValue (true));
      NS_LOG_INFO ("------ 3N Producer is mobile ------");
    }
  producerHelper.Install (providerNodes);

  NS_LOG_INFO ("------ Installing 3N Consumer Application ------ ");
  NS_LOG_INFO ("Consumer Interests/Second frequency: " << intFreq);

  // Create the consumer node on the mobile node - same as in ndnSIM
  nnn::AppHelper consumerHelper ("ns3::nnn::ICNConsumerCbr");
  consumerHelper.SetPrefix ("/waseda/sato");
  consumerHelper.SetAttribute ("Frequency", DoubleValue (intFreq));
  if (contentSize > 0)
    {
      consumerHelper.SetAttribute ("MaxSeq", IntegerValue (maxSeq));
    }
  consumerHelper.SetAttribute("StartTime", TimeValue (Seconds(grace)));
  consumerHelper.SetAttribute("StopTime", TimeValue (Seconds(endTime+grace)));
  consumerHelper.SetAttribute ("RetxTimer", TimeValue (Seconds(retxtime)));

  if (consumerMobile)
    {
      consumerHelper.SetAttribute ("IsMobile", BooleanValue (true));
      NS_LOG_INFO ("------ 3N Consumer is mobile ------");
    }
  consumerHelper.Install (consumerNodes);

  NS_LOG_INFO ("------Ready for execution!------");

  Simulator::Stop (Seconds (endTime+(2*grace)));
  Simulator::Run ();
  Simulator::Destroy ();
}
