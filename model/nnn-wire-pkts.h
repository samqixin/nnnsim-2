/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
* Copyright (c) 2016 Jairo Eduardo Lopez
*
* This file is part of nnnsim.
*
* nnn-wire-pkts.h is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* nnn-wire-pkts.h is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with nnn-wire-pkts.h. If not, see <http://www.gnu.org/licenses/>.
*
* Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
*/
#ifndef MODEL_NNN_WIRE_PKTS_H_
#define MODEL_NNN_WIRE_PKTS_H_

#include "ns3/nnnsim-aen.h"
#include "ns3/nnnsim-aden.h"
#include "ns3/nnnsim-den.h"
#include "ns3/nnnsim-do.h"
#include "ns3/nnnsim-du.h"
#include "ns3/nnnsim-en.h"
#include "ns3/nnnsim-inf.h"
#include "ns3/nnnsim-nullp.h"
#include "ns3/nnnsim-oen.h"
#include "ns3/nnnsim-ren.h"
#include "ns3/nnnsim-so.h"
#include "ns3/nnnsim-rhr.h"
#include "ns3/nnnsim-ohr.h"
#include "ns3/nnnsim-ackp.h"

#endif /* MODEL_NNN_WIRE_PKTS_H_ */
