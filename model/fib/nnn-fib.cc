/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-fib.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-fib.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-fib.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/names.h"
#include "ns3/node.h"
#include "ns3/string-center.h"

#include "ns3/nnn-naming.h"
#include "ns3/nnn-icn-naming.h"

#include "nnn-fib.h"

namespace ns3
{
  namespace nnn
  {
    TypeId
    Fib::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::Fib") // cheating ns3 object system
	.SetParent<Object> ()
	.SetGroupName ("Nnn")

	.AddTraceSource ("Name3NEntries", "Returns the number of part entries after updates",
			 MakeTraceSourceAccessor (&Fib::m_icn_part_entries_trace),
			 "ns3::nnn::fib::Default::EntriesTracedCallback")
	.AddTraceSource ("NamePoAEntries", "Returns the number of part entries after updates",
			 MakeTraceSourceAccessor (&Fib::m_icn_part_poa_entries_trace),
			 "ns3::nnn::fib::Default::EntriesTracedCallback")
	;
      return tid;
    }

    std::ostream&
    operator<< (std::ostream& os, const Fib &fib)
    {
      os << "FIB:" << std::endl << "================================================================================" << std::endl;
      os << boost::format(fib_formatter) % "DST PRFX" % (boost::format(fibfm_formatter) % "FC" % "PRI" % "S" % "M") % "Part" % "Inserted" % "Mod part" % "Last seen";
      os << "--------------------------------------------------------------------------------" << std::endl;
      fib.Print (os);
      os << "================================================================================" << std::endl;
      return os;
    }

  } // namespace nnn
} // namespace ns3
