/*
 * nnn-poa-metric-container.h
 *
 *  Created on: May 21, 2017
 *      Author: jelfn
 */

#ifndef MODEL_FIB_NNN_POA_METRIC_CONTAINER_H_
#define MODEL_FIB_NNN_POA_METRIC_CONTAINER_H_

#include "ns3/address.h"
#include "ns3/nstime.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

namespace ns3
{
  namespace nnn
  {
    namespace fib
    {

      struct PoA_metric {

	PoA_metric (Address poa, uint8_t status, int32_t metric, Time sRtt, Time rttVar, Time realDelay)
	: m_poa (poa)
	, m_status (status)
	, m_metric (metric)
	, m_sRtt (sRtt)
	, m_rttVar (rttVar)
	, m_realDelay (realDelay)
	{
	}

	Address m_poa;
	uint8_t m_status;
	int32_t m_metric;
	Time m_sRtt;
	Time m_rttVar;
	Time m_realDelay;
      };

      class i_poa { };
      class i_poa_metric { };

      struct PoAMetricContainer :
	  public boost::multi_index::multi_index_container<
	      PoA_metric,
	      boost::multi_index::indexed_by<
	          // Sort by Address
	          boost::multi_index::ordered_unique<
	          boost::multi_index::tag<i_poa>,
	          boost::multi_index::member<PoA_metric, Address, &PoA_metric::m_poa>
                  >,

                  // Sort by metric
                  boost::multi_index::ordered_non_unique<
                  boost::multi_index::tag<i_poa_metric>,
                      boost::multi_index::composite_key<
                          PoA_metric,
                          boost::multi_index::member<PoA_metric,uint8_t,&PoA_metric::m_status>,
                          boost::multi_index::member<PoA_metric,int32_t,&PoA_metric::m_metric>
                      >
                  >
             >
      > { };

      typedef PoAMetricContainer::index<i_poa>::type poa_by_name;
      typedef PoAMetricContainer::index<i_poa_metric>::type poa_by_metric;
    }
  }
}

#endif /* MODEL_FIB_NNN_POA_METRIC_CONTAINER_H_ */
