/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2018 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-part-seen-container.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-part-seen-container.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-part-seen-container.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef MODEL_FIB_NNN_PART_SEEN_CONTAINER_H_
#define MODEL_FIB_NNN_PART_SEEN_CONTAINER_H_

#include "ns3/nstime.h"
#include "ns3/nnn-address.h"
#include "ns3/nnn-face.h"

#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

using boost::multi_index_container;
using namespace ::boost::multi_index;

namespace ns3
{
  namespace nnn
  {
    struct PartPoASeen
    {
      PartPoASeen (Ptr<Face> _face, Address _poa)
      : m_face          (_face)
      , m_poa           (_poa)
      , m_firstInserted (Simulator::Now ())
      , m_lastSeen      (Simulator::Now ())
      , m_partmod       (Simulator::Now ())
      , m_part          (std::numeric_limits<uint32_t>::max ())
      {
      }

      PartPoASeen (Ptr<Face> _face, Address _poa, uint32_t part)
      : m_face          (_face)
      , m_poa           (_poa)
      , m_firstInserted (Simulator::Now ())
      , m_lastSeen      (Simulator::Now ())
      , m_partmod       (Simulator::Now ())
      , m_part          (part)
      {
      }

      ~PartPoASeen ()
      {
	m_face = 0;
      }

      void
      UpdateLastSeen ()
      {
	m_lastSeen = Simulator::Now ();
      }

      void
      UpdatePart (uint32_t part)
      {
	m_part = part;
	m_partmod = Simulator::Now ();
      }

      void
      ModifyFirstInserted (Time time)
      {
	m_firstInserted = time;
      }

      bool operator< (const PartPoASeen e) const
      {
	if (m_part == e.m_part)
	  {
	    if (m_lastSeen == e.m_lastSeen)
	      {
		return (m_firstInserted < e.m_firstInserted);
	      }
	    else
	      {
		return (m_lastSeen > e.m_lastSeen);
	      }
	  }
	else
	  {
	    return (m_part > e.m_part);
	  }
      }

      Ptr<Face> m_face;
      Address m_poa;
      Time m_firstInserted;
      Time m_lastSeen;
      Time m_partmod;
      uint32_t m_part;
    };

    struct Part3NSeen
    {
      Part3NSeen (Ptr<const NNNAddress> _name)
      : m_name          (_name)
      , m_firstInserted (Simulator::Now ())
      , m_lastSeen      (Simulator::Now ())
      , m_partmod       (Simulator::Now ())
      , m_part          (std::numeric_limits<uint32_t>::max ())
      {
      }

      Part3NSeen (Ptr<const NNNAddress> _name, uint32_t part)
      : m_name          (_name)
      , m_firstInserted (Simulator::Now ())
      , m_lastSeen      (Simulator::Now ())
      , m_partmod       (Simulator::Now ())
      , m_part          (part)
      {
      }

      ~Part3NSeen ()
      {
	m_name = 0;
      }

      void
      UpdateLastSeen ()
      {
	m_lastSeen = Simulator::Now ();
      }

      void
      UpdatePart (uint32_t part)
      {
	m_part = part;
	m_partmod = Simulator::Now ();
      }

      void
      ModifyFirstInserted (Time time)
      {
	m_firstInserted = time;
      }

      bool operator< (const Part3NSeen e) const
      {
	if (m_part == e.m_part)
	  {
	    if (m_lastSeen == e.m_lastSeen)
	      {
		return (m_firstInserted < e.m_firstInserted);
	      }
	    else
	      {
		return (m_lastSeen > e.m_lastSeen);
	      }
	  }
	else
	  {
	    return (m_part > e.m_part);
	  }
      }

      Ptr<const NNNAddress> m_name;
      Time m_firstInserted;
      Time m_lastSeen;
      Time m_partmod;
      uint32_t m_part;
    };

    class p_natural { };
    class p_3name { };
    class p_lastest { };
    class p_part { };

    class p_face { };
    class p_poa {};

    typedef multi_index_container<
	    Part3NSeen,
	    indexed_by<
	        // Sort by Part3NSeen structure
	        ordered_unique<
		    tag<p_natural>,
		    identity<Part3NSeen>
                >,
		// Sort by 3N name
		ordered_non_unique<
		    tag<p_3name>,
		    member<Part3NSeen, Ptr<const NNNAddress>, &Part3NSeen::m_name>,
		    Ptr3NComp
		>,
		// Sort by latest seen
		ordered_non_unique<
		    tag<p_lastest>,
		    member<Part3NSeen, Time, &Part3NSeen::m_lastSeen>,
		    std::greater<Time>
                >,
		// Sort by highest part
		ordered_non_unique<
		    tag<p_part>,
		    member<Part3NSeen, uint32_t, &Part3NSeen::m_part>,
		    std::greater<uint32_t>
                >
        >
    > PartsSeenContainer;

    typedef PartsSeenContainer::index<p_natural>::type parts_seen_by_natural_order;
    typedef PartsSeenContainer::index<p_3name>::type parts_seen_by_name;
    typedef PartsSeenContainer::index<p_lastest>::type parts_seen_by_latest;
    typedef PartsSeenContainer::index<p_part>::type parts_seen_by_part_time;

    typedef multi_index_container<
	PartPoASeen,
	indexed_by<
	// Sort by PartPoASeen structure
	    ordered_unique<
	        tag<p_natural>,
		identity<PartPoASeen>
	    >,
	    // Sort by Face
	    ordered_non_unique<
	        tag<p_face>,
		member<PartPoASeen, Ptr<Face>, &PartPoASeen::m_face>,
		PtrFaceComp
	    >,
	    // Sort by PoA
	    ordered_non_unique<
	        tag<p_poa>,
		member<PartPoASeen, Address, &PartPoASeen::m_poa>
            >,
	    // Sort by latest seen
	    ordered_non_unique<
	        tag<p_lastest>,
		member<PartPoASeen, Time, &PartPoASeen::m_lastSeen>,
		std::greater<Time>
            >,
	    // Sort by highest part
	    ordered_non_unique<
	        tag<p_part>,
		member<PartPoASeen, uint32_t, &PartPoASeen::m_part>,
		std::greater<uint32_t>
            >
        >
    > PartsPoASeenContainer;

    typedef PartsPoASeenContainer::index<p_natural>::type parts_poa_seen_by_natural_order;
    typedef PartsPoASeenContainer::index<p_face>::type parts_poa_seen_by_face;
    typedef PartsPoASeenContainer::index<p_poa>::type parts_poa_seen_by_poa;
    typedef PartsPoASeenContainer::index<p_lastest>::type parts_poa_seen_by_latest;
    typedef PartsPoASeenContainer::index<p_part>::type parts_poa_seen_by_part_time;
  }
}
#endif /* MODEL_FIB_NNN_PART_SEEN_CONTAINER_H_ */
