/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-fib-entry.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-fib-entry.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-fib-entry.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#ifndef NNN_FIB_ENTRY_H
#define NNN_FIB_ENTRY_H

#include "ns3/ptr.h"
#include "ns3/nstime.h"
#include "ns3/traced-value.h"

#include "ns3/nnn-face.h"
#include "ns3/nnn-icn-name.h"
#include "ns3/nnn-limits.h"

#include "ns3/nnn-naming.h"
#include "nnn-poa-metric-container.h"
#include "ns3/nnn-names-seen-container.h"
#include "nnn-part-seen-container.h"

namespace ns3
{
  namespace icn
  {
    class Name;
  }

  namespace nnn
  {

    class Fib;

    /**
     * @ingroup nnn-fib
     * @brief Namespace for FIB operations
     */
    namespace fib {

      /**
       * @ingroup ndn-fib
       * \brief Structure holding various parameters associated with a (FibEntry, Face) tuple
       */
      class FaceMetric
      {
      public:
	/**
	 * @brief Color codes for FIB face status
	 */

	enum Status { ICN_FIB_GREEN = 1,
	  ICN_FIB_YELLOW = 2,
	  ICN_FIB_RED = 3 };
	/**
	 * \brief Metric constructor
	 *
	 * \param face Face for which metric
	 * \param cost Initial value for routing cost
	 */
	FaceMetric (Ptr<Face> face, int32_t cost)
	: m_face (face)
	, m_status (ICN_FIB_YELLOW)
	, m_routingCost (cost)
	, m_sRtt   (Seconds (0))
	, m_rttVar (Seconds (0))
	, m_realDelay (Seconds (0))
	, m_firstSeen (Simulator::Now ())
	, m_lastSeen (Simulator::Now ())
	{
	  CalculateBestPoA ();
	}

	FaceMetric (Ptr<Face> face, Address poa, int32_t cost)
	: m_face (face)
	, m_status (ICN_FIB_YELLOW)
	, m_routingCost (cost)
	, m_sRtt   (Seconds (0))
	, m_rttVar (Seconds (0))
	, m_realDelay (Seconds (0))
	, m_firstSeen (Simulator::Now ())
	, m_lastSeen (Simulator::Now ())
	{
	  m_poas.insert(PoA_metric(poa, ICN_FIB_YELLOW, cost, Seconds (0), Seconds (0), Seconds (0)));

	  CalculateBestPoA ();
	}

	/**
	 * \brief Comparison operator used by boost::multi_index::identity<>
	 */
	bool
	operator< (const FaceMetric &fm) const { return *m_face < *fm.m_face; } // return identity of the face

	/**
	 * @brief Comparison between FaceMetric and Face
	 */
	bool
	operator< (const Ptr<Face> &face) const { return *m_face < *face; }

	/**
	 * @brief Return Face associated with FaceMetric
	 */
	Ptr<Face>
	GetFace () const { return m_face; }

	void
	AddPoA (Address poa, int32_t cost)
	{
	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.find (poa);

	  if (it == poa_index.end())
	    {
	      m_poas.insert (PoA_metric(poa, ICN_FIB_YELLOW, cost, Seconds (0), Seconds (0), Seconds (0)));
	    }
	  else
	    {
	      PoA_metric tmp = *it;
	      tmp.m_metric = cost;
	      tmp.m_status = ICN_FIB_YELLOW;
	      poa_index.replace(it, tmp);
	    }

	  CalculateBestPoA ();
	  m_lastSeen = Simulator::Now ();
	}

	/**
	 * \brief Recalculate smoothed RTT and RTT variation
	 * \param rttSample RTT sample
	 */
	void
	UpdateRtt (const Time &rttSample);

	/**
	 * @brief Get current status of FIB entry
	 */
	Status
	GetStatus () const
	{
	  return m_status;
	}

	/**
	 * @brief Set current status of FIB entry
	 */
	void
	SetStatus (Status status)
	{
	  m_status.Set (status);
	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.begin ();

	  // Save all the iterators
	  std::vector<poa_by_name::iterator> view;
	  for( ; it != poa_index.end(); ++it){
	      view.push_back(it);
	  }

	  // Now use modify to hit all the contents
	  for(size_t num = 0; num < view.size(); ++num){
	      poa_index.modify(view[num], [&](PoA_metric &tmp){ tmp.m_status = status; });
	  }
	  m_lastSeen = Simulator::Now ();
	}

	void
	UpdateGlobalInfo()
	{
	  poa_by_metric& metric_index = m_poas.get<i_poa_metric> ();
	  //poa_by_metric::iterator it = metric_index.begin();
	  poa_by_metric::reverse_iterator it = metric_index.rbegin();

	  if (it != metric_index.rend())
	    {
	      PoA_metric tmp = *it;
	      m_status.Set (static_cast<Status>(tmp.m_status));
	      m_routingCost = tmp.m_metric;
	    }

	  CalculateBestPoA ();
	  m_lastSeen = Simulator::Now ();
	}

	void
	SetPoAStatus (Address poa, Status status)
	{
	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.find (poa);
	  if (it != poa_index.end())
	    {
	      PoA_metric tmp = *it;
	      tmp.m_status = status;
	      poa_index.replace(it, tmp);
	    }

	  UpdateGlobalInfo();
	}

	/**
	 * @brief Get current routing cost
	 */
	int32_t
	GetRoutingCost () const
	{
	  return m_routingCost;
	}

	/**
	 * @brief Set routing cost
	 */
	void
	SetRoutingCost (int32_t routingCost)
	{
	  m_routingCost = routingCost;
	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.begin ();

	  // Save all the iterators
	  std::vector<poa_by_name::iterator> view;
	  for( ; it != poa_index.end(); ++it){
	      view.push_back(it);
	  }

	  // Now use modify to hit all the contents
	  for(size_t num = 0; num < view.size(); ++num){
	      poa_index.modify(view[num], [&](PoA_metric &tmp){ tmp.m_metric = routingCost; });
	  }

	  UpdateGlobalInfo();
	}

	void
	SetPoARoutingCost (Address poa, int32_t routingCost)
	{
	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.find (poa);
	  if (it != poa_index.end())
	    {
	      PoA_metric tmp = *it;
	      tmp.m_metric = routingCost;
	      poa_index.replace(it, tmp);
	    }

	  UpdateGlobalInfo();
	}

	/**
	 * @brief Get current estimate for smoothed RTT value
	 */
	Time
	GetSRtt () const
	{
	  return m_sRtt;
	}

	/**
	 * @brief Get current estimate for the RTT variation
	 */
	Time
	GetRttVar () const
	{
	  return m_rttVar;
	}

	/**
	 * @brief Get real propagation delay to the producer, calculated based on NS-3 p2p link delays
	 */
	Time
	GetRealDelay () const
	{
	  return m_realDelay;
	}

	/**
	 * @brief Set real propagation delay to the producer, calculated based on NS-3 p2p link delays
	 */
	void
	SetRealDelay (Time realDelay)
	{
	  m_realDelay = realDelay;

	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.begin ();

	  // Save all the iterators
	  std::vector<poa_by_name::iterator> view;
	  for( ; it != poa_index.end(); ++it){
	      view.push_back(it);
	  }

	  // Now use modify to hit all the contents
	  for(size_t num = 0; num < view.size(); ++num){
	      poa_index.modify(view[num], [&](PoA_metric &tmp){ tmp.m_realDelay = realDelay; });
	  }
	}

	void
	SetPoARealDelay (Address poa, Time realDelay)
	{
	  poa_by_name& poa_index =  m_poas.get<i_poa> ();
	  poa_by_name::iterator it = poa_index.find (poa);

	  if (it != poa_index.end())
	    {
	      PoA_metric tmp = *it;
	      tmp.m_realDelay = realDelay;
	      poa_index.replace(it, tmp);
	    }
	}


	Address
	GetBestPoA () const
	{
	  return m_curr_best;
	}

	void
	CalculateBestPoA ()
	{
	  poa_by_metric& metric_index = m_poas.get<i_poa_metric> ();
	  poa_by_metric::iterator it = metric_index.begin();

	  if (m_face->IsPointToPoint())
	    {
	      if (it != metric_index.end())
		{
		  m_curr_best = (*it).m_poa;
		}
	      else
		{
		  m_curr_best = m_face->GetBroadcastAddress ();
		}
	    }
	  else
	    {
	      m_curr_best = m_face->GetBroadcastAddress ();
	    }
	}

	/**
	 * @brief Get direct access to status trace
	 */
	TracedValue<Status> &
	GetStatusTrace ()
	{
	  return m_status;
	}

      private:
	friend std::ostream& operator<< (std::ostream& os, const FaceMetric &metric);

      private:
	Ptr<Face> m_face; ///< Face
	TracedValue<Status> m_status; ///< \brief Status of the next hop:
	int32_t m_routingCost;        ///< \brief routing protocol cost (interpretation of the value depends on the underlying routing protocol)
	Time m_sRtt;                  ///< \brief smoothed round-trip time
	Time m_rttVar;                ///< \brief round-trip time variation
	Time m_realDelay;             ///< \brief real propagation delay to the producer, calculated based on NS-3 p2p link delays

	Address m_curr_best;
	PoAMetricContainer m_poas;

      public:
	Time m_firstSeen;
	Time m_lastSeen;
      };

      /// @cond include_hidden
      class i_face {};
      class i_metric {};
      class i_nth {};
      class i_min_seq {};
      class i_fib_lastseen {};
      /// @endcond

      /**
       * @ingroup nnn-fib
       * @brief Typedef for indexed face container of Entry
       *
       * Currently, there are 2 indexes:
       * - by face (used to find record and update metric)
       * - by metric (face ranking)
       * - random access index (for fast lookup on nth face). Order is
       *   maintained manually to be equal to the 'by metric' order
       */
      struct FaceMetricContainer :
	public boost::multi_index::multi_index_container<
	    FaceMetric,
	    boost::multi_index::indexed_by<
	        // For fast access to elements using Face
	        boost::multi_index::ordered_unique<
	            boost::multi_index::tag<i_face>,
	            boost::multi_index::const_mem_fun<FaceMetric,Ptr<Face>,&FaceMetric::GetFace>
	        >,

		// List of available faces ordered by (status, m_routingCost)
		boost::multi_index::ordered_non_unique<
		    boost::multi_index::tag<i_metric>,
		    boost::multi_index::composite_key<
		        FaceMetric,
			boost::multi_index::const_mem_fun<FaceMetric,FaceMetric::Status,&FaceMetric::GetStatus>,
			boost::multi_index::const_mem_fun<FaceMetric,int32_t,&FaceMetric::GetRoutingCost>
	            >
	        >,
		boost::multi_index::ordered_non_unique<
		    boost::multi_index::tag<i_fib_lastseen>,
		    boost::multi_index::composite_key<
		        FaceMetric,
			boost::multi_index::member<FaceMetric, Time, &FaceMetric::m_lastSeen>,
			boost::multi_index::member<FaceMetric, Time, &FaceMetric::m_firstSeen>
	            >,
		    boost::multi_index::composite_key_compare<
		        std::greater<Time>,
			std::greater<Time>
		    >
		>,
		// To optimize nth candidate selection (sacrifice a little bit space to gain speed)
		boost::multi_index::random_access<
		    boost::multi_index::tag<i_nth>
	        >
	    >
	>  {};

      typedef FaceMetricContainer::index<i_face>::type faces_by_ptr;
      typedef FaceMetricContainer::index<i_metric>::type faces_by_metric;
      typedef FaceMetricContainer::index<i_fib_lastseen>::type faces_by_lastseen;
      typedef FaceMetricContainer::index<i_nth>::type faces_by_index;

      /**
       * @ingroup nnn-fib
       * \brief Structure for FIB table entry, holding indexed list of
       *        available faces and their respective metrics
       */
      class Entry : public Object
      {
	public:
	typedef Entry base_type;

	public:
	class NoFaces {}; ///< @brief Exception class for the case when FIB entry is not found

	/**
	 * \brief Constructor
	 * \param prefix smart pointer to the prefix for the FIB entry
	 */
	Entry (Ptr<Fib> fib, const Ptr<const icn::Name> &prefix)
	: m_fib (fib)
	, m_prefix (prefix)
	, m_needsProbing (false)
	{
	}

	/**
	 *\brief Constructor
	 *\param prefix smart pointer to the prefix for the FIB entry
	 *\param addr smart pointer to the 3N name seen
	 */
	Entry (Ptr<Fib> fib, const Ptr<const icn::Name> &prefix, const Ptr<const NNNAddress> &addr)
	: m_fib (fib)
	, m_prefix (prefix)
	, m_needsProbing (false)
	{
	  m_src_addrs.insert(Part3NSeen (addr));
	}

	/**
	 *\brief Constructor
	 *\param prefix smart pointer to the prefix for the FIB entry
	 *\param face   smart pointer to the Face
	 *\param poa    Address for Face
	 */
	Entry (Ptr<Fib> fib, const Ptr<const icn::Name> &prefix, const Ptr<Face> &face, Address poa)
	: m_fib (fib)
	, m_prefix (prefix)
	, m_needsProbing (false)
	{
	  m_src_poa_addrs.insert(PartPoASeen (face, poa));
	}

	/**
	 * \brief Update status of FIB next hop
	 * \param status Status to set on the FIB entry
	 */
	void UpdateStatus (Ptr<Face> face, FaceMetric::Status status);

	void UpdateStatus (Ptr<Face> face, const Address& addr, FaceMetric::Status status);

	/**
	 * \brief Add or update routing metric of FIB next hop
	 *
	 * Initial status of the next hop is set to YELLOW
	 */
	void AddOrUpdateRoutingMetric (Ptr<Face> face, int32_t metric);

	/**
	 * \brief Set real delay to the producer
	 */
	void
	SetRealDelayToProducer (Ptr<Face> face, Time delay);

	/**
	 * @brief Invalidate face
	 *
	 * Set routing metric on all faces to max and status to RED
	 */
	void
	Invalidate ();

	/**
	 * @brief Update RTT averages for the face
	 */
	void
	UpdateFaceRtt (Ptr<Face> face, const Time &sample);

	void
	Update3NSrcDestination (Ptr<const NNNAddress> addr);

	bool
	Update3NSrcDestination (Ptr<const NNNAddress> addr, uint32_t low_part);

	void
	Update3NPossibleDestination (Ptr<const NNNAddress> addr);

	void
	Update3NPossibleDestination (Ptr<const NNNAddress> addr, uint32_t low_part);

	void
	UpdatePoADestination (Ptr<Face> face, const Address& addr, int32_t metric);

	bool
	UpdatePoADestination (Ptr<Face> face, const Address& addr, uint32_t low_part, int32_t metric);

	size_t
	GetPartSrcEntries () const;

	size_t
	GetPartPoAEntries () const;

	/**
	 * \brief Get prefix for the FIB entry
	 */
	const icn::Name&
	GetPrefix () const { return *m_prefix; }

	/**
	 * \brief Find "best route" candidate, skipping `skip' first candidates (modulo # of faces)
	 *
	 * throws Entry::NoFaces if m_faces.size()==0
	 */
	const FaceMetric &
	FindBestCandidate (uint32_t skip = 0) const;

	Ptr<const NNNAddress>
	FindNewest3NDestination (uint32_t skip = 0);

	std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp>
	Find3NDestinations (Time threshold = Seconds (0));

	PartsSeenContainer
	Find3NDestinationsByTime (Time threshold = Seconds (0));

	PartsPoASeenContainer
	FindPoADestinationsByTime (Time threshold = Seconds (0));

	/**
	 * @brief Remove record associated with `face`
	 */
	void
	RemoveFace (const Ptr<Face> &face)
	{
	  m_faces.erase (face);
	}

	/**
	 * @brief Get pointer to access FIB, to which this entry is added
	 */
	Ptr<Fib>
	GetFib ();

      protected:
	bool
	UpdatePartsSeenContainer (int st, Ptr<const NNNAddress> addr, uint32_t low_part);

	void
	PrintDebug (int st);

      private:
	friend std::ostream& operator<< (std::ostream& os, const Entry &entry);

      public:
	Ptr<Fib> m_fib;                    ///< \brief FIB to which entry is added

	Ptr<const icn::Name> m_prefix;     ///< \brief Prefix of the FIB entry
	FaceMetricContainer m_faces; ///< \brief Indexed list of faces

	bool m_needsProbing;               ///< \brief flag indicating that probing should be performed
	PartsSeenContainer m_src_addrs;    ///< \brief Container for all the 3N SRC names and ICN parts seen for a particular prefix
	PartsSeenContainer m_dst_addrs;    ///< \brief Container for all the 3N DST names and ICN parts seen for a particular prefix
	PartsPoASeenContainer m_src_poa_addrs; ///< \brief Container for all the PoA SRC names and ICN parts seens for a particular prefix
      };

      std::ostream& operator<< (std::ostream& os, const Entry &entry);
      std::ostream& operator<< (std::ostream& os, const FaceMetric &metric);

    } // namespace fib
  } // namespace nnn
} // namespace ns3

#endif // NNN_FIB_ENTRY_H
