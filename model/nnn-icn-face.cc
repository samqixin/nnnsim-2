/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-face.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-face.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-face.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jelfn@sislacom.com>
 *
 */
#include "ns3/assert.h"
#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/packet.h"
#include "ns3/pointer.h"
#include "ns3/simulator.h"
#include "ns3/uinteger.h"

#include "nnn-face.h"
#include "nnn-icn-face.h"
#include "ns3/nnn-icn-pdus.h"

#include "ns3/nnn-icn-wire.h"
#include "ns3/wire-nnnsim-icn-naming.h"
#include "ns3/nnnsim-icn-data.h"
#include "ns3/nnnsim-icn-interest.h"

#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-icn-forwarding-strategy.h"
namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.ICNFace");
  namespace nnn
  {

    NS_OBJECT_ENSURE_REGISTERED (Face);

    TypeId
    ICNFace::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::ICNFace")
   		  .SetParent<Face> ()
		  .SetGroupName ("Nnn")
		  .AddAttribute ("Id", "Face id (unique integer for the Nnn stack on this node)",
				 TypeId::ATTR_GET, // allow only getting it.
				 UintegerValue (0),
				 MakeUintegerAccessor (&ICNFace::m_id),
				 MakeUintegerChecker<uint32_t> ())
		  ;
      return tid;
    }

    ICNFace::ICNFace (Ptr<Node> node)
    : Face (node)
    , m_upstreamInterestHandler   (MakeNullCallback< void, Ptr<ICNFace>, Ptr<Interest> > ())
    , m_upstreamDataHandler   (MakeNullCallback< void, Ptr<ICNFace>, Ptr<Data> > ())
    {
      SetFlags (Face::ICN);

      // Enable ICN protocols
      m_nminus1_protocols["ICN"] = true;
    }

    ICNFace::~ICNFace ()
    {
    }

    bool
    ICNFace::SendInterest (Ptr<const Interest> interest_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << interest_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsICNEnabled ())
	{
	  return false;
	}

      return SendICN (icn::Wire::FromInterest (interest_o));
    }

    bool
    ICNFace::SendData (Ptr<const Data> data_o)
    {
      NS_LOG_FUNCTION (this << boost::cref (*this) << data_o);

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsICNEnabled ())
	{
	  return false;
	}

      return SendICN (icn::Wire::FromData (data_o));
    }

    bool
    ICNFace::ReceiveICN (Ptr<const Packet> p)
    {
      NS_LOG_FUNCTION (this << p << p->GetSize ());

      if (!IsUp ())
	{
	  return false;
	}

      if (!IsICNEnabled ())
	{
	  return false;
	}

      Ptr<Packet> packet = p->Copy (); // give upper layers a rw copy of the packet
      // Attempt to check ICN headers
      try
      {
	  icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(packet);
	  switch (icnType)
	  {
	    case icn::HeaderHelper::INTEREST_ICN:
	      return ReceiveInterest (icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM));
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      return ReceiveData (icn::Wire::ToData (packet, icn::Wire::WIRE_FORMAT_NDNSIM));
	      break;
	  }
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_LOG_DEBUG ("Unknown ICN header.");
      }

      // Fall back to 3N PDUs
      return Face::Receive3N(p);
    }

    bool
    ICNFace::ReceiveInterest (Ptr<Interest> interest_i)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsICNEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained raw Interest");

      m_upstreamInterestHandler (this, interest_i);
      return true;
    }

    bool
    ICNFace::ReceiveData (Ptr<Data> data_i)
    {
      if (!IsUp ())
	{
	  return false;
	}

      if (!IsICNEnabled ())
	{
	  return false;
	}

      NS_LOG_DEBUG ("Obtained raw Data");

      m_upstreamDataHandler (this, data_i);
      return true;
    }

    void
    ICNFace::RegisterProtocolHandlers (Ptr<ForwardingStrategy> fw)
    {
      NS_LOG_FUNCTION_NOARGS ();

      Face::RegisterProtocolHandlers(fw);
    }

    void
    ICNFace::UnRegisterProtocolHandlers ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      Face::UnRegisterProtocolHandlers ();
    }

    void
    ICNFace::EnableICN (bool enable)
    {
      m_nminus1_protocols["ICN"] = enable;
    }

    bool
    ICNFace::IsICNEnabled ()
    {
      std::map<std::string,bool>::iterator it;
      it = m_nminus1_protocols.find ("ICN");
      if (it != m_nminus1_protocols.end ())
	{
	  return it->second;
	}
      else
	return false;
    }
  } /* namespace nnn */
} /* namespace ns3 */
