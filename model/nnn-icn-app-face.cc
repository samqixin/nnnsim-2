/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-app-face.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-app-face.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-app-face.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"
#include "ns3/ptr.h"

#include "ns3/nnn-fib.h"
#include "nnn-icn-app-face.h"
#include "ns3/nnn-icn-app.h"
#include "ns3/nnn-pure-icn-app.h"
#include "ns3/nnn-pdus.h"
#include "ns3/nnn-icn-pdus.h"

#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-icn-wire.h"
#include "ns3/nnn-wire.h"

#include "ns3/nnn-icn-forwarding-strategy.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.ICNAppFace");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (ICNAppFace);

    TypeId
    ICNAppFace::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::ICNAppFace")
	  .SetParent<AppFace> ()
	  .SetGroupName ("Nnn")
	  ;
      return tid;
    }

    ICNAppFace::ICNAppFace (Ptr<ICNApp> app)
    : AppFace (DynamicCast<App> (app))
    , m_app (app)
    , m_usePureICN (false)
    {
      NS_LOG_FUNCTION (this << app);

      NS_ASSERT (m_app != 0);
      SetFlags (Face::APPLICATION);

      m_node = app->GetNode ();
      m_ifup = false;
      m_id = (uint32_t)-1;
      m_metric = 0;

      m_AppIsMobile = false;
      m_NodeIsMobile = false;

      m_app->TraceConnectWithoutContext("BecameMobile", MakeCallback(&ICNAppFace::SetAppMobile, this));
    }

    ICNAppFace::ICNAppFace (Ptr<PureICNApp> app)
    : AppFace (app->GetNode ())
    {
      NS_LOG_FUNCTION (this << app);
      m_pureicnapp = app;
      m_usePureICN = true;

      NS_LOG_FUNCTION (this << app);
      NS_ASSERT (m_pureicnapp != 0);

      SetFlags (Face::APPLICATION);
      m_node = app->GetNode ();
      m_ifup = false;
      m_id = (uint32_t)-1;
      m_metric = 0;

      m_AppIsMobile = false;
      m_NodeIsMobile = false;

      m_upstreamNULLpHandler = MakeNullCallback <void, Ptr<Face>, Ptr<NULLp> > ();
      m_upstreamSOHandler = MakeNullCallback< void, Ptr<Face>, Ptr<SO> > ();
      m_upstreamDOHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DO> > ();
      m_upstreamENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<EN> > ();
      m_upstreamAENHandler =MakeNullCallback< void, Ptr<Face>, Ptr<AEN> > ();
      m_upstreamRENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<REN> > ();
      m_upstreamDENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DEN> > ();
      m_upstreamINFHandler = MakeNullCallback< void, Ptr<Face>, Ptr<INF> > ();
      m_upstreamDUHandler = MakeNullCallback< void, Ptr<Face>, Ptr<DU> > ();
      m_upstreamOENHandler = MakeNullCallback< void, Ptr<Face>, Ptr<OEN> > ();

      m_pureicnapp->TraceConnectWithoutContext("BecameMobile", MakeCallback(&ICNAppFace::SetAppMobile, this));
    }

    ICNAppFace::~ICNAppFace ()
    {
      NS_LOG_FUNCTION_NOARGS ();
    }

    ICNAppFace::ICNAppFace ()
    : m_usePureICN (false)
    , m_AppIsMobile (false)
    , m_NodeIsMobile (false)
    , m_has3NName (false)
    {
      EnableAppICN(false);
    }

    ICNAppFace::ICNAppFace (const ICNAppFace &)
    : m_usePureICN (false)
    , m_AppIsMobile (false)
    , m_NodeIsMobile (false)
    , m_has3NName (false)
    {
    }

    ICNAppFace&
    ICNAppFace::operator= (const ICNAppFace &)
    {
      return *((ICNAppFace*)0);
    }

    void
    ICNAppFace::RegisterProtocolHandlers (Ptr<ForwardingStrategy> fw)
    {
      NS_LOG_FUNCTION_NOARGS ();

      // Call the inherited register protocols
      AppFace::RegisterProtocolHandlers(fw);

      m_icn_fw = DynamicCast<ICN3NForwardingStrategy> (fw);

      // Connect the callbacks
      m_icn_fw->TraceConnectWithoutContext("Got3NName", MakeCallback (&ICNAppFace::SetCurr3NName, this));
      m_icn_fw->TraceConnectWithoutContext("No3NName", MakeCallback (&ICNAppFace::SetNo3NName, this));
      m_icn_fw->TraceConnectWithoutContext("BecameMobile", MakeCallback (&ICNAppFace::SetNodeMobile, this));
      m_icn_fw->TraceConnectWithoutContext("ChangedLifetime", MakeCallback (&ICNAppFace::SetPDULifetime, this));

      // Find out if the node is mobile
      m_NodeIsMobile = m_icn_fw->IsMobile();

      // Find out if the node has a 3N name
      m_has3NName = m_icn_fw->Has3NName();

      // If it has a name, retrieve it
      if (m_has3NName)
	{
	  m_currSrc = m_icn_fw->GetNode3NNamePtr ();
	}

      // Currently not using the App level mobility but leaving code block
      // for later expansion.
      if (m_usePureICN)
	{
	  m_AppIsMobile = false;
	}
      else
	{
	  m_AppIsMobile = false;
	}
    }

    void
    ICNAppFace::UnRegisterProtocolHandlers ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      // Call the inherited register protocols
      AppFace::UnRegisterProtocolHandlers();

      // Disconnect the callbacks
      m_icn_fw->TraceDisconnectWithoutContext("Got3NName", MakeCallback (&ICNAppFace::SetCurr3NName, this));
      m_icn_fw->TraceDisconnectWithoutContext("No3NName", MakeCallback (&ICNAppFace::SetNo3NName, this));
      m_icn_fw->TraceDisconnectWithoutContext("BecameMobile", MakeCallback (&ICNAppFace::SetNodeMobile, this));
      m_icn_fw->TraceDisconnectWithoutContext("ChangedLifetime", MakeCallback (&ICNAppFace::SetPDULifetime, this));
    }

    bool
    ICNAppFace::SendInterest (Ptr<const Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);

      if (!IsUp ())
	{
	  return false;
	}

      // We have to distinguish between the App types
      // There is probably a better way to do this, but not certain
      if (m_usePureICN)
	{
	  if (interest->GetInterestType () > 0)
	    m_pureicnapp->OnNack (interest);
	  else
	    m_pureicnapp->OnInterest (interest);
	}
      else
	{
	  if (interest->GetInterestType () > 0)
	    m_app->OnInterestType (interest);
	  else
	    m_app->OnInterest (interest);
	}
      return true;
    }

    bool
    ICNAppFace::SendData (Ptr<const Data> data)
    {
      NS_LOG_FUNCTION (this << data);

      if (!IsUp ())
	{
	  return false;
	}

      if (m_usePureICN)
	{
	  m_pureicnapp->OnData(data);
	}
      else
	{
	  m_app->OnData (data);
	}
      return true;
    }

    bool
    ICNAppFace::SendNULLp (Ptr<const NULLp> nullpObject)
    {
      NS_LOG_FUNCTION (this << nullpObject);

      if (!IsUp ())
	{
	  return false;
	}

      if (nullpObject->GetPDUPayloadType () == ICN_NNN)
	{
	  if (m_usePureICN)
	    {
	      NS_LOG_INFO ("Accessing Pure ICN App");
	      m_pureicnapp->OnICN (nullpObject->GetPayload ());
	    }
	  else
	    {
	      NS_LOG_INFO ("Accessing ICN App");
	      Ptr<NULLp> tmp = Create <NULLp> (*nullpObject);
	      m_app->OnNULLp (tmp);
	    }
	}
      return true;
    }

    bool
    ICNAppFace::SendSO (Ptr<const SO> soObject)
    {
      NS_LOG_FUNCTION (this << soObject);
      if (!IsUp ())
	{
	  return false;
	}

      if (soObject->GetPDUPayloadType () == ICN_NNN)
	{
	  if (m_usePureICN)
	    {
	      NS_LOG_INFO ("Accessing Pure ICN App");
	      m_pureicnapp->OnICN (soObject->GetPayload ());
	    }
	  else
	    {
	      NS_LOG_INFO ("Accessing ICN App");
	      Ptr<SO> tmp = Create <SO> (*soObject);
	      m_app->OnSO (tmp);
	    }
	}
      return true;
    }

    bool
    ICNAppFace::SendDO (Ptr<const DO> doObject)
    {
      NS_LOG_FUNCTION (this << doObject);
      if (!IsUp ())
	{
	  return false;
	}

      if (doObject->GetPDUPayloadType () == ICN_NNN)
	{
	  if (m_usePureICN)
	    {
	      NS_LOG_INFO ("Accessing Pure ICN App");
	      m_pureicnapp->OnICN (doObject->GetPayload ());
	    }
	  else
	    {
	      NS_LOG_INFO ("Accessing ICN App");
	      Ptr<DO> tmp = Create <DO> (*doObject);
	      m_app->OnDO (tmp);
	    }
	}

      return true;
    }

    bool
    ICNAppFace::SendDU (Ptr<const DU> duObject)
    {
      NS_LOG_FUNCTION (this << duObject);
      if (!IsUp ())
	{
	  return false;
	}

      if (duObject->GetPDUPayloadType () == ICN_NNN)
	{
	  if (m_usePureICN)
	    {
	      NS_LOG_INFO ("Accessing Pure ICN App");
	      m_pureicnapp->OnICN (duObject->GetPayload ());
	    }
	  else
	    {
	      NS_LOG_INFO ("Accessing ICN App");
	      Ptr<DU> tmp = Create <DU> (*duObject);
	      m_app->OnDU (tmp);
	    }
	}
      return true;
    }

    bool
    ICNAppFace::SendIn3NPDU (Ptr<Packet> pkt)
    {
      NS_LOG_FUNCTION (this);
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  NS_LOG_WARN ("Wired 3N PDU not supported");
	  return false;
	}

      // We have no information from the App, find out what we can do without it
      if (m_NodeIsMobile)
	{
	  NS_LOG_INFO ("The node is mobile");
	  if (m_has3NName)
	    {
	      NS_LOG_INFO ("We have a 3N name: " << *m_currSrc);
	      // It is flagged. Use the 3N name
	      NS_LOG_INFO ("Send in SO PDU src: " << *m_currSrc);
	      Ptr<SO> so_o = Create<SO> ();
	      so_o->SetPDUPayloadType(ICN_NNN);
	      so_o->SetLifetime(m_3n_lifetime);
	      so_o->SetName(m_currSrc);
	      so_o->SetPayload(pkt);
	      m_upstreamSOHandler(this, so_o);
	    }
	  else
	    {
	      NS_LOG_INFO ("We have no 3N name");
	      NS_LOG_INFO ("Send in NULLp PDU");
	      Ptr<NULLp> nullp_o = Create<NULLp> ();
	      nullp_o->SetPDUPayloadType (ICN_NNN);
	      nullp_o->SetLifetime(m_3n_lifetime);
	      nullp_o->SetPayload (pkt);
	      m_upstreamNULLpHandler (this, nullp_o);
	    }
	}
      else
	{
	  NS_LOG_INFO ("The node is not mobile");

	  NS_LOG_INFO ("Send in NULLp PDU");
	  Ptr<NULLp> nullp_o = Create<NULLp> ();
	  nullp_o->SetPDUPayloadType (ICN_NNN);
	  nullp_o->SetLifetime(m_3n_lifetime);
	  nullp_o->SetPayload (pkt);
	  m_upstreamNULLpHandler (this, nullp_o);
	}

      return true;
    }

    bool
    ICNAppFace::SendIn3NPDU (Ptr<const icn::Name> name, Ptr<Packet> pkt)
    {
      NS_LOG_FUNCTION (this);
      if (!IsUp ())
	{
	  // no tracing here. If we were off while receiving, we shouldn't even know that something was there
	  return false;
	}

      if (!IsNminus3NEnabled ())
	{
	  NS_LOG_WARN ("Wired 3N PDU not supported");
	  return false;
	}

      // Check for a possible destination
      Ptr<const NNNAddress> addr = m_icn_fw->NNNForLongestPrefixMatch(name);

      if (!addr)
	{
	  NS_LOG_INFO ("We have no 3N name for " << *name);
	  return SendIn3NPDU (pkt);
	}
      else
	{
	  NS_LOG_INFO ("We have " << *addr << " as destination for " << *name);
	  if (m_NodeIsMobile)
	    {
	      NS_LOG_INFO ("The node is mobile");
	      if (m_has3NName)
		{
		  NS_LOG_INFO ("We have a 3N name: " << *m_currSrc);
		  // It is flagged. Use the 3N name
		  NS_LOG_INFO ("Send in DU PDU src: " << *m_currSrc << " dst: " << *addr);
		  Ptr<DU> du_o = Create<DU> ();
		  du_o->SetPDUPayloadType(ICN_NNN);
		  du_o->SetLifetime(m_3n_lifetime);
		  du_o->SetSrcName(m_currSrc);
		  du_o->SetDstName(addr);
		  du_o->SetPayload(pkt);
		  m_upstreamDUHandler(this, du_o);
		}
	      else
		{
		  NS_LOG_INFO ("We do not have a 3N name");
		  NS_LOG_INFO ("Send in DO PDU to " << *addr);
		  Ptr<DO> do_o = Create<DO> ();
		  do_o->SetPDUPayloadType (ICN_NNN);
		  do_o->SetLifetime(m_3n_lifetime);
		  do_o->SetName(addr);
		  do_o->SetPayload (pkt);
		  m_upstreamDOHandler (this, do_o);
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("The node is not mobile");
	      NS_LOG_INFO ("Send in DO PDU to " << *addr);
	      Ptr<DO> do_o = Create<DO> ();
	      do_o->SetPDUPayloadType (ICN_NNN);
	      do_o->SetLifetime(m_3n_lifetime);
	      do_o->SetName(addr);
	      do_o->SetPayload (pkt);
	      m_upstreamDOHandler (this, do_o);
	    }
	}

      return true;
    }

    std::ostream&
    ICNAppFace::Print (std::ostream& os) const
    {
      os << "dev=local(" << GetId() << ")";
      return os;
    }

    void
    ICNAppFace::SetAppMobile (bool mobile)
    {
      NS_LOG_FUNCTION (this);
      m_AppIsMobile = mobile;
    }

    bool
    ICNAppFace::IsAppMobile () const
    {
      return m_AppIsMobile;
    }

    void
    ICNAppFace::SetNodeMobile (bool mobile)
    {
      NS_LOG_FUNCTION (this);
      m_NodeIsMobile = mobile;
    }

    bool
    ICNAppFace::IsNodeMobile () const
    {
      return m_NodeIsMobile;
    }

    void
    ICNAppFace::SetCurrSrc (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this);
      m_currSrc = addr;
    }

    Ptr<const NNNAddress>
    ICNAppFace::GetCurrSrc () const
    {
      return m_currSrc;
    }

    void
    ICNAppFace::SetPDULifetime (Time lifetime)
    {
      NS_LOG_FUNCTION (this);
      m_3n_lifetime = lifetime;
    }

    Time
    ICNAppFace::GetPDULifetime () const
    {
      return m_3n_lifetime;
    }

    void
    ICNAppFace::SetNo3NName ()
    {
      NS_LOG_FUNCTION (this);
      m_has3NName = false;
    }

    void
    ICNAppFace::SetCurr3NName (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this);
      m_has3NName = true;
      m_currSrc = addr;
    }


    Ptr<const NNNAddress>
    ICNAppFace::GetCurr3NName () const
    {
      return m_currSrc;
    }

    bool
    ICNAppFace::ConnectToApp (Ptr<Packet> p)
    {
      bool ok = false;

      if (!IsAppICNEnabled())
	{
	  NS_LOG_DEBUG ("Face does not support direct ICN PDUs");
	  return false;
	}

      if (!ok)
	{
	  NS_LOG_DEBUG ("De-encapsulating payload of 3N PDU");
	  try
	  {
	      NNN_PDU_TYPE type = nnn::HeaderHelper::GetNNNHeaderType (p);
	      switch (type)
	      {
		case nnn::NULL_NNN:
		  ok = SendNULLp (nnn::Wire::ToNULLp (p, Wire::WIRE_FORMAT_NNNSIM));
		  break;
		case nnn::SO_NNN:
		  ok = SendSO (nnn::Wire::ToSO (p, Wire::WIRE_FORMAT_NNNSIM));
		  break;
		case nnn::DO_NNN:
		  ok = SendDO (nnn::Wire::ToDO (p, Wire::WIRE_FORMAT_NNNSIM));
		  break;
		case nnn::DU_NNN:
		  ok = SendDU (nnn::Wire::ToDU (p, Wire::WIRE_FORMAT_NNNSIM));
		  break;
		default:
		  NS_FATAL_ERROR ("Not supported 3N header");
		  return false;
	      }
	      // exception will be thrown if packet is not recognized
	  }
	  catch (nnn::UnknownHeaderException)
	  {
	      NS_LOG_DEBUG ("Unknown 3N header");
	      NS_FATAL_ERROR ("Unknown PDU type. Should not happen!");
	      return false;
	  }
	}

      if (!ok)
	{
	  NS_LOG_DEBUG ("Transforming ICN packet to Ptr class");
	  try
	  {
	      icn::HeaderHelper::Type icnType = icn::HeaderHelper::GetICNHeaderType(p);
	      switch (icnType)
	      {
		case icn::HeaderHelper::INTEREST_ICN:
		  ok = SendInterest (icn::Wire::ToInterest (p, icn::Wire::WIRE_FORMAT_NDNSIM));
		  break;
		case icn::HeaderHelper::CONTENT_OBJECT_ICN:
		  ok = SendData (icn::Wire::ToData (p, icn::Wire::WIRE_FORMAT_NDNSIM));
		  break;
	      }
	  }
	  catch (icn::UnknownHeaderException)
	  {
	      NS_LOG_DEBUG ("Unknown ICN header.");
	  }
	}

      if (!ok)
	ok = AppFace::ConnectToApp (p);

      return ok;
    }
  } // namespace nnn
} // namespace ns3
