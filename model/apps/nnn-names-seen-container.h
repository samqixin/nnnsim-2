/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-names-seen-container.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-names-seen-container.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-names-seen-container.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#ifndef MODEL_APPS_NNN_NAMES_SEEN_CONTAINER_H_
#define MODEL_APPS_NNN_NAMES_SEEN_CONTAINER_H_

#include "ns3/nstime.h"

#include "ns3/nnn-address.h"

#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/member.hpp>

namespace ns3
{
  namespace nnn
  {
    struct NamesSeen
    {
      NamesSeen (Ptr<const NNNAddress> _name)
      : name (_name)
      , firstSeen (Simulator::Now ())
      , lastSeen (Simulator::Now ())
      {
      }

      NamesSeen (Ptr<const NNNAddress> _name, Time _first, Time _last)
      : name (_name)
      , firstSeen (_first)
      , lastSeen (_last)
      {
      }

      ~NamesSeen ()
      {
	name = 0;
      }

      bool operator< (const NamesSeen e) const
      {
	// Check when the name was first seen
	if (firstSeen == e.firstSeen)
	  {
	    // Attempt to order by the last time this name was seen
	    return lastSeen < e.lastSeen;
	  }
	else
	  {
	    // If they aren't the same, then just order simply
	    return firstSeen < e.firstSeen;
	  }
      }

      Ptr<const NNNAddress> name;
      Time firstSeen;
      Time lastSeen;
    };

    class i_name { };
    class i_firstseen { };
    class i_lastseen { };
    class i_uname { };

    struct NamesSeenContainer :
	public boost::multi_index::multi_index_container<
	    NamesSeen,
	    boost::multi_index::indexed_by<
	        // Sort by NameSeen structure
	        boost::multi_index::ordered_unique<
		    boost::multi_index::tag<i_firstseen>,
		    boost::multi_index::identity<NamesSeen>
                >,
		// Sort by 3N name
		boost::multi_index::ordered_unique<
		    boost::multi_index::tag<i_name>,
		    boost::multi_index::member<NamesSeen, Ptr<const NNNAddress>, &NamesSeen::name>,
		    Ptr3NComp
		>,
		// Sort by latest seen
		boost::multi_index::ordered_unique<
		    boost::multi_index::tag<i_lastseen>,
		    boost::multi_index::composite_key<
		        NamesSeen,
			boost::multi_index::member<NamesSeen, Time, &NamesSeen::lastSeen>
		    >,
		    boost::multi_index::composite_key_compare<
		        std::greater<Time>
                    >
                >,
		// Sort by last seen time and largest name
		boost::multi_index::ordered_unique<
		    boost::multi_index::tag<i_uname>,
		    boost::multi_index::composite_key<
		        NamesSeen,
			boost::multi_index::member<NamesSeen, Time, &NamesSeen::lastSeen>,
			boost::multi_index::member<NamesSeen, Ptr<const NNNAddress>, &NamesSeen::name>
		    >,
		    boost::multi_index::composite_key_compare<
		        std::greater<Time>,
			Ptr3NSizeMaxComp
		    >
                >
            >
        > { };

    typedef NamesSeenContainer::index<i_name>::type names_seen_by_name;
    typedef NamesSeenContainer::index<i_firstseen>::type names_seen_by_time;
    typedef NamesSeenContainer::index<i_lastseen>::type names_seen_by_latesttime;
    typedef NamesSeenContainer::index<i_uname>::type names_seen_by_uname;
  }
}
#endif /* MODEL_APPS_NNN_NAMES_SEEN_CONTAINER_H_ */
