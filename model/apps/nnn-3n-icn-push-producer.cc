/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-3n-icn-push-producer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-3n-icn-push-producer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-3n-icn-push-producer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-3n-icn-push-producer.h"

#include "ns3/double.h"
#include "ns3/integer.h"
#include "ns3/boolean.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"

#include "ns3/nnn-fib.h"
#include "ns3/nnn-fib-entry.h"
#include "ns3/nnn-forwarding-strategy.h"
#include "ns3/nnn-fw-hop-count-tag.h"
#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-pdus.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.NNNICNPushProducer");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (NNNICNPushProducer);

    TypeId
    NNNICNPushProducer::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::NNNICNPushProducer")
	  .SetGroupName ("Nnn")
	  .SetParent<ICNProducerApp> ()
	  .AddConstructor<NNNICNPushProducer> ()
	  .AddAttribute ("Frequency", "Frequency of interest packets",
			 StringValue ("1.0"),
			 MakeDoubleAccessor (&NNNICNPushProducer::m_frequency),
			 MakeDoubleChecker<double> ())
	  .AddAttribute("Randomize", "Type of send time randomization: none (default), uniform, exponential",
			StringValue ("none"),
			MakeStringAccessor (&NNNICNPushProducer::SetRandomizationType, &NNNICNPushProducer::GetRandomizationType),
			MakeStringChecker ())
	  .AddAttribute("MaxSeq", "Maximum sequence number to request",
			IntegerValue (std::numeric_limits<uint32_t>::max ()),
			MakeIntegerAccessor (&NNNICNPushProducer::m_seqMax),
			MakeIntegerChecker<uint32_t> ())
	  .AddAttribute("EdgePush", "Specifies if the Producer pushes to an edge",
			BooleanValue (false),
			MakeBooleanAccessor(&NNNICNPushProducer::IsEdgePush, &NNNICNPushProducer::SetEdgePush),
			MakeBooleanChecker())
	  .AddAttribute("CentroidPush", "Specifies if the Producer pushes to a centroid",
			BooleanValue (false),
			MakeBooleanAccessor(&NNNICNPushProducer::IsCentroidPush, &NNNICNPushProducer::SetCentroidPush),
			MakeBooleanChecker())
	  .AddAttribute("AnchorPush", "Specifies if the Producer pushes to a specific address",
			BooleanValue (false),
			MakeBooleanAccessor(&NNNICNPushProducer::IsAnchorPush, &NNNICNPushProducer::SetAnchorPush),
			MakeBooleanChecker())
	  .AddAttribute("Anchor", "Specifies the 3N name to which to AnchorPush",
			NNNAddressValue (),
			MakeNNNAddressAccessor (&NNNICNPushProducer::GetAnchor, &NNNICNPushProducer::AddAnchor),
			MakeNNNAddressChecker ())
	  .AddAttribute("DirectInterest", "Specifies whether the Producer answers to direct Interest PDUs",
			BooleanValue (false),
			MakeBooleanAccessor(&NNNICNPushProducer::m_directInterest),
			MakeBooleanChecker())
	;
      return tid;
    }

    NNNICNPushProducer::NNNICNPushProducer ()
    : ICNProducerApp()
    , m_edge_push (false)
    , m_centroid_push (false)
    , m_anchor_push (false)
    , m_directInterest (false)
    , m_frequency (1.0)
    , m_seq (0)
    , m_seqMax (std::numeric_limits<uint32_t>::max ())
    , m_firstTime (true)
    , useUni (false)
    , expRandom (CreateObject<ExponentialRandomVariable> ())
    , m_randomType ("")
    {
    }

    NNNICNPushProducer::~NNNICNPushProducer ()
    {
      m_push_destinations.clear ();
      expRandom = 0;
      m_current3Nname = 0;
      m_face = 0;
    }

    Ptr<Packet>
    NNNICNPushProducer::CreateData ()
    {
//      NS_LOG_FUNCTION_NOARGS ();

      // Create the name for the Data packet
      Ptr<icn::Name> dataName = Create<icn::Name> (m_prefix);
      // Add the current sequence number
      dataName->appendSeqNum(m_seq);

      // Create the Data packet
      Ptr<Data> data = Create<Data> (Create<Packet> (m_virtualPayloadSize));
      data->SetName (dataName);
      data->SetFreshness (m_freshness);
      data->SetTimestamp (Simulator::Now());

      data->SetSignature (m_signature);
      if (m_keyLocator.size () > 0)
	{
	  data->SetKeyLocator (Create<icn::Name> (m_keyLocator));
	}


      // Increment the current sequence number
      m_seq++;
      NS_LOG_INFO ("Creating Pkt: " << *dataName << " Interest Name: " << data->GetName () <<
		   " SeqNo " << std::dec << data->GetName ().getSeqNum () << ", Incremented to " << m_seq);

      // Log that we are to send out a Data packet
      m_transmittedDatas (data, this, m_face);
      return icn::Wire::FromData (data);
    }

    void
    NNNICNPushProducer::FlowidEliminated (uint32_t flowid)
    {
      if (flowid == m_3n_mapme_flowid)
	{
	  NS_LOG_DEBUG ("MapMe Flowid: " << flowid << " has been eliminated");
	  m_3n_mapme_flowid = 0;
	  m_3n_mapme_seq = 0;
	  m_firstMapMe = true;
	}
      else
	{
	  NS_LOG_DEBUG ("Regular Flowid: " << flowid << " has been eliminated");
	}

      App::FlowidEliminated (flowid);
    }

    void
    NNNICNPushProducer::SendMapMeInterest()
    {
      NS_LOG_FUNCTION_NOARGS ();

      Ptr<icn::Name> name_prefix = Create<icn::Name> (m_prefix);
      name_prefix->appendSeqNum (m_mapme_seq);

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (uniRandom->GetValue ());
      interest->SetName                (name_prefix);
      interest->SetInterestLifetime    (m_interestLifeTime);
      interest->SetInterestType        (Interest::MAP_ME_INTEREST_UPDATE);
      interest->SetSequenceNumber      (m_mapme_seq);
      interest->SetPartNumber          (m_seq);
      interest->SetRetransmissionTime  (m_mapme_retransmit);

      Time now = Simulator::Now ();

      NS_LOG_INFO ("Sending MapMe Interest with Interest name: " << *name_prefix << " MapMe SeqNo " << m_mapme_seq);
      NS_LOG_DEBUG ("Interest Type: " << unsigned(interest->GetInterestType()) << " MapMe Interest Nonce: " << interest->GetNonce());
      NS_LOG_DEBUG ("Lifetime: " << (now + m_interestLifeTime).GetSeconds () << " Retransmission time: " << (now + m_mapme_retransmit).GetSeconds ());
      NS_LOG_DEBUG ("Part number: " << interest->GetPartNumber ());
      // Log that we are to send out a Interest packet
      m_transmittedInterests (interest, this, m_face);

      // Encode the packet
      Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);

      // Verify that we have a flowid for the MapMe PDU
      if (m_3n_mapme_flowid == 0)
	{

	  m_3n_mapme_flowid = m_fw->GenerateAppFlowid();
	  m_3n_mapme_seq = 0;
	  m_firstMapMe = true;
	  NS_LOG_DEBUG ("Obtained Flowid: " << m_3n_mapme_flowid << " for MapMe");
	}

      if (m_edge_push)
	{
	  NS_LOG_INFO ("Edge pushing");
	  if (m_has3Nname)
	    {
	      NS_LOG_INFO ("Pushing with DO to " << m_current3Nname->getSectorName());
	      Ptr<DO> do_o = Create<DO> ();
	      Ptr<const NNNAddress> dst = Create<const NNNAddress> (m_current3Nname->getSectorName ());
	      do_o->SetFlowid (m_3n_mapme_flowid);
	      do_o->SetSequence (m_3n_mapme_seq);
	      if (m_firstMapMe)
		{
		  do_o->SetPDURF (true);
		  do_o->SetRate (8192);
		  do_o->SetPDUWindow(m_3n_mapme_window);
		  m_firstMapMe = false;
		}
	      do_o->SetName (dst);
	      do_o->SetPDUPayloadType (ICN_NNN);
	      do_o->SetLifetime(m_3n_lifetime);
	      do_o->SetPayload (retPkt);

	      m_face->ReceiveDO(do_o);
	      m_transmittedDOs (do_o, this, m_face);
	      m_mapme_seq++;
	      m_3n_mapme_seq++;
	    }
	  else
	    {
	      NS_LOG_WARN ("Have no 3N name, cannot edge push!");
	    }
	}
      else if (m_centroid_push || m_anchor_push)
	{
	  if (m_centroid_push)
	    {
	      NS_LOG_INFO ("Centroid pushing, no need to update via MapMe");
	    }
	  else
	    {
	      NS_LOG_INFO ("Anchor pushing, no need to update via MapMe");
	    }
	}
      else
	{
	  NS_LOG_ERROR ("No Push method active - check for errors");
	}
    }

    void
    NNNICNPushProducer::SendPacket ()
    {
      // Verify that this Producer is active
      if (!m_active) return;

//      NS_LOG_FUNCTION_NOARGS ();

      // Verify that there are still sequence numbers to create
      if (m_seq >= m_seqMax)
	{
	  NS_LOG_DEBUG("Surpassed maximum sequence number!");
	  return;
	}

      uint32_t flowid = 0;
      uint32_t seq = 0;
      bool first = true;

      if (m_edge_push)
	{
	  NS_LOG_INFO ("Edge pushing");
	  if (m_has3Nname)
	    {
	      std::tie(flowid, seq, first) = FindFlowidSeq (0, 0);

	      // Create a Data packet with the next sequence number
	      Ptr<Packet> retPkt = CreateData();

	      Ptr<DO> do_o = Create<DO> ();
	      do_o->SetFlowid (flowid);
	      do_o->SetSequence (seq);
	      if (first)
		{
		  do_o->SetPDURF(true);
		  do_o->SetPDUWindow(m_Window);
		  do_o->SetRate(m_Rate);
		  NS_LOG_INFO ("Pushing with DO flowid: " << flowid << " Sequence: " << seq << "RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s with SeqNo " <<  (m_seq - 1) << " to " << m_current3Nname->getSectorName());
		}
	      else
		{
		  NS_LOG_INFO ("Pushing with DO flowid: " << flowid << " Sequence: " << seq << " with SeqNo " <<  (m_seq - 1) << " to " << m_current3Nname->getSectorName());
		}
	      Ptr<const NNNAddress> dst = Create<const NNNAddress> (m_current3Nname->getSectorName ());
	      do_o->SetName (dst);
	      do_o->SetPDUPayloadType (ICN_NNN);
	      do_o->SetLifetime(m_3n_lifetime);
	      do_o->SetPayload (retPkt);

	      m_face->ReceiveDO(do_o);
	      m_transmittedDOs (do_o, this, m_face);

	      m_3n_seq.find(flowid)->second++;
	    }
	  else
	    {
	      NS_LOG_WARN ("Have no 3N name, cannot edge push!");
	    }
	}
      else if (m_centroid_push || m_anchor_push)
	{
	  if (m_centroid_push)
	    {
	      NS_LOG_INFO ("Centroid pushing");
	    }
	  else
	    {
	      NS_LOG_INFO ("Anchor pushing");
	    }

	  if (m_push_destinations.empty())
	    {
	      NS_LOG_WARN ("There are no push destinations! Returning");
	    }
	  else
	    {
	      // Create a Data packet with the next sequence number
	      Ptr<Packet> retPkt = CreateData();
	      std::set <Ptr<const NNNAddress>, Ptr3NComp>::iterator it;

	      for (it = m_push_destinations.begin (); it != m_push_destinations.end (); ++it)
		{
		  NS_LOG_DEBUG ("Will push DO with SeqNo "<< (m_seq -1 ) << " to " << **it);
		}

	      uint32_t count = 0;

	      for (it = m_push_destinations.begin(); it != m_push_destinations.end(); ++it)
		{
		  std::tie(flowid, seq, first) = FindFlowidSeq (0, count);

		  NS_LOG_INFO ("Executing push of DO Flowid: " << flowid << " Sequence: " << seq << " with SeqNo " << (m_seq -1 ) << " to " << **it);
		  Ptr<DO> do_o = Create<DO> ();
		  do_o->SetFlowid (flowid);
		  do_o->SetSequence (seq);
		  if (first)
		    {
		      do_o->SetPDURF(true);
		      do_o->SetPDUWindow(m_Window);
		      do_o->SetRate(m_Rate);
		      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
		    }
		  do_o->SetName (*it);
		  do_o->SetPDUPayloadType (ICN_NNN);
		  do_o->SetLifetime(m_3n_lifetime);
		  do_o->SetPayload (retPkt);

		  m_face->ReceiveDO(do_o);
		  m_transmittedDOs (do_o, this, m_face);

		  m_3n_seq.find(flowid)->second++;
		  count++;
		  first = true;
		}
	    }
	}
      else
	{
	  NS_LOG_ERROR ("No Push method active - check for errors");
	}

      ScheduleNextPacket ();
    }

    void
    NNNICNPushProducer::OnInterest (Ptr<Interest> interest)
    {
      NS_LOG_FUNCTION (this << interest);

      if (!m_active) return;

      ICNApp::OnInterest (interest); // tracing inside
    }

    void
    NNNICNPushProducer::OnNULLp (Ptr<NULLp> nullpObject)
    {
      NS_LOG_FUNCTION (this << nullpObject);

      if (!m_active) return;

      ICNApp::OnNULLp(nullpObject);

      NS_LOG_FUNCTION (this << nullpObject);

      Ptr<Packet> packet = nullpObject->GetPayload ()->Copy ();
      uint16_t pdutype = nullpObject->GetPDUPayloadType ();

      NS_LOG_INFO (this << " obtained pdu type " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  try
	  {
	      icn::HeaderHelper::Type type = icn::HeaderHelper::GetICNHeaderType (packet);
	      Ptr<Interest> interest = 0;
	      switch (type)
	      {
		case icn::HeaderHelper::INTEREST_ICN:
		  interest = icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM);
		  break;
		case icn::HeaderHelper::CONTENT_OBJECT_ICN:
		  break;
	      }

	      if (interest != 0)
		{
		  OnInterest (interest);

		  uint8_t res = interest->GetInterestType();

		  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
		    {
		      if (res == Interest::MAP_ME_INTEREST_UPDATE_ACK)
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe ACK for " << interest->GetName ());
			}
		      else
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe information for " << interest->GetName ());
			}
		    }
		  else if (Interest::NACK_LOOP <= res)
		    {
		      NS_LOG_INFO ("Obtained an Interest with NACK information");
		    }
		  else
		    {
		      NS_LOG_INFO ("Obtained a normal Interest packet");
		      if (m_directInterest)
			{
			  uint32_t s_flowid = 0;
			  uint32_t seq = 0;
			  bool first = true;
			  std::tie(s_flowid, seq, first) = FindFlowidSeq (nullpObject, 0);

			  Ptr<Packet> retPkt = CreateReturnData(interest);

			  NS_LOG_INFO ("Responding NULLp Flowid: " << nullpObject->GetFlowid () << " with NULLp Flowid: " << s_flowid);
			  Ptr<NULLp> nullp_o = Create<NULLp> ();
			  nullp_o->SetFlowid (s_flowid);
			  nullp_o->SetSequence (seq);
			  if (first)
			    {
			      nullp_o->SetPDURF(true);
			      nullp_o->SetPDUWindow(m_Window);
			      nullp_o->SetRate(m_Rate);
			      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
			    }
			  nullp_o->SetPDUPayloadType (pdutype);
			  nullp_o->SetLifetime (m_3n_lifetime);
			  nullp_o->SetPayload (retPkt);

			  m_face->ReceiveNULLp (nullp_o);
			  m_transmittedNULLps (nullp_o, this, m_face);

			  m_3n_seq.find(s_flowid)->second++;
			}
		    }
		}
	      else
		{
		  NS_LOG_ERROR ("Interest was NULL, check code");
		}
	      // exception will be thrown if packet is not recognized
	  }
	  catch (icn::UnknownHeaderException)
	  {
	      NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
	  }
	}
      else
	{
	  NS_LOG_ERROR ("Received non ICN_NNN PDU");
	}
    }

    void
    NNNICNPushProducer::OnSO (Ptr<SO> soObject)
    {
      NS_LOG_FUNCTION (this << soObject);
      if (!m_active) return;

      App::OnSO(soObject);

      Ptr<Packet> packet = soObject->GetPayload ()->Copy ();
      uint16_t pdutype = soObject->GetPDUPayloadType ();

      NS_LOG_INFO (this << " obtained pdu type " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  try
	  {
	      icn::HeaderHelper::Type type = icn::HeaderHelper::GetICNHeaderType (packet);
	      Ptr<Interest> interest = 0;
	      switch (type)
	      {
		case icn::HeaderHelper::INTEREST_ICN:
		  interest = icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM);
		  break;
		case icn::HeaderHelper::CONTENT_OBJECT_ICN:
		  break;
	      }

	      if (interest != 0)
		{
		  OnInterest (interest);
		  uint8_t res = interest->GetInterestType();

		  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
		    {
		      if (res == Interest::MAP_ME_INTEREST_UPDATE_ACK)
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe ACK for " << interest->GetName ());
			}
		      else
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe information for " << interest->GetName ());
			}
		    }
		  else if (Interest::NACK_LOOP <= res)
		    {
		      NS_LOG_INFO ("Obtained an Interest with NACK information");
		    }
		  else
		    {
		      NS_LOG_INFO ("Obtained a normal Interest packet");

		      NS_LOG_INFO ("Attempting to insert destination " << soObject->GetName ());

		      // Get the current time
		      Time now = Simulator::Now ();
		      // Attempt to insert the seen name
		      names_seen_by_name& names_index = m_possibleRequesters.get<i_name> ();
		      names_seen_by_name::iterator it = names_index.find (soObject->GetNamePtr());

		      if (it != names_index.end ())
			{
			  NS_LOG_INFO ("Have already seen " << soObject->GetName ());
			}
		      else
			{
			  m_possibleRequesters.insert(NamesSeen (soObject->GetNamePtr()));
			  RecalculatePushDestination ();
			}

		      if (m_directInterest)
			{
			  Ptr<Packet> retPkt = CreateReturnData(interest);

			  uint32_t s_flowid = 0;
			  uint32_t seq = 0;
			  bool first = true;
			  std::tie(s_flowid, seq, first) = FindFlowidSeq (soObject, 0);

			  NS_LOG_INFO ("Responding to SO Flowid: " << soObject->GetFlowid () << " with DO to " << soObject->GetName () << " with Flowid: " << s_flowid);
			  Ptr<DO> do_o = Create<DO> ();
			  do_o->SetFlowid (s_flowid);
			  do_o->SetSequence (seq);
			  if (first)
			    {
			      do_o->SetPDURF(true);
			      do_o->SetPDUWindow(m_Window);
			      do_o->SetRate(m_Rate);
			      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
			    }
			  do_o->SetName (soObject->GetNamePtr ());
			  do_o->SetPDUPayloadType (pdutype);
			  do_o->SetLifetime(m_3n_lifetime);
			  do_o->SetPayload (retPkt);

			  m_face->ReceiveDO(do_o);
			  m_transmittedDOs (do_o, this, m_face);

			  m_3n_seq.find(s_flowid)->second++;
			}
		    }
		}
	      else
		{
		  NS_LOG_ERROR ("Interest was NULL, check code");
		}

	      // exception will be thrown if packet is not recognized
	  }
	  catch (icn::UnknownHeaderException)
	  {
	      NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
	  }
	}
    }

    void
    NNNICNPushProducer::OnDO (Ptr<DO> doObject)
    {
      NS_LOG_FUNCTION (this << doObject);
      if (!m_active) return;
      ICNApp::OnDO (doObject);

      Ptr<Packet> packet = doObject->GetPayload ()->Copy ();
      uint16_t pdutype = doObject->GetPDUPayloadType ();
      NS_LOG_INFO (this << " obtained pdu type " << pdutype);
      if (pdutype == ICN_NNN)
	{
	  try
	  {
	      icn::HeaderHelper::Type type = icn::HeaderHelper::GetICNHeaderType (packet);
	      Ptr<Interest> interest = 0;
	      switch (type)
	      {
		case icn::HeaderHelper::INTEREST_ICN:
		  interest = icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM);
		  break;
		case icn::HeaderHelper::CONTENT_OBJECT_ICN:
		  break;
	      }
	      if (interest != 0)
		{
		  OnInterest (interest);
		  uint8_t res = interest->GetInterestType();

		  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
		    {
		      if (res == Interest::MAP_ME_INTEREST_UPDATE_ACK)
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe ACK for " << interest->GetName ());
			}
		      else
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe information for " << interest->GetName ());
			}
		    }
		  else if (Interest::NACK_LOOP <= res)
		    {
		      NS_LOG_INFO ("Obtained an Interest with NACK information");
		    }
		  else
		    {
		      NS_LOG_INFO ("Obtained a normal Interest packet");
		      if (m_directInterest)
			{
			  Ptr<Packet> retPkt = CreateReturnData(interest);

			  uint32_t s_flowid = 0;
			  uint32_t seq = 0;
			  bool first = true;
			  std::tie(s_flowid, seq, first) = FindFlowidSeq (doObject, 0);

			  NS_LOG_INFO ("Responding to DO Flowid: " << doObject->GetFlowid () << " with NULL with Flowid: " << s_flowid);
			  Ptr<NULLp> nullp_o = Create<NULLp> ();
			  nullp_o->SetFlowid (s_flowid);
			  nullp_o->SetSequence (seq);
			  if (first)
			    {
			      nullp_o->SetPDURF(true);
			      nullp_o->SetPDUWindow(m_Window);
			      nullp_o->SetRate(m_Rate);
			      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
			    }
			  nullp_o->SetLifetime (m_3n_lifetime);
			  nullp_o->SetPDUPayloadType (pdutype);
			  nullp_o->SetPayload (retPkt);
			  m_face->ReceiveNULLp (nullp_o);
			  m_transmittedNULLps (nullp_o, this, m_face);

			  m_3n_seq.find(s_flowid)->second++;
			}
		    }
		}
	      // exception will be thrown if packet is not recognized
	  }
	  catch (icn::UnknownHeaderException)
	  {
	      NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
	  }
	}
    }

    void
    NNNICNPushProducer::OnDU (Ptr<DU> duObject)
    {
      NS_LOG_FUNCTION (this << duObject);
      if (!m_active) return;

      ICNApp::OnDU(duObject);

      NS_LOG_FUNCTION (this << duObject);

      Ptr<Packet> packet = duObject->GetPayload ()->Copy ();
      uint16_t pdutype = duObject->GetPDUPayloadType ();

      NS_LOG_INFO (this << " obtained pdu type " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  try
	  {
	      icn::HeaderHelper::Type type = icn::HeaderHelper::GetICNHeaderType (packet);
	      Ptr<Interest> interest = 0;
	      switch (type)
	      {
		case icn::HeaderHelper::INTEREST_ICN:
		  interest = icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM);
		  break;
		case icn::HeaderHelper::CONTENT_OBJECT_ICN:
		  break;
	      }

	      if (interest != 0)
		{
		  OnInterest (interest);
		  uint8_t res = interest->GetInterestType();

		  if (Interest::MAP_ME_INTEREST_UPDATE <= res && res < Interest::NACK_LOOP)
		    {
		      if (res == Interest::MAP_ME_INTEREST_UPDATE_ACK)
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe ACK for " << interest->GetName ());
			}
		      else
			{
			  NS_LOG_INFO ("Obtained an Interest with MapMe information for " << interest->GetName ());
			}
		    }
		  else if (Interest::NACK_LOOP <= res)
		    {
		      NS_LOG_INFO ("Obtained an Interest with NACK information");
		    }
		  else
		    {
		      NS_LOG_INFO ("Obtained a normal Interest packet");

		      NS_LOG_INFO ("Attempting to insert destination " << duObject->GetSrcName ());
		      // Get the current time
		      Time now = Simulator::Now ();
		      // Attempt to insert the seen name
		      names_seen_by_name& names_index = m_possibleRequesters.get<i_name> ();
		      names_seen_by_name::iterator it = names_index.find (duObject->GetSrcNamePtr ());

		      if (it != names_index.end ())
			{
			  NS_LOG_INFO ("Have already seen " << duObject->GetSrcName ());
			}
		      else
			{
			  m_possibleRequesters.insert(NamesSeen (duObject->GetSrcNamePtr ()));
			  RecalculatePushDestination ();
			}

		      NS_LOG_INFO ("Obtained a normal Interest packet");
		      if (m_directInterest)
			{
			  Ptr<Packet> retPkt = CreateReturnData(interest);
			  uint32_t s_flowid = 0;
			  uint32_t seq = 0;
			  bool first = true;
			  std::tie(s_flowid, seq, first) = FindFlowidSeq (duObject, 0);

			  NS_LOG_INFO ("Responding to DU Flowid: " << duObject->GetFlowid () << " with DO  " << duObject->GetSrcName () << " with Flowid: " << s_flowid);

			  NS_LOG_INFO ("Responding with DO to " << duObject->GetSrcName ());
			  Ptr<DO> do_o = Create<DO> ();
			  do_o->SetFlowid (s_flowid);
			  do_o->SetSequence (seq);
			  if (first)
			    {
			      do_o->SetPDURF(true);
			      do_o->SetPDUWindow(m_Window);
			      do_o->SetRate(m_Rate);
			      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
			    }
			  do_o->SetName (duObject->GetSrcNamePtr ());
			  do_o->SetPDUPayloadType (pdutype);
			  do_o->SetLifetime(m_3n_lifetime);
			  do_o->SetPayload (retPkt);

			  m_face->ReceiveDO(do_o);
			  m_transmittedDOs (do_o, this, m_face);

			  m_3n_seq.find(s_flowid)->second++;
			}
		    }
		}

	      // exception will be thrown if packet is not recognized
	  }
	  catch (icn::UnknownHeaderException)
	  {
	      NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
	  }
	}
    }

    void
    NNNICNPushProducer::SetEdgePush (bool value)
    {
      NS_LOG_FUNCTION (this);

      if (value)
	{
	  NS_LOG_INFO ("Setting Producer to push to immediate edge");
	  m_edge_push = true;
	  m_centroid_push = false;
	  m_anchor_push = false;
	}
    }

    bool
    NNNICNPushProducer::IsEdgePush () const
    {
      return m_edge_push;
    }

    void
    NNNICNPushProducer::SetCentroidPush (bool value)
    {
      NS_LOG_FUNCTION (this);
      if (value)
	{
	  NS_LOG_INFO ("Setting Producer to push to centroid");
	  m_edge_push = false;
	  m_centroid_push = true;
	  m_anchor_push = false;
	}
    }

    bool
    NNNICNPushProducer::IsCentroidPush () const
    {
      return m_centroid_push;
    }

    void
    NNNICNPushProducer::SetAnchorPush (bool value)
    {
      NS_LOG_FUNCTION (this);
      if (value)
	{
	  NS_LOG_INFO ("Setting Producer to push to anchor list");
	  m_edge_push = false;
	  m_centroid_push = false;
	  m_anchor_push = true;
	}
    }

    bool
    NNNICNPushProducer::IsAnchorPush () const
    {
      return m_anchor_push;
    }

    NNNAddress
    NNNICNPushProducer::GetAnchor () const
    {
      std::set<Ptr<const NNNAddress>, Ptr3NComp>::iterator it = m_push_destinations.begin ();

      if (it != m_push_destinations.end ())
	{
	  return (*it)->getName();
	}
      else
	{
	  return NNNAddress ();
	}
    }

    void
    NNNICNPushProducer::AddAnchor (NNNAddress name)
    {
      NS_LOG_FUNCTION (this);

      if (name.isEmpty())
	{
	  NS_LOG_WARN ("Attempted to add an empty 3N name");
	  return;
	}

      if (!m_anchor_push)
	{
	  NS_LOG_WARN ("Adding " << name << " to push destinations while not using Push Anchor method. Check configuration!");
	}

      NS_LOG_INFO ("Will Anchor to " << name);
      m_push_destinations.insert(Create <const NNNAddress> (name));
    }

    void
    NNNICNPushProducer::GotName (Ptr <const NNNAddress> name)
    {
      NS_LOG_FUNCTION (this);
      // We know that the underlying ForwardingStrategy has a name

      // Flag the change
      m_has3Nname = true;

      // Update the 3N name used by the application to create PDUs
      m_current3Nname = name;

      NS_LOG_INFO ("App will now use 3N name (" << *m_current3Nname << ")");

      // Because the name is so important, we must recalculate the
      // push destination
      RecalculatePushDestination();
    }

    void
    NNNICNPushProducer::NoName ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      m_has3Nname = false;

      // Because the name is so important, we must recalculate the
      // push destination
      RecalculatePushDestination();
    }

    void
    NNNICNPushProducer::StartApplication ()
    {
      NS_LOG_FUNCTION_NOARGS ();
      NS_ASSERT (GetNode ()->GetObject<Fib> () != 0);

      ICNProducerApp::StartApplication ();

      m_fw->TraceConnectWithoutContext("FlowidElimination", MakeCallback (&App::FlowidEliminated, this));

      NS_LOG_DEBUG ("NodeID: " << GetNode ()->GetId ());

      Ptr<Fib> fib = GetNode ()->GetObject<Fib> ();

      Ptr<fib::Entry> fibEntry = fib->Add (m_prefix, m_face, 0);

      fibEntry->UpdateStatus (m_face, fib::FaceMetric::ICN_FIB_GREEN);

      // 2 times reciprocal of frequency
      m_Window = 2 * m_frequency;
      // Reciprocal of frequency times payload divided by 1024 -> kilobits per second
      m_Rate = ceil(m_frequency * ((m_virtualPayloadSize * 8) / 1000.0));

      NS_LOG_DEBUG ("Frequency: " << m_frequency << " Window: " << m_Window << " Rate: " << m_Rate);
      NS_LOG_INFO ("Finished setting up NNNICNPushProducer application");

      if (m_useMapMe)
	{
	  NS_LOG_INFO ("Initializing MapMe");
	  SendMapMeInterest ();
	}

      if (m_directInterest)
	{
	  NS_LOG_LOGIC ("Will respond directly to Interests");
	}
      else
	{
	  NS_LOG_LOGIC ("Will not respond directly to any Interests");
	}

      NS_LOG_INFO ("Setting up first packet to send");
      ScheduleNextPacket ();
    }

    void
    NNNICNPushProducer::StopApplication ()
    {
      NS_LOG_FUNCTION_NOARGS ();
      NS_ASSERT (GetNode ()->GetObject<Fib> () != 0);

      ICNProducerApp::StopApplication ();
    }

    void
    NNNICNPushProducer::ScheduleNextPacket ()
    {
      if (m_firstTime)
	{
	  m_sendEvent = Simulator::Schedule (Seconds (0.0),
					     &NNNICNPushProducer::SendPacket, this);
	  m_firstTime = false;
	}
      else if (!m_sendEvent.IsRunning ())
	{
	  double sched;
	  if (useUni)
	    {
	      sched = uniRandom->GetValue();
	    }
	  else
	    {
	      sched = expRandom->GetValue ();
	    }

	  m_sendEvent = Simulator::Schedule (
	      (m_randomType == "none") ? Seconds (1.0 / m_frequency) : Seconds (sched),
		  &NNNICNPushProducer::SendPacket, this);
	}
    }

    void
    NNNICNPushProducer::SetRandomizationType (const std::string &value)
    {
      NS_LOG_FUNCTION (this << value);

      if (value == "uniform")
	{
	  uniRandom->SetAttribute ("Min", DoubleValue (0.0));
	  uniRandom->SetAttribute ("Max", DoubleValue (2 * 1.0 / m_frequency));
	  useUni = true;
	}
      else if (value == "exponential")
	{
	  expRandom->SetAttribute ("Mean", DoubleValue (1.0 / m_frequency));
	  expRandom->SetAttribute ("Bound", DoubleValue (50 * 1.0 / m_frequency));
	  useUni = false;
	}

      m_randomType = value;
    }

    std::string
    NNNICNPushProducer::GetRandomizationType () const
    {
      NS_LOG_FUNCTION_NOARGS ();
      return m_randomType;
    }

    void
    NNNICNPushProducer::RecalculatePushDestination ()
    {
      NS_LOG_FUNCTION_NOARGS ();

      if (m_edge_push)
	{
	  NS_LOG_INFO ("Edge push destination recalculation: Unrequired");
	}
      else if (m_centroid_push)
	{
	  NS_LOG_INFO ("Centroid push destination recalculation");

	  if (!m_current3Nname)
	    {
	      NS_LOG_WARN ("We have no 3N name, cannot send anything!");
	      return;
	    }

	  NNNAddress closestSector = m_current3Nname->getSectorName ();

	  if (closestSector.isEmpty())
	    {
	      NS_LOG_WARN ("We have no 3N name, cannot send anything!");
	      return;
	    }

	  names_seen_by_name& names_index = m_possibleRequesters.get<i_name> ();
	  names_seen_by_name::iterator it = names_index.begin ();

	  for (; it != names_index.end (); ++it)
	    {
	      NS_LOG_INFO ("Obtained a request from " << *(it->name));
	      closestSector = closestSector.getClosestSector (*(it->name));
	      NS_LOG_INFO ("Closest sector is currently " << closestSector);
	    }

	  const NNNAddress tmp = closestSector.getName ();
	  NS_LOG_INFO ("Current closest sector is: " << tmp);
	  Ptr<const NNNAddress> insertionAddr = Create <const NNNAddress> (tmp);
	  std::set<Ptr<const NNNAddress>, Ptr3NComp>::iterator it2;

	  if (m_push_destinations.empty())
	    {
	      // If the closest sector has not been introduced
	      NS_LOG_INFO ("Making " << closestSector << " the new centroid");
	      m_push_destinations.insert(insertionAddr);
	    }
	  else
	    {
	      // Check that the 3N name we found isn't the parent of something we already
	      // have
	      NS_LOG_INFO ("Verifying " << tmp << " is not a parent of something we already have");
	      it2 = m_push_destinations.begin ();
	      for (; it2 != m_push_destinations.end (); )
		{
		  if (closestSector.isParentSectorSet(*it2))
		    {
		      NS_LOG_INFO ("Replacing " << **it2 << " with " << closestSector);
		      it2 = m_push_destinations.erase(it2);
		    }
		  else
		    ++it2;
		}

	      // Check that the 3N name we found isn't the subsector of something we
	      // already have
	      bool isSubsector = false;
	      it2 = m_push_destinations.begin ();
	      NS_LOG_INFO ("Verifying " << tmp << " is not a subsector of something we already have");
	      for (; it2 != m_push_destinations.end (); )
		{
		  if (closestSector.isSubSector(**it2))
		    {
		      // Break if we found at least one address which for which our
		      // candidate is a subsector
		      isSubsector = true;
		      break;
		    }
		  else
		    ++it2;
		}

	      if (!isSubsector)
		{
		  NS_LOG_INFO ("Candidate " << closestSector << " is completely new, adding");
		  m_push_destinations.insert(insertionAddr);
		}
	      else
		{
		  NS_LOG_INFO ("Candidate " << closestSector << " is already aggregated. Skipping");
		}
	    }

	  NS_LOG_DEBUG ("---- Current destinations ----");
	  it2 = m_push_destinations.begin ();
	  for (; it2 != m_push_destinations.end (); ++it2)
	    {
	      NS_LOG_DEBUG ("" << **it2);
	    }
	  NS_LOG_DEBUG ("------------------------------");
	}
      else if (m_anchor_push)
	{
	  NS_LOG_INFO ("Anchor push destination recalculation: Unrequired");
	}
      else
	{
	  NS_LOG_ERROR ("No Push method active - check for errors");
	}
    }
  } /* namespace nnn */
} /* namespace ns3 */
