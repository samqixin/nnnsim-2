/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-consumer.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-consumer.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-consumer.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/callback.h"
#include "ns3/double.h"
#include "ns3/integer.h"
#include "ns3/log.h"
#include "ns3/names.h"
#include "ns3/packet.h"
#include "ns3/ptr.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"

#include <boost/ref.hpp>

#include "nnn-icn-consumer.h"
#include "ns3/nnn-pdus.h"
#include "ns3/nnn-icn-app-face.h"
#include "ns3/nnn-forwarding-strategy.h"
#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-rtt-estimator.h"
#include "ns3/nnn-fw-hop-count-tag.h"
#include "ns3/nnn-rtt-mean-deviation.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.ICNConsumer");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (Consumer);

    TypeId
    Consumer::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::ICNConsumer")
	  .SetGroupName ("Nnn")
	  .SetParent<ICNApp> ()
	  .AddAttribute ("StartSeq", "Initial sequence number",
			 IntegerValue (0),
			 MakeIntegerAccessor(&Consumer::m_seq),
			 MakeIntegerChecker<int32_t>())
	  .AddAttribute ("Prefix","Name of the Interest",
			 StringValue ("/"),
			 MakeNameAccessor (&Consumer::m_interestName),
			 icn::MakeNameChecker ())
	  .AddAttribute ("LifeTime", "LifeTime for interest packet",
			 StringValue ("2s"),
			 MakeTimeAccessor (&Consumer::m_interestLifeTime),
			 MakeTimeChecker ())
	  .AddAttribute ("RetxTimer",
			 "Timeout defining how frequent retransmission timeouts should be checked",
			 StringValue ("50ms"),
			 MakeTimeAccessor (&Consumer::GetRetxTimer, &Consumer::SetRetxTimer),
			 MakeTimeChecker ())
	  .AddAttribute ("Anycast",
			 "Whether to use an any cast approach with 3N for the Consumer",
			 BooleanValue (false),
			 MakeBooleanAccessor(&Consumer::m_anycast),
			 MakeBooleanChecker ())
	  .AddAttribute ("UseDestinations",
			 "Whether to use PDUs with destinations for the Consumer",
			 BooleanValue (true),
			 MakeBooleanAccessor(&Consumer::m_useDestination),
			 MakeBooleanChecker ())
	  .AddAttribute ("NameThreshold",
			 "Maximum time to consider a 3N name as a valid anycast destination",
			 StringValue("20s"),
			 MakeTimeAccessor(&Consumer::m_3n_threshold),
			 MakeTimeChecker ())
	  .AddAttribute ("RetransmitRetries",
			 "Number of attempts to retransmit an Interest PDU",
			 IntegerValue (4),
			 MakeIntegerAccessor(&Consumer::m_ICN_retransmission_attempts),
			 MakeIntegerChecker<uint32_t> ())
	  .AddTraceSource ("LastRetransmittedInterestDataDelay", "Delay between last retransmitted Interest and received Data",
			 MakeTraceSourceAccessor (&Consumer::m_lastRetransmittedInterestDataDelay),
			 "ns3::nnn::ICNConsumer::LastRetransmittedInterestDataDelayTracedCallback")
	  .AddTraceSource ("FirstInterestDataDelay", "Delay between first transmitted Interest and received Data",
			 MakeTraceSourceAccessor (&Consumer::m_firstInterestDataDelay),
			 "ns3::nnn::ICNConsumer::FirstInterestDataDelayTracedCallback")
	  .AddTraceSource ("InterestLoss", "When application gives up retransmitting Interest",
			   MakeTraceSourceAccessor (&Consumer::m_InterestLoss),
			   "ns3::nnn::ICNConsumer:LossInterestTracedCallback");
	;
      return tid;
    }

    Consumer::Consumer ()
    : m_rand   (CreateObject<UniformRandomVariable> ())
    , m_seq    (0)
    , m_seqMax (0) // don't request anything
    , m_anycast (false)
    , m_ICN_retransmission_attempts (std::numeric_limits<uint32_t>::max ())
    , m_infinite_retransmits (true)
    , m_useDestination (false)
    {
      NS_LOG_FUNCTION_NOARGS ();

      m_rtt = CreateObject<RttMeanDeviation> ();

      m_rand->SetAttribute ("Min", DoubleValue (0));
      m_rand->SetAttribute ("Max", DoubleValue (std::numeric_limits<uint32_t>::max ()));
    }

    void
    Consumer::SetRetxTimer (Time retxTimer)
    {
      m_retxTimer = retxTimer;
      if (m_retxEvent.IsRunning ())
	{
	  // m_retxEvent.Cancel (); // cancel any scheduled cleanup events
	  Simulator::Remove (m_retxEvent); // slower, but better for memory
	}

      // schedule even with new timeout
      m_retxEvent = Simulator::Schedule (m_retxTimer,
					 &Consumer::CheckRetxTimeout, this);
    }

    Time
    Consumer::GetRetxTimer () const
    {
      return m_retxTimer;
    }

    void
    Consumer::CheckRetxTimeout ()
    {
      Time now = Simulator::Now ();

      Time rto = m_rtt->RetransmitTimeout ();
      // NS_LOG_DEBUG ("Current RTO: " << rto.ToDouble (Time::S) << "s");

      while (!m_seqTimeouts.empty ())
	{
	  SeqTimeoutsContainer::index<i_timestamp>::type::iterator entry =
	      m_seqTimeouts.get<i_timestamp> ().begin ();
	  if (entry->time + rto <= now) // timeout expired?
	    {
	      uint32_t seqNo = entry->seq;
	      NS_LOG_DEBUG ("SeqNo " << seqNo << " has timed out, Entry: " << entry->time << " RTO: " << rto.ToDouble(Time::S) << "s");
	      m_seqTimeouts.get<i_timestamp> ().erase (entry);
	      OnTimeout (seqNo);
	    }
	  else
	    break; // nothing else to do. All later packets need not be retransmitted
	}

      m_retxEvent = Simulator::Schedule (m_retxTimer,
					 &Consumer::CheckRetxTimeout, this);
    }

    // Application Methods
    void
    Consumer::StartApplication () // Called at time specified by Start
    {
      NS_LOG_FUNCTION_NOARGS ();

      // do base stuff
      ICNApp::StartApplication ();

      m_fw->TraceConnectWithoutContext("FlowidElimination", MakeCallback (&App::FlowidEliminated, this));

      if (m_ICN_retransmission_attempts == std::numeric_limits<uint32_t>::max ())
	{
	  m_infinite_retransmits = true;
	  NS_LOG_LOGIC ("No limit to retransmissions");
	}
      else
	{
	  m_infinite_retransmits = false;
	  NS_LOG_LOGIC ("Maximum number of retransmissions possible: " << m_ICN_retransmission_attempts);
	}
      ScheduleNextPacket ();
    }

    void
    Consumer::StopApplication () // Called at time specified by Stop
    {
      NS_LOG_FUNCTION_NOARGS ();

      // cancel periodic packet generation
      Simulator::Cancel (m_sendEvent);

      // cleanup base stuff
      ICNApp::StopApplication ();
    }

    void
    Consumer::SendPacket ()
    {
      if (!m_active) return;

      NS_LOG_FUNCTION_NOARGS ();

      uint32_t seq=std::numeric_limits<uint32_t>::max (); //invalid
      bool retransmission = false;

      while (m_retxSeqs.size ())
	{
	  seq = *m_retxSeqs.begin ();
	  m_retxSeqs.erase (m_retxSeqs.begin ());
	  retransmission = true;
	  break;
	}

      if (seq == std::numeric_limits<uint32_t>::max ())
	{
	  if (m_seqMax != std::numeric_limits<uint32_t>::max ())
	    {
	      if (m_seq >= m_seqMax)
		{
		  return; // we are totally done
		}
	    }

	  seq = m_seq++;
	}

      if (retransmission)
	NS_LOG_WARN ("> Attempting to retransmit Interest SeqNo " << std::dec << seq);

      if (!CanSendOutInterest(seq))
	{
	  // Call again until an actual Interest can be sent
	  SendPacket ();
	  return;
	}

      WillSendOutInterest (seq);

      Ptr<icn::Name> nameWithSequence = Create<icn::Name> (m_interestName);
      nameWithSequence->appendSeqNum (seq);

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (m_rand->GetValue ());
      interest->SetName                (nameWithSequence);
      interest->SetInterestLifetime    (m_interestLifeTime);

      FwHopCountTag hopCountTag;
      interest->GetPayload ()->AddPacketTag (hopCountTag);

      m_transmittedInterests (interest, this, m_face);

      // Encode the packet
      Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);

      uint32_t flowid = 0;
      uint32_t seq_3n = 0;
      bool first = true;

      std::tie(flowid, seq_3n, first) = FindFlowidSeq (0, 0);

      bool useDO = false;
      bool useNULL = false;
      bool useSO = false;
      bool useDU = false;

      if (m_useDestination)
	{
	  if (!m_possibleDestination || m_possibleDestination->isEmpty ())
	    {
	      if (m_has3Nname)
		{
		  useSO = true;
		}
	      else
		{
		  useNULL = true;
		}
	    }
	  else
	    {
	      if (m_has3Nname)
		{
		  useDU = true;
		}
	      else
		{
		  useDO = true;
		}
	    }
	}
      else
	{
	  if (m_has3Nname)
	    {
	      useSO = true;
	    }
	  else
	    {
	      useNULL = true;
	    }
	}

      if (useNULL)
	{
	  NS_LOG_INFO ("> Interest SeqNo " << std::dec << seq << " using NULLp Flowid: " << flowid << " Sequence: " << seq_3n << " PDU");
	  Ptr<NULLp> nullp_o = Create<NULLp> ();
	  nullp_o->SetFlowid (flowid);
	  nullp_o->SetSequence (seq_3n);
	  if (first)
	    {
	      nullp_o->SetPDURF(true);
	      nullp_o->SetPDUWindow(m_Window);
	      nullp_o->SetRate(m_Rate);
	      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
	    }
	  nullp_o->SetPDUPayloadType (ICN_NNN);
	  nullp_o->SetLifetime(m_3n_lifetime);
	  nullp_o->SetPayload (retPkt);
	  m_face->ReceiveNULLp(nullp_o);
	  m_transmittedNULLps (nullp_o, this, m_face);

	  m_3n_seq.find(flowid)->second++;
	}

      if (useSO)
	{
	  // It is flagged. Use the 3N name
	  NS_LOG_INFO ("> Interest SeqNo " << std::dec << seq << " using SO Flowid: " << flowid << " Sequence: " << seq_3n << " PDU src: " << *m_current3Nname);
	  Ptr<SO> so_o = Create<SO> ();
	  so_o->SetFlowid (flowid);
	  so_o->SetSequence (seq_3n);
	  if (first)
	    {
	      so_o->SetPDURF(true);
	      so_o->SetPDUWindow(m_Window);
	      so_o->SetRate(m_Rate);
	      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
	    }
	  so_o->SetPDUPayloadType(ICN_NNN);
	  so_o->SetLifetime(m_3n_lifetime);
	  so_o->SetName(m_current3Nname);
	  so_o->SetPayload(retPkt);
	  m_face->ReceiveSO(so_o);
	  m_transmittedSOs (so_o, this, m_face);

	  m_3n_seq.find(flowid)->second++;
	}

      if (useDO)
	{
	  // We have a possible destination, create a DO PDU
	  NS_LOG_INFO ("> Interest SeqNo " << std::dec << seq << " using DO Flowid: " << flowid << " Sequence: " << seq_3n << " PDU dst: " << *m_possibleDestination);
	  Ptr<DO> do_o = Create<DO> ();
	  do_o->SetFlowid (flowid);
	  do_o->SetSequence (seq_3n);
	  if (first)
	    {
	      do_o->SetPDURF(true);
	      do_o->SetPDUWindow(m_Window);
	      do_o->SetRate(m_Rate);
	      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
	    }
	  do_o->SetPDUPayloadType (ICN_NNN);
	  do_o->SetLifetime(m_3n_lifetime);
	  do_o->SetName(m_possibleDestination);
	  do_o->SetPayload (retPkt);
	  m_face->ReceiveDO(do_o);
	  m_transmittedDOs (do_o, this, m_face);

	  m_3n_seq.find(flowid)->second++;
	}

      if (useDU)
	{
	  NS_LOG_INFO ("> Interest SeqNo " << std::dec << seq << " using DU Flowid: " << flowid << " Sequence: " << seq_3n << " PDU src: " << *m_current3Nname << " dst: " << *m_possibleDestination);
	  Ptr<DU> du_o = Create<DU> ();
	  du_o->SetFlowid (flowid);
	  du_o->SetSequence (seq_3n);
	  if (first)
	    {
	      du_o->SetPDURF(true);
	      du_o->SetPDUWindow(m_Window);
	      du_o->SetRate(m_Rate);
	      NS_LOG_INFO ("RF with Window: " << m_Window << " Rate: " << m_Rate << " kbit/s");
	    }
	  du_o->SetPDUPayloadType (ICN_NNN);
	  du_o ->SetLifetime(m_3n_lifetime);
	  du_o->SetSrcName (m_current3Nname);
	  du_o->SetDstName (m_possibleDestination);
	  du_o->SetPayload (retPkt);
	  m_face->ReceiveDU (du_o);
	  m_transmittedDUs (du_o, this, m_face);

	  m_3n_seq.find(flowid)->second++;
	}

      ScheduleNextPacket ();
    }

    ///////////////////////////////////////////////////
    //          Process incoming packets             //
    ///////////////////////////////////////////////////

    void
    Consumer::Deencapsulate3N(Ptr<Packet> packet)
    {
//      NS_LOG_FUNCTION (this << packet);

      try
      {
	  icn::HeaderHelper::Type type = icn::HeaderHelper::GetICNHeaderType (packet);
	  Ptr<Data> data = 0;
	  Ptr<Interest> interest = 0;
	  switch (type)
	  {
	    case icn::HeaderHelper::CONTENT_OBJECT_ICN:
	      data = icn::Wire::ToData (packet, icn::Wire::WIRE_FORMAT_NDNSIM);
	      break;
	    case icn::HeaderHelper::INTEREST_ICN:
	      interest = icn::Wire::ToInterest (packet, icn::Wire::WIRE_FORMAT_NDNSIM);
	      break;
	  }

	  if (data != 0)
	    OnData(data);

	  if (interest != 0)
	    OnInterestType(interest);

	  // exception will be thrown if packet is not recognized
      }
      catch (icn::UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown ICN header. Should not happen");
      }
    }

    void
    Consumer::OnData (Ptr<Data> data)
    {
      if (!m_active) return;

      ICNApp::OnData (data); // tracing inside

      NS_LOG_FUNCTION (this << data);

      // NS_LOG_INFO ("Received content object: " << boost::cref(*data));

      uint32_t seq = data->GetName ().get (-1).toSeqNum ();
      NS_LOG_INFO ("< DATA for SeqNo " << std::dec << seq);

      int hopCount = -1;
      FwHopCountTag hopCountTag;
      if (data->GetPayload ()->PeekPacketTag (hopCountTag))
	{
	  hopCount = hopCountTag.Get ();
	}

      SeqTimeoutsContainer::iterator entry = m_seqLastDelay.find (seq);
      if (entry != m_seqLastDelay.end ())
	{
	  m_lastRetransmittedInterestDataDelay (this, seq, Simulator::Now () - entry->time, hopCount);
	}

      entry = m_seqFullDelay.find (seq);
      if (entry != m_seqFullDelay.end ())
	{
	  Time delay = Simulator::Now () - entry->time;
	  NS_LOG_INFO ("< DATA for SeqNo " << seq << " full delay: " << delay.GetSeconds());
	  m_firstInterestDataDelay (this, seq, delay, m_seqRetxCounts[seq], hopCount);
	}
      else
	{
	  NS_LOG_DEBUG ("< DATA for SeqNo " << seq << " we have probably already satisfied this request");
	}

      m_seqRetxCounts.erase (seq);
      m_seqFullDelay.erase (seq);
      m_seqLastDelay.erase (seq);

      m_seqTimeouts.erase (seq);
      m_retxSeqs.erase (seq);

      m_rtt->AckSeq (SequenceNumber32 (seq));
    }

    void
    Consumer::OnNULLp (Ptr<NULLp> nullpObject)
    {
      if (!m_active) return;

      App::OnNULLp(nullpObject);

      NS_LOG_FUNCTION_NOARGS();

      Ptr<Packet> packet = nullpObject->GetPayload ()->Copy ();
      uint16_t pdutype = nullpObject->GetPDUPayloadType ();

//      NS_LOG_INFO (this << " obtained pdu type " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  Deencapsulate3N(packet);
	}
    }

    void
    Consumer::OnSO (Ptr<SO> soObject)
    {
      if (!m_active) return;

      App::OnSO(soObject);

      NS_LOG_FUNCTION (this << soObject);

      Ptr<Packet> packet = soObject->GetPayload ()->Copy ();
      uint16_t pdutype = soObject->GetPDUPayloadType ();

      NS_LOG_INFO (this << " obtained pdu type " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  // Get the current time
	  Time now = Simulator::Now ();
	  // Verify that the destination list doesn't have this name
	  Ptr<const NNNAddress> received = soObject->GetNamePtr ();

	  NS_LOG_DEBUG ("Searching in seen 3N names for " << *received);

	  names_seen_by_name& names_index = m_possible_authoritative_Destinations.get<i_name> ();
	  names_seen_by_name::iterator it = names_index.find (received);

	  if (it != names_index.end ())
	    {
	      NS_LOG_DEBUG ("Found entry for 3N name: " << *received);
	      NS_LOG_DEBUG ("Updating " << *received << " last seen to: " << now);

	      NamesSeen tmp = *it;

	      tmp.lastSeen = now;
	      names_index.replace(it, tmp);

	      if (*m_possibleDestination != *received)
		{
		  NS_LOG_DEBUG ("Current destination is different, switching to " << *received);
		  m_possibleDestinationTime = now;
		  m_possibleDestination = received;
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("First time seeing " << *received << ", adding to list, updating destination");
	      m_possibleDestinationTime = now;
	      m_possibleDestination = received;

	      m_possible_authoritative_Destinations.insert(NamesSeen (received));
	    }

	  Deencapsulate3N(packet);
	}
    }

    void
    Consumer::OnDO (Ptr<DO> doObject)
    {
      if (!m_active) return;

      App::OnDO(doObject);

      NS_LOG_FUNCTION (this << doObject);

      Ptr<Packet> packet = doObject->GetPayload ()->Copy ();
      uint16_t pdutype = doObject->GetPDUPayloadType ();

      NS_LOG_INFO (this << " obtained pdu type " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  Deencapsulate3N(packet);
	}
    }

    void
    Consumer::OnDU (Ptr<DU> duObject)
    {
      if (!m_active) return;

      App::OnDU(duObject);

      NS_LOG_FUNCTION (this << duObject);

      Ptr<Packet> packet = duObject->GetPayload ()->Copy ();
      uint16_t pdutype = duObject->GetPDUPayloadType ();
      bool authoritative = duObject->IsAuthoritative ();

      NS_LOG_INFO ("Obtained " << std::boolalpha << authoritative << " authoritative pdu type: " << pdutype);

      if (pdutype == ICN_NNN)
	{
	  // Get the current time
	  Time now = Simulator::Now ();
	  // Verify that the destination list doesn't have this name
	  Ptr<const NNNAddress> received = duObject->GetSrcNamePtr ();

	  NS_LOG_DEBUG ("Searching in seen 3N names for " << *received);

	  if (authoritative)
	    {
	      names_seen_by_name& names_index = m_possible_authoritative_Destinations.get<i_name> ();
	      names_seen_by_name::iterator it = names_index.find (received);

	      if (it != names_index.end ())
		{
		  NS_LOG_DEBUG ("Found entry for authoritative 3N name: " << *received);
		  NS_LOG_DEBUG ("Updating " << *received << " last seen to: " << now);

		  NamesSeen tmp = *it;

		  tmp.lastSeen = now;
		  names_index.replace(it, tmp);

		  if (*m_possibleDestination != *received)
		    {
		      NS_LOG_DEBUG ("Current destination is different, switching to " << *received);
		      m_possibleDestinationTime = now;
		      m_possibleDestination = received;
		    }
		}
	      else
		{
		  NS_LOG_DEBUG ("First time seeing " << *received << ", adding to list, updating destination");
		  m_possible_authoritative_Destinations.insert(NamesSeen (received));

		  m_possibleDestinationTime = now;
		  m_possibleDestination = received;
		}

	      m_authoritativeDestination = true;
	    }
	  else
	    {
	      Time threshold = m_possibleDestinationTime + m_possibleDestinationLag;
	      names_seen_by_name& names_index = m_possible_nonauthoritative_Destinations.get<i_name> ();
	      names_seen_by_name::iterator it = names_index.find (received);

	      if (it != names_index.end ())
		{
		  NS_LOG_DEBUG ("Found entry for non-authoritative 3N name: " << *received);
		  NS_LOG_DEBUG ("Updating " << *received << " last seen to: " << now);

		  NamesSeen tmp = *it;

		  tmp.lastSeen = now;
		  names_index.replace(it, tmp);
		}
	      else
		{
		  m_possible_nonauthoritative_Destinations.insert(NamesSeen (received));
		}

	      if (m_anycast)
		{
		  if (m_has3Nname)
		    {
		      if (!m_possible_nonauthoritative_Destinations.empty ())
			{
			  names_seen_by_uname& index = m_possible_nonauthoritative_Destinations.get<i_uname> ();

			  std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp> possible_list;
			  std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp> ret_list;
			  names_seen_by_uname::iterator it2 = index.begin ();
			  while (it2 != index.end ())
			    {
			      if ((Simulator::Now () - it2->lastSeen) < m_3n_threshold)
				{
				  possible_list.insert (it2->name);
				}
			      ++it2;
			    }

			  if (!possible_list.empty())
			    {
			      ret_list = NNNAddress::Filter3NLeafNames (possible_list);

//			      int distance = std::numeric_limits<int>::max ();
//			      int tmpdist;
			      NNNAddress anycast_name = m_current3Nname->getSectorName();

			      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>::iterator it6 = ret_list.begin ();
			      while (it6 != ret_list.end ())
				{
				  anycast_name = anycast_name.getClosestSector (**it6);
				  ++it6;
				}

			      bool changeName = false;
			      if (!m_possibleDestination)
				{
				  changeName = true;
				}
			      else
				{
				  if (*m_possibleDestination != anycast_name)
				    {
				      changeName = true;
				    }
				}

			      if (changeName)
				{
				  NS_LOG_DEBUG ("Changing anycast position to " << anycast_name);
				  m_possibleDestinationTime = now;
				  m_possibleDestination = Create <const NNNAddress> (anycast_name);
				}
			    }
			  else
			    {
			      NS_LOG_WARN ("Have no newly seen names. Check transmissions!");
			    }
			}
		      else
			{
			  NS_LOG_WARN ("Cannot do anything with no non-authoritative destinations in the list");
			}
		    }
		  else
		    {
		      NS_LOG_WARN ("No not have a 3N name, cannot calculate anycast destination!");
		    }
		}
	      else
		{
		  if (threshold < now && !m_authoritativeDestination)
		    {
		      NS_LOG_DEBUG("Threshold " << threshold << " vs " << now << ", updating non-authorative destination to " << *received);

		      m_possibleDestinationTime = now;
		      m_possibleDestination = received;
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Threshold not surpassed: " << threshold << " vs " << now << " maintaining: " << *m_possibleDestination);
		    }
		}
	    }

	  Deencapsulate3N(packet);
	}
    }

    void
    Consumer::OnInterestType (Ptr<Interest> interest)
    {
      if (!m_active) return;

      ICNApp::OnInterestType (interest); // tracing inside

      // NS_LOG_DEBUG ("Nack type: " << interest->GetNack ());

      // NS_LOG_FUNCTION (interest->GetName ());

      // NS_LOG_INFO ("Received NACK: " << boost::cref(*interest));
      uint32_t seq = interest->GetName ().get (-1).toSeqNum ();
      NS_LOG_INFO ("< NACK for " << std::dec << seq);
      // std::cout << Simulator::Now ().ToDouble (Time::S) << "s -> " << "NACK for " << seq << "\n";

      // put in the queue of interests to be retransmitted
      // NS_LOG_INFO ("Before: " << m_retxSeqs.size ());
      m_retxSeqs.insert (seq);
      // NS_LOG_INFO ("After: " << m_retxSeqs.size ());

      m_seqTimeouts.erase (seq);

      m_rtt->IncreaseMultiplier ();             // Double the next RTO ??
      ScheduleNextPacket ();
    }

    void
    Consumer::OnTimeout (uint32_t sequenceNumber)
    {
      NS_LOG_FUNCTION (std::dec << sequenceNumber);
      // std::cout << Simulator::Now () << ", TO: " << sequenceNumber << ", current RTO: " << m_rtt->RetransmitTimeout ().ToDouble (Time::S) << "s\n";

      m_rtt->IncreaseMultiplier ();             // Double the next RTO
      m_rtt->SentSeq (SequenceNumber32 (sequenceNumber), 1); // make sure to disable RTT calculation for this sample
      m_retxSeqs.insert (sequenceNumber);
      ScheduleNextPacket ();
    }

    void
    Consumer::WillSendOutInterest (uint32_t sequenceNumber)
    {
      NS_LOG_DEBUG ("Adding SeqNo " << std::dec << sequenceNumber << " to backlog queue. Transmission count: " <<  (m_seqRetxCounts[sequenceNumber] +1));
      NS_LOG_DEBUG ("Backlog of " << m_seqTimeouts.size () << " items");

      m_seqTimeouts.insert (SeqTimeout (sequenceNumber, Simulator::Now ()));
      m_seqFullDelay.insert (SeqTimeout (sequenceNumber, Simulator::Now ()));

      m_seqLastDelay.erase (sequenceNumber);
      m_seqLastDelay.insert (SeqTimeout (sequenceNumber, Simulator::Now ()));

      m_seqRetxCounts[sequenceNumber]++;

      m_rtt->SentSeq (SequenceNumber32 (sequenceNumber), 1);
    }

    bool
    Consumer::CanSendOutInterest (uint32_t sequenceNumber)
    {
      if (m_infinite_retransmits)
	{
	  return true;
	}
      else
	{
	  if (m_seqRetxCounts[sequenceNumber] < m_ICN_retransmission_attempts)
	    {
	      return true;
	    }
	  else
	    {
	      NS_LOG_WARN ("Reached maximum retransmissions of " << m_ICN_retransmission_attempts <<
			   " for SeqNo " << sequenceNumber << " assuming loss");
	      m_InterestLoss (this, sequenceNumber, m_ICN_retransmission_attempts);
	      m_seqRetxCounts.erase (sequenceNumber);
	      m_seqFullDelay.erase (sequenceNumber);
	      m_seqLastDelay.erase (sequenceNumber);

	      m_seqTimeouts.erase (sequenceNumber);
	      m_retxSeqs.erase (sequenceNumber);
	      return false;
	    }
	}
    }


  } // namespace nnn
} // namespace ns3
