/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2014 Waseda University, Sato Laboratory
 *   Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 *   This file is part of nnnsim.
 *
 *  nnn-app-face.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-app-face.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-app-face.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *
 *   Original template made for ndnSIM for University of California,
 *   Los Angeles by Ilya Moiseenko
 */
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/packet.h"
#include "ns3/node.h"
#include "ns3/simulator.h"

#include "ns3/nnn-app.h"
#include "nnn-app-face.h"
#include "ns3/nnn-pdus.h"

#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-wire.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.AppFace");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (AppFace);

    TypeId
    AppFace::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::AppFace")
		.SetParent<Face> ()
		.SetGroupName ("Nnn")
		;
      return tid;
    }

    AppFace::AppFace (Ptr<App> app)
    : Face (app->GetNode ())
    , m_app (app)
    {
      NS_LOG_FUNCTION (this << app);

      NS_ASSERT (m_app != 0);
      SetFlags (Face::APPLICATION);
    }

    AppFace::AppFace (Ptr<Node> node)
    : Face (node)
    {
    }

    AppFace::~AppFace ()
    {
      NS_LOG_FUNCTION_NOARGS ();
    }

    AppFace::AppFace ()
    : Face (0)
    {
    }

    AppFace::AppFace (const AppFace &)
    : Face (0)
    {
    }

    AppFace&
    AppFace::operator= (const AppFace &)
    {
      return *((AppFace*)0);
    }

    bool
    AppFace::SendSO (Ptr<const SO> soObject)
    {
      NS_LOG_FUNCTION (this << soObject);

      if (!IsUp ())
	{
	  return false;
	}

      Ptr<SO> tmp = Create <SO> (*soObject);
      m_app->OnSO (tmp);
      return true;
    }

    bool
    AppFace::SendNULLp (Ptr<const NULLp> nullpObject)
    {
      NS_LOG_FUNCTION (this << nullpObject);

      if (!IsUp ())
	{
	  return false;
	}

      Ptr<NULLp> tmp = Create <NULLp> (*nullpObject);
      m_app->OnNULLp (tmp);
      return true;
    }

    bool
    AppFace::SendDO (Ptr<const DO> doObject)
    {
      NS_LOG_FUNCTION (this << doObject);

      if (!IsUp ())
	{
	  return false;
	}

      Ptr<DO> tmp = Create <DO> (*doObject);
      m_app->OnDO (tmp);
      return true;
    }

    bool
    AppFace::SendDU (Ptr<const DU> duObject)
    {
      NS_LOG_FUNCTION (this << duObject);

      if (!IsUp ())
	{
	  return false;
	}

      Ptr<DU> tmp = Create <DU> (*duObject);
      m_app->OnDU (tmp);
      return true;
    }

    std::ostream&
    AppFace::Print (std::ostream& os) const
    {
      os << "dev=local(" << GetId() << ")";
      return os;
    }

    bool
    AppFace::ConnectToApp (Ptr<Packet> packet)
    {
      NS_LOG_FUNCTION (this << packet);
      if (!IsApp3NEnabled())
	{
	  NS_LOG_DEBUG ("AppFace has no direct 3N support");
	  return false;
	}

      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::NULL_NNN:
	      return AppFace::SendNULLp (Wire::ToNULLp (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::SO_NNN:
	      return AppFace::SendSO (Wire::ToSO (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DO_NNN:
	      return AppFace::SendDO (Wire::ToDO (packet, Wire::WIRE_FORMAT_NNNSIM));
	    case nnn::DU_NNN:
	      return AppFace::SendDU (Wire::ToDU (packet, Wire::WIRE_FORMAT_NNNSIM));
	    default:
	      NS_FATAL_ERROR ("Not supported 3N header");
	      return false;
	  }
	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_LOG_DEBUG ("Unknown 3N header");
	  NS_FATAL_ERROR ("Unknown PDU type. Should not happen!");
	  return false;
      }
    }
  } // namespace nnn
} // namespace ns3
