/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-app-face.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-app-face.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-app-face.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_ICN_APP_FACE_H
#define NNN_ICN_APP_FACE_H

#include "ns3/traced-callback.h"

#include "ns3/nnn-app-face.h"

#include "ns3/nnn-icn-name.h"
#include "ns3/nnn-icn-pdus.h"
#include "ns3/nnn-icn-wire.h"
#include "ns3/wire-nnnsim-icn-naming.h"

namespace ns3
{
  class Packet;

  namespace nnn
  {
    class Interest;
    class Data;

    class ICN3NForwardingStrategy;

    class ICNApp;
    class PureICNApp;

    /**
     * \ingroup nnn-face
     * \brief Implementation of 3N ICN capable application Face
     *
     * This class defines basic functionality of Face. Face is core
     * component responsible for actual delivery of data packet to and
     * from 3N stack
     *
     * \see AppFace, NetDeviceFace
     */
    class ICNAppFace : public AppFace
    {
    public:
      static TypeId
      GetTypeId ();

      /**
       * \brief Default constructor
       */
      ICNAppFace (Ptr<ICNApp> app);
      virtual ~ICNAppFace();

      ICNAppFace (Ptr<PureICNApp> app);

      ////////////////////////////////////////////////////////////////////
      // methods overloaded from Face

      // We need to keep a registry of the FIB
      virtual void
      RegisterProtocolHandlers (Ptr<ForwardingStrategy> fw);

      virtual void
      UnRegisterProtocolHandlers ();

      // Methods to be used by the ForwardingStrategy (mostly)
      virtual bool
      SendInterest (Ptr<const Interest> interest);

      virtual bool
      SendData (Ptr<const Data> data);

      virtual bool
      SendNULLp (Ptr<const NULLp> nullpObject);

      virtual bool
      SendSO (Ptr<const SO> soObject);

      virtual bool
      SendDO (Ptr<const DO> doObject);

      virtual bool
      SendDU (Ptr<const DU> duObject);

      // Functions only to be used by Apps
      // Usually for ICN Data PDUs as there is no "destination"
      virtual bool
      SendIn3NPDU (Ptr<Packet> pkt);

      // Usually for ICN Interest PDUs as there is a concrete destination
      virtual bool
      SendIn3NPDU (Ptr<const icn::Name> name, Ptr<Packet> pkt);

      virtual std::ostream&
      Print (std::ostream &os) const;

      // Methods to connect callbacks
      virtual void
      SetAppMobile (bool mobile);

      virtual bool
      IsAppMobile () const;

      virtual void
      SetNodeMobile (bool mobile);

      virtual bool
      IsNodeMobile () const;

      virtual void
      SetCurrSrc (Ptr<const NNNAddress> addr);

      virtual Ptr<const NNNAddress>
      GetCurrSrc () const;

      virtual void
      SetPDULifetime (Time lifetime);

      virtual Time
      GetPDULifetime () const;

      virtual void
      SetNo3NName ();

      virtual void
      SetCurr3NName (Ptr<const NNNAddress> addr);

      virtual Ptr<const NNNAddress>
      GetCurr3NName () const;

      virtual bool
      ConnectToApp (Ptr<Packet> p);

      ////////////////////////////////////////////////////////////////////

    private:
      ICNAppFace ();
      ICNAppFace (const ICNAppFace &); ///< \brief Disabled copy constructor
      ICNAppFace& operator= (const ICNAppFace &); ///< \brief Disabled copy operator

      // Variables

      // Holds the applications
      Ptr<ICNApp> m_app;
      Ptr<PureICNApp> m_pureicnapp;

      // Holds the ForwardingStrategy that has ICN understanding
      Ptr<ICN3NForwardingStrategy> m_icn_fw;

      // Variables used to affect how the Face interacts
      bool m_usePureICN;
      bool m_AppIsMobile;
      bool m_NodeIsMobile;
      bool m_has3NName;
      Ptr<const NNNAddress> m_currSrc;
      Time m_3n_lifetime;
    };
  } // namespace nnn
} // namespace ns3

#endif // NNN_ICN_APP_FACE_H
