/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2018 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-flooding.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-flooding.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-flooding.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_FW_NNN_ICN_FLOODING_H_
#define NNN_FW_NNN_ICN_FLOODING_H_

#include "nnn-icn-forwarding-strategy.h"

namespace ns3
{
  namespace nnn
  {

    class ICNFlooding : public ICN3NForwardingStrategy
    {
    public:
      static TypeId GetTypeId ();

      ICNFlooding ();

      virtual
      ~ICNFlooding ();

      void
      UpdatePITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu, Ptr<Face> face, Time lifetime);

      void
      UpdateLayeredPITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu,
			     Ptr<Face> face, Time lifetime,
			     const Address& from);

      void
      UpdateFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest);

      void
      UpdateLayeredFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest, const Address& from, const Address& to);

      void
      ProcessLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme, const Address& from);

      void
      LayeredMapMeAckProcess (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme, const Address& from);

      void
      RetransmitLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme, const Address& from);

      void
      TransmitLayeredMapMeAck (Ptr<DATAPDU> pdu, Ptr<const Interest> mapme, Ptr<Face> face, const Address& from);

      void
      SatisfyPendingInterest (Ptr<DATAPDU> pdu,
			      Ptr<Face> inFace,
			      Ptr<const Data> data,
			      Ptr<pit::Entry> pitEntry);

      void
      SatisfyPITEntryFromCache (Ptr<DATAPDU> pdu,
				Ptr<Face> inFace, // 0 allowed (from cache)
				Ptr<const Data> data,
				Ptr<pit::Entry> pitEntry);

      void
      SatisfyPendingLayeredInterest (Ptr<DATAPDU> pdu,
				     Ptr<Face> inFace, // 0 allowed (from cache)
				     Ptr<const Data> data,
				     Ptr<pit::Entry> pitEntry);

      bool
      DoPropagateInterest (Ptr<DATAPDU> pdu,
			   Ptr<Face> inFace,
			   Ptr<const Interest> interest,
      			   Ptr<pit::Entry> pitEntry);

      bool
      DoPropagateLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				  Ptr<const Interest> interest,
				  Ptr<pit::Entry> pitEntry,
				  const Address& from);

      bool
      DoPropagateData(Ptr<DATAPDU> pdu, Ptr<Face> inFace, Ptr<const Data> data);
    };

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_FW_NNN_ICN_FLOODING_H_ */
