/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-forwarding-strategy.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-forwarding-strategy.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-forwarding-strategy.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_ICN_FORWARDING_STRATEGY_H_
#define NNN_ICN_FORWARDING_STRATEGY_H_

#include "nnn-forwarding-strategy.h"

#include "ns3/nnn-pit.h"
#include "ns3/nnn-pit-entry.h"

#include "ns3/nnn-icn-face.h"
#include "ns3/nnn-names-seen-container.h"
#include "ns3/nnn-part-seen-container.h"

namespace ns3
{
  namespace nnn
  {
    class Fib;
    namespace fib
    {
      class Entry;
      class FaceMetric;
    }

    class TFib;
    namespace tfib
    {
      class Entry;
      class FaceMetric;
    }

    class Interest;
    class Data;
    class ContentStore;

    class ICN3NForwardingStrategy : public ForwardingStrategy
    {
    public:
      static TypeId GetTypeId ();

      /**
       * @brief Helper function to retrieve logging name for the forwarding strategy
       */
      ICN3NForwardingStrategy ();
      virtual
      ~ICN3NForwardingStrategy ();

      /**
       * \brief Actual processing of incoming 3N NULLps.
       *
       * Processing NULLp PDUs
       * @param face incoming face
       * @param null_p NULLp PDU
       */
      virtual void
      OnNULLp (Ptr<Face> face, Ptr<NULLp> null_p);

      virtual void
      OnLayeredNULLp (Ptr<Face> face, Ptr<NULLp> null_p, const Address& from, const Address& to);


      /**
       * \brief Actual processing of incoming 3N SOs.
       *
       * Processing SO PDUs
       * @param face incoming face
       * @param so_p SO PDU
       */
      virtual void
      OnSO (Ptr<Face> face, Ptr<SO> so_p);

      virtual void
      OnLayeredSO (Ptr<Face> face, Ptr<SO> so_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming 3N DOs
       *
       * Processing DO PDUs
       * @param face incoming face
       * @param do_p DO PDU
       */
      virtual void
      OnDO (Ptr<Face> face, Ptr<DO> do_p);

      virtual void
      OnLayeredDO (Ptr<Face> face, Ptr<DO> do_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming NNN DUs
       *
       * Processing DU PDUs
       * @param face    incoming face
       * @param du_p    DU PDU
       */
      virtual void
      OnDU (Ptr<Face> face, Ptr<DU> du_p);

      virtual void
      OnLayeredDU (Ptr<Face> face, Ptr<DU> du_p, const Address& from, const Address& to);


      /**
       * \brief Actual processing of incoming ICN Interests
       *
       * Processing Interests
       * @param face incoming face
       * @param interest_p Interest packet
       */
      virtual void
      OnInterest (Ptr<Face> face, Ptr<Interest> interest_p);

      virtual void
      OnLayeredInterest (Ptr<Face> face, Ptr<Interest> interest_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming ICN Data
       *
       * Processing Data
       * @param face    incoming face
       * @param data_p    Data packet
       */
      virtual void
      OnData (Ptr<Face> face, Ptr<Data> data_p);

      virtual void
      OnLayeredData (Ptr<Face> face, Ptr<Data> data_p, const Address& from, const Address& to);

      virtual void
      ProcessICNPDU (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Packet> icn_pdu);

      virtual void
      ProcessLayeredICNPDU (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Packet> icn_pdu,
			    const Address& from, const Address& to);

      virtual void
      UpdatePITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu, Ptr<Face> face, Time lifetime);

      virtual void
      UpdateLayeredPITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu,
			     Ptr<Face> face, Time lifetime,
			     const Address& from);

      virtual void
      UpdateFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest);

      virtual void
      UpdateLayeredFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface, Ptr<const icn::Name> interest, const Address& from, const Address& to);

      virtual void
      ProcessInterest (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> interest);

      virtual void
      ProcessLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> interest,
			      const Address& from);

      virtual void
      ProcessData (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Data> data);

      virtual void
      ProcessLayeredData (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Data> data, const Address& from, const Address& to);

      virtual void
      ProcessNack (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> nack);

      virtual void
      ProcessMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme);

      virtual void
      MapMeAckProcess (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme);

      virtual void
      ProcessLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme, const Address& from);

      virtual void
      RetransmitMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme);

      virtual void
      RetransmitLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face, Ptr<Interest> mapme, const Address& from);

      virtual void
      TransmitMapMeAck (Ptr<DATAPDU> pdu, Ptr<const Interest> mapme, Ptr<Face> face);

      virtual void
      TransmitLayeredMapMeAck (Ptr<DATAPDU> pdu, Ptr<const Interest> mapme, Ptr<Face> face, const Address& from);

      /**
       * @brief Event fired just before PIT entry is removed by timeout
       * @param pitEntry PIT entry to be removed
       */
      virtual void
      WillEraseTimedOutPendingInterest (Ptr<pit::Entry> pitEntry);

      /**
       * @brief Event fired every time a FIB entry is added to FIB
       * @param fibEntry FIB entry that was added
       */
      virtual void
      DidAddFibEntry (Ptr<fib::Entry> fibEntry);

      /**
       * @brief Fired just before FIB entry will be removed from FIB
       * @param fibEntry FIB entry that will be removed
       */
      virtual void
      WillRemoveFibEntry (Ptr<fib::Entry> fibEntry);


      /**
       * @brief Gives the most recently seen 3N name using a particuarl ICN name
       * @param fibEntry FIB entry that will be removed
       */
      virtual Ptr<const NNNAddress>
      NNNForLongestPrefixMatch (Ptr<const icn::Name> name);

      bool
      IsSharingPoA () const;

      void
      SetSharePoA (bool value);

      bool
      IsUsingNonAuthoritativeDU () const;

      void
      SetNonAuthoritativeDU(bool value);

      bool
      HasTFIBEnabled () const;

      void
      EnableTFIB (bool value);

      bool
      IsEnablingFIBUpdates () const;

      void
      EnableFIBUpdates (bool value);

      bool
      IsEnablingSavingUnrequestedData () const;

      void
      EnableSavingUnrequestedData (bool value);

    protected:
      /**
       * @brief An event that is fired every time a new PIT entry is created
       *
       * Note that if ICN node is receiving a similar interest (interest for the same name),
       * then either DidReceiveDuplicateInterest, DidSuppressSimilarInterest, or DidForwardSimilarInterest
       * will be called
       *
       * Suppression of similar Interests is controlled using ShouldSuppressIncomingInterest virtual method
       *
       * @param inFace  incoming face
       * @param header  deserialized Interest header
       * @param pitEntry created PIT entry (incoming and outgoing face sets are empty)
       *
       * @see DidReceiveDuplicateInterest, DidSuppressSimilarInterest, DidForwardSimilarInterest, ShouldSuppressIncomingInterest
       */
      virtual void
      DidCreatePitEntry (Ptr<Face> inFace,
			 Ptr<const Interest> interest,
			 Ptr<pit::Entry> pitEntry);

      /**
       * @brief An event that is fired every time a new PIT entry cannot be created (e.g., PIT container imposes a limit)
       *
       * Note that this call can be called only for non-similar Interest (i.e., there is an attempt to create a new PIT entry).
       * For any non-similar Interests, either FailedToCreatePitEntry or DidCreatePitEntry is called.
       *
       * @param inFace   incoming face
       * @param interest Interest packet
       */
      virtual void
      FailedToCreatePitEntry (Ptr<Face> inFace,
			      Ptr<const Interest> interest);

      /**
       * @brief An event that is fired every time a duplicated Interest is received
       *
       * This even is the last action that is performed before the Interest processing is halted
       *
       * @param inFace  incoming face
       * @param interest Interest packet
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       *
       * @see DidReceiveDuplicateInterest, DidSuppressSimilarInterest, DidForwardSimilarInterest, ShouldSuppressIncomingInterest
       */
      virtual void
      DidReceiveDuplicateInterest (Ptr<Face> inFace,
				   Ptr<const Interest> interest,
				   Ptr<pit::Entry> pitEntry);

      virtual void
      DidReceiveDuplicateLayeredInterest (Ptr<DATAPDU> pdu,
					  Ptr<Face> inFace,
					  Ptr<const Interest> interest,
					  Ptr<pit::Entry> pitEntry,
					  const Address& from);

      /**
       * @brief An event that is fired every time when a similar Interest is received and suppressed (collapsed)
       *
       * This even is the last action that is performed before the Interest processing is halted
       *
       * @param inFace  incoming face
       * @param interest Interest packet
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       *
       * @see DidReceiveDuplicateInterest, DidForwardSimilarInterest, ShouldSuppressIncomingInterest
       */
      virtual void
      DidSuppressSimilarInterest (Ptr<Face> inFace,
				  Ptr<const Interest> interest,
				  Ptr<pit::Entry> pitEntry);

      /**
       * @brief An event that is fired every time when a similar Interest is received and further forwarded (not suppressed/collapsed)
       *
       * This even is fired just before handling the Interest to PropagateInterest method
       *
       * @param inFace  incoming face
       * @param interest Interest packet
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       *
       * @see DidReceiveDuplicateInterest, DidSuppressSimilarInterest, ShouldSuppressIncomingInterest
       */
      virtual void
      DidForwardSimilarInterest (Ptr<Face> inFace,
				 Ptr<const Interest> interest,
				 Ptr<pit::Entry> pitEntry);

      /**
       * @brief An even that is fired when Interest cannot be forwarded
       *
       * Note that the event will not fire if  retransmission detection is enabled (by default)
       * and retransmitted Interest cannot by forwarded.  For more details, refer to the implementation.
       *
       * @param inFace  incoming face
       * @param interest Interest header
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       *
       * @see DetectRetransmittedInterest
       */
      virtual void
      DidExhaustForwardingOptions (Ptr<DATAPDU> pdu,
				   Ptr<Face> inFace,
				   Ptr<const Interest> interest,
				   Ptr<pit::Entry> pitEntry);

      virtual void
      DidReceiveValidNack (Ptr<DATAPDU> pdu,
			   Ptr<Face> inFace,
			   uint32_t nackCode,
			   Ptr<const Interest> nack,
			   Ptr<pit::Entry> pitEntry);

      /**
       * @brief Method that implements logic to distinguish between new and retransmitted interest
       *
       * This method is called only when DetectRetransmissions attribute is set true (by default).
       *
       * Currently, the retransmission detection logic relies on the fact that list of incoming faces
       * already has inFace (i.e., a similar interest is received on the same face more than once).
       *
       * @param inFace  incoming face
       * @param interest Interest header
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       * @return true if Interest should be considered as retransmitted
       */
      virtual bool
      DetectRetransmittedInterest (Ptr<Face> inFace,
				   Ptr<const Interest> interest,
				   Ptr<pit::Entry> pitEntry);

      /**
       * @brief Even fired just before Interest will be satisfied
       *
       * Note that when Interest is satisfied from the cache, incoming face will be 0
       *
       * @param inFace  incoming face
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       */
      virtual void
      WillSatisfyPendingInterest (Ptr<Face> inFace,
				  Ptr<pit::Entry> pitEntry);

      virtual void
      WillSatisfyPendingLayeredInterest (Ptr<Face> inFace,
					 Ptr<pit::Entry> pitEntry,
					 const Address& from);
      /**
       * @brief Actual procedure to satisfy Interest
       *
       * Note that when Interest is satisfied from the cache, incoming face will be 0
       *
       * @param pdu The 3N PDU that created this information
       * @param inFace  incoming face
       * @param data    Data packet
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       */
      virtual void
      SatisfyPendingInterest (Ptr<DATAPDU> pdu,
			      Ptr<Face> inFace, // 0 allowed (from cache)
			      Ptr<const Data> data,
			      Ptr<pit::Entry> pitEntry);

      virtual void
      SatisfyPendingLayeredInterest (Ptr<DATAPDU> pdu,
				     Ptr<Face> inFace, // 0 allowed (from cache)
				     Ptr<const Data> data,
				     Ptr<pit::Entry> pitEntry);

      /**
       * @brief Event which is fired just after data was send out on the face
       *
       * @param inFace   incoming face of the Data
       * @param outFace  outgoing face
       * @param data     Data packet
       * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
       */
      virtual void
      DidSendOutData (Ptr<Face> inFace,
		      Ptr<Face> outFace,
		      Ptr<const Data> data,
		      Ptr<pit::Entry> pitEntry);

      /**
       * @brief Event which is fired every time a requested (solicited) DATA packet (there is an active PIT entry) is received
       *
       * @param inFace  incoming face
       * @param data    Data packet
       * @param didCreateCacheEntry flag indicating whether a cache entry was added for this data packet or not (e.g., packet already exists in cache)
       */
      virtual void
      DidReceiveSolicitedData (Ptr<Face> inFace,
			       Ptr<const Data> data,
			       bool didCreateCacheEntry);

      /**
       * @brief Event which is fired every time an unsolicited DATA packet (no active PIT entry) is received
       *
       * The current implementation allows ignoring unsolicited DATA (by default), or cache it by setting
       * attribute CacheUnsolicitedData true
       *
       * @param inFace  incoming face
       * @param data    Data packet
       * @param didCreateCacheEntry flag indicating whether a cache entry was added for this data packet or not (e.g., packet already exists in cache)
       */
      virtual void
      DidReceiveUnsolicitedData (Ptr<Face> inFace,
				 Ptr<const Data> data,
				 bool didCreateCacheEntry);

      /**
       * @brief Method implementing logic to suppress (collapse) similar Interests
       *
       * In the base class implementation this method checks list of incoming/outgoing faces of the PIT entry
       * (for new Intersets, both lists are empty before this call)
       *
       * For more details, refer to the source code.
       *
       * @param inFace  incoming face
       * @param interest Interest packet
       * @param payload Data payload
       */
      virtual bool
      ShouldSuppressIncomingInterest (Ptr<Face> inFace,
				      Ptr<const Interest> interest,
				      Ptr<pit::Entry> pitEntry);

      /**
       * @brief Method to check whether Interest can be send out on the particular face or not
       *
       * In the base class, this method perfoms two checks:
       * 1. If inFace is equal to outFace (when equal, Interest forwarding is prohibited unless there is a specific address attached (broadcast mediums))
       * 2. Whether Interest should be suppressed (list of outgoing faces include outFace),
       * considering (if enabled) retransmission logic
       *
       * @param inFace     incoming face of the Interest
       * @param outFace    proposed outgoing face of the Interest
       * @param addr       PoA name to use for sending PDU (secondary check for broadcast mediums using PoA names to send PDUs)
       * @param interest   Interest packet
       * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
       *
       * @see DetectRetransmittedInterest
       */
      virtual bool
      CanSendOutInterest (Ptr<DATAPDU> pdu,
			  Ptr<Face> inFace,
			  Ptr<Face> outFace,
			  Address addr,
			  Ptr<const Interest> interest,
			  Ptr<pit::Entry> pitEntry);

      virtual bool
      CanSendOutLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace, Ptr<Face> outFace,
				 Address addr, Ptr<const Interest> interest,
				 Ptr<pit::Entry> pitEntry,
				 const Address& from);

      /**
       * @brief Method implementing actual interest forwarding, taking into account CanSendOutInterest decision
       *
       * If event returns false, then there is some kind of a problem exists
       *
       * @param pdu The 3N PDU that created this information
       * @param inFace     incoming face of the Interest
       * @param outFace    proposed outgoing face of the Interest
       * @param interest Interest packet
       * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
       * @param addr Address to use when sending PDUs
       *
       * @see CanSendOutInterest
       */
      virtual bool
      TrySendOutInterest (Ptr<DATAPDU> pdu,
			  Ptr<Face> inFace,
			  Ptr<Face> outFace,
			  Address addr,
			  Ptr<const Interest> interest,
			  Ptr<pit::Entry> pitEntry);

      virtual bool
      TrySendOutLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				 Ptr<Face> outFace, Address addr,
				 Ptr<const Interest> interest,
				 Ptr<pit::Entry> pitEntry,
				 const Address& from);

      /**
       * @brief Event fired just after forwarding the Interest
       *
       * @param inFace     incoming face of the Interest
       * @param outFace    outgoing face of the Interest
       * @param interest Interest packet
       * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
       */
      virtual void
      DidSendOutInterest (Ptr<Face> inFace,
			  Ptr<Face> outFace,
			  Ptr<const Interest> interest,
			  Ptr<pit::Entry> pitEntry);

      virtual void
      DidDropInterest (Ptr<Face> face, Ptr<const Interest> interest);

      /**
       * @brief Wrapper method, which performs general tasks and calls DoPropagateInterest method
       *
       * General tasks so far are adding face to the list of incoming face, updating
       * PIT entry lifetime, calling DoPropagateInterest, and retransmissions (enabled by default).
       *
       * @param pdu The 3N PDU that created this information
       * @param inFace     incoming face
       * @param interest   Interest packet
       * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
       *
       * @see DoPropagateInterest
       */

      virtual void
      PropagateInterest (Ptr<DATAPDU> pdu,
			 Ptr<Face> inFace,
			 Ptr<const Interest> interest,
			 Ptr<pit::Entry> pitEntry);

      virtual void
      PropagateLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				Ptr<const Interest> interest,
				Ptr<pit::Entry> pitEntry,
				const Address& from);

      /**
       * @brief Virtual method to perform Interest propagation according to the forwarding strategy logic
       *
       * In most cases, this is the call that needs to be implemented/re-implemented in order
       * to perform forwarding of Interests according to the desired logic.
       *
       * There is also PropagateInterest method (generally, do not require to be overriden)
       * which performs general tasks (adding face to the list of incoming face, updating
       * PIT entry lifetime, calling DoPropagateInterest, as well as perform retransmissions (enabled by default).
       *
       * @param pdu The 3N PDU that created this information
       * @param inFace     incoming face
       * @param interest   Interest packet
       * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
       *
       * @return true if interest was successfully propagated, false if all options have failed
       *
       * @see PropagateInterest
       */
      virtual bool
      DoPropagateInterest (Ptr<DATAPDU> pdu,
			   Ptr<Face> inFace,
			   Ptr<const Interest> interest,
			   Ptr<pit::Entry> pitEntry);

      virtual bool
      DoPropagateLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				  Ptr<const Interest> interest,
				  Ptr<pit::Entry> pitEntry,
				  const Address& from);

      /**
       * @brief Virtual method to perform Data propagation according to the NNST routing logic when no PIT Entry is available
       *
       * In most cases, this is the call that needs to be implemented/re-implemented in order
       * to perform forwarding of Data without PIT involvement.
       *
       * @param pdu The 3N PDU that created this information
       * @param inFace     incoming face
       * @param data       Data packet
       *
       * @return true if Data was successfully send, false if all options have failed
       *
       * @see PropagateInterest
       */
      virtual bool
      DoPropagateData (Ptr<DATAPDU> pdu,
		       Ptr<Face> inFace,
		       Ptr<const Data> data);

      virtual bool
      TryToSendRawData (Ptr<Face> inFace, Ptr<Face> outFace, Ptr<const Data> data,  Ptr<pit::Entry> pitEntry);

      virtual bool
      TryToSendDataInNULL (uint32_t orig_flow, Ptr<Face> inFace, Ptr<Face> outFace, Ptr<const Data> data,  Ptr<pit::Entry> pitEntry);

      virtual bool
      TryToSendDataInNULLToPoA (uint32_t orig_flow, Ptr<Face> inFace, Ptr<Face> outFace,
				Ptr<const Data> data,  Ptr<pit::Entry> pitEntry,
				const Address& to);

      virtual bool
      TryToSendRawDataToPoA (Ptr<Face> inFace, Ptr<Face> outFace,
			     Ptr<const Data> data,  Ptr<pit::Entry> pitEntry,
			     const Address& to);

      virtual bool
      TryToSendDataInDO (Ptr<DO> do_o, Address addr, Ptr<Face> inFace, Ptr<Face> outFace,
			 Ptr<const Data> data, Ptr<pit::Entry> pitEntry);

      virtual bool
      TryToSendDataInDU (Ptr<DU> du_o, Address addr, Ptr<Face> inFace, Ptr<Face> outFace,
			 Ptr<const Data> data, Ptr<pit::Entry> pitEntry);

      std::set<Ptr<const NNNAddress>, Ptr3NSizeMinComp>
      SelectMulticastDestinations (std::set<Ptr<const NNNAddress>, Ptr3NSizeMaxComp> con);

      NamesSeenContainer
      SelectMulticastDestinationsByTime (PartsSeenContainer con, Ptr<const NNNAddress> trigger);

      std::list<fib::FaceMetric>
      FindFacesBy3NDestinations (Ptr<fib::Entry> fibEntry);

      virtual std::pair<std::set<Ptr<Face> >,int>
      PropagateInterestinDO (Ptr<DO> pdu, Ptr<Face> inFace, Ptr<const Interest> interest,
			     Ptr<pit::Entry> pitEntry);

      virtual std::pair<std::set<Ptr<Face> >,int>
      PropagateInterestinDU (Ptr<DU> pdu, Ptr<Face> inFace, Ptr<const Interest> interest,
			     Ptr<pit::Entry> pitEntry);

      virtual std::tuple<uint32_t,uint32_t,bool>
      FindFlowidSeq (Ptr<DATAPDU> pdu, uint32_t count);

      bool
      EnqueueInterest (Ptr<Face> face, Address dst, Ptr<const Interest> pdu);

      bool
      EnqueueData (Ptr<Face> face, Address dst, Ptr<const Data> pdu);

    protected:
      virtual void NotifyNewAggregate (); ///< @brief Even when object is aggregated to another Object
      virtual void DoDispose (); ///< @brief Do cleanup

      typedef void (* DataTracedCallback)
	  (const Ptr<const Data>, const Ptr<const Face>);

      typedef void (* InterestTracedCallback)
	  (const Ptr<const Interest>, const Ptr<const Face>);

      typedef void (* PITEntryTracedCallback)
	  (const Ptr<const pit::Entry>);

      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const Interest>,
      Ptr<const Face> > m_outInterests; ///< @brief Transmitted interests trace

      TracedCallback<Ptr<const Interest>,
      Ptr<const Face> > m_inInterests; ///< @brief trace of incoming Interests

      TracedCallback<Ptr<const Interest>,
      Ptr<const Face> > m_dropInterests; ///< @brief trace of dropped Interests

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const Data>,
      bool /*from cache*/,
      Ptr<const Face> > m_outData; ///< @brief trace of outgoing Data

      TracedCallback<Ptr<const Data>,
      Ptr<const Face> > m_inData; ///< @brief trace of incoming Data

      TracedCallback<Ptr<const Data>,
      Ptr<const Face> > m_dropData;  ///< @brief trace of dropped Data

      ////////////////////////////////////////////////////////////////////

      // ndnSIM style Nacks
      TracedCallback<Ptr<const Interest>,
      Ptr<const Face> > m_outNacks; ///< @brief trace of outgoing NACKs

      TracedCallback<Ptr<const Interest>,
      Ptr<const Face> > m_inNacks; ///< @brief trace of incoming NACKs

      TracedCallback<Ptr<const Interest>,
      Ptr<const Face> > m_dropNacks; ///< @brief trace of dropped NACKs

      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////

      TracedCallback< Ptr<const pit::Entry> > m_satisfiedInterests;
      TracedCallback< Ptr<const pit::Entry> > m_timedOutInterests;

      TracedCallback<> m_nonauthoritateDU;                     ///< @brief Trace when Node can use non-authoritative DU

      bool m_detectRetransmissions;
      bool m_cacheUnsolicitedDataFromApps;
      bool m_cacheUnsolicitedData;
      bool m_nacksEnabled;
      bool m_EnableTFIB; ///< \brief Flag to know whether MapMe functions are enabled
      bool m_sharePoAInfo; ///< \brief Flag to know whether the forwarding strategy will collect and use PoA information
      bool m_EnableNonAuthoritativeDU; ///< \brief Flag for use of non-authoritative DUs.
      bool m_EnableFIBUpdates; ///< \brief Flag to know whether ICN PDUs will update the FIB.
      bool m_EnableSavingUnrequestedData; ///< \brief Flag to know whether unrequested ICN Data packets are saved in a Content Store
      bool m_Enable_Anycast; ///< \brief Flag to permit the creation of 3N PDUs that go to the nearest central point of a set of 3N names
      bool m_Enable_Multicast; /// \brief Flag to permit the creation of 3N PDUs that go to all the most distant 3N names in an FIB
      bool m_Enable_3N_face_time_select; /// \brief Flag to use 3N names in the selection of Faces for Interest propagation
      bool m_Enable_FIB_part_select;      /// \brief Flag to use FIB part numbers and 3N names in the selection of Faces for Interest propagation
      bool m_Enable_Flooding;             /// \brief Flag to enable ICN Flooding. Only used in ICNFlooding class

      Ptr<Pit> m_pit; ///< \brief Reference to PIT to which this forwarding strategy is associated
      Ptr<Fib> m_fib; ///< \brief Reference to FIB to which this forwarding strategy is associated
      Ptr<ContentStore> m_contentStore; ///< \brief Content store (for caching purposes only)
      Ptr<TFib> m_tfib; ///< \brief Reference to TFIB to which this forwarding strategy is associated
      Ptr<Pit> m_awaiting_mapme_acks; ///< \brief Reference to PIT structure used to keep track of MapMe Acknowledgments
      int32_t m_icn_fib_metric; ///< \brief The default metric for inserting new FIB entries
      uint32_t m_mapme_retrys; ///< \brief The number of attempts the Forwarding Strategy will attempt to retransmit an Interest with MapMe information
      Time m_fib_3n_threshold; ///< \brief Time threshold to consider a not updated 3N name in an FIB valid
    };

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_ICN_FORWARDING_STRATEGY_H_ */
