/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-forwarding-strategy.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-forwarding-strategy.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-forwarding-strategy.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *          Zhu Li <phillipszhuli1990@gmail.com>
 */
#ifndef NNN_FORWARDING_STRATEGY_H
#define NNN_FORWARDING_STRATEGY_H

#include "ns3/address.h"
#include "ns3/nstime.h"
#include "ns3/callback.h"
#include "ns3/object.h"
#include "ns3/packet.h"
#include "ns3/random-variable-stream.h"
#include "ns3/traced-callback.h"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/bimap/bimap.hpp>

#include "ns3/nnn-pdu-rxbuffer.h"

#include "ns3/nnn-naming.h"

#include <queue>

namespace ns3
{
  namespace nnn
  {
    /**
     * @ingroup nnn
     * @defgroup nnn-fw 3N forwarding strategies
     */

    /**
     * @ingroup nnn-fw
     * @brief Namespace for Forwarding Strategy operations
     */
    namespace fw
    {
    }
    class NNNAddress;

    class NNNPDU;
    class NULLp;
    class SO;
    class DO;
    class DU;
    class EN;
    class AEN;
    class REN;
    class DEN;
    class ADEN;
    class OEN;
    class INF;
    class RHR;
    class OHR;
    class ACKP;

    class NamesContainer;
    class NamesContainerEntry;
    class FaceContainer;

    class NNST;
    namespace nnst
    {
      class Entry;
      class FaceMetric;
    }

    class NNPT;
    namespace nnpt { class Entry; }

    namespace name { class Component; }

    class NNNAddrAggregator;
    class PDUBuffer;
    class MPDUBuffer;
    class PDUQueue;
    class RxBufferContainer;

    class Face;

    /**
     * @ingroup nnn-fw
     * @brief Abstract base class for 3N forwarding strategies
     */
    class ForwardingStrategy : public Object
    {
    public:
      struct PtrNNNComp
      {
	bool operator () (const Ptr<const NNNAddress> &lhs , const Ptr<const NNNAddress>  &rhs) const
	{
	  return *lhs < *rhs;
	}
      };

      struct PtrFaceComp
      {
	bool operator () (const Ptr<Face> &lhs, const Ptr<Face> &rhs) const
	{
	  return *lhs < *rhs;
	}
      };

      typedef boost::bimaps::bimap<uint32_t,uint32_t> fw_flowid_bimap;
      typedef fw_flowid_bimap::value_type fw_flowid_combo;

      static TypeId GetTypeId ();

      /**
       * @brief Default constructor
       */
      ForwardingStrategy ();
      virtual ~ForwardingStrategy ();

      bool
      SupportsDirect3N ();

      bool
      SupportsDirectICN ();

      void
      SetMobile(bool value);

      bool
      IsMobile() const;

      virtual void
      SetNode3NName (Ptr<const NNNAddress> name, Time lease, bool fixed);

      virtual const NNNAddress&
      GetNode3NName ();

      virtual Ptr<const NNNAddress>
      GetNode3NNamePtr ();

      // Produces a random 3N name under the delegated name space
      virtual Ptr<const NNNAddress>
      produce3NName ();

      virtual bool
      Has3NName ();

      virtual bool
      GoesBy3NName (Ptr<const NNNAddress> addr);

      virtual bool
      HasFixed3NName ();

      virtual void
      SetRetxTimer (Time retx);

      virtual Time
      GetRetxTimer () const;

      virtual void
      Set3NReTx (uint32_t retx);

      virtual uint32_t
      Get3NReTx () const;

      virtual void
      Set3NPDULifetime (Time lifetime);

      virtual Time
      Get3NPDULifetime () const;

      virtual void
      flushBuffer (Ptr<Face> face, Ptr<NNNAddress> oldName, Ptr<NNNAddress> newName);

      virtual void
      hasDisenrolled ();

      virtual void
      hasReenrolled  ();

      virtual void
      EmptyDisenrollPDUBuffer ();

      virtual void
      AskACKedPDUs (bool value);

      virtual bool
      IsAskingACKedPDUs () const;

      virtual void
      ReturnsACKedPDUs (bool value);

      virtual bool
      IsReturningACKedPDUs () const;

      uint32_t
      GetMin3NWindow () const;

      void
      SetMin3NWindow (uint32_t value);

      uint32_t
      GetMax3NWindow () const;

      void
      SetMax3NWindow (uint32_t value);

      /**
       * \brief Actual processing of incoming 3N ENs
       *
       * Processing EN PDUs
       * @param face    incoming face
       * @param en_p    EN PDU
       */
      virtual void
      OnEN (Ptr<Face> face, Ptr<EN> en_p);

      /**
       * \brief Actual processing of incoming 3N AENs.
       *
       * Processing AEN PDUs
       * @param face     incoming face
       * @param aen_p AEN PDU
       */
      virtual void
      OnAEN (Ptr<Face> face, Ptr<AEN> aen_p);

      /**
       * \brief Actual processing of incoming 3N RENs
       *
       * Processing REN PDUs
       * @param face    incoming face
       * @param ren_p    REN PDU
       */
      virtual void
      OnREN (Ptr<Face> face, Ptr<REN> ren_p);

      /**
       * \brief Actual processing of incoming 3N DENs
       *
       * Processing DEN PDUs
       * @param face    incoming face
       * @param den_p    DEN PDU
       */
      virtual void
      OnDEN (Ptr<Face> face, Ptr<DEN> den_p);

      virtual void
      deleteAndPropagateDEN (Ptr<const NNNAddress> addr);

      /**
       * \brief Propagate the DEN
       *
       * Propagate the DEN
       * @param face    incoming face
       * @param den_p    DEN PDU
       */
      virtual void
      PropagateDEN (Ptr<const NNNAddress> addr);

      /**
       * \brief Actual processing of incoming 3N ADENs
       *
       * Processing ADEN PDUs
       * @param face    incoming face
       * @param aden_p    ADEN PDU
       */
      virtual void
      OnADEN (Ptr<Face> face, Ptr<ADEN> aden_p);

      /**
       * \brief Actual processing of incoming 3N DENs
       *
       * Processing OEN PDUs
       * @param face    incoming face
       * @param oen_p    OEN PDU
       */
      virtual void
      OnOEN (Ptr<Face> face, Ptr<OEN> oen_p);

      /**
       * \brief Actual processing of incoming 3N INFs
       *
       * Processing INF PDUs
       * @param face    incoming face
       * @param inf_p    INF PDU
       */
      virtual void
      OnINF (Ptr<Face> face, Ptr<INF> inf_p);

      /**
       * \brief Actual processing of incoming 3N NULLps.
       *
       * Processing NULLp PDUs
       * @param face incoming face
       * @param null_p NULLp PDU
       */
      virtual void
      OnNULLp (Ptr<Face> face, Ptr<NULLp> null_p);

      virtual void
      OnLayeredNULLp (Ptr<Face> face, Ptr<NULLp> null_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming 3N SOs.
       *
       * Processing SO PDUs
       * @param face incoming face
       * @param so_p SO PDU
       */
      virtual void
      OnSO (Ptr<Face> face, Ptr<SO> so_p);

      virtual void
      OnLayeredSO (Ptr<Face> face, Ptr<SO> so_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming 3N DOs
       *
       * Processing DO PDUs
       * @param face incoming face
       * @param do_p DO PDU
       */
      virtual void
      OnDO (Ptr<Face> face, Ptr<DO> do_p);

      virtual void
      OnLayeredDO (Ptr<Face> face, Ptr<DO> do_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming NNN DUs
       *
       * Processing DU PDUs
       * @param face    incoming face
       * @param du_p    DU PDU
       */
      virtual void
      OnDU (Ptr<Face> face, Ptr<DU> du_p);

      virtual void
      OnLayeredDU (Ptr<Face> face, Ptr<DU> du_p, const Address& from, const Address& to);

      /**
       * \brief Actual processing of incoming RHRs
       *
       * Processing RHR PDUs
       * @param face    incoming face
       * @param rhr_p   RHR PDU
       */
      virtual void
      OnRHR (Ptr<Face> face, Ptr<RHR> rhr_p);

      /**
       * \brief Actual processing of incoming OHRs
       *
       * Processing OHR PDUs
       * @param face    incoming face
       * @param rhr_p   OHR PDU
       */
      virtual void
      OnOHR (Ptr<Face> face, Ptr<OHR> ohr_p);

      /**
       * \brief Actual processing of incoming ACKP
       *
       * Processing ACKP PDUs
       * @param face    incoming face
       * @param ack_p   ACKP PDU
       */
      virtual void
      OnACKP (Ptr<Face> face, Ptr<ACKP> ack_p);

      virtual void
      OnLayeredACKP (Ptr<Face> face, Ptr<ACKP> ack_p, const Address& from, const Address& to);

      /**
       * @brief Event fired every time face is added to NNN stack
       * @param face face to be removed
       */
      virtual void
      AddFace (Ptr<Face> face);

      /**
       * @brief Event fired every time face is removed from NNN stack
       * @param face face to be removed
       *
       * For example, when an application terminates, AppFace is removed and this method called by NNN stack.
       */
      virtual void
      RemoveFace (Ptr<Face> face);

      virtual std::vector<Address>
      GetAllPoANames (Ptr<Face> face);

      // Usually called by NetDevice Assoc callbacks in order to affect the ForwardingStrategy's
      // behaviour. If the callback is linked, we assume that the node that is using this
      // ForwardingStrategy is completely using 3N abilities.
      virtual void
      PoAModification (std::string context, const Mac48Address mac);

      virtual void
      Enroll ();

      virtual void
      Reenroll ();

      virtual void
      Disenroll ();

      virtual void
      DisenrollName (Ptr<const NNNAddress> addr);

      virtual void
      ExecuteEnrollment (Ptr<Face> face);

      virtual void
      EndAEN (Ptr<const NNNAddress> addr);

      virtual void
      AskHandshake (Ptr<const NNNAddress> addr);

      virtual void
      RetransmitOEN (Ptr<Face> face, Ptr<const NNNAddress> addr, Address dest_addr, Ptr<OEN> oen_p);

      virtual void
      RetransmitDEN (Ptr<const NNNAddress> addr);

      virtual void
      Retransmit3NWindow (RxPDU data);

      virtual void
      Migrate3NWindow (RxPDU data);

      virtual int8_t
      txPreparation (Ptr<DATAPDU> pdu, Ptr<Face> outFace, Address dst_poa, bool retx);

      virtual void
      txReset (Ptr<DATAPDU> pdu);

      virtual void
      TransmitACKP (Ptr<Face> face, Address dst);

      virtual void
      TransmitFlowidACKP (Ptr<Face> face, Address dst, uint32_t flowid);

      virtual void
      TransmitLossACKP (Ptr<Face> face, Address dst, uint32_t flowid);

      uint32_t
      GenerateAppFlowid ();

      /**
       * @brief Event fired every time a NNST entry is added to NNST
       * @param NNSTEntry NNST entry that was added
       */
      virtual void
      DidAddNNSTEntry (Ptr<nnst::Entry> NNSTEntry);

      /**
       * @brief Fired just before NNST entry will be removed from NNST
       * @param NNSTEntry NNST entry that will be removed
       */
      virtual void
      WillRemoveNNSTEntry (Ptr<nnst::Entry> NNSTEntry);

      /**
       * @brief Event fired every time a NNPT entry is added to NNPT
       * @param NNPTEntry NNPT entry that was added
       */
      virtual void
      DidAddNNPTEntry (Ptr<nnpt::Entry> NNPTEntry);

      /**
       * @brief Fired just before NNPT entry will be removed from NNST
       * @param NNPTEntry NNPT entry that will be removed
       */
      virtual void
      WillRemoveNNPTEntry (Ptr<nnpt::Entry> NNPTEntry);

    protected:

      typedef void (* ENTracedCallback)
	  (const Ptr<const EN>, const Ptr<const Face>);

      typedef void (* AENTracedCallback)
	  (const Ptr<const AEN>, const Ptr<const Face>);

      typedef void (* DENTracedCallback)
	  (const Ptr<const DEN>, const Ptr<const Face>);

      typedef void (* ADENTracedCallback)
	  (const Ptr<const ADEN>, const Ptr<const Face>);

      typedef void (* RENTracedCallback)
	  (const Ptr<const REN>, const Ptr<const Face>);

      typedef void (* OENTracedCallback)
	  (const Ptr<const OEN>, const Ptr<const Face>);

      typedef void (* INFTracedCallback)
	  (const Ptr<const INF>, const Ptr<const Face>);

      typedef void (* NULLpTracedCallback)
	  (const Ptr<const NULLp>, const Ptr<const Face>);

      typedef void (* SOTracedCallback)
	  (const Ptr<const SO>, const Ptr<const Face>);

      typedef void (* DOTracedCallback)
	  (const Ptr<const DO>, const Ptr<const Face>);

      typedef void (* DUTracedCallback)
	  (const Ptr<const DU>, const Ptr<const Face>);

      typedef void (* RHRTracedCallback)
 	  (const Ptr<const RHR>, const Ptr<const Face>);

       typedef void (* OHRTracedCallback)
 	  (const Ptr<const OHR>, const Ptr<const Face>);

       typedef void (* ACKPTracedCallback)
	   (const Ptr<const ACKP>, const Ptr<const Face>);

      typedef void (* NNNAddressTracedCallback)
	  (const Ptr<const NNNAddress>);

      typedef void (* IDTracedCallback)
	  (uint32_t);

      typedef void (* NNNVoidTracedCallback)
	  (void);

      typedef void (* NNNTimeTracedCallback)
	  (Time);

      typedef void (* NNNBoolTracedCallback)
	  (bool);

    protected:
      // inherited from Object class
      virtual void NotifyNewAggregate (); ///< @brief Even when object is aggregated to another Object
      virtual void DoDispose (); ///< @brief Do cleanup

      std::tuple<uint32_t, uint32_t>
      ObtainFlowidSeqSent (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      bool
      HasSentACKReceivedFlowid (Ptr<Face> outFace, Address dst, uint32_t flowid);

      void
      SetACKSentReceivedFlowid (Ptr<Face> outFace, Address dst, uint32_t flowid, bool value = true);

      virtual std::map<uint32_t, uint32_t>
      VerifyReceivedFlowidMaxSeq (Ptr<Face> outFace, Address dst);

      Flowid_Sender
      ObtainSentFlowidInfo (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      void
      SetupFlowidSeq (Ptr<DATAPDU> out_pdu, Ptr<Face> outFace, Address dst, bool retx);

      bool
      UpdateLastSeenACKP (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t seq, bool out_of_order);

      virtual bool
      UpdateReceivedPDUs (Ptr<DATAPDU> pdu, Ptr<Face> inFace, Address src);

      virtual void
      UpdateSentPDUs (Ptr<DATAPDU> pdu, Ptr<Face> outFace, Address dst, bool retx);

      uint32_t
      ObtainFlowidWindowSent (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      uint32_t
      ObtainFlowidRateSent (Ptr<Face> face, Address dst, uint32_t flowid);

      Time
      ObtainNextPDUSentWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      Time
      ObtainRetransmissionInterval (RxPDU data);

      void
      UpdatePDUBeforeACKP (RxPDU data);

      bool
      VerifyFlowid (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      virtual void
      VerifyAndMigrateBroadcast (uint32_t flowid, Ptr<Face> inFace, Address src);

      void
      UpdatePDUCounters (Ptr<DATAPDU> pdu, Ptr<Face> face, int8_t ok, bool retx);

      void
      UpdatePDUCountersB (Ptr<DATAPDU> pdu, Ptr<Face> face, bool ok, bool retx);

      bool
      CanTransmitACKP (std::set<uint32_t> seen, uint32_t flowid, uint32_t window, uint32_t start, uint32_t end);

      bool
      CanWaitForACKP (RxPDU data);

      void
      ClearTxSequences (RxPDU data);

      std::set<Ptr<Face> >
      GetAllFaceSet ();

      bool
      EnqueueNULLp (Ptr<Face> face, Address dst, Ptr<NULLp> pdu);

      bool
      EnqueueSO (Ptr<Face> face, Address dst, Ptr<SO> pdu);

      bool
      EnqueueDO (Ptr<Face> face, Address dst, Ptr<DO> pdu);

      bool
      EnqueueDU (Ptr<Face> face, Address dst, Ptr<DU> pdu);

      bool
      EnqueueEN (Ptr<Face> face, Address dst, Ptr<EN> pdu);

      bool
      EnqueueAEN (Ptr<Face> face, Address dst,Ptr<AEN> pdu);

      bool
      EnqueueOEN (Ptr<Face> face, Address dst,Ptr<OEN> pdu);

      bool
      EnqueueREN (Ptr<Face> face, Address dst,Ptr<REN> pdu);

      bool
      EnqueueDEN (Ptr<Face> face, Address dst,Ptr<DEN> pdu);

      bool
      EnqueueADEN (Ptr<Face> face, Address dst,Ptr<ADEN> pdu);

      bool
      EnqueueINF (Ptr<Face> face, Address dst,Ptr<INF> pdu);

      bool
      EnqueueRHR (Ptr<Face> face, Address dst,Ptr<RHR> pdu);

      bool
      EnqueueOHR (Ptr<Face> face, Address dst, Ptr<OHR> pdu);

      bool
      EnqueueACKP (Ptr<Face> face, Address dst, Ptr<ACKP> pdu);

    protected:

      Ptr<NNPT> m_nnpt;                   ///< \brief Reference to NNPT
      Ptr<NNST> m_nnst;                   ///< \brief Reference to NNST
      Ptr<NNST> m_awaiting_oen_response;  /// \brief Reference to awaiting response, using NNST
      Ptr<NNST> m_awaiting_den_response;  /// \brief Reference to awaiting response, using NNST
      Ptr<NNST> m_waiting_aden_timeout;   /// \brief Reference to waiting for ADEN timeout, using NNST

      Ptr<FaceContainer> m_faces;         ///< \brief List of Faces attached to this node
      Ptr<NamesContainer> m_node_names;   ///< \brief 3N names container for personal names
      Ptr<NamesContainer> m_leased_names; ///< \brief 3N names container for node leased names
      Ptr<NamesContainer> m_handshaken_names; ///< \brief 3N names container for names for which a handshake has taken place
      Ptr<NamesContainer> m_waiting_handshake_timeout;  /// \brief To waiting for handshake timeout using NamesContainer

      Ptr<PDUBuffer> m_node_pdu_buffer;   ///< \brief Buffer for Node using forwarding strategy
      Ptr<MPDUBuffer> m_node_den_buffer;  ///< \brief Buffer for DEN used in disassociation process
      Ptr<PDUQueue> m_node_pdu_queue;         ///< \brief Queue for when a node does not have 3N name

      Ptr<RxBufferContainer> m_node_rxbuffer_container;  ///< \brief Buffer for Rx PDUs
      Ptr<RxBufferContainer> m_node_txbuffer_container;  ///< \brief Buffer for Tx PDUs

      std::set<TxInfo> tx_list;

      pdu_rx_tracker m_node_rx_track;

      std::map <Ptr<const NNNAddress>, Time, PtrNNNComp> m_node_lease_times;

      std::set <Ptr<const NNNAddress>, PtrNNNComp> m_fixed_name_pending;
      std::set <Ptr<Face>, PtrFaceComp> m_returnEN_faces;

      std::set <NNNAddress> m_names_in_ram;
      std::map<uint32_t, EventId> m_aen_end;

      EventId m_oen_timer;
      EventId m_en_timer;
      EventId m_ren_timer;
      EventId m_den_timer;

      bool m_produce3Nnames;

      Time m_3n_last_oen_time;
      Time m_3n_lease_time;
      Time m_3n_retransmit_time;
      Time m_3n_ackgenerate_time;
      Time m_3n_lifetime;
      Time m_3n_backoff_window;
      Time m_3n_wireless_wait;
      Time m_3n_MPL;

      uint32_t m_3n_min_pdu_window;
      uint32_t m_3n_max_pdu_window;
      uint32_t m_3n_default_rate;
      Time m_3n_pdu_window_retx;

      int32_t m_standardMetric;
      uint64_t m_producedNameNumber;

      bool m_on_ren_oen;
      bool m_sent_ren;
      bool m_aden_from_en;

      bool m_AskFixedName;
      bool m_GenerateFixedName;
      bool m_in_transit_redirect;
      bool m_3n_only_buffer;
      bool m_3n_buffer_after_den;

      // Possible lower level protocols supported
      bool m_n1minus_3n_support;  ///< @brief Flag to indicate that the Node can use lower layer 3N functions
      bool m_n1minus_icn_support; ///< @brief Flag to indicate that the Node can use lower layer ICN functions

      bool m_use3NName;    ///< @brief Flag to indicate that the Node will use a 3N name
      bool m_isMobile;     ///< @brief Flag to indicate that the Node is mobile
      bool m_useHR;        ///< @brief Flag to indicate that we are using Handshake routing
      bool m_ask_ACKP;      ///< @brief Flag to indicate that we will use ACKP PDUs for 3N data transfer
      bool m_return_ACKP;  ///< @brief Flag to indicate that the node will accept returning ACKP PDUs
      std::map<int, Address> last_seen_mac;        ///< @brief Map of last seen Addresses for forwarding strategy faces
      std::map<int, Address>::iterator last_seen_mac_it;
      bool m_hasDisenrolled;                        ///< @brief Flag to know if this forwarding strategy is in a disenroll event
      uint32_t m_retransmission_attempts;           ///< @brief Flag to know how many 3N retransmission attempts to try
      bool m_infinite_retransmission;               ///< @brief Flag to make retransmissions infinite
      bool m_buffer_during_disenroll;               ///< @brief Flag to attempt to buffer PDUs heading out during reenroll

      Ptr<UniformRandomVariable> uniRandom;        ///< @brief Uniform random number generator
      Ptr<UniformRandomVariable> backoffRandom;        ///< @brief Uniform random number generator

      FaceSequenceContainer m_sequence_received;
      FaceSequenceContainer m_sequence_sent;

      FlowidReceivedContainer m_flowids_accepted;
      FlowidSentContainer m_flowids_forwarded;

      FaceEventContainer m_flowid_event_keeper;

      std::map<uint32_t, uint64_t> m_face_pdu_counter;

      std::set<uint32_t> m_flowids_received;
      std::set<uint32_t> m_flowids_sent;

      fw_flowid_bimap m_combo_flowids;
      std::map<uint32_t, uint32_t> m_3n_seq;
      std::map<uint32_t, uint32_t> m_3n_window;

      uint32_t m_combos;

      std::map<uint32_t, std::queue<Address> > m_face_3n_address_queue;
      std::map<uint32_t, Ptr<PDUQueue> > m_face_3n_pdu_queue;
      std::map<uint32_t, EventId> m_face_3n_tx_event;
      std::map<uint32_t, Time> m_face_3n_tx_timer;

      std::map<uint32_t, std::queue<Address> > m_face_icn_address_queue;
      std::map<uint32_t, Ptr<PDUQueue> > m_face_icn_pdu_queue;
      std::map<uint32_t, EventId> m_face_icn_tx_event;
      std::map<uint32_t, Time> m_face_icn_tx_timer;

      bool m_useQueues;

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const EN>,
      Ptr<const Face> > m_outENs; ///< @brief trace of outgoing EN

      TracedCallback<Ptr<const EN>,
      Ptr<const Face> > m_inENs; ///< @brief trace of incoming EN

      TracedCallback<Ptr<const EN>,
      Ptr<const Face> > m_dropENs;  ///< @brief trace of dropped EN

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const AEN>,
      Ptr<const Face> > m_outAENs; ///< @brief trace of outgoing AEN

      TracedCallback<Ptr<const AEN>,
      Ptr<const Face> > m_inAENs; ///< @brief trace of incoming AEN

      TracedCallback<Ptr<const AEN>,
      Ptr<const Face> > m_dropAENs;  ///< @brief trace of dropped AEN

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const DEN>,
      Ptr<const Face> > m_outDENs; ///< @brief trace of outgoing DEN

      TracedCallback<Ptr<const DEN>,
      Ptr<const Face> > m_inDENs; ///< @brief trace of incoming DEN

      TracedCallback<Ptr<const DEN>,
      Ptr<const Face> > m_dropDENs;  ///< @brief trace of dropped DEN

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const ADEN>,
      Ptr<const Face> > m_outADENs; ///< @brief trace of outgoing ADEN

      TracedCallback<Ptr<const ADEN>,
      Ptr<const Face> > m_inADENs; ///< @brief trace of incoming ADEN

      TracedCallback<Ptr<const ADEN>,
      Ptr<const Face> > m_dropADENs;  ///< @brief trace of dropped ADEN

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const REN>,
      Ptr<const Face> > m_outRENs; ///< @brief trace of outgoing REN

      TracedCallback<Ptr<const REN>,
      Ptr<const Face> > m_inRENs; ///< @brief trace of incoming REN

      TracedCallback<Ptr<const REN>,
      Ptr<const Face> > m_dropRENs;  ///< @brief trace of dropped REN

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const OEN>,
      Ptr<const Face> > m_outOENs; ///< @brief trace of outgoing OEN

      TracedCallback<Ptr<const OEN>,
      Ptr<const Face> > m_inOENs; ///< @brief trace of incoming OEN

      TracedCallback<Ptr<const OEN>,
      Ptr<const Face> > m_dropOENs;  ///< @brief trace of dropped OEN

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const INF>,
      Ptr<const Face> > m_outINFs; ///< @brief trace of outgoing INF

      TracedCallback<Ptr<const INF>,
      Ptr<const Face> > m_inINFs; ///< @brief trace of incoming INF

      TracedCallback<Ptr<const INF>,
      Ptr<const Face> > m_dropINFs;  ///< @brief trace of dropped INF

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const NULLp>,
      Ptr<const Face> > m_outNULLps; ///< @brief trace of outgoing NULLp

      TracedCallback<Ptr<const NULLp>,
      Ptr<const Face> > m_rxNULLps; /// <@brief trace retransmitted NULLp

      TracedCallback<Ptr<const NULLp>,
      Ptr<const Face> > m_inNULLps; ///< @brief trace of incoming NULLp

      TracedCallback<Ptr<const NULLp>,
      Ptr<const Face> > m_dropNULLps;  ///< @brief trace of dropped NULLp

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const SO>,
      Ptr<const Face> > m_outSOs; ///< @brief Transmitted SOs trace

      TracedCallback<Ptr<const SO>,
      Ptr<const Face> > m_rxSOs; /// <@brief trace retransmitted SO

      TracedCallback<Ptr<const SO>,
      Ptr<const Face> > m_inSOs; ///< @brief trace of incoming SOs

      TracedCallback<Ptr<const SO>,
      Ptr<const Face> > m_dropSOs; ///< @brief trace of dropped SOs

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const DO>,
      Ptr<const Face> > m_outDOs; ///< @brief trace of outgoing DO

      TracedCallback<Ptr<const DO>,
      Ptr<const Face> > m_rxDOs; /// <@brief trace retransmitted DO

      TracedCallback<Ptr<const DO>,
      Ptr<const Face> > m_inDOs; ///< @brief trace of incoming DO

      TracedCallback<Ptr<const DO>,
      Ptr<const Face> > m_dropDOs;  ///< @brief trace of dropped DO

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const DU>,
      Ptr<const Face> > m_outDUs; ///< @brief trace of outgoing DU

      TracedCallback<Ptr<const DU>,
      Ptr<const Face> > m_rxDUs; /// <@brief trace retransmitted DU

      TracedCallback<Ptr<const DU>,
      Ptr<const Face> > m_inDUs; ///< @brief trace of incoming DU

      TracedCallback<Ptr<const DU>,
      Ptr<const Face> > m_dropDUs;  ///< @brief trace of dropped DU

      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const RHR>,
      Ptr<const Face> > m_outRHRs; ///< @brief trace of outgoing RHR

      TracedCallback<Ptr<const RHR>,
      Ptr<const Face> > m_inRHRs; ///< @brief trace of incoming RHR

      TracedCallback<Ptr<const RHR>,
      Ptr<const Face> > m_dropRHRs;  ///< @brief trace of dropped RHR

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const OHR>,
      Ptr<const Face> > m_outOHRs; ///< @brief trace of outgoing OHR

      TracedCallback<Ptr<const OHR>,
      Ptr<const Face> > m_inOHRs; ///< @brief trace of incoming OHR

      TracedCallback<Ptr<const OHR>,
      Ptr<const Face> > m_dropOHRs;  ///< @brief trace of dropped OHR

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const ACKP>,
      Ptr<const Face> > m_outACKPs; ///< @brief trace of outgoing ACKP

      TracedCallback<Ptr<const ACKP>,
      Ptr<const Face> > m_inACKPs; ///< @brief trace of incoming ACKP

      TracedCallback<Ptr<const ACKP>,
      Ptr<const Face> > m_dropACKPs;  ///< @brief trace of dropped ACKP

      ////////////////////////////////////////////////////////////////////

      TracedCallback<Ptr<const NNNAddress> > m_got3Nname;  ///< @bried Trace when the Node obtained a 3N name
      TracedCallback<> m_no3Nname;                         ///< @brief Trace when the Node has lost its 3N name
      TracedCallback<uint32_t> m_canDisassociate;          ///< @brief Trace when Node can disassociate
      TracedCallback<bool> m_mobility;                     ///< @brief Trace when Node is mobile
      TracedCallback<Time> m_lifetimeChange;               ///< @brief When 3N PDU is changed
      TracedCallback<> m_PoAAssociation;                   ///< @brief Trace when the Node has successfully changed PoA
      TracedCallback<uint32_t> m_FlowIdElimination;                ///< @brief Trace when a Flowid expires

    private:
      // Number generator
      boost::random::mt19937_64 gen;

      bool
      IsWaitingForNextWindowACK (RxPDU data);

      bool
      IsWaitingForPrevWindowACK (RxPDU data);

      bool
      IsACKPOutOfOrder (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t sequence);

      bool
      CancelFlowidWindowWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      bool
      CancelWindowWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t sequence);

      void
      CancelAndEliminateWindowWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid);

      void
      CancelAndEliminateReceivedFlowids (Ptr<Face> face, Address dst);

      void
      CancelAndEliminateAllSentFlowids (Ptr<Face> face);

      void
      ResetFlowidSenderInfo (uint32_t flowid, uint32_t face_id, uint16_t dst_id, uint32_t ini_sequence, uint32_t window);

      void
      ResetReTxNumSenderInfo (uint32_t flowid, uint32_t face_id, uint16_t dst_id);

      void
      IncreaseReTxNumSenderInfo (uint32_t flowid, uint32_t face_id, uint16_t dst_id);

      bool
      CanReTxFlowidWindow (uint32_t flowid, uint32_t face_id, uint16_t dst_id);

      void
      MigrateAllSentFlowids (Ptr<Face> face);

      int8_t
      SendDATAPDU (RxPDU data, Ptr<DATAPDU> pdu);

      int8_t
      SendDATAPDUToApp (Ptr<Face> face, Ptr<DATAPDU> pdu);

      int8_t
      CheckSendDATAPDU (RxPDU data, Ptr<DATAPDU> pdu, bool retx);

      void
      TransmitNext3NWindow (Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence);

      void
      EliminateSentFlowid (uint32_t face, uint16_t dst, uint32_t flowid);

      void
      EliminateReceivedFlowid (uint32_t face, uint16_t dst, uint32_t flowid);

      static void
      PrintPDUSets (uint32_t flowid, std::set<uint32_t> t, uint32_t start, uint32_t end, std::string msg, bool error = false);

      void
      Forward3NPDU (Ptr<Face> face);

      void
      ForwardICNPDU (Ptr<Face> face);
    };
  } // namespace nnn
} // namespace ns3

#endif /* NNN_FORWARDING_STRATEGY_H */
