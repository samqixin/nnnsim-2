/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2018 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-icn-flooding.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-icn-flooding.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-icn-flooding.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-icn-flooding.h"
#include "ns3/log.h"
#include "ns3/nnn-addr-aggregator.h"
#include "ns3/nnn-face.h"
#include "ns3/nnn-face-container.h"
#include "ns3/nnn-rxbuffercontainer.h"
#include "ns3/nnn-fib.h"
#include "ns3/nnn-fib-entry.h"
#include "ns3/nnn-tfib.h"
#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-icn-content-store.h"
#include "ns3/nnn-icn-header-helper.h"
#include "ns3/nnn-icn-naming.h"
#include "ns3/nnn-icn-pdus.h"
#include "ns3/nnn-names-container.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-nnnsim-icn-wire.h"
#include "ns3/nnn-nnpt.h"
#include "ns3/nnn-nnst.h"
#include "ns3/nnn-nnst-entry.h"
#include "ns3/nnn-nnst-entry-facemetric.h"
#include "ns3/nnn-pdu-buffer.h"
#include "ns3/nnn-rxbuffercontainer.h"
#include "ns3/nnn-pdus.h"
#include "ns3/nnn-pit-entry-incoming-face.h"
#include "ns3/nnn-pit-entry-outgoing-face.h"
#include "ns3/nnn-wire.h"
#include "ns3/wire-nnnsim-naming.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.fw.icn.f");

  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (ICNFlooding);

    TypeId
    ICNFlooding::GetTypeId ()
    {
      static TypeId tid = TypeId ("ns3::nnn::ICNFlooding")
     					  .SetGroupName ("Nnn")
					  .SetParent<ICN3NForwardingStrategy> ()
					  .AddConstructor<ICNFlooding> ()
					  ;
      return tid;
    }

    ICNFlooding::ICNFlooding ()
    {
      m_Enable_Flooding = true;
      m_Enable_3N_face_time_select = false;
      m_Enable_FIB_part_select = false;
      m_nacksEnabled = false;
    }

    ICNFlooding::~ICNFlooding ()
    {
    }

    void
    ICNFlooding::UpdatePITEntry (Ptr<pit::Entry> pitEntry, Ptr<DATAPDU> pdu,
				 Ptr<Face> face, Time lifetime)
    {
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () << " Face: " << face->GetId ());

      if (pdu == 0)
	{
	  // Received a raw ICN PDU
	  pitEntry->AddIncoming(face, true);
	  pitEntry->UpdateLifetime (lifetime);
	}
      else
	{
	  pitEntry->AddIncoming(face);
	  pitEntry->UpdateLifetime (lifetime);
	}
    }

    void
    ICNFlooding::UpdateLayeredPITEntry (Ptr<pit::Entry> pitEntry,
					Ptr<DATAPDU> pdu, Ptr<Face> face,
					Time lifetime, const Address& from)
    {
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << pitEntry->GetPrefix ().get (-1).toSeqNum () << " Face: " << face->GetId ());

      if (!pdu)
	{
	  NS_LOG_INFO ("Saving Interest from " << face->GetId () << " PoA: " << from);
	  pitEntry->AddIncoming(face, from, true, false);
	  pitEntry->UpdateLifetime (lifetime);
	}
      else
	{
	  NS_LOG_INFO ("Saving Interest from " << face->GetId () << " PoA: " << from << " in PDU and no 3N SRC");
	  pitEntry->AddIncoming (face, from, false, true);
	  pitEntry->UpdateLifetime (lifetime);
	}
    }

    void
    ICNFlooding::UpdateFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface,
				 Ptr<const icn::Name> interest)
    {
      NS_LOG_FUNCTION (inface->GetId () << interest->toUri ());

      // Cut the Interest to have only the basic data (we assume that the ending is the sequence as in ndnSIM)
      // Does not handle icn::Name postfix (which has been deactivated from Producer)
      icn::Name dirname = interest->getSubName (0, interest->size()-1);
      Ptr<icn::Name> ptr_dirname = Create<icn::Name> (dirname);

      Ptr<fib::Entry> fibEntry;

      //      NS_LOG_DEBUG ("Initial " << *m_fib);

      NS_LOG_INFO ("Updating FIB for " << dirname);

      fibEntry = m_fib->Find (dirname);

      if (!fibEntry)
	{
	  NS_LOG_INFO ("No FIB found for " << dirname << " will create");
	  // I am heavily assuming that an application would have already
	  // installed an FIB entry. If an entry from an Application using 3N
	  // isn't stopped, then it might cause loops
	  if (!inface->isAppFace())
	    {
	      NS_LOG_DEBUG ("Adding information for " << dirname << " via Face " << inface->GetId ());
	      fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric);
	    }
	  else
	    {
	      NS_LOG_WARN ("Received a ICN packet from an application. Probably a push. Please verify if this is correct");
	    }
	}
      else
	{
	  fibEntry->AddOrUpdateRoutingMetric(inface, m_icn_fib_metric);
	}
      //      NS_LOG_DEBUG ("Final " << *m_fib);
    }

    void
    ICNFlooding::UpdateLayeredFIBEntry (Ptr<DATAPDU> pdu, Ptr<Face> inface,
					Ptr<const icn::Name> interest,
					const Address& from, const Address& to)
    {
      NS_LOG_FUNCTION (inface->GetId () << interest->toUri () << from << to);

      Ptr<fib::Entry> fibEntry;

      //      NS_LOG_DEBUG ("Initial " << *m_fib);

      // Cut the Interest to have only the basic data (we assume that the ending is the sequence as in ndnSIM)
      // Does not handle icn::Name postfix (which has been deactivated from Producer)
      icn::Name dirname = interest->getSubName (0, interest->size()-1);
      Ptr<icn::Name> ptr_dirname = Create<icn::Name> (dirname);

      NS_LOG_INFO ("Updating FIB for " << dirname);

      fibEntry = m_fib->Find (dirname);

      if (!fibEntry)
	{
	  NS_LOG_INFO ("No FIB found for " << dirname << " will create");
	  // I am heavily assuming that an application would have already
	  // installed an FIB entry. If an entry from an Application using 3N
	  // isn't stopped, then it might cause loops
	  if (!inface->isAppFace())
	    {
	      NS_LOG_DEBUG ("Adding information for " << dirname << " via Face " << inface->GetId () << " PoA: " << from);
	      fibEntry = m_fib->Add (dirname, inface, m_icn_fib_metric, from);
	    }
	  else
	    {
	      NS_LOG_WARN ("Received a ICN packet from an application. Probably a push. Please verify if this is correct");
	    }
	}
      else
	{
	  fibEntry->UpdatePoADestination(inface, from, m_icn_fib_metric);
	}
      //      NS_LOG_DEBUG ("Final " << *m_fib);
    }

    void
    ICNFlooding::ProcessLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				      Ptr<Interest> mapme, const Address& from)
    {
      NS_LOG_FUNCTION (this);

      // Log the Interest packet
      m_inInterests (mapme, inFace);

      // Verify if the TFIB is enabled
      if (m_EnableTFIB)
	{
	  if (mapme->GetInterestType () == Interest::MAP_ME_INTEREST_UPDATE)
	    {
	      NS_LOG_INFO ("Received an Interest with MapMe Update via Face " << inFace->GetId () << " PoA: " << from);
	      NS_LOG_INFO ("Updating TFIB and FIB for " << mapme->GetName () << " with sequence " << mapme->GetSequenceNumber ());

	      Ptr<tfib::Entry> tfib_entry = m_tfib->Find (mapme->GetName ());

	      NS_LOG_DEBUG ("Previous " << *m_tfib);
	      bool forwardMapMe = false;
	      // If we find no information in the TFIB for this information, we
	      // can just go ahead and add the information to the FIB
	      if (tfib_entry == 0)
		{
		  NS_LOG_INFO ("Completely new info for prefix: " << mapme->GetName ());
		  NS_LOG_DEBUG ("Introducing name: " << mapme->GetName () << " Face: " << inFace->GetId () << " PoA: " << from);
		  NS_LOG_DEBUG ("Sequence: " << mapme->GetSequenceNumber () << " Retransmission: " << mapme->GetRetransmissionTime ());
		  NS_LOG_DEBUG ("Part: " << mapme->GetPartNumber ());

		  m_tfib->Add(inFace, from, mapme, 0);

		  forwardMapMe = true;
		}
	      else
		{
		  if (tfib_entry->GetNewestSequence () < mapme->GetSequenceNumber ())
		    {
		      NS_LOG_INFO ("Updating new information for prefix: " << mapme->GetName () << " PoA: " << from);
		      NS_LOG_DEBUG ("New sequence for for prefex: " << mapme->GetName () << " is: " << mapme->GetSequenceNumber ());
		      // Found new information, updating TFIB
		      tfib_entry->AddOrUpdate(inFace, from, mapme, 0);

		      forwardMapMe = true;
		    }
		  else
		    {
		      NS_LOG_WARN ("Obtained an MapMe Interest with an outdated sequence number");
		      m_dropInterests (mapme, inFace);
		    }
		}

	      NS_LOG_DEBUG ("Current " << *m_tfib);

	      // If we have new information, forward the Interest with MapMe information
	      if (forwardMapMe)
		{
		  // Now set up the ACKs and forward the information
		  // Search for the temporary PIT with the Interest
		  Ptr<pit::Entry> mapme_ack_Entry = m_awaiting_mapme_acks->Lookup (*mapme);
		  if (mapme_ack_Entry == 0)
		    {
		      mapme_ack_Entry = m_awaiting_mapme_acks->Create (mapme);
		      if (mapme_ack_Entry != 0)
			{
			  NS_LOG_INFO ("Created entry to wait for MapMe ACK for " << mapme->GetName () << " SeqN: " << mapme->GetPartNumber());
			}
		      else
			{
			  NS_LOG_WARN ("Couldn't create entry to wait for MapMe ACK for " << mapme->GetName () << " SeqN: " << mapme->GetPartNumber());
			  return;
			}

		      UpdateLayeredPITEntry (mapme_ack_Entry, pdu, inFace, Seconds (1.0), from);
		    }
		  else
		    {
		      NS_LOG_WARN ("We already have an ACK waiting for " << mapme->GetName() << " SeqN: " << mapme->GetPartNumber() << " check logs");
		    }

		  // Transmit a MapMe Interest packet
		  RetransmitLayeredMapMe (pdu, inFace, mapme, from);

		  // Create a MapMe ACK to deal with the Awaiting PIT structure
		  TransmitLayeredMapMeAck(pdu, mapme_ack_Entry->GetInterest (), inFace, from);
		}
	    }
	  else if (mapme->GetInterestType () == Interest::MAP_ME_INTEREST_UPDATE_ACK)
	    {
	      NS_LOG_INFO ("Received an Interest with MapMe ACK via Face " << inFace->GetId () << " PoA: " << from);
	      NS_LOG_INFO ("Obtained an ACK for Interest MapMe: " << mapme->GetName ());

	      LayeredMapMeAckProcess(pdu, inFace, mapme, from);
	    }
	  else
	    {
	      NS_LOG_WARN ("Obtained an MapMe Interest packet with unknown type!");
	      m_dropInterests (mapme, inFace);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Obtained an MapMe Interest packet, but node does not support TFIB");
	  m_dropInterests (mapme, inFace);
	}
    }

    void
    ICNFlooding::LayeredMapMeAckProcess (Ptr<DATAPDU> pdu, Ptr<Face> face,
					 Ptr<Interest> mapme,
					 const Address& from)
    {
      NS_LOG_FUNCTION (this);

      Ptr<pit::Entry> pitEntry = m_awaiting_mapme_acks->Lookup (*mapme);

      if (pitEntry != 0)
	{
	  Ptr<tfib::Entry> tfib_entry = m_tfib->Find(mapme->GetName ());

	  if (tfib_entry == 0)
	    {
	      NS_LOG_WARN ("Did not find a TFIB entry for " << mapme->GetName ());
	      return;
	    }

	  uint32_t curr_seq = mapme->GetSequenceNumber ();

	  NS_LOG_DEBUG ("Current " << *m_tfib);

	  if (!tfib_entry->HasSequence(curr_seq))
	    {
	      NS_LOG_WARN ("Interest MapMe ACK is using a SeqN: " << curr_seq << " for which we have no information, returning");
	      return;
	    }
	  else
	    {
	      uint32_t nst_seq = tfib_entry->GetNewestSequence();
	      if (curr_seq != nst_seq)
		{
		  NS_LOG_WARN ("Interest MapMe ACK received with SeqN: " << curr_seq << " but newest is SeqN: " << nst_seq << " verify");
		}
	      else
		{
		  NS_LOG_DEBUG ("Interest MapMe ACK is using a SeqN: " << curr_seq);
		}
	    }

	  NS_LOG_DEBUG ("Removing Face " << face->GetId () << " from Interest MapMe ACK waiting list for " << mapme->GetName ());
	  // Remove the MapMe information from the PIT
	  pitEntry->RemoveOutgoing(face);

	  icn::Name mapmed_name = mapme->GetName ().getBaseName ();

	  if (pitEntry->GetOutgoingCount() == 0)
	    {
	      NS_LOG_DEBUG ("Have obtained all Interest MapMe ACKs, removing entries");
	      // If we have obtained all the ACKs we were waiting for, mark for erase
	      pitEntry->ClearIncoming ();
	      m_awaiting_mapme_acks->MarkErased(pitEntry);

	      // Having ACKed all MapMe information, proceed to update FIB
	      Ptr<fib::Entry> fib_entry = m_fib->Find(mapmed_name);

	      NS_LOG_DEBUG ("Previous " << *m_fib);

	      if (fib_entry == 0)
		{
		  NS_LOG_DEBUG ("Did not find information for " << mapmed_name << " introducing to FIB");
		  // Didn't find an exact match, introduce it
		  m_fib->Add(mapmed_name, tfib_entry->GetFace (curr_seq), m_icn_fib_metric, tfib_entry->GetPoA (curr_seq));
		}
	      else
		{
		  NS_LOG_DEBUG ("Found match for " << mapmed_name << " updating FIB PoA: " << tfib_entry->GetPoA (curr_seq));
		  // Found an exact match, update it
		  fib_entry->UpdatePoADestination(tfib_entry->GetFace (curr_seq), tfib_entry->GetPoA (curr_seq), m_icn_fib_metric);
		}

	      NS_LOG_DEBUG ("Current " << *m_fib);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Still waiting for " << pitEntry->GetOutgoingCount () << " ACKs for SeqN: " << curr_seq);
	    }
	}
      else
	{
	  NS_LOG_WARN ("We are not waiting for an ACK for MapMe: " << mapme->GetName ());
	  m_dropInterests (mapme, face);
	}
    }

    void
    ICNFlooding::RetransmitLayeredMapMe (Ptr<DATAPDU> pdu, Ptr<Face> face,
					 Ptr<Interest> mapme,
					 const Address& from)
    {
      NS_LOG_FUNCTION (this);
      // Check if the Entry continues to exist in the structure
      NS_LOG_DEBUG ("Verifying that we are waiting for an ACK for " << mapme->GetName ());
      Ptr<pit::Entry> mapme_ack_Entry = m_awaiting_mapme_acks->Lookup (*mapme);

      Ptr<Face> tmp;
      bool ok = false;

      if (mapme_ack_Entry != 0)
	{
	  NS_LOG_DEBUG ("We are waiting for a ACK, beginning forwarding");
	  // Forward the MapMe information
	  if (!pdu)
	    {
	      NS_LOG_INFO ("Attempting to send MapMe directly in ICN");
	      // This is a special case because the FIB was already modified when the
	      // Application was attached to the node
	      if (face->isAppFace())
		{
		  NS_LOG_INFO ("Received the MapMe Interest from Application");
		  // Transmit the MapMe through all the interfaces not of Application type
		  for (uint32_t i = 0; i < m_faces->GetN (); i++)
		    {
		      tmp = m_faces->Get (i);
		      // Check that the Face is not of type APPLICATION
		      if (!tmp->isAppFace ())
			{
			  if (tmp->IsNetworkCompatibilityEnabled ("ICN"))
			    {
			      NS_LOG_INFO ("Sending Interest MapMe Update via face " << tmp->GetId ());
			      ok = tmp->SendInterest (mapme);

			      if (!ok)
				{
				  m_dropInterests (mapme, tmp);
				  NS_LOG_DEBUG ("Cannot satisfy Interest MapMe Update to " << tmp->GetId ());
				}
			      else
				{
				  // Log that a Interest packet was sent
				  m_outInterests (mapme, tmp);
				  mapme_ack_Entry->AddOutgoing (tmp);
				}
			    }
			  else
			    {
			      NS_LOG_WARN ("Attempted to send raw ICN through unsupported Face");
			    }
			}
		    }
		}
	      else
		{
		  NS_LOG_INFO ("Received the MapMe Interest from another node");
		  Ptr<icn::Name> mapmed_name = Create<icn::Name> (mapme->GetName ().getBaseName ());
		  NS_LOG_DEBUG ("Searching in FIB for " << *mapmed_name);
		  NS_LOG_DEBUG ("Current " << *m_fib);

		  Ptr<fib::Entry> fib_entry = m_fib->LongestPrefixMatch(mapmed_name);

		  if (!fib_entry)
		    {
		      NS_LOG_DEBUG ("No information in FIB for " << *mapmed_name << " returning");
		      return;
		    }

		  // Here we pick the next place to forward to using the ICN strategy
		  BOOST_FOREACH (const fib::FaceMetric &metricFace, fib_entry->m_faces.get<fib::i_metric> ())
		  {
		    tmp = metricFace.GetFace ();

		    if (tmp->isAppFace())
		      {
			continue;
		      }

		    if (tmp->GetId () == face->GetId ())
		      {
			continue;
		      }

		    if (tmp->IsNetworkCompatibilityEnabled ("ICN"))
		      {
			NS_LOG_INFO ("Sending MapMe Interest Update via Face " << tmp->GetId ());
			ok = tmp->SendInterest (mapme);

			if (!ok)
			  {
			    m_dropInterests (mapme, tmp);
			    NS_LOG_DEBUG ("Cannot satisfy Interest to " << tmp->GetId ());
			  }
			else
			  {
			    // Log that a Interest packet was sent
			    m_outInterests (mapme, tmp);
			    mapme_ack_Entry->AddOutgoing (tmp);
			  }
		      }
		    else
		      {
			NS_LOG_WARN ("Attempted to send raw ICN through unsupported Face");
		      }
		  }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Attempting to send MapMe in 3N PDU");
	      NS_LOG_DEBUG ("Serializing the Interest");
	      // Encode the packet
	      Ptr<Packet> retPkt = icn::Wire::FromInterest(mapme, icn::Wire::WIRE_FORMAT_NDNSIM);

	      Address t_poa;

	      if (face->isAppFace())
		{
		  // This is a special case because the FIB was already modified when the
		  // Application was attached to the node
		  NS_LOG_INFO ("Received the Interest MapMe Update from Application");

		  NS_LOG_INFO ("Creating NULLp in which to send MapMe Interest");
		  Ptr<NULLp> nullp_o = Create<NULLp> ();
		  nullp_o->SetPDUPayloadType (ICN_NNN);
		  nullp_o->SetLifetime (m_3n_lifetime);
		  nullp_o->SetPayload (retPkt);

		  // Transmit the MapMe through all the interfaces not of Application type
		  for (uint32_t i = 0; i < m_faces->GetN (); i++)
		    {
		      tmp = m_faces->Get (i);
		      // Check that the Face is not of type APPLICATION
		      if (tmp->isAppFace ())
			{
			  continue;
			}

		      // We can't return the packet to the same Face
		      if (face->GetId () == tmp->GetId ())
			{
			  continue;
			}

		      // In this case, we have no PoA information, so send in broadcast
		      NS_LOG_INFO ("Sending Interest MapMe Update in NULLp via Face " << tmp->GetId ());
		      ok = tmp->SendNULLp (nullp_o);

		      if (!ok)
			{
			  m_dropInterests (mapme, tmp);
			  m_dropNULLps (nullp_o, tmp);
			  NS_LOG_DEBUG ("Cannot satisfy data to " << tmp->GetId ());
			}
		      else
			{
			  NS_LOG_DEBUG ("Transmitted NULLp with MapMe Update via Face " << tmp->GetId ());
			  m_outNULLps (nullp_o, tmp);
			  m_outInterests (mapme, tmp);
			  mapme_ack_Entry->AddOutgoing (tmp);
			}
		    }
		}
	      else
		{
		  NS_LOG_INFO ("Received the MapMe Interest from another node");
		  Ptr<icn::Name> mapmed_name = Create<icn::Name> (mapme->GetName ().getBaseName ());
		  NS_LOG_DEBUG ("Searching in FIB for " << *mapmed_name);
		  NS_LOG_DEBUG ("Current " << *m_fib);

		  Ptr<fib::Entry> fib_entry = m_fib->LongestPrefixMatch(mapmed_name);
		  if (!fib_entry)
		    {
		      NS_LOG_DEBUG ("No information in FIB for " << *mapmed_name << " returning");
		      return;
		    }

		  Ptr<NULLp> nullp_o;
		  Ptr<DATAPDU> pdu_o;

		  NS_LOG_INFO ("Creating NULLp in which to send MapMe Interest");
		  nullp_o = Create<NULLp> ();
		  nullp_o->SetPDUPayloadType (ICN_NNN);
		  nullp_o->SetLifetime (m_3n_lifetime);
		  nullp_o->SetPayload (retPkt);

		  pdu_o = DynamicCast <DATAPDU> (nullp_o);

		  // Here we pick the next place to forward to using the ICN strategy
		  BOOST_FOREACH (const fib::FaceMetric &metricFace, fib_entry->m_faces.get<fib::i_metric> ())
		  {
		    tmp = metricFace.GetFace ();
		    t_poa = metricFace.GetBestPoA();

		    if (tmp->isAppFace())
		      {
			continue;
		      }

		    if (tmp->GetId () == face->GetId ())
		      {
			continue;
		      }

		    NS_LOG_INFO ("Sending MapMe Interest MapMe Update in NULLp via Face " << tmp->GetId () << " PoA: " << t_poa);

		    bool normalSend = true;
		    // Double check Address is valid
		    if (t_poa.IsInvalid())
		      {
			if (m_ask_ACKP)
			  {
			    std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), tmp);

			    if (!ret.empty())
			      {
				normalSend = false;
				int8_t n3ok = txPreparation (pdu_o, tmp, tmp->GetBroadcastAddress(), false);
				ok = (n3ok >= 0);
			      }
			    else
			      {
				NS_LOG_WARN ("Face " << tmp->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
				SetupFlowidSeq(pdu_o, tmp, tmp->GetBroadcastAddress (), false);
				UpdateSentPDUs(pdu_o, tmp, tmp->GetBroadcastAddress (), false);
			      }
			  }
			else
			  {
			    txReset(pdu_o);
			  }

			NS_LOG_DEBUG ("PoA information in FIB seems invalid, sending in broadcast");
			if (normalSend)
			  {
			    ok = tmp->SendNULLp (nullp_o);

			    UpdatePDUCountersB(pdu_o, tmp, ok, false);
			  }
		      }
		    else
		      {
			if (m_ask_ACKP)
			  {
			    std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), tmp);

			    if (!ret.empty())
			      {
				normalSend = false;
				int8_t n3ok = txPreparation (pdu_o, tmp, t_poa, false);
				ok = (n3ok >= 0);
			      }
			    else
			      {
				NS_LOG_WARN ("Face " << face->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
				SetupFlowidSeq(pdu_o, tmp, t_poa, false);
				UpdateSentPDUs(pdu_o, tmp, t_poa, false);
			      }
			  }
			else
			  {
			    txReset(pdu_o);
			  }

			if (normalSend)
			  {
			    ok = tmp->SendNULLp (nullp_o, t_poa);

			    UpdatePDUCountersB(pdu_o, tmp, ok, false);
			  }
		      }

		    if (!ok)
		      {
			m_dropInterests (mapme, tmp);
		      }
		    else
		      {
			NS_LOG_DEBUG ("Transmitted NULL with Interest MapMe Update via Face " << tmp->GetId ());
			m_outInterests (mapme, tmp);
			mapme_ack_Entry->AddOutgoing (tmp);
		      }
		  }
		}
	    }

	  mapme_ack_Entry->IncreaseCurrentRetransmissions();
	  uint32_t currTransmissions = mapme_ack_Entry->GetCurrentRetransmissions ();
	  NS_LOG_DEBUG ("Current number of MapMe attempts for: " << mapme->GetName() << " at: " << currTransmissions);

	  if (currTransmissions <= m_mapme_retrys)
	    {
	      // Schedule retransmission of MapMe should we get no ACK
	      NS_LOG_DEBUG ("Scheduling retransmission of MapMe Interest, should we get no ACK in " << mapme->GetRetransmissionTime());
	      Simulator::Schedule (mapme->GetRetransmissionTime(), &ICNFlooding::RetransmitLayeredMapMe, this, pdu, face, mapme, from);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Exceeded permitted number of Interest with MapMe info retries at: " << m_mapme_retrys);

	      NS_LOG_DEBUG ("Clearing Faces which were waiting for ACKs");
	      mapme_ack_Entry->ClearOutgoing();

	      LayeredMapMeAckProcess (pdu, face, mapme, from);
	    }
	}
      else
	{
	  NS_LOG_INFO (mapme->GetName () << " MapMe Interest, has been acked, cancelling retransmission");
	  return;
	}
    }

    void
    ICNFlooding::TransmitLayeredMapMeAck (Ptr<DATAPDU> pdu,
					  Ptr<const Interest> mapme,
					  Ptr<Face> face, const Address& from)
    {
      NS_LOG_FUNCTION (this);

      if (face->isAppFace())
	{
	  NS_LOG_INFO ("Received ACK from Application, no need to ACK");
	  return;
	}

      Ptr<Interest> interest = Create<Interest> ();
      interest->SetNonce               (uniRandom->GetValue());
      interest->SetName                (mapme->GetName ());
      interest->SetInterestLifetime    (mapme->GetInterestLifetime ());
      interest->SetInterestType        (Interest::MAP_ME_INTEREST_UPDATE_ACK);
      interest->SetSequenceNumber      (mapme->GetSequenceNumber ());
      interest->SetRetransmissionTime  (mapme->GetRetransmissionTime ());
      interest->SetPartNumber          (mapme->GetPartNumber ());

      NS_LOG_INFO ("Transmitting Interest MapMe ACK with name: " << mapme->GetName () << " SeqN: " << mapme->GetSequenceNumber () << " PoA: " << from);
      NS_LOG_DEBUG ("Attempting to transmit Interest MapMe ACK via Face " << face->GetId ());
      bool ok = false;

      // Check whether we obtained a Raw ICN packet or a 3N PDU
      if (pdu == 0)
	{
	  NS_LOG_DEBUG ("Checking if capable of direct ICN transmission");
	  if (face->IsNetworkCompatibilityEnabled ("ICN"))
	    {
	      ok = face->SendInterest (interest, from);
	      if (ok)
		{
		  NS_LOG_DEBUG ("Transmitted Interest MapMe ACK via Face " << face->GetId ());
		  m_outInterests (interest, face);
		}
	      else
		{
		  NS_LOG_DEBUG ("Could not transmit Interest MapMe ACK via Face " << face->GetId ());
		  m_dropInterests (interest, face);
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("Received a raw ICN packet, but don't have one capable of transmission. Check code");
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Encoding Interest packet");
	  // Encode the packet
	  Ptr<Packet> retPkt = icn::Wire::FromInterest(interest, icn::Wire::WIRE_FORMAT_NDNSIM);

	  NS_LOG_DEBUG ("Creating NULLp for transmission");
	  Ptr<NULLp> null_p_o = Create<NULLp> ();
	  // Set the lifetime of the 3N PDU
	  null_p_o->SetLifetime (m_3n_lifetime);
	  // Configure payload for PDU
	  null_p_o->SetPayload (retPkt);
	  // Signal that the PDU had an ICN PDU as payload
	  null_p_o->SetPDUPayloadType (ICN_NNN);

	  bool normalSend = true;

	  if (m_ask_ACKP)
	    {
	      std::vector<Ptr<const NNNAddress> > ret = m_nnst->OneHopNameInfo(GetNode3NNamePtr(), face);

	      if (!ret.empty())
		{
		  normalSend = false;
		  int8_t n3ok = txPreparation (null_p_o, face, from, false);
		  ok = (n3ok >= 0);
		}
	      else
		{
		  NS_LOG_WARN ("Face " << face->GetId () << " has no apparent 3N nodes, not expecting ACKPs");
		  SetupFlowidSeq (null_p_o, face, from, false);
		  UpdateSentPDUs(null_p_o, face, from, false);
		}
	    }
	  else
	    {
	      txReset(null_p_o);
	    }

	  if (normalSend)
	    {
	      // Send out the NULL PDU
	      ok = face->SendNULLp (null_p_o, from);

	      UpdatePDUCountersB(null_p_o, face, ok, false);
	    }

	  if (!ok)
	    {
	      m_dropInterests (interest, face);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Transmitted NULLp with Interest MapMe ACK via Face " << face->GetId ());
	      m_outInterests (interest, face);
	    }
	}
    }

    void
    ICNFlooding::SatisfyPendingInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
					 Ptr<const Data> data,
					 Ptr<pit::Entry> pitEntry)
    {
      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " Satisfying pending Interests for " << data->GetName());

      if (inFace != 0)
	pitEntry->RemoveIncoming (inFace);

      if (m_isMobile)
	NS_LOG_INFO ("Node is mobile");

      // Convert the Data PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);

      // Check where the Data PDU came from and divide them as such
      if (inFace == 0)
	{
	  NS_LOG_INFO ("On " << myAddr << " satisfying Interest from local CS");

	  BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	    uint32_t incomingID = incoming.m_face->GetId ();
	    bool sawRawICN = incoming.SawRawICN ();

	    if (incoming.m_face->isAppFace ())
	      NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);

	    NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);
	    TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);

	    // In case we have seen raw ICN PDUs
	    if (sawRawICN)
	      {
		NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID);
		TryToSendRawData(inFace, incoming.m_face, data, pitEntry);
	      }
	  }
	}
      else
	{
	  NS_LOG_INFO ("On " << myAddr << " received PDU is satisfying Interest");

	  BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	    uint32_t incomingID = incoming.m_face->GetId ();
	    bool sawRawICN = incoming.SawRawICN ();

	    if (incoming.m_face->isAppFace ())
	      NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);

	    NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID);
	    TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face, data, pitEntry);

	    // In case we have seen raw ICN PDUs
	    if (sawRawICN)
	      {
		NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID);

		TryToSendRawData (inFace, incoming.m_face, data, pitEntry);
	      }
	  }
	}

      NS_LOG_INFO ("Finished satisfying, clearing PIT Entry");

      // All incoming interests are satisfied. Remove them
      pitEntry->ClearIncoming ();

      // Remove all outgoing faces
      pitEntry->ClearOutgoing ();

      // Set pruning timeout on PIT entry (instead of deleting the record)
      m_pit->MarkErased (pitEntry);
    }

    void
    ICNFlooding::SatisfyPITEntryFromCache (Ptr<DATAPDU> pdu,
					   Ptr<Face> inFace, // 0 allowed (from cache)
					   Ptr<const Data> data, Ptr<pit::Entry> pitEntry)
    {
      // Convert the Data PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);

      BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	bool isAppFace = false;
	uint32_t incomingID = incoming.m_face->GetId ();
	std::map<Address,pit::PoAInInfo> poa_map = incoming.GetPoADestinations();
	std::map<Address,pit::PoAInInfo>::iterator it;

	if (incoming.m_face->isAppFace ())
	  {
	    NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);
	    isAppFace = true;
	  }

	for (it = poa_map.begin (); it != poa_map.end (); ++it)
	  {
	    if (isAppFace)
	      {
		NS_LOG_INFO ("Satisfying using NULL out App Face " << incomingID);
		TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
				     data, pitEntry);
	      }
	    else
	      {
		NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID <<
			     " PoA: " << it->first);
		TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
					  data, pitEntry, it->first);
	      }

	    // In case we have seen raw ICN PDUs
	    if ((it->second).m_raw_icn)
	      {
		if (isAppFace)
		  {
		    NS_LOG_INFO ("Satisfying using raw ICN Data out App Face " << incomingID);
		    TryToSendRawData (inFace, incoming.m_face, data, pitEntry);
		  }
		else
		  {
		    NS_LOG_INFO ("Attempting to satisfy using raw ICN Data out Face " << incomingID <<
				 " PoA: " << it->first);
		    TryToSendRawDataToPoA (inFace, incoming.m_face,
					   data, pitEntry, it->first);
		  }
	      }
	  }
      }
    }

    void
    ICNFlooding::SatisfyPendingLayeredInterest (Ptr<DATAPDU> pdu,
						Ptr<Face> inFace, // 0 allowed (from cache)
						Ptr<const Data> data, Ptr<pit::Entry> pitEntry)
    {
      NNNAddress myAddr = GetNode3NName ();

      NS_LOG_INFO ("On " << myAddr << " Satisfying pending Interests for " << data->GetName() << " SeqNo " << data->GetName ().get (-1).toSeqNum ());

      if (inFace != 0)
	pitEntry->RemoveIncoming (inFace);

      if (m_isMobile)
	NS_LOG_INFO ("Node is mobile");

      // Convert the Data PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData (data);

      // Check where the Data PDU came from and divide them as such
      if (inFace == 0)
	{
	  SatisfyPITEntryFromCache(pdu, inFace, data, pitEntry);
	}
      else
	{
	  NS_LOG_INFO ("On " << myAddr << " received PDU is satisfying Interest");

	  BOOST_FOREACH(const pit::IncomingFace &incoming, pitEntry->GetIncoming ()) {

	    bool isAppFace = false;
	    uint32_t incomingID = incoming.m_face->GetId ();
	    std::map<Address,pit::PoAInInfo> poa_map = incoming.GetPoADestinations();
	    std::map<Address,pit::PoAInInfo>::iterator it;

	    if (incoming.m_face->isAppFace ())
	      {
		NS_LOG_INFO ("Satisfying an Interest from an Application Face " << incomingID);
		isAppFace = true;
	      }

	    for (it = poa_map.begin (); it != poa_map.end (); ++it)
	      {
		// In case the structure has seen a NULL PDU
		if (isAppFace)
		  {
		    NS_LOG_INFO ("Satisfying using NULLp out App Face " << incomingID);
		    TryToSendDataInNULL (pdu->GetFlowid (), inFace, incoming.m_face,
							 data, pitEntry);
		  }
		else
		  {
		    NS_LOG_INFO ("Satisfying using NULLp out Face " << incomingID <<
				 " PoA: " << it->first);
		    TryToSendDataInNULLToPoA (pdu->GetFlowid (), inFace, incoming.m_face,
					      data, pitEntry, it->first);
		  }

		// In case we have seen raw ICN PDUs
		if ((it->second).m_raw_icn)
		  {
		    if (isAppFace)
		      {
			NS_LOG_INFO ("Satisfying using raw ICN Data out App Face " << incomingID);
			TryToSendRawData (inFace, incoming.m_face, data, pitEntry);
		      }
		    else
		      {
			NS_LOG_INFO ("Attempting to satisfy using ICN Data out Face " << incomingID <<
				     " PoA: " << it->first);
			TryToSendRawDataToPoA (inFace, incoming.m_face,
					       data, pitEntry, it->first);
		      }
		  }
	      }
	  }
	}

      NS_LOG_INFO ("Finished satisfying, clearing PIT Entry");

      // All incoming interests are satisfied. Remove them
      pitEntry->ClearIncoming ();

      // Remove all outgoing faces
      pitEntry->ClearOutgoing ();

      // Set pruning timeout on PIT entry (instead of deleting the record)
      m_pit->MarkErased (pitEntry);
    }

    bool
    ICNFlooding::DoPropagateInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				      Ptr<const Interest> interest,
				      Ptr<pit::Entry> pitEntry)
    {
      NS_LOG_FUNCTION (this);
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << interest->GetName ().get (-1).toSeqNum () << " Face: " << inFace->GetId ());
      NS_ASSERT_MSG (m_pit != 0, "PIT should be aggregated with forwarding strategy");

      int propagatedCount = 0;

      std::set<Ptr<Face> > ret;
      std::set<Ptr<Face> > tmp1;
      std::pair<std::set<Ptr<Face> >,int> tmp2;

      // ICN PDU to packet conversion
      Ptr<Packet> icn_pdu;

      // Check if we have been given a raw ICN PDU
      if (pdu != 0)
	{
	  // Convert the Interest PDU into a NS-3 Packet
	  icn_pdu = ns3::icn::Wire::FromInterest (interest);
	}

      NS_LOG_DEBUG ("Current " << *m_fib);

      NS_LOG_DEBUG ("Utilizing Flooding");
      BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ()) {
	if (ret.find (metricFace.GetFace ()) != ret.end())
	  {
	    NS_LOG_DEBUG ("Face: " << metricFace.GetFace ()->GetId () << " was found in the given set");
	    continue;
	  }

	// Now we actually attempt to forward the PDU
	if (!TrySendOutInterest (pdu, inFace, metricFace.GetFace (), Address(), interest, pitEntry))
	  {
	    continue;
	  }

	ret.insert(metricFace.GetFace ());
	propagatedCount++;
      }

      return propagatedCount > 0;
    }

    bool
    ICNFlooding::DoPropagateLayeredInterest (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
					     Ptr<const Interest> interest,
					     Ptr<pit::Entry> pitEntry,
					     const Address& from)
    {
      NS_LOG_DEBUG ("Processing Interest SeqNo " << std::dec << interest->GetName ().get (-1).toSeqNum () << " Face: " << inFace->GetId ());
      NS_ASSERT_MSG (m_pit != 0, "PIT should be aggregated with forwarding strategy");

      int propagatedCount = 0;

      std::set<Ptr<Face> > ret;
      std::set<Ptr<Face> > tmp1;
      std::pair<std::set<Ptr<Face> >,int> tmp2;

      // ICN PDU to packet conversion
      Ptr<Packet> icn_pdu;

      // Check if we have been given a raw ICN PDU
      if (pdu != 0)
	{
	  // Convert the Interest PDU into a NS-3 Packet
	  icn_pdu = ns3::icn::Wire::FromInterest (interest);
	}

      NS_LOG_DEBUG ("Utilizing Flooding");
      BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_fib_lastseen> ()) {
	if (ret.find (metricFace.GetFace ()) != ret.end())
	  {
	    continue;
	  }

	ret.insert(metricFace.GetFace ());
	if (!TrySendOutLayeredInterest (pdu, inFace, metricFace.GetFace (), metricFace.GetBestPoA(), interest, pitEntry, from))
	  {
	    continue;
	  }

	propagatedCount++;
      }

      // Flood the Faces not connected to the FIB
      Ptr<Face> tmp;
      NS_LOG_DEBUG ("Flooding Faces not directly connected to FIB");
      for (uint32_t i = 0; i < m_faces->GetN (); i++)
	{
	  tmp = m_faces->Get (i);
	  if (ret.find (tmp) != ret.end ())
	    {
	      continue;
	    }

	  if (!TrySendOutLayeredInterest (pdu, inFace, tmp, tmp->GetBroadcastAddress(), interest, pitEntry, from))
	    {
	      continue;
	    }

	  ret.insert(tmp);
	  propagatedCount++;
	}

      return propagatedCount > 0;
    }

    bool
    ICNFlooding::DoPropagateData (Ptr<DATAPDU> pdu, Ptr<Face> inFace,
				  Ptr<const Data> data)
    {
      if (pdu == 0)
	{
	  NS_LOG_WARN ("Received a raw ICN PDU. Can do nothing with this");
	  return false;
	}
      NS_LOG_DEBUG ("Processing DATA SeqNo " << std::dec << data->GetName ().get (-1).toSeqNum () << " Face: " << inFace->GetId ());
      // Convert the Interest PDU into a NS-3 Packet
      Ptr<Packet> icn_pdu = ns3::icn::Wire::FromData(data);

      // Pointers and flags for PDU types
      Ptr<DO> do_i;
      bool wasDO = false;
      Ptr<DU> du_i;
      bool wasDU = false;
      bool ok = false;

      uint32_t pduid = pdu->GetPacketId();
      switch(pduid)
      {
	case DO_NNN:
	  // Convert pointer to DO PDU
	  do_i = DynamicCast<DO> (pdu);
	  wasDO = true;
	  break;
	case DU_NNN:
	  // Convert pointer to DU PDU
	  du_i = DynamicCast<DU> (pdu);
	  wasDU = true;
	  break;
	default:
	  break;
      }

      Ptr<Face> foutFace;

      // Pointers to use when we have DO or DU PDUs
      std::pair<Ptr<Face>, Address> tmp;
      Address destAddr;
      NNNAddress newdst;
      Ptr<NNNAddress> newdstPtr;
      Ptr<const NNNAddress> constdstPtr;
      Ptr<const NNNAddress> tmpNewdst;
      Ptr<DO> do_o_spec;
      Ptr<DU> du_o_spec;
      bool nnptRedirect = false;

      NNNAddress myAddr = GetNode3NName ();

      if (wasDO || wasDU)
	{
	  // First obtain the name
	  if (wasDO)
	    {
	      newdst = do_i->GetName ();
	      constdstPtr = do_i->GetNamePtr();
	      tmpNewdst = do_i->GetNamePtr();
	      NS_LOG_INFO ("Propagating Data DO heading to " << newdst << " Flowid: " << do_i->GetFlowid () << " Sequence: " << do_i->GetSequence ());
	    }
	  else if (wasDU)
	    {
	      newdst = du_i->GetDstName ();
	      constdstPtr = du_i->GetDstNamePtr();
	      tmpNewdst = du_i->GetDstNamePtr();
	      NS_LOG_INFO ("Propagating Data DU from " << du_i->GetSrcName() << "  heading to " << newdst << " Flowid: " << du_i->GetFlowid () << " Sequence: " << du_i->GetSequence ());
	    }

	  // Check if we are already at the destination (search through all the node names acquired)
	  if (m_node_names->foundName(constdstPtr))
	    {
	      NS_LOG_INFO ("We have reached desired destination, looking for Apps");
	      Ptr<Face> tmpFace;
	      // We have reached the destination, look for Apps
	      for (uint32_t i = 0; i < m_faces->GetN (); i++)
		{
		  tmpFace = m_faces->Get (i);
		  // Check that the Face is of type APPLICATION
		  if (tmpFace->isAppFace ())
		    {
		      if (wasDO)
			ok = tmpFace->SendDO (do_i);
		      else if (wasDU)
			ok = tmpFace->SendDU (du_i);

		      if (!ok)
			{
			  // Log Data drops
			  m_dropData (data, tmpFace);
			  if (wasDO)
			    m_dropDOs (do_i, tmpFace);
			  else if (wasDU)
			    m_dropDUs (du_i, tmpFace);

			  NS_LOG_DEBUG ("Cannot satisfy data to " << *tmpFace);
			}
		      else
			{
			  // Log that a Data PDU was sent
			  m_outData (data, false, tmpFace);

			  if (wasDO)
			    {
			      NS_LOG_INFO ("Satisfying with DO");
			      m_outDOs (do_i, tmpFace);
			    }
			  else if (wasDU)
			    {
			      NS_LOG_INFO ("Satisfying with DU");
			      m_outDUs (du_i, tmpFace);
			    }
			}
		    }
		}
	      return ok;
	    }

	  // We may have obtained a DEN so we need to check
	  if (m_node_pdu_buffer->DestinationExists (newdst) && !m_nnpt->foundOldName(constdstPtr))
	    {
	      if (!newdst.isDirectSubSector (myAddr))
		{
		  NS_LOG_INFO ("On " << myAddr << ", " << newdst << " is not a direct subsector, buffering and not forwarding PDU");
		  if (wasDO)
		    {
		      m_node_pdu_buffer->PushDO (newdst, do_i);
		    }
		  else if (wasDU)
		    {
		      m_node_pdu_buffer->PushDU (newdst, du_i);
		    }
		  if (m_3n_only_buffer)
		    return true;
		}
	    }

	  // Check if the NNPT has any information for this particular 3N name
	  if (m_in_transit_redirect && m_nnpt->foundOldName(constdstPtr))
	    {
	      // Retrieve the new 3N name destination and update variable
	      tmpNewdst = m_nnpt->findPairedNamePtr (constdstPtr);
	      newdst = tmpNewdst->getName ();
	      // Flag that the NNPT made a change
	      nnptRedirect = true;
	    }

	  if (nnptRedirect)
	    {
	      NS_LOG_INFO ("On " << myAddr << " we are redirecting from " << *constdstPtr << " to " << newdst);
	      NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnpt);
	      if (wasDO)
		{
		  // Create a new DO PDU to send the data
		  do_o_spec = Create<DO> ();
		  // Set the new 3N name
		  do_o_spec->SetName (tmpNewdst);
		  // Set the lifetime of the 3N PDU
		  do_o_spec->SetLifetime (m_3n_lifetime);
		  // Configure payload for PDU
		  do_o_spec->SetPayload (icn_pdu);
		  // Signal that the PDU had an ICN PDU as payload
		  do_o_spec->SetPDUPayloadType (ICN_NNN);
		}
	      else if (wasDU)
		{
		  // Create a new DU PDU to send the data
		  du_o_spec = Create<DU> ();
		  // Use the original DU's Src 3N name
		  du_o_spec->SetSrcName (du_i->GetSrcNamePtr ());
		  // Set the new 3N name destination
		  du_o_spec->SetDstName (tmpNewdst);
		  // Set the lifetime of the 3N PDU
		  du_o_spec->SetLifetime (m_3n_lifetime);
		  // Use the original DU's Src 3N name
		  du_o_spec->SetAuthoritative (du_i->IsAuthoritative ());
		  // Configure payload for PDU
		  du_o_spec->SetPayload (icn_pdu);
		  // Signal that the PDU had an ICN PDU as payload
		  du_o_spec->SetPDUPayloadType (ICN_NNN);
		}
	    }

	  // Need routing information, attempt handshake
	  if (m_useHR)
	    AskHandshake (tmpNewdst);

	  NS_LOG_DEBUG ("On " << myAddr << " " << *m_nnst);

	  // We have an dis-enroll procedure
	  if (m_buffer_during_disenroll && (m_hasDisenrolled || m_nnst->isEmpty()))
	    {
	      NS_LOG_INFO ("Node is in dis-enroll or NNST might be blank, queuing PDU for later");

	      if (wasDO)
		{
		  if (nnptRedirect)
		    {
		      m_node_pdu_queue->pushDO(do_o_spec, Seconds(0));
		    }
		  else
		    {
		      m_node_pdu_queue->pushDO(do_i, Seconds (0));
		    }
		}
	      else if (wasDU)
		{

		  if (nnptRedirect)
		    {
		      m_node_pdu_queue->pushDU(du_o_spec, Seconds(0));
		    }
		  else
		    {
		      m_node_pdu_queue->pushDU(du_i, Seconds (0));
		    }
		}
	      else
		{
		  NS_LOG_WARN ("Attempted check of PDU type. None found, check if this is desired effect");
		}

	      return true;
	    }

	  // Roughly find the next hop
	  tmp = m_nnst->ClosestSectorFaceInfo (newdst, 0);

	  // Update the variables for Face and PoA name
	  foutFace = tmp.first;
	  destAddr = tmp.second;

	  if (foutFace == 0)
	    {
	      NS_LOG_WARN ("For some reason the NNST has returned an empty Face, skipping");
	    }
	  else
	    {
	      if (nnptRedirect)
		{
		  if (wasDO)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (do_o_spec, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(do_o_spec);
			  ok = foutFace->SendDO (do_o_spec, destAddr);
			  UpdatePDUCountersB (do_o_spec, foutFace, ok, false);
			}
		    }
		  else if (wasDU)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (du_o_spec, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(du_o_spec);
			  ok = foutFace->SendDU (du_o_spec, destAddr);
			  UpdatePDUCountersB (du_o_spec, foutFace, ok, false);
			}
		    }

		  if (!ok)
		    {
		      // Log Data drops
		      m_dropData (data, foutFace);
		    }
		  else
		    {
		      // Log that a Data PDU was sent
		      m_outData (data, false, foutFace);
		    }
		}
	      else
		{
		  if (wasDO)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (do_i, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(do_i);
			  ok = foutFace->SendDO (do_i, destAddr);
			  UpdatePDUCountersB (do_i, foutFace, ok, false);
			}
		    }
		  else if (wasDU)
		    {
		      if (m_ask_ACKP)
			{
			  int8_t n3ok = txPreparation (du_i, foutFace, destAddr, false);
			  ok = (n3ok >= 0);
			}
		      else
			{
			  txReset(du_i);
			  ok = foutFace->SendDU (du_i, destAddr);
			  UpdatePDUCountersB (du_i, foutFace, ok, false);
			}
		    }

		  if (!ok)
		    {
		      // Log Data drops
		      m_dropData (data, foutFace);
		    }
		  else
		    {
		      // Log that a Data PDU was sent
		      m_outData (data, false, foutFace);
		    }
		}
	    }
	}
      else
	{
	  NS_LOG_INFO ("Received either NULL or SO. Without a PIT entry we can do nothing with this information");
	}

      return ok;
    }

  } /* namespace nnn */
} /* namespace ns3 */
