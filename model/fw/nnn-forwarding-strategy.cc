/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-forwarding-strategy.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-forwarding-strategy.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-forwarding-strategy.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *          Zhu Li <phillipszhuli1990@gmail.com>
 */

// nnnSIM - 3N data
#include "nnn-forwarding-strategy.h"
#include "ns3/nnn-face.h"
#include "ns3/nnn-pdus.h"
#include "ns3/nnn-nnnsim-wire.h"
#include "ns3/nnn-wire-pkts.h"

#include "ns3/nnn-nnst.h"
#include "ns3/nnn-nnst-entry.h"
#include "ns3/nnn-nnst-entry-facemetric.h"
#include "ns3/nnn-nnpt.h"

#include "ns3/nnn-names-container.h"
#include "ns3/nnn-face-container.h"
#include "ns3/nnn-pdu-buffer.h"
#include "ns3/nnn-mpdu-buffer.h"
#include "ns3/nnn-addr-aggregator.h"
#include "ns3/nnn-header-helper.h"
#include "ns3/nnn-face-container.h"
#include "ns3/nnn-rxbuffercontainer.h"

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/ref.hpp>
#include <boost/tuple/tuple.hpp>

#include "ns3/assert.h"
#include "ns3/boolean.h"
#include "ns3/integer.h"
#include "ns3/log.h"
#include "ns3/node.h"
#include "ns3/object-base.h"
#include "ns3/ptr.h"
#include "ns3/simulator.h"
#include "ns3/string.h"
#include "ns3/type-id.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/nstime.h"

#include <sys/types.h>
#include <unistd.h>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace ll = boost::lambda;

// Max label
#define MAX3NLABEL 429496729

namespace ns3
{
  namespace nnn
  {
    NS_OBJECT_ENSURE_REGISTERED (ForwardingStrategy);

    NS_LOG_COMPONENT_DEFINE ("nnn.fw");

    TypeId
    ForwardingStrategy::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::ForwardingStrategy")
	  .SetGroupName ("Nnn")
	  .SetParent<Object> ()

	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutENs", "OutENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outENs),
			   "ns3::nnn::ForwardingStrategy::ENTracedCallback")

	  .AddTraceSource ("InENs", "InENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inENs),
			   "ns3::nnn::ForwardingStrategy::ENTracedCallback")

	  .AddTraceSource ("DropENs", "DropENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropENs),
			   "ns3::nnn::ForwardingStrategy::ENTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutAENs", "OutAENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outAENs),
			   "ns3::nnn::ForwardingStrategy::AENTracedCallback")

	  .AddTraceSource ("InAENs", "InAENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inAENs),
			   "ns3::nnn::ForwardingStrategy::AENTracedCallback")

	  .AddTraceSource ("DropAENs", "DropAENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropAENs),
			   "ns3::nnn::ForwardingStrategy::AENTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutDENs", "OutDENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outDENs),
			   "ns3::nnn::ForwardingStrategy::DENTracedCallback")

	  .AddTraceSource ("InDENs", "InDENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inDENs),
			   "ns3::nnn::ForwardingStrategy::DENTracedCallback")

	  .AddTraceSource ("DropDENs", "DropDENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropDENs),
			   "ns3::nnn::ForwardingStrategy::DENTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutADENs", "OutADENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outADENs),
			   "ns3::nnn::ForwardingStrategy::ADENTracedCallback")

	  .AddTraceSource ("InADENs", "InADENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inADENs),
			   "ns3::nnn::ForwardingStrategy::ADENTracedCallback")

	  .AddTraceSource ("DropADENs", "DropADENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropADENs),
			   "ns3::nnn::ForwardingStrategy::ADENTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutRENs", "OutRENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outRENs),
			   "ns3::nnn::ForwardingStrategy::RENTracedCallback")

	  .AddTraceSource ("InRENs", "InRENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inRENs),
			   "ns3::nnn::ForwardingStrategy::RENTracedCallback")

	  .AddTraceSource ("DropRENs", "DropRENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropRENs),
			   "ns3::nnn::ForwardingStrategy::RENTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutOENs", "OutOENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outOENs),
			   "ns3::nnn::ForwardingStrategy::OENTracedCallback")

	  .AddTraceSource ("InOENs", "InOENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inOENs),
			   "ns3::nnn::ForwardingStrategy::OENTracedCallback")

	  .AddTraceSource ("DropOENs", "DropOENs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropOENs),
			   "ns3::nnn::ForwardingStrategy::OENTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutINFs", "OutINFs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outINFs),
			   "ns3::nnn::ForwardingStrategy::INFTracedCallback")

	  .AddTraceSource ("InINFs", "InINFs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inINFs),
			   "ns3::nnn::ForwardingStrategy::INFTracedCallback")

	  .AddTraceSource ("DropINFs", "DropINFs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropINFs),
			   "ns3::nnn::ForwardingStrategy::INFTracedCallback")

	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutNULLps", "OutNULLps",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outNULLps),
			   "ns3::nnn:ForwardingStrategy::NULLpTracedCallback")

	  .AddTraceSource ("rxNULLps", "rxNULLps",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_rxNULLps),
			   "ns3::nnn:ForwardingStrategy::NULLpTracedCallback")

	  .AddTraceSource ("InNULLps", "InNULLps",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inNULLps),
			   "ns3::nnn:ForwardingStrategy::NULLpTracedCallback")

	  .AddTraceSource ("DropNULLps", "DropNULLps",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropNULLps),
			   "ns3::nnn:ForwardingStrategy::NULLpTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutSOs", "OutSOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outSOs),
			   "ns3::nnn::ForwardingStrategy::SOTracedCallback")

	  .AddTraceSource ("rxSOs", "rxSOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_rxSOs),
			   "ns3::nnn:ForwardingStrategy::SOTracedCallback")

	  .AddTraceSource ("InSOs", "InSOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inSOs),
			   "ns3::nnn::ForwardingStrategy::SOTracedCallback")

	  .AddTraceSource ("DropSOs", "DropSOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropSOs),
			   "ns3::nnn::ForwardingStrategy::SOTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutDOs", "OutDOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outDOs),
			   "ns3::nnn::ForwardingStrategy::DOTracedCallback")

	  .AddTraceSource ("rxDOs", "rxDOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_rxDOs),
			   "ns3::nnn:ForwardingStrategy::DOTracedCallback")

	  .AddTraceSource ("InDOs", "InDOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inDOs),
			   "ns3::nnn::ForwardingStrategy::DOTracedCallback")

	  .AddTraceSource ("DropDOs", "DropDOs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropDOs),
			   "ns3::nnn::ForwardingStrategy::DOTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutDUs", "OutDUs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outDUs),
			   "ns3::nnn::ForwardingStrategy::DUTracedCallback")

	  .AddTraceSource ("rxDUs", "rxDUs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_rxDUs),
			   "ns3::nnn:ForwardingStrategy::DUTracedCallback")

	  .AddTraceSource ("InDUs", "InDUs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inDUs),
			   "ns3::nnn::ForwardingStrategy::DUTracedCallback")

	  .AddTraceSource ("DropDUs", "DropDUs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropDUs),
			   "ns3::nnn::ForwardingStrategy::DUTracedCallback")

	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutRHRs", "OutRHRs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outRHRs),
			   "ns3::nnn::ForwardingStrategy::RHRTracedCallback")

	  .AddTraceSource ("InRHRs", "InRHRs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inRHRs),
			   "ns3::nnn::ForwardingStrategy::RHRTracedCallback")

	  .AddTraceSource ("DropRHRs", "DropRHRs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropRHRs),
			   "ns3::nnn::ForwardingStrategy::RHRTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutOHRs", "OutOHRs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outOHRs),
			   "ns3::nnn::ForwardingStrategy::OHRTracedCallback")

	  .AddTraceSource ("InOHRs", "InOHRs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inOHRs),
			   "ns3::nnn::ForwardingStrategy::OHRTracedCallback")

	  .AddTraceSource ("DropOHRs", "DropOHRs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropOHRs),
			   "ns3::nnn::ForwardingStrategy::OHRTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("OutACKPs", "OutACKPs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_outACKPs),
			   "ns3::nnn::ForwardingStrategy::ACKPTracedCallback")

	  .AddTraceSource ("InACKPs", "InACKPs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_inACKPs),
			   "ns3::nnn::ForwardingStrategy::ACKPTracedCallback")

	  .AddTraceSource ("DropACKPs", "DropACKPs",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_dropACKPs),
			   "ns3::nnn::ForwardingStrategy::ACKPTracedCallback")

	  ////////////////////////////////////////////////////////////////////

	  .AddTraceSource ("Got3NName", "Traces when the forwarding strategy has a 3N name",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_got3Nname),
			   "ns3::nnn::ForwardingStrategy::NNNAddressTracedCallback")

	  .AddTraceSource ("No3NName", "Traces when the forwarding strategy is waiting for a 3N name",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_no3Nname),
			   "ns3::nnn::ForwardingStrategy::NNNVoidTracedCallback")

	  .AddTraceSource ("CanDisassociate", "Traces when the forwarding strategy has seen a ADEN and can disassociate",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_canDisassociate),
			   "ns3::nnn::ForwardingStrategy::IDTracedCallback")

	  .AddTraceSource ("ChangedLifetime", "Traces when the forwarding strategy changed its 3N PDU lifetime",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_lifetimeChange),
			   "ns3::nnn::ForwardingStrategy::NNNTimeTracedCallback")

	  .AddTraceSource("BecameMobile", "Traces when the forwarding strategy is mobile",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_mobility),
			   "ns3::nnn::ForwardingStrategy::NNNBoolTracedCallback")

	  .AddTraceSource("PoAAssociation", "Traces when the forwarding strategy is mobile",
			  MakeTraceSourceAccessor (&ForwardingStrategy::m_PoAAssociation),
			  "ns3::nnn::ForwardingStrategy::NNNVoidTracedCallback")

	  .AddTraceSource ("FlowidElimination", "Traces when the forwarding strategy has eliminated a Flowid",
			   MakeTraceSourceAccessor (&ForwardingStrategy::m_FlowIdElimination),
			   "ns3::nnn::ForwardingStrategy::IDTracedCallback")

	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////
	  ////////////////////////////////////////////////////////////////////

	  .AddAttribute ("AskFixed3Nname", "Tells the node to ask for a fixed 3N name when enrolling",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ForwardingStrategy::m_AskFixedName),
			 MakeBooleanChecker ())

	  .AddAttribute ("Produce3Nnames", "Produce 3N names from delegated name space",
			 BooleanValue (true),
			 MakeBooleanAccessor (&ForwardingStrategy::m_produce3Nnames),
			 MakeBooleanChecker ())

	  .AddAttribute ("GenerateFixed3Nname", "Tells the node if it can generate fixed 3N names for enrolling nodes",
			 BooleanValue (false),
			 MakeBooleanAccessor (&ForwardingStrategy::m_GenerateFixedName),
			 MakeBooleanChecker ())

	  .AddAttribute ("3NLeasetime", "Lease time for a 3N name (Only in use if Produce3Nnames is used)",
			 StringValue ("300s"),
			 MakeTimeAccessor (&ForwardingStrategy::m_3n_lease_time),
			 MakeTimeChecker ())

	  .AddAttribute ("3NLifetime", "LifeTime for 3N PDUs",
			 StringValue ("1s"),
			 MakeTimeAccessor (&ForwardingStrategy::m_3n_lifetime),
			 MakeTimeChecker ())

	  .AddAttribute ("3NRetransmitTime", "Time for a 3N PDU without an ACK to attempt to retransmit",
			 StringValue ("5ms"),
			 MakeTimeAccessor (&ForwardingStrategy::GetRetxTimer, &ForwardingStrategy::SetRetxTimer),
			 MakeTimeChecker ())

	  .AddAttribute ("3NAckGenerateTime", "Time for a 3N capable node to generate an ACK for a particular 3N PDU",
			 StringValue ("5100us"),
			 MakeTimeAccessor (&ForwardingStrategy::m_3n_ackgenerate_time),
			 MakeTimeChecker ())

	  .AddAttribute ("StandardMetric", "Standard Metric in NNST for new entries (Only in use if Produce3NNames is used)",
			 IntegerValue (6),
			 MakeIntegerAccessor (&ForwardingStrategy::m_standardMetric),
			 MakeIntegerChecker<int32_t>())

	  .AddAttribute("Use3NName", "Specifies if the Node will use a 3N name. Currently only affects PoAModification calls",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::m_use3NName),
			MakeBooleanChecker())

	  .AddAttribute("IsMobile", "Specifies if the Node is mobile",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::IsMobile, &ForwardingStrategy::SetMobile),
			MakeBooleanChecker())

	  .AddAttribute("UseHR", "Specifies if the Node is using Handshake routing",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::m_useHR),
			MakeBooleanChecker ())

	  .AddAttribute("AskACKedPDUs", "Flags Node will ask for ACKP PDUs",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::IsAskingACKedPDUs, &ForwardingStrategy::AskACKedPDUs),
			MakeBooleanChecker ())

	  .AddAttribute("ReturnACKedPDUs", "Flags Node will permit the return of ACKP PDUs",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::IsReturningACKedPDUs, &ForwardingStrategy::ReturnsACKedPDUs),
			MakeBooleanChecker ())

	  .AddAttribute("InTransitRedirect", "Whether PDUs can be redirected in transit",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::m_in_transit_redirect),
			MakeBooleanChecker ())

	  .AddAttribute("PDUBufferDuringDisenroll", "Whether PDUs are buffered when in an disenroll state",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::m_buffer_during_disenroll),
			MakeBooleanChecker ())

	  .AddAttribute("3NRetransmitRetries", "Specifies the number of retransmission retries",
			IntegerValue (4),
			MakeIntegerAccessor(&ForwardingStrategy::Get3NReTx, &ForwardingStrategy::Set3NReTx),
			MakeIntegerChecker<uint32_t> ())

	  .AddAttribute("3NWirelessWait", "Time to wait for a wireless face to reassociate",
			StringValue("1009us"),
			MakeTimeAccessor(&ForwardingStrategy::m_3n_wireless_wait),
			MakeTimeChecker ())

	  .AddAttribute("3NPDUMinWindowSize", "Default number of PDUs that define a minimum window",
			IntegerValue (16),
			MakeIntegerAccessor(&ForwardingStrategy::GetMin3NWindow, &ForwardingStrategy::SetMin3NWindow),
			MakeIntegerChecker<uint32_t> ())

	  .AddAttribute("3NPDUMaxWindowSize", "Default number of PDUs that define a maximum window",
			IntegerValue (64),
			MakeIntegerAccessor(&ForwardingStrategy::GetMax3NWindow, &ForwardingStrategy::SetMax3NWindow),
			MakeIntegerChecker<uint32_t> ())

	  .AddAttribute("3NRate", "Default rate for flows in kilobits per second",
			IntegerValue (1248),
			MakeIntegerAccessor(&ForwardingStrategy::m_3n_default_rate),
			MakeIntegerChecker<uint32_t> ())

	  .AddAttribute("3NWindowReTx", "Amount of time before a retransmission of a window is executed",
			StringValue("45ms"),
			MakeTimeAccessor(&ForwardingStrategy::m_3n_pdu_window_retx),
			MakeTimeChecker ())

	  .AddAttribute("3NMPL", "Amount of time to traverse one network hop",
			StringValue("5ms"),
			MakeTimeAccessor(&ForwardingStrategy::m_3n_MPL),
			MakeTimeChecker ())

	  .AddAttribute("3NInifiniteReTx", "Make the nodes retransmit indefinitely",
			BooleanValue (true),
			MakeBooleanAccessor(&ForwardingStrategy::m_infinite_retransmission),
			MakeBooleanChecker())

	  .AddAttribute("3NOnlyBuffer", "Regarding 3N buffers, the Forwarding Strategy only buffers and doesn't forward 3N PDUs under DEN/INF conditions",
			BooleanValue (true),
			MakeBooleanAccessor(&ForwardingStrategy::m_3n_only_buffer),
			MakeBooleanChecker())

	  .AddAttribute("3NBufferAfterDEN", "Toggles saving PDUs after a DEN PDU",
			BooleanValue (false),
			MakeBooleanAccessor(&ForwardingStrategy::m_3n_buffer_after_den),
			MakeBooleanChecker())

	  .AddAttribute("3NQueues", "Enables use of queues at forwarding level",
			BooleanValue (true),
			MakeBooleanAccessor(&ForwardingStrategy::m_useQueues),
			MakeBooleanChecker())

	  // Required for testing at this moment
	  .AddConstructor <ForwardingStrategy> ()
	;
      return tid;
    }

    ForwardingStrategy::ForwardingStrategy ()
    : m_awaiting_oen_response     (Create<NNST> ())
    , m_awaiting_den_response     (Create<NNST> ())
    , m_waiting_aden_timeout      (Create<NNST> ())
    , m_faces                     (Create<FaceContainer> ())
    , m_node_names                (Create<NamesContainer> ())
    , m_leased_names              (Create<NamesContainer> ())
    , m_handshaken_names          (Create<NamesContainer> ())
    , m_waiting_handshake_timeout (Create<NamesContainer> ())
    , m_node_pdu_buffer           (Create<PDUBuffer> (3*(m_3n_MPL + m_3n_retransmit_time + m_3n_ackgenerate_time)))
    , m_node_den_buffer           (Create<MPDUBuffer> (3*(m_3n_MPL + m_3n_retransmit_time + m_3n_ackgenerate_time + m_3n_lifetime)))
    , m_node_pdu_queue            (Create<PDUQueue> ())
    , m_node_rxbuffer_container   (Create<RxBufferContainer> ())
    , m_node_txbuffer_container   (Create<RxBufferContainer> ())
    , m_producedNameNumber        (0)
    , m_on_ren_oen                (false)
    , m_sent_ren                  (false)
    , m_aden_from_en              (false)
    , m_hasDisenrolled            (false)
    , m_retransmission_attempts   (0)
    , m_infinite_retransmission   (false)
    , uniRandom                   (CreateObject<UniformRandomVariable> ())
    , backoffRandom               (CreateObject<UniformRandomVariable> ())
    , m_combos                    (0)
    {
      m_node_names->RegisterCallbacks(
	  MakeCallback (&ForwardingStrategy::Reenroll, this),
	  MakeCallback (&ForwardingStrategy::Enroll, this)
      );

      uniRandom->SetAttribute ("Min", DoubleValue (1));
      uniRandom->SetAttribute ("Max", DoubleValue (std::numeric_limits<uint32_t>::max ()));

      // Connect the expiry of ADEN timeout to the propagation of DEN
      m_waiting_aden_timeout->TraceConnectWithoutContext ("nameExpired", MakeCallback(&ForwardingStrategy::PropagateDEN, this));

      // Modify renewal time for m_waiting_handshake_timeout to avoid assert issues with event scheduling
      m_waiting_handshake_timeout->SetDefaultRenewal (MilliSeconds(m_3n_ackgenerate_time));

      // This forces the seconds to be printed in non-scientific notation
      NS_LOG_INFO (std::fixed);

      // Must turn on the supported protocols
      m_n1minus_3n_support = true;
      // Within this particular class, direct ICN protocols are not supported
      m_n1minus_icn_support = false;

      m_3n_last_oen_time = Seconds (0);
    }

    ForwardingStrategy::~ForwardingStrategy ()
    {
    }

    bool
    ForwardingStrategy::SupportsDirect3N ()
    {
      return m_n1minus_3n_support;
    }

    bool
    ForwardingStrategy::SupportsDirectICN ()
    {
      return m_n1minus_icn_support;
    }

    void
    ForwardingStrategy::SetMobile(bool value)
    {
      m_isMobile = value;
      m_mobility(m_isMobile);
    }

    bool
    ForwardingStrategy::IsMobile() const
    {
      return m_isMobile;
    }

    void
    ForwardingStrategy::SetNode3NName (Ptr<const NNNAddress> name, Time lease, bool fixed)
    {
      NS_LOG_FUNCTION (this);

      if (fixed)
	NS_LOG_INFO("Adding fixed 3N name " << *name << " to node");
      else
	NS_LOG_INFO("Adding 3N name " << *name << " to node");

      m_node_names->addEntry(name, lease, fixed);

      // TracedCallback to let Apps know we have a name
      m_got3Nname (name);
    }

    const NNNAddress&
    ForwardingStrategy::GetNode3NName ()
    {
      return *m_node_names->findNewestName();
    }

    Ptr<const NNNAddress>
    ForwardingStrategy::GetNode3NNamePtr ()
    {
      return m_node_names->findNewestName();
    }

    Ptr<const NNNAddress>
    ForwardingStrategy::produce3NName ()
    {
      NS_LOG_FUNCTION (this);
      bool produced = false;

      Ptr<const NNNAddress> final = 0;

      if (Has3NName ())
	{
	  // Get this nodes currently functioning 3N name and copy it to a new variable
	  NNNAddress base = GetNode3NName ();

	  while (!produced)
	    {
	      // Create an empty container for the 3N name
	      Ptr<NNNAddress> ret;

	      // Create a random numerical component
	      name::Component tmp = name::Component ();
	      // Add the information
	      tmp.fromNumber(m_producedNameNumber);

	      // Create a NNNAddress with base
	      ret = Create <NNNAddress> (base.toDotHex());
	      // Append the new component
	      ret->append(tmp);

	      if (m_leased_names->foundName(ret))
		{
		  NS_LOG_INFO ("We found this name within leased names, cycling through");
		  m_producedNameNumber++;
		  continue;
		}

	      if (m_node_lease_times.find (ret) != m_node_lease_times.end ())
		{
		  NS_LOG_INFO ("We found this name within pending acknowledged names, cycling through");
		  m_producedNameNumber++;
		  continue;
		}

	      // If everything has passed, flag end and update known number
	      produced = true;
	      final = ret;
	      m_producedNameNumber++;
	    }

	  NS_LOG_INFO ("Produced a 3N name " << *final);
	}
      else
	{
	  NS_LOG_DEBUG ("No 3N from which to derive!");
	}
      return final;
    }

    bool
    ForwardingStrategy::Has3NName ()
    {
      return (!m_node_names->isEmpty());
    }

    bool
    ForwardingStrategy::GoesBy3NName (Ptr<const NNNAddress> addr)
    {
      return m_node_names->foundName (addr);
    }

    bool
    ForwardingStrategy::HasFixed3NName ()
    {
      return m_node_names->hasFixedName ();
    }

    void
    ForwardingStrategy::SetRetxTimer (Time retx)
    {
      Time A = Seconds((8.192 * m_3n_max_pdu_window) / m_3n_default_rate);
      Time R = m_retransmission_attempts*A;
      Time bt = 3*(retx + A + R);
      NS_LOG_DEBUG ("ReTx Time: " << retx.GetSeconds() << " PDU buffer Timer: " << bt.GetSeconds());
      m_node_pdu_buffer->SetReTX(bt);
      m_3n_retransmit_time = retx;
    }

    Time
    ForwardingStrategy::GetRetxTimer () const
    {
      return m_3n_retransmit_time;
    }

    void
    ForwardingStrategy::Set3NReTx (uint32_t retx)
    {
      NS_ASSERT (retx > 0);
      m_retransmission_attempts = retx + 1;
    }

    uint32_t
    ForwardingStrategy::Get3NReTx () const
    {
      return m_retransmission_attempts - 1;
    }

    void
    ForwardingStrategy::Set3NPDULifetime (Time lifetime)
    {
      m_3n_lifetime = lifetime;
      m_lifetimeChange (m_3n_lifetime);
    }

    Time
    ForwardingStrategy::Get3NPDULifetime () const
    {
      return m_3n_lifetime;
    }

    void
    ForwardingStrategy::flushBuffer(Ptr<Face> face, Ptr<NNNAddress> oldName, Ptr<NNNAddress> newName)
    {
      NS_LOG_FUNCTION (this << face->GetId () << *oldName << " to " << *newName);
      NNNAddress myAddr = GetNode3NName ();
      if (m_node_pdu_buffer->DestinationExists(oldName))
	{
	  uint buffer_size = m_node_pdu_buffer->QueueSize(oldName);
	  NS_LOG_INFO ("On " << myAddr << " found a queue for " << *oldName << " of " << buffer_size << ", attempting to flush");
	  // The actual queue
	  std::queue<Ptr<Packet> > addrQueue = m_node_pdu_buffer->PopQueue(oldName);

	  // Dummy Pointers to the PDU types
	  Ptr<DO> do_o_orig;
	  Ptr<DU> du_o_orig;

	  uint32_t do_flush = 0;
	  uint32_t du_flush = 0;
	  uint32_t do_ack_stop = 0;
	  uint32_t du_ack_stop = 0;
	  uint32_t count = 1;

	  std::pair<Ptr<Face>, Address> closestSector;

	  Ptr<Face> outFace;
	  Address destAddr;

	  while (!addrQueue.empty())
	    {
	      NS_LOG_DEBUG ("On " << myAddr << " flushing buffer PDU " << count << " of " << buffer_size);
	      // Get the PDU at the front of the queue
	      Ptr<Packet> queuePDU = addrQueue.front();

	      switch(HeaderHelper::GetNNNHeaderType(queuePDU))
	      {
		case DO_NNN:
		  // Convert the Packet back to a DO for manipulation
		  do_o_orig = wire::nnnSIM::DO::FromWire(queuePDU);

		  // Renew the DO lifetime
		  do_o_orig->SetLifetime(m_3n_lifetime);
		  // Change the DO 3N name to the new name
		  do_o_orig->SetName(newName);

		  // Find where to send the SO
		  closestSector = m_nnst->ClosestSectorFaceInfo(newName, 0);

		  outFace = closestSector.first;
		  destAddr = closestSector.second;

		  if (outFace == 0)
		    {
		      NS_LOG_INFO ("For some reason the NNST has returned an empty Face, skipping");
		    }
		  else
		    {
		      if (m_ask_ACKP)
			{
			  int8_t rt = txPreparation(do_o_orig, outFace, destAddr, false);

			  if (rt == 0)
			    {
			      do_flush++;
			    }
			  else if (rt == 1)
			    {
			      do_ack_stop++;
			    }
			}
		      else
			{
			  txReset(do_o_orig);
			  // Send the created DO PDU
			  if (m_useQueues)
			    EnqueueDO(outFace, destAddr, do_o_orig);
			  else
			    outFace->SendDO(do_o_orig, destAddr);

			  // Log the DO sending
			  m_outDOs(do_o_orig, outFace);
			  do_flush++;
			}
		    }
		  break;
		case DU_NNN:
		  // Convert the Packet back to a DU for manipulation
		  du_o_orig = wire::nnnSIM::DU::FromWire(queuePDU);

		  // Renew the DU lifetime
		  du_o_orig->SetLifetime(m_3n_lifetime);

		  // Change the DU 3N names to the new names if necessary
		  if (du_o_orig->GetDstName() == *oldName)
		    du_o_orig->SetDstName(newName);

		  // Find where to send the DU
		  closestSector = m_nnst->ClosestSectorFaceInfo(du_o_orig->GetDstNamePtr(), 0);

		  outFace = closestSector.first;
		  destAddr = closestSector.second;

		  if (outFace == 0)
		    {
		      NS_LOG_INFO ("For some reason the NNST has returned an empty Face, skipping");
		    }
		  else
		    {
		      if (m_ask_ACKP)
			{
			  int8_t rt = txPreparation(du_o_orig, outFace, destAddr, false);

			  if (rt == 0)
			    {
			      du_flush++;
			    }
			  else if (rt == 1)
			    {
			      du_ack_stop++;
			    }
			}
		      else
			{
			  txReset(du_o_orig);
			  // Send the created DU PDU
			  outFace->SendDU(du_o_orig, destAddr);

			  // Log the DU sending
			  m_outDUs(du_o_orig, outFace);
			  du_flush++;
			}
		    }
		  break;
		default:
		  NS_LOG_INFO("Obtained unknown PDU");
	      }
	      // Pop the queue and continue
	      addrQueue.pop ();
	      count++;
	    }

	  // Make sure we delete the entry for oldName in the buffer
	  m_node_pdu_buffer->RemoveDestination(oldName);

	  NS_LOG_INFO ("On " << myAddr << " flushed " << *oldName << " -> " << *newName << " <->  DO: " << do_flush << " DU: " << du_flush <<
		       " DO stopped by ACK: " << do_ack_stop << " DU stopped by ACK: " << du_ack_stop);
	}
      else
	{
	  NS_LOG_INFO ("On " << myAddr << " no buffer found for " << *oldName << ", continuing");
	}
    }

    void
    ForwardingStrategy::hasDisenrolled ()
    {
      NS_LOG_FUNCTION (this);
      m_hasDisenrolled = true;
    }

    void
    ForwardingStrategy::hasReenrolled ()
    {
      NS_LOG_FUNCTION (this);
      m_hasDisenrolled = false;
    }

    void
    ForwardingStrategy::EmptyDisenrollPDUBuffer ()
    {
      NS_LOG_FUNCTION (this);

      m_3n_last_oen_time = Simulator::Now ();

      std::pair<Ptr<Face>, Address> closestSector;

      Ptr<Face> outFace;
      Address destAddr;

      // Dummy Pointers to the PDU types
      Ptr<DO> do_o_orig;
      Ptr<DU> du_o_orig;

      uint32_t do_flush = 0;
      uint32_t du_flush = 0;
      uint32_t do_dst_changes = 0;
      uint32_t du_src_changes = 0;
      uint32_t du_dst_changes = 0;

      if (m_node_pdu_queue->isEmpty ())
	{
	  NS_LOG_DEBUG ("No PDUs saved, returning");
	  return;
	}

      Ptr<const NNNAddress> constsrcPtr = 0;
      Ptr<const NNNAddress> constdstPtr = 0;
      Ptr<const NNNAddress> newName = 0;

      NS_LOG_INFO ("Retransmitting " << m_node_pdu_queue->size() << " PDUs that were held back");
      while (!m_node_pdu_queue->isEmpty ())
	{
	  // Get the PDU at the front of the queue
	  Ptr<Packet> queuePDU = m_node_pdu_queue->pop ();

	  switch(HeaderHelper::GetNNNHeaderType(queuePDU))
	  {
	    case DO_NNN:
	      // Convert the Packet back to a DO for manipulation
	      do_o_orig = wire::nnnSIM::DO::FromWire(queuePDU);

	      // Renew the DO lifetime
	      do_o_orig->SetLifetime(m_3n_lifetime);

	      constdstPtr = do_o_orig->GetNamePtr ();

	      // Verify destination
	      if (m_nnpt->foundOldName(constdstPtr))
		{
		  // Retrieve the new 3N name destination and update variable
		  newName = m_nnpt->findPairedNamePtr (constdstPtr);
		  do_o_orig->SetName (newName);
		  do_dst_changes++;
		}

	      // Find where to send the DO
	      closestSector = m_nnst->ClosestSectorFaceInfo(do_o_orig->GetName (), 0);

	      outFace = closestSector.first;
	      destAddr = closestSector.second;

	      if (outFace == 0)
		{
		  NS_LOG_WARN ("NNST is probably empty, check if this is desired effect");
		}
	      else
		{
		  if (m_ask_ACKP)
		    {
		      int8_t rt = txPreparation(do_o_orig, outFace, destAddr, false);

		      if (rt == 0)
			{
			  do_flush++;
			}
		      else if (rt == 1)
			{
			  NS_LOG_DEBUG ("Still waiting for ACK");
			}
		    }
		  else
		    {
		      txReset(do_o_orig);
		      // Send the created DO PDU
		      if (m_useQueues)
			EnqueueDO(outFace, destAddr, do_o_orig);
		      else
			outFace->SendDO(do_o_orig, destAddr);

		      // Log the DO sending
		      m_outDOs(do_o_orig, outFace);
		      do_flush++;
		    }
		}
	      break;
	    case DU_NNN:
	      // Convert the Packet back to a DU for manipulation
	      du_o_orig = wire::nnnSIM::DU::FromWire(queuePDU);

	      // Renew the DU lifetime
	      du_o_orig->SetLifetime(m_3n_lifetime);

	      constsrcPtr = du_o_orig->GetSrcNamePtr ();
	      constdstPtr = du_o_orig->GetDstNamePtr();

	      // Verify if you should modify the DU src
	      if (m_node_names->foundName(constsrcPtr))
		{
		  newName = m_node_names->findNewestName ();
		  if (*constsrcPtr != *newName)
		    {
		      du_o_orig->SetSrcName (newName);
		      du_src_changes++;
		    }
		}

	      // Verify destination
	      if (m_nnpt->foundOldName(constdstPtr))
		{
		  // Retrieve the new 3N name destination and update variable
		  newName = m_nnpt->findPairedNamePtr (constdstPtr);
		  du_o_orig->SetDstName (newName);
		  du_dst_changes++;
		}

	      // Find where to send the DU
	      closestSector = m_nnst->ClosestSectorFaceInfo(du_o_orig->GetDstNamePtr(), 0);

	      outFace = closestSector.first;
	      destAddr = closestSector.second;

	      if (outFace == 0)
		{
		  NS_LOG_WARN ("NNST is probably empty, check if this is desired effect");
		}
	      else
		{
		  if (m_ask_ACKP)
		    {
		      int8_t rt = txPreparation(du_o_orig, outFace, destAddr, false);

		      if (rt == 0)
			{
			  du_flush++;
			}
		      else if (rt == 1)
			{
			  NS_LOG_DEBUG ("Still waiting for ACK");
			}
		    }
		  else
		    {
		      txReset(du_o_orig);
		      // Send the created DU PDU
		      if (m_useQueues)
			EnqueueDU(outFace, destAddr, du_o_orig);
		      else
			outFace->SendDU(du_o_orig, destAddr);

		      // Log the DU sending
		      m_outDUs(du_o_orig, outFace);
		      du_flush++;
		    }
		}
	      break;
	    default:
	      NS_LOG_INFO("Obtained unsupported PDU, check if this is the desired effect");
	  }
	}

      NS_LOG_INFO ("On " << GetNode3NName () << " flushed DO: " << do_flush << " Dst changes: " << do_dst_changes);
      NS_LOG_INFO ("On " << GetNode3NName () << " flushed DU: " << du_flush << " Src changes: " << du_src_changes  << " Dst changes: " << du_dst_changes);
    }

    void
    ForwardingStrategy::OnEN (Ptr<Face> face, Ptr<EN> en_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inENs (en_p, face);

      // Find if we can produce 3N names
      if (!m_produce3Nnames)
	{
	  NS_LOG_INFO ("Not set to produce 3N names, returning");
	  m_dropENs (en_p, face);
	  return;
	}

      if(!Has3NName ())
	{
	  NS_LOG_INFO ("We do not currently have a name! Returning!");
	  m_dropENs (en_p, face);
	  return;
	}

      // Find out if the EN is asking for a fixed name
      bool askingFixed = en_p->IsFixed ();

      if (askingFixed && !m_GenerateFixedName)
	{
	  NS_LOG_INFO ("The received EN is asking for fixed name which we can't produce, returning");
	  m_dropENs (en_p, face);
	  return;
	}

      // Check if the PoA list given has already been sent at OEN
      if (m_awaiting_oen_response->FoundPoAs (en_p->GetPoas ()))
	{
	  NS_LOG_INFO ("The received EN has already been sent an OEN, returning!");
	  m_dropENs (en_p, face);
	  return;
	}

      // Check if the particular PoAs in the EN have already been submitted to this edge node
      if (m_nnst->FoundPoAs (en_p->GetPoas ()))
	{
	  NS_LOG_INFO ("The PoAs in this EN have already sent an AEN and should have a name, returning!");
	  m_dropENs (en_p, face);
	  return;
	}

      // Find out if we can produce fixed 3N names
      NNNAddress myAddr = GetNode3NName ();
      NS_LOG_INFO("On " << myAddr << " producing 3N name for new node");

      // Get the first Address from the EN PDU (this probably requires more tuning)
      Address destAddr = en_p->GetOnePoa(0);

      NS_LOG_INFO ("On " << myAddr << ", will return OEN to " << destAddr);

      // Get all the PoA Address in the EN PDU to fill the NNST
      std::vector<Address> poaAddrs = en_p->GetPoas ();
      // This is a copy of the PoAs to be returned later
      std::vector<Address> returnAddrs = en_p->GetPoas ();

      NS_LOG_INFO ("Received PoA Names: ");
      for (size_t i = 0; i < poaAddrs.size (); i++)
	{
	  NS_LOG_INFO (i << ": " << poaAddrs[i]);
	}

      // Produce a 3N name
      Ptr<const NNNAddress> produced3Nname = produce3NName ();

      // Add the new information into the Awaiting Response NNST type structure
      // Create a complete timeout of 3*(Retransmit + Ack_Generate + Lifetime) - remember absolute time
      Time now = Simulator::Now () + 3*(m_3n_retransmit_time + m_3n_ackgenerate_time + m_3n_lifetime);

      // The first PoA is given higher priority
      m_awaiting_oen_response->Add (produced3Nname, face, poaAddrs.front (), now, m_standardMetric -1, false);

      // Eliminate the first one, since it has already been added
      poaAddrs.erase (poaAddrs.begin ());

      // Insert the remaining PoAs
      m_awaiting_oen_response->Add (produced3Nname, face, poaAddrs, now, m_standardMetric, false);

      NS_LOG_INFO ("On " << myAddr << " creating OEN PDU to send");
      // Create an OEN PDU to respond
      Ptr<OEN> oen_p = Create<OEN> (produced3Nname);
      oen_p->SetLifetime(m_3n_lifetime);
      // Ensure that the lease time is set in the PDU
      // We send the lease out in absolute simulator time
      Time absoluteLease = Simulator::Now () + m_3n_lease_time;
      oen_p->SetLeasetime (absoluteLease);
      // Add the PoA names to the PDU
      oen_p->AddPoa (returnAddrs);
      // Add my name to the PDU
      oen_p->SetSrcName (GetNode3NName ());
      // Add personal PoAs
      oen_p->AddPersonalPoa (GetAllPoANames (face));
      // Flag is the name is fixed or not
      oen_p->SetFixed (askingFixed);

      // Send the create OEN PDU out the way it came
      if (m_useQueues)
	EnqueueOEN(face, destAddr, oen_p);
      else
	face->SendOEN (oen_p, destAddr);

      NS_LOG_INFO ("Making a lease entry in " << myAddr << " for " <<*produced3Nname << " until " << absoluteLease.GetSeconds());

      // Maintain the lease time given to the 3N name for further checking
      m_node_lease_times[oen_p->GetNamePtr()] = absoluteLease;

      // If we were asked for a fixed name, we need to keep track of it
      if (askingFixed)
	{
	  NS_LOG_INFO ("The created lease will be for a fixed node!");
	  m_fixed_name_pending.insert (produced3Nname);
	}

      m_outOENs (oen_p, face);

      // Schedule retransmission of OEN if we haven't received an AEN
      m_oen_timer = Simulator::Schedule(m_3n_retransmit_time, &ForwardingStrategy::RetransmitOEN, this, face, produced3Nname, destAddr, oen_p);
    }

    void
    ForwardingStrategy::OnAEN (Ptr<Face> face, Ptr<AEN> aen_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inAENs (aen_p, face);

      // Find out if we can produce 3N names
      if (!m_produce3Nnames)
	{
	  NS_LOG_INFO ("Can't produce 3N names, returning");
	  return;
	}

      // Get the basic 3N name from the AEN
      NNNAddress tmp = aen_p->GetName ();
      NNNAddress myAddr = GetNode3NName ();
      bool askingFixed = aen_p->IsFixed ();

      NS_LOG_INFO("On " << myAddr << " obtained AEN for " << tmp);

      // Check to see if the 3N name in the PDU belongs to this sector
      if (myAddr != tmp.getSectorName ())
	{
	  NS_LOG_WARN ("The AEN does not belong to this node's sector!, returning");
	  return;
	}

      // Check if we have this entry in the waiting list
      if (!m_awaiting_oen_response->FoundName(tmp))
	{
	  NS_LOG_WARN ("The 3N name has timed out or been acked, returning!");
	  return;
	}

      // Check if we can produce fixed 3N names
      if (askingFixed && !m_GenerateFixedName)
	{
	  NS_LOG_WARN ("The received AEN is asking for fixed name which we can't produce, returning");
	  return;
	}

      NS_LOG_INFO("Found lease entry for seen " << tmp);

      // Get the list of PoAs in the AEN PDU
      std::vector<Address> receivedPoas = aen_p->GetPoas ();

      if (!m_awaiting_oen_response->EqualPoAs (tmp, receivedPoas))
	{
	  NS_LOG_WARN ("Found differences in received PoAs for " << tmp << ", rejecting!");
	  return;
	}

      // Get the name that will finally be used
      Ptr<const NNNAddress> newName = aen_p->GetNamePtr ();

      // Attempt to lower memory usage
      std::set<NNNAddress>::iterator it3 = m_names_in_ram.find(newName->getName());

      if (it3 != m_names_in_ram.end ())
	{
	  newName = Ptr<const NNNAddress> (&(*it3));
	}
      else
	{
	  m_names_in_ram.insert (*newName);
	}
      // --------

      NS_LOG_INFO("Found no differences in PoAs for " << tmp);

      Time absoluteLeaseTime = aen_p->GetLeasetime();
      std::map<Ptr<const NNNAddress>, Time, PtrNNNComp>::iterator it;

      it = m_node_lease_times.find(newName);

      // Check that the lease times coincide with what was recorded
      // Remember that the Wire classes round to Time values to uint16_t so we
      // will not have the femtoseconds
      if (it != m_node_lease_times.end ())
	{
	  Time recordedLeaseTime = it->second;

	  NS_LOG_INFO ("Checking discrepancies between recorded time " << recordedLeaseTime.GetSeconds() << " and obtained time " << absoluteLeaseTime.GetSeconds() << " in " << myAddr << " for " << tmp);

	  if (absoluteLeaseTime <= recordedLeaseTime)
	    {
	      // No issues
	      NS_LOG_INFO ("Lease time checks out for " << tmp);
	      m_node_lease_times.erase (newName);
	    }
	  else
	    {
	      NS_LOG_INFO ("Found discrepancy between recorded time and obtained for " << tmp);
	      return;
	    }
	}
      else
	{
	  NS_LOG_INFO ("No record found for " << *newName << " returning");
	  return;
	}

      std::set<Ptr<const NNNAddress>, PtrNNNComp>::iterator it2;

      // Search the fixed pending names structure for the 3N name in the AEN
      it2 = m_fixed_name_pending.find(newName);

      if (askingFixed)
	{
	  if (it2 == m_fixed_name_pending.end ())
	    {
	      NS_LOG_INFO ("There is no fixed record for " << *newName);
	      return;
	    }
	  else
	    {
	      NS_LOG_INFO ("Fixed record for " << *newName << " checks out");
	      m_fixed_name_pending.erase (newName);
	    }
	}

      NS_LOG_DEBUG ("Verifying NNPT for new name " << tmp);
      NS_LOG_DEBUG ("On " << myAddr << ":" << std::endl << *m_nnpt);

      // If there is an NNPT entry, it means we had a REN before. We need to
      // assure the network of this change
      if (m_nnpt->foundNewName (newName))
	{
	  Ptr<NNNAddress> registeredOldName = Create<NNNAddress> (m_nnpt->findPairedOldName(newName));
	  Ptr<NNNAddress> registeredNewName = Create<NNNAddress> (newName->getName());

	  NS_LOG_INFO("We have had a reenrolling node used to go by " << *registeredOldName << " now uses " << *registeredNewName);
	  NS_LOG_INFO("Attempting to flush buffer");

	  // Check if the Node was in our subsector
	  if (!registeredOldName->isDirectSubSector (myAddr))
	    {
	      // If node was not originally in our sector, create a INF PDU
	      NS_LOG_INFO (*registeredOldName << " was not in our sector, creating INF PDU");
	      Ptr<INF> inf_o = Create<INF> ();
	      inf_o->SetLifetime(m_3n_lifetime);

	      // Fill the necessary information
	      inf_o->SetOldName (registeredOldName);
	      inf_o->SetNewName (registeredNewName);
	      inf_o->SetRemainLease (m_nnpt->findNameExpireTime(registeredOldName));

	      // Find where to send the INF
	      std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (registeredOldName->getSectorName(), 0);

	      Ptr<Face> outFace = tmp.first;
	      Address destAddr = tmp.second;

	      if (outFace == 0)
		{
		  NS_LOG_INFO ("For some reason the NNST has returned an empty Face, skipping");
		}
	      else
		{
		  // Send the created INF PDU
		  if (m_useQueues)
		    EnqueueINF(outFace, destAddr, inf_o);
		  else
		    outFace->SendINF (inf_o, destAddr);
		  // Log that the INF PDU was sent
		  m_outINFs (inf_o, outFace);
		}
	    }

	  // If we happen to be in the same subsector, the buffer will have something
	  // the check is good practice
	  flushBuffer (face, registeredOldName, registeredNewName);
	}

      NS_LOG_INFO("Adding NNST information for " << tmp << " until " << absoluteLeaseTime.GetSeconds ());

      // Add the new information to the NNST

      // The first PoA is given higher priority
      m_nnst->Add (newName, face, receivedPoas.front (), absoluteLeaseTime, m_standardMetric -1, askingFixed);

      // Eliminate the first PoA since it has already been added
      receivedPoas.erase (receivedPoas.begin ());

      m_nnst->Add(newName, face, receivedPoas, absoluteLeaseTime, m_standardMetric, askingFixed);

      NS_LOG_DEBUG ("On " << myAddr << ": " << *m_nnst);

      NS_LOG_INFO("Adding lease information for " << tmp << " until " << absoluteLeaseTime.GetSeconds ());
      // Add the information the the leased NodeNameContainer
      m_leased_names->addEntry(newName, absoluteLeaseTime, askingFixed);

      NS_LOG_INFO("Saving face " << *face << " for REN purposes");
      // Add the Face where the AEN came from (used for REN purposes)
      m_returnEN_faces.insert (face);

      NS_LOG_INFO("Eliminating OEN retransmission for " << *newName);
//      Simulator::Schedule (3*m_3n_retransmit_time, &ForwardingStrategy::EndAEN, this, newName);
      EndAEN (newName);
    }

    void
    ForwardingStrategy::EndAEN (Ptr<const NNNAddress> addr)
    {
      NS_LOG_INFO ("Removing " << *addr);

      // Eliminate 3N name from pending least
      m_awaiting_oen_response->Remove (addr);

      m_oen_timer.Cancel ();
    }

    void
    ForwardingStrategy::OnREN (Ptr<Face> face, Ptr<REN> ren_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inRENs (ren_p, face);

      // Check we can actually produce 3N names
      if (!m_produce3Nnames)
	{
	  NS_LOG_INFO ("We cannot generate names! Returning");
	  m_dropRENs (ren_p, face);
	  return;
	}

      if (!Has3NName ())
	{
	  NS_LOG_INFO ("We do not currently have a name! Returning");
	  m_dropRENs (ren_p, face);
	  return;
	}

      // Find out if the REN is asking for a fixed name
      bool askingFixed = ren_p->IsFixed ();

      if (askingFixed && !m_GenerateFixedName)
	{
	  NS_LOG_INFO ("The received REN is asking for fixed name which we can't produce, returning");
	  m_dropRENs (ren_p, face);
	  return;
	}

      // Check if the PoA list given has already been sent at OEN
      if (m_awaiting_oen_response->FoundPoAs (ren_p->GetPoas ()))
	{
	  NS_LOG_INFO ("The received REN has already been sent an OEN, returning!");
	  m_dropRENs (ren_p, face);
	  return;
	}

      NNNAddress myAddr = GetNode3NName ();

      // Get the 3N name that the node was using
      Ptr<const NNNAddress> reenroll = ren_p->GetNamePtr();
      // Produce a new 3N name
      Ptr<const NNNAddress> produced3Nname;

      // If the 3N name is in your subsector, verify the lease
      if (reenroll->isDirectSubSector (myAddr))
	{
	  Time absolutelease = m_nnst->GetLeaseTime(*reenroll);

	  NS_LOG_WARN ("3N name " << *reenroll << " has a lease until " << absolutelease.GetSeconds () << " seconds, assuming retransmission error");
	  return;
	}
      else
	{
	  produced3Nname = produce3NName ();
	}

      NS_LOG_INFO("On " << myAddr << " producing 3N name for reenrolling node " << *reenroll);

      // Get the first Address from the REN PDU
      Address destAddr = ren_p->GetOnePoa(0);

      // Get all the PoA Address in the REN PDU to fill the NNST
      std::vector<Address> poaAddrs = ren_p->GetPoas ();
      // This is a copy of the PoAs to be returned later
      std::vector<Address> returnAddrs = ren_p->GetPoas ();

      // Add the new information into the Awaiting Response NNST type structure
      // Create a complete timeout of 3*(Retransmit + Ack_Generate + Lifetime) - remember absolute time
      Time now = Simulator::Now () + 3*(m_3n_retransmit_time + m_3n_ackgenerate_time + m_3n_lifetime);

      // The first PoA is given higher priority
      m_awaiting_oen_response->Add (produced3Nname, face, poaAddrs.front (), now, m_standardMetric -1, false);

      // Eliminate the first one, since it has already been added
      poaAddrs.erase (poaAddrs.begin ());

      // Insert the remaining PoAs
      m_awaiting_oen_response->Add (produced3Nname, face, poaAddrs, now, m_standardMetric, false);

      NS_LOG_INFO("On " << myAddr << " creating OEN PDU to send");
      // Create an OEN PDU to respond
      Ptr<OEN> oen_p = Create<OEN> (produced3Nname->getName());
      oen_p->SetLifetime(m_3n_lifetime);
      // Ensure that the lease time is set in the PDU
      // We send the lease out in absolute simulator time
      Time absoluteLease = Simulator::Now () + m_3n_lease_time;
      oen_p->SetLeasetime (absoluteLease);
      // Add the PoA names to the PDU
      oen_p->AddPoa (returnAddrs);
      // Add my name to the PDU
      oen_p->SetSrcName (GetNode3NName ());
      // Add personal PoAs
      oen_p->AddPersonalPoa (GetAllPoANames (face));
      // Flag is the name is fixed or not
      oen_p->SetFixed (askingFixed);

      // Send the create OEN PDU out the way it came
      if (m_useQueues)
	EnqueueOEN(face, destAddr, oen_p);
      else
	face->SendOEN (oen_p, destAddr);

      Time remaining = ren_p->GetRemainLease ();
      NS_LOG_INFO("On " << myAddr << " creating an NNPT entry for " << *reenroll << " -> " << *produced3Nname << " until " << remaining.GetSeconds ());

      // Regardless of the name, we need to update the NNPT
      m_nnpt->addEntry (reenroll, produced3Nname, remaining);

      NS_LOG_INFO ("Making a lease entry in " << myAddr << " for " <<*produced3Nname << " until " << absoluteLease.GetSeconds ());
      // Maintain the lease time given to the 3N name for further checking
      m_node_lease_times[oen_p->GetNamePtr()] = absoluteLease;

      NS_LOG_DEBUG ("On " << myAddr << ":" << std::endl << *m_nnpt);

      // If we were asked for a fixed name, we need to keep track of it
      if (askingFixed)
	{
	  NS_LOG_INFO ("The created lease will be for a fixed node!");
	  m_fixed_name_pending.insert (produced3Nname);
	}

      m_outOENs (oen_p, face);

      // Schedule retransmission of OEN if we haven't received an AEN
      m_oen_timer = Simulator::Schedule(m_3n_retransmit_time, &ForwardingStrategy::RetransmitOEN, this, face, produced3Nname, destAddr, oen_p);
    }

    void
    ForwardingStrategy::OnDEN (Ptr<Face> face, Ptr<DEN> den_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inDENs (den_p, face);

      NNNAddress myAddr = GetNode3NName ();
      Ptr<NNNAddress> leavingAddr = Create<NNNAddress> (den_p->GetNamePtr ()->getName ());

      NS_LOG_INFO ("On " << myAddr << ", " << *leavingAddr << " is leaving");

      if (myAddr.isParentSector(*leavingAddr) || myAddr.isSubSector(*leavingAddr))
	{
	  if (m_return_ACKP)
	    {
	      NS_LOG_DEBUG ("Pushing ACKP before window due to disenroll");
	      Ptr<Face> outFace;
	      Address dst;
	      std::tie(outFace, dst) = m_nnst->ClosestSectorFaceInfo (leavingAddr, 0);
	      if (outFace)
		{
		  TransmitACKP (outFace, dst);
		}
	      else
		{
		  NS_LOG_WARN ("Face was null. Check code!");
		}
	    }

	  NS_LOG_INFO ("Adding " << *leavingAddr << " to buffers");

	  // We know the node sending the DEN is moving. His lease time will be maintained
	  // All we need to do is tell the buffer to keep the packets to that destination
	  if (m_3n_buffer_after_den)
	    m_node_pdu_buffer->AddDestination (leavingAddr, den_p->GetExpireTime ());

	  // The DEN PDU sends the lease expiry time in absolute simulator time
	  Time lease = den_p->GetExpireTime();

	  // Return a ADEN to the Face that sent the DEN
	  NS_LOG_INFO("Pushing ADEN with Node name " << *leavingAddr << " with lease until " << lease.GetSeconds ());

	  std::vector<Address> poanames = den_p->GetPoas ();

	  // Create the DEN PDU to transmit
	  Ptr<ADEN> aden_o = Create<ADEN> ();
	  // Set the lifetime for the ADEN PDU
	  aden_o->SetLifetime (m_3n_lifetime);
	  // Set the 3N name for the ADEN
	  aden_o->SetName (*leavingAddr);
	  // Set the 3N name expire time
	  aden_o->SetExpireTime(lease);
	  // Add all the PoA names we found
	  for (size_t i = 0; i < poanames.size (); i++)
	    {
	      aden_o->AddPoa (poanames[i]);
	    }
	  // Send the ADEN throughout the Faces
	  bool ok;
	  if (m_useQueues)
	    ok = EnqueueADEN(face, face->GetBroadcastAddress(), aden_o);
	  else
	    ok = face->SendADEN (aden_o);

	  if (ok)
	    {
	      m_outADENs (aden_o, face);
	      // At this point, we should at least reset the DEN flags
	      m_sent_ren = false;
	      m_on_ren_oen = false;
	    }

	  m_outADENs (aden_o, face);

	  Time now = Simulator::Now () + m_3n_retransmit_time;

	  if (m_waiting_aden_timeout->FoundName(*leavingAddr))
	    {
	      NS_LOG_WARN ("Name " << *leavingAddr << " is still active, probably retransmission, do not propagate");

	      m_waiting_aden_timeout->UpdateLeaseTime (*leavingAddr, lease);

	      // Schedule retransmission of OEN if we haven't received an AEN
	      //	      Simulator::Schedule(shift, &ForwardingStrategy::deleteAndPropagateDEN, this, leavingAddr);
	    }
	  else
	    {
	      NS_LOG_INFO ("Record released name at " << lease.GetSeconds() << " for " <<  *leavingAddr << " after sending ADEN and saving DEN");

	      // The first PoA is given higher priority
	      m_waiting_aden_timeout->Add (leavingAddr, face, poanames.front (), lease, m_standardMetric -1, false);

	      // Eliminate the first one, since it has already been added
	      poanames.erase (poanames.begin ());

	      // Insert the remaining PoAs
	      m_waiting_aden_timeout->Add (leavingAddr, face, poanames, now, m_standardMetric, false);

	      // Save the Addr and the DEN in a buffer
	      m_node_den_buffer->AddDestination(leavingAddr, lease);

	      m_node_den_buffer->PushDEN(leavingAddr, den_p);

	      NS_LOG_INFO ("Executing DEN propagation");
	      PropagateDEN(leavingAddr);

	      if (myAddr.isSubSector(*leavingAddr))
		{
		  NS_LOG_INFO ("On " << myAddr << ", parent " << *leavingAddr <<
			       " is leaving, forcing re-enrollment at " << (now + 2*m_3n_retransmit_time).GetSeconds());
		  Simulator::Schedule(2*m_3n_retransmit_time, &ForwardingStrategy::Reenroll, this);
		}
	    }
	}
      else
	{
	  NS_LOG_WARN ("Got a DEN for " << *leavingAddr << " that doesn't belong to my name tree, skipping");
	}
    }

    void
    ForwardingStrategy::deleteAndPropagateDEN (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this << *addr);

      Time now = Simulator::Now ();

      // Check if the NNST structure for DEN waiting for reply has the name we are checking for
      if (m_waiting_aden_timeout->FoundName(*addr))
	{
	  Time exitTime = m_waiting_aden_timeout->GetLeaseTime (*addr);
	  // If the name exists, check the time
	  if (now > exitTime)
	    {
	      // We are past the time, call the DEN propagation
	      PropagateDEN(addr);
	    }
	  else
	    {
	      NS_LOG_INFO ("This shouldn't happen - " << now << " vs " << exitTime);
	    }
	}
      else
	{
	  NS_LOG_INFO ("Normal flow, NNST structure deleted " << *addr);
	  // The NNST structure took care of everything, call the propagation
	  PropagateDEN (addr);
	}
    }

    void
    ForwardingStrategy::PropagateDEN (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this << *addr);

      Ptr<NNNAddress> leavingAddr = Create<NNNAddress> (addr->toDotHex ());

      if (m_node_den_buffer->DestinationExists (leavingAddr))
	{
	  NS_LOG_INFO ("Found information for " << *addr);
	  NNNAddress myAddr = GetNode3NName ();

	  NS_LOG_DEBUG ("Attempting to extract DEN queue for " << *leavingAddr);
	  std::queue<Ptr<Packet> > addrQueue = m_node_den_buffer->PopQueue(leavingAddr);

	  while (!addrQueue.empty ())
	    {
	      NS_LOG_DEBUG ("Obtaining a packet from the queue");
	      // Get the PDU at the front of the queue
	      Ptr<Packet> queuePDU = addrQueue.front();

	      NS_LOG_DEBUG ("Converting the packet to DEN PDU type");
	      // Convert the Packet back to a DEN
	      Ptr<DEN> den_p = wire::nnnSIM::DEN::FromWire(queuePDU);

	      NS_LOG_DEBUG ("Checking the distances of the subsectors");
	      // If the DEN packet arrives at a node that is less than 2 hops away, then we
	      // forward the DEN packet to the parent of this node
	      if (leavingAddr->distance (myAddr) <= 2 && leavingAddr->isSubSector (myAddr))
		{
		  NS_LOG_INFO ("We can still attempt to propagate DEN to parents");
		  // Now we forward the DEN information to the higher hierarchical nodes
		  std::vector<std::pair<Ptr<Face>, Address> > hierarchicalFaces = m_nnst->OneHopParentSectorFaceInfo (myAddr, 0);
		  std::vector<std::pair<Ptr<Face>, Address> >::iterator it;

		  Ptr<Face> outFace;
		  Address destAddr;
		  bool propagated = false;
		  bool ok = false;

		  for (it = hierarchicalFaces.begin (); it != hierarchicalFaces.end (); ++it)
		    {
		      outFace = it->first;
		      destAddr = it->second;

		      NS_LOG_INFO ("Pushing DEN out Face " << *outFace);

		      if (m_useQueues)
			ok = EnqueueDEN(outFace, destAddr, den_p);
		      else
			ok = outFace->SendDEN (den_p, destAddr);

		      if (ok)
			{
			  propagated = true;
			  // Log that the DEN PDU was sent
			  m_outDENs (den_p, outFace);
			}
		    }

		  if (propagated)
		    NS_LOG_INFO ("On " << myAddr << " found parent sectors to propagate DEN to");
		  else
		    NS_LOG_INFO ("On " << myAddr << " no parent sectors to propagate DEN to, stopping propagation");
		}
	      else
		{
		  NS_LOG_INFO ("On " << myAddr << " we have left the sector and are too far, stopping parent propagation");
		}

	      if (leavingAddr->isParentSector(myAddr))
		{
		  NS_LOG_INFO ("We can still attempt to propagate DEN to subsectors");
		  // Now we forward the DEN information to the lower nodes
		  std::vector<std::pair<Ptr<Face>, Address> > hierarchicalFaces = m_nnst->OneHopSubSectorFaceInfo (myAddr, 0);
		  std::vector<std::pair<Ptr<Face>, Address> >::iterator it;

		  Ptr<Face> outFace;
		  Address destAddr;
		  bool propagated = false;
		  bool ok = false;

		  for (it = hierarchicalFaces.begin (); it != hierarchicalFaces.end (); ++it)
		    {
		      outFace = it->first;
		      destAddr = it->second;

		      NS_LOG_INFO ("Pushing DEN out Face " << *outFace);

		      if (m_useQueues)
			ok = EnqueueDEN(outFace, destAddr, den_p);
		      else
			ok = outFace->SendDEN (den_p, destAddr);

		      if (ok)
			{
			  propagated = true;
			  // Log that the DEN PDU was sent
			  m_outDENs (den_p, outFace);
			}
		    }

		  if (propagated)
		    NS_LOG_INFO ("On " << myAddr << " found subsectors to propagate DEN to");
		  else
		    NS_LOG_INFO ("On " << myAddr << " no subsectors to propagate DEN to, stopping propagation");
		}
	      else
		{
		  NS_LOG_INFO ("On " << myAddr << " we have left the sector, stopping subsector propagation");
		}

	      addrQueue.pop ();
	    }

	  NS_LOG_DEBUG ("Deleting DEN queue for " << *leavingAddr);
	  m_node_den_buffer->RemoveDestination(leavingAddr);
	}
    }

    void
    ForwardingStrategy::OnADEN (Ptr<Face> face, Ptr<ADEN> aden_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inADENs (aden_p, face);

      NNNAddress myAddr = GetNode3NName ();
      Ptr<const NNNAddress> obtainedName = aden_p->GetNamePtr();

      if (myAddr == *obtainedName)
	{
	  if (m_den_timer.IsRunning())
	    {
	      NS_LOG_DEBUG ("On " << myAddr << " got an ADEN, removing entry from awaiting DEN response!");
	      m_awaiting_den_response->Remove (obtainedName);

//	      NS_LOG_DEBUG ("On " << myAddr << " eliminating information in NNST for Disenrolled Face " << face->GetId());
//	      m_nnst->RemoveFromAll(face);

//	      NS_LOG_DEBUG ("On " << myAddr << " removing any Flowids that are remaining");
//	      CancelAndEliminateAllSentFlowids(face);
	      NS_LOG_INFO ("On " << myAddr << " stack readying for disassociation!");
	      hasDisenrolled();

	      NS_LOG_INFO ("On " << myAddr << " cancelling disenroll");
	      m_den_timer.Cancel();

	      NS_LOG_INFO ("On " << myAddr << " clear to disassociate!");
	      m_canDisassociate (face->GetId ());
	    }
	  else
	    {
	      NS_LOG_WARN ("Have already obtained ADEN for " << myAddr << " probably retransmission, ignoring");
	    }
	}
      else if (GoesBy3NName(obtainedName) && m_aden_from_en)
	{
	  if (m_den_timer.IsRunning())
	    {
	      NS_LOG_INFO ("On " << myAddr << " got an ADEN for EN obtained " << *obtainedName << " removing entry from awaiting DEN response!");
	      m_awaiting_den_response->Remove (obtainedName);

	      NS_LOG_INFO ("On " << myAddr << " cancelling disenroll");
	      m_den_timer.Cancel();

	      m_aden_from_en = false;
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("On " << myAddr << " received an ADEN that was not for this node. Was for " << *obtainedName << "!");
	}
    }

    void
    ForwardingStrategy::OnOEN (Ptr<Face> face, Ptr<OEN> oen_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inOENs (oen_p, face);

      // ---- Attempt to lower memory usage ----
      Ptr<const NNNAddress> obtainedName = oen_p->GetNamePtr();

      std::set<NNNAddress>::iterator it2 = m_names_in_ram.find(obtainedName->getName());

      if (it2 != m_names_in_ram.end ())
	{
	  obtainedName = Ptr<const NNNAddress> (&(*it2));
	}
      else
	{
	  m_names_in_ram.insert (*obtainedName);
	}
      // --------

      // The OEN PDU sends the lease expiry time in absolute simulator time
      Time lease = oen_p->GetLeasetime();

      NS_LOG_INFO ("Obtained with " << *obtainedName << " with lease until " << lease.GetSeconds());

      // Check if we have obtained a fixed name when we asked for one
      if (m_AskFixedName != oen_p->IsFixed ())
	{
	  if (m_AskFixedName)
	    NS_LOG_INFO ("Asked for a fixed name, didn't receive what was requested, returning");
	  else
	    NS_LOG_INFO ("Asked for a mobile name, didn't receive what was requested, returning");

	  return;
	}

      // Get a list of this node's PoAs
      std::vector<Address> myPoas = GetAllPoANames (face);

      // Set difference requires the vectors to be sorted. Must unfortunately be done.
      // Create the sorted vector to be the size of myPoas
      std::vector<Address> sorted_myPoas (myPoas.size ());
      std::partial_sort_copy (myPoas.begin (), myPoas.end (), sorted_myPoas.begin (), sorted_myPoas.end ());

      // Get the list of PoAs in the OEN
      std::vector<Address> oenPoas = oen_p->GetPoas ();

      std::vector<Address> sorted_oenPoas (oenPoas.size ());
      std::partial_sort_copy (oenPoas.begin (), oenPoas.end (), sorted_oenPoas.begin (), sorted_oenPoas.end ());

      // Check differences
      std::vector<Address> diff;

      // Check if there are any differences
      std::set_difference(sorted_myPoas.begin (), sorted_myPoas.end (),
			  sorted_oenPoas.begin (), sorted_oenPoas.end (),
			  std::inserter(diff, diff.begin ()));

      if (diff.size () != 0)
	{
	  NS_LOG_INFO ("We found discrepancies with the destination PoAs. This PDU is not for this node");


	  NS_LOG_DEBUG ("Node PoAs -");
	  for (size_t i = 0; i < myPoas.size (); i++ )
	    {
	      NS_LOG_DEBUG (i << ": " << myPoas[i]);
	    }
	  NS_LOG_DEBUG ("Received PoAs -");
	  for (size_t i = 0; i < oenPoas.size (); i++ )
	    {
	      NS_LOG_DEBUG (i << ": " << oenPoas[i]);
	    }

	  NS_LOG_DEBUG ("Difference in PoAs -");
	  for (size_t i = 0; i < diff.size (); i ++)
	    {
	      NS_LOG_DEBUG (i << ": " << diff[i]);
	    }
	}
      else
	{
	  NS_LOG_INFO ("We found no discrepancies with destination PoAs, this seems to be for us");

	  NS_LOG_DEBUG ("Node PoAs -");
	  for (size_t i = 0; i < myPoas.size (); i++ )
	    {
	      NS_LOG_DEBUG (i << ": " << myPoas[i]);
	    }
	  NS_LOG_DEBUG ("Received PoAs -");
	  for (size_t i = 0; i < oenPoas.size (); i++ )
	    {
	      NS_LOG_DEBUG (i << ": " << oenPoas[i]);
	    }

	  NS_LOG_DEBUG ("Difference in PoAs -");
	  for (size_t i = 0; i < diff.size (); i ++)
	    {
	      NS_LOG_DEBUG (i << ": " << diff[i]);
	    }

	  std::vector<Address> personalPoas = oen_p->GetPersonalPoas ();

	  // Get the first PoA name
	  Address top = personalPoas.front ();

	  // Erase the first PoA name
	  personalPoas.erase (personalPoas.begin ());

	  bool migrated = false;
	  bool accepted_name = false;

	  // Check if we have a 3N name
	  if (Has3NName ())
	    {
	      Ptr<const NNNAddress> curr = GetNode3NNamePtr();
	      // As long as the name is not the same, we can use the name
	      if (*curr != *obtainedName)
		{
		  if (curr->compareName(*obtainedName) > 0)
		    {
		      NS_LOG_INFO ("Node had " << GetNode3NName () << " now taking " << *obtainedName << " until " << lease.GetSeconds());
		      SetNode3NName(obtainedName, lease, m_AskFixedName);

		      // In theory we restart the NNST with the information from the first NE
		      NS_LOG_DEBUG ("Eliminating information in NNST for previously disenrolled Face: " << face->GetId());
		      m_nnst->RemoveFromAll(face);

		      migrated = true;
		      accepted_name = true;
		    }
		  else
		    {
		      NS_LOG_WARN ("Node has " << GetNode3NName () << " which is preferred to " << *obtainedName << ", rejecting 3N name offer");

		      NS_LOG_INFO ("Integrating 3N name information for "  << *(oen_p->GetSrcNamePtr ()) << " from Face: " << face->GetId ());
		    }

		  // Add the information to NNST
		  m_nnst->Add (oen_p->GetSrcNamePtr (), face, top, lease, m_standardMetric -1, m_AskFixedName);
		  m_nnst->Add (oen_p->GetSrcNamePtr (), face, personalPoas, lease, m_standardMetric, m_AskFixedName);
		}
	      else
		{
		  NS_LOG_INFO ("Node is already using " << GetNode3NName () << ". Probably retransmission issue, retransmitting AEN");
		  accepted_name = true;
		}

	      if (migrated && !m_hasDisenrolled)
		{
		  NS_LOG_INFO ("Obtained better name " << *obtainedName << " , disenrolling " << *curr);
		  DisenrollName(curr);
		}
	    }
	  else
	    {
	      NS_LOG_INFO("Node has no name, taking " << *obtainedName << " until " << lease.GetSeconds ());
	      SetNode3NName(obtainedName, lease, m_AskFixedName);

	      // Add the information to NNST
	      m_nnst->Add (oen_p->GetSrcNamePtr (), face, top, lease, m_standardMetric -1, m_AskFixedName);
	      m_nnst->Add(oen_p->GetSrcNamePtr (), face, personalPoas, lease, m_standardMetric, m_AskFixedName);

	      accepted_name = true;
	    }

	  NS_LOG_INFO ("Added information to NNST. New " << *m_nnst);

	  if (!accepted_name)
	    {
	      NS_LOG_WARN ("Offered 3N name: " << *obtainedName << " not accepted, not pushing AEN");
	      return;
	    }

	  // Now create the AEN PDU to respond
	  Ptr<AEN> aen_p = Create<AEN> (*obtainedName);
	  aen_p->SetLifetime(m_3n_lifetime);
	  // Ensure that the lease time is set right (continues to be in absolute simulator time)
	  aen_p->SetLeasetime (lease);
	  // Add the PoAs to the response PDU
	  aen_p->AddPoa(myPoas);
	  // Flag if you have obtained a fixed 3N name
	  aen_p->SetFixed (m_AskFixedName);

	  uint32_t face_id = face->GetId();

	  std::map<uint32_t, EventId>::iterator it = m_aen_end.find(face_id);

	  Time now = Simulator::Now ();

	  if (it != m_aen_end.end())
	    {
	      if (migrated)
		{
		  NS_LOG_DEBUG ("Event existed, but 3N name changed, reschedule Enrollment execution at " << (now + m_3n_retransmit_time).GetSeconds ());
		  it->second.Cancel();
		  m_aen_end[face_id] = Simulator::Schedule(Seconds (0), &ForwardingStrategy::ExecuteEnrollment, this, face);
		}
	      else
		{
		  if (it->second.IsRunning())
		    {
		      NS_LOG_DEBUG ("Event existed, but AEN probably didn't arrive, retransmit AEN and reschedule Enrollment execution at " << (now + m_3n_retransmit_time).GetSeconds ());
		      it->second.Cancel();
		      m_aen_end[face_id] = Simulator::Schedule(Seconds (0), &ForwardingStrategy::ExecuteEnrollment, this, face);
		    }
		  else
		    {
		      NS_LOG_WARN ("Migration event has expired! Only pushing new AEN");
		    }
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("First time seeing this OEN, scheduling Enrollment execution at " << now.GetSeconds ());
	      m_aen_end[face_id] = Simulator::Schedule(Seconds (0), &ForwardingStrategy::ExecuteEnrollment, this, face);
	    }

	  NS_LOG_INFO("Pushing AEN with Node name " << *obtainedName << " with lease until " << lease.GetSeconds ());
	  // Send the created AEN PDU
	  if (m_useQueues)
	    EnqueueAEN(face, face->GetBroadcastAddress(), aen_p);
	  else
	    face->SendAEN(aen_p);

	  m_outAENs (aen_p, face);

	  if (m_en_timer.IsRunning ())
	    {
	      NS_LOG_INFO ("Got OEN, cancelling next enroll");
	      m_en_timer.Cancel ();
	    }

	  if (m_ren_timer.IsRunning ())
	    {
	      NS_LOG_INFO ("Got OEN, cancelling next reenroll");
	      m_ren_timer.Cancel ();
	    }
	}
    }

    void
    ForwardingStrategy::ExecuteEnrollment (Ptr<Face> face)
    {
      NS_LOG_FUNCTION(this);

      hasReenrolled ();

      // If this came from a REN PDU, signal that you have received
      if (m_sent_ren)
	{
	  m_sent_ren = false;
	  m_on_ren_oen = true;
	}

      // Signal that we are completely associated
      NS_LOG_INFO ("Calling PoA Association callback");
      m_PoAAssociation ();

      // Empty buffers
      EmptyDisenrollPDUBuffer ();

      // Migrate any flowids that have not been ACKed or are in limbo
      NS_LOG_DEBUG ("On " << GetNode3NName () << " migrating any remaining Flowids for Face: " << face->GetId ());
      MigrateAllSentFlowids(face);
    }

    void
    ForwardingStrategy::OnINF (Ptr<Face> face, Ptr<INF> inf_p)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_inINFs (inf_p, face);

      bool ok = false;
      bool routed = false;
      bool propagated = false;

      NNNAddress myAddr = GetNode3NName ();

      Ptr<NNNAddress> oldName = Create<NNNAddress> (inf_p->GetOldName ());
      Ptr<NNNAddress> newName = Create<NNNAddress> (inf_p->GetNewName ());

      NNNAddress endSector = inf_p->GetOldNamePtr ()->getSectorName ();

      if (myAddr != endSector)
	{
	  NS_LOG_INFO("On " << myAddr << " have not yet reached sector. Attempting to forward to " << endSector);

	  // Roughly pick the next hop that would bring us closer to the endSector
	  std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endSector, 0);

	  // Just like with DEN, we need to inform our higher ups that things have changed in this sector
	  std::vector<std::pair<Ptr<Face>, Address> > hierarchicalFaces = m_nnst->OneHopParentSectorFaceInfo (myAddr, 0);
	  std::vector<std::pair<Ptr<Face>, Address> >::iterator it;

	  Ptr<Face> outFace;
	  // First send out the routed Face
	  Ptr<Face> routedFace = tmp.first;
	  Address destAddr = tmp.second;

	  if (routedFace != face && routedFace != 0)
	    {
	      if (m_useQueues)
		ok = EnqueueINF(routedFace, destAddr, inf_p);
	      else
		ok = routedFace->SendINF (inf_p, destAddr);
	    }

	  if (ok)
	    {
	      // Log that the INF PDU was sent
	      m_outINFs (inf_p, routedFace);
	      routed = true;
	    }

	  if (routed)
	    NS_LOG_INFO ("On " << myAddr << " forwarded one hop closer to " << endSector);

	  // Check how far we are from old Name
	  if (oldName->distance (myAddr) <= 2)
	    {
	      for (it = hierarchicalFaces.begin (); it != hierarchicalFaces.end (); ++it)
		{
		  // Check the information for the hierarchical Faces
		  outFace = it->first;
		  destAddr = it->second;

		  // Should we stumble on a face that is how we got here, skip
		  if (outFace == face)
		    continue;

		  // If we routed the INF already, skip this face
		  if (routed && routedFace == face)
		    continue;

		  // After all checks, start sending
		  ok = outFace->SendINF (inf_p, destAddr);

		  if (ok)
		    {
		      // Log that the INF PDU was sent
		      m_outINFs (inf_p, outFace);
		      propagated = true;
		    }
		}

	      if (propagated)
		NS_LOG_INFO ("On " << myAddr << " found parent sector to propagate to");
	    }
	  else
	    {
	      NS_LOG_INFO ("On " << myAddr << " we are too far from " << endSector << ", no forwarding to parent sectors");
	    }
	}
      else
	{
	  NS_LOG_INFO ("On " << myAddr << " reached sector, ending, removing NNST information");

	  m_nnst->Remove (inf_p->GetOldNamePtr ());
	}

      // We have inserted the INF information. Now flush the relevant buffer
      flushBuffer (face, oldName, newName);

      NS_LOG_INFO ("On " << myAddr << " creating NNPT Entry " << *inf_p->GetOldNamePtr () << "\t" << *inf_p->GetNewNamePtr () << "\t" << inf_p->GetRemainLease ());
      // Update our NNPT with the information in the INF PDU
      m_nnpt->addEntry (inf_p->GetOldNamePtr (), inf_p->GetNewNamePtr (), inf_p->GetRemainLease ());
    }

    void
    ForwardingStrategy::OnNULLp (Ptr<Face> face, Ptr<NULLp> null_p)
    {
    }

    void
    ForwardingStrategy::OnLayeredNULLp (Ptr<Face> face, Ptr<NULLp> null_p,
					const Address& from, const Address& to)
    {
    }

    void
    ForwardingStrategy::OnSO (Ptr<Face> face, Ptr<SO> so_p)
    {
    }

    void
    ForwardingStrategy::OnLayeredSO (Ptr<Face> face, Ptr<SO> so_p,
				     const Address& from, const Address& to)
    {
    }

    void
    ForwardingStrategy::OnDO (Ptr<Face> face, Ptr<DO> do_p)
    {
    }

    void
    ForwardingStrategy::OnLayeredDO (Ptr<Face> face, Ptr<DO> do_p,
				     const Address& from,
				     const Address& to)
    {
    }

    void
    ForwardingStrategy::OnDU (Ptr<Face> face, Ptr<DU> du_p)
    {
    }



    void
    ForwardingStrategy::OnLayeredDU (Ptr<Face> face, Ptr<DU> du_p,
				     const Address& from,
				     const Address& to)
    {
    }

    void
    ForwardingStrategy::OnRHR (Ptr<Face> face, Ptr<RHR> rhr_p)
    {
      NS_LOG_FUNCTION (this);

      Ptr<const NNNAddress> query_name = rhr_p->GetQueryNamePtr ();

      if (!query_name)
	{
	  NS_LOG_DEBUG ("Query name is NULL, something is off!");
	  return;
	}

      NS_LOG_INFO ("Received query for " << *query_name);
      bool hasSrc = rhr_p->HasSource ();
      bool isDirected = rhr_p->HasDestination ();

      bool amFixed = false;
      bool src_fixed = false;
      bool query_fixed = false;
      uint32_t query_distance = 0;
      bool query_definitive  = false;
      Time query_lease = Seconds (0);
      Time src_lease = Seconds (0);
      Ptr<const NNNAddress> src_name;
      Ptr<const NNNAddress> dst_name;
      bool hasName = Has3NName ();
      bool ok = false;

      if (hasSrc)
	{
	  src_name = rhr_p->GetSrcNamePtr ();
	  src_fixed = rhr_p->IsSrcFixed ();
	  src_lease = rhr_p->GetSrcLease ();

	  if (!src_name)
	    {
	      NS_LOG_DEBUG ("Src name is NULL, something is off!");
	      return;
	    }
	  NS_LOG_INFO ("Query is from: " << *src_name << " isFixed: " << src_fixed << " Lease: " << src_lease);
	}

      if (isDirected)
	{
	  dst_name = rhr_p->GetDstNamePtr ();

	  if (!dst_name)
	    {
	      NS_LOG_DEBUG ("Dst name is NULL, something is off!");
	      return;
	    }

	  NS_LOG_INFO ("Query was directed to: " << *dst_name);
	}

      // Variables needed for further modifications
      bool redirect3N = false;
      Ptr<Face> outFace;
      Address destAddr;
      NNNAddress endDest;
      Ptr<const NNNAddress> searchDest;

      // Begin creating the returning OHR
      Ptr<OHR> ohr_o = Create<OHR> ();

      // If this node has a name, add that information to the OHR
      if (hasName)
	{
	  Ptr<const NNNAddress> srcName = GetNode3NNamePtr ();
	  amFixed = m_node_names->isFixed (srcName);
	  Time lease = m_node_names->findNameExpireTime (srcName);

	  NS_LOG_INFO ("Creating OHR at: " << *srcName << " fixed: " << amFixed << " Lease: " << lease);
	  ohr_o->SetSrcName (srcName);
	  ohr_o->SetSrcFixed (amFixed);
	  ohr_o->SetSrcLease (lease);
	}

      // If the RHR had a source name, use that as the destination
      if (hasSrc)
	{
	  NS_LOG_INFO ("OHR is being created to go to " << *src_name);

	  redirect3N = m_nnpt->foundOldName (src_name);
	  searchDest = m_nnpt->findPairedNamePtr (src_name);
	  endDest = searchDest->getName ();

	  if (redirect3N)
	    NS_LOG_INFO ("We are redirecting " << *src_name << " to " << *searchDest);
	  else
	    {
	      NS_LOG_DEBUG ("Didn't find any redirection for " << *src_name);
	    }

	  ohr_o->SetDstName (searchDest);
	}
      else
	{
	  NS_LOG_INFO ("Got a RHR without a source. Nothing we can do, returning");
	  return;
	}

      // Update the distance to the source
      ohr_o->SetDestDistance (0);

      // Now we decide how to fill in the information about the query
      // and how to route the OHR we constructed when necessary
      if (isDirected)
	{
	  // Check if we are the target
	  if (GoesBy3NName (dst_name))
	    {
	      NS_LOG_INFO ("The RHR was directed at this node. Attempting to answer");

	      if (GoesBy3NName (query_name))
		{
		  query_distance = 0;
		  query_definitive = true;
		  query_fixed = m_node_names->isFixed (query_name);
		  query_lease = m_node_names->findNameExpireTime (query_name);
		  NS_LOG_INFO ("We are the node looked by the RHR, distance to return: " << query_distance);
		  NS_LOG_INFO ("Query lease: " << query_lease);
		}
	      else
		{
		  NS_LOG_INFO ("Searching NNST for " << *query_name);
		  Ptr<nnst::Entry> tmpEntry = m_nnst->Find (query_name);
		  if (!tmpEntry)
		    {
		      // We don't have a clear path, so we calculate the distance from the
		      // us to the nearest intermediate node
		      tmpEntry = m_nnst->ClosestSector (query_name);

		      query_distance = tmpEntry->FindBestCandidate ().GetRoutingCost ();
		      // No direct path, might as well make sure it is known that this should be asked again
		      // some time soon
		      query_lease = m_node_names->findNewestExpireTime () / 8;
		      NS_LOG_INFO ("We do not have a direct path, calculating distance: " << query_distance);
		      NS_LOG_INFO ("Query lease: " << query_lease);
		    }
		  else
		    {
		      // We have seen this address, hand over the information
		      query_distance = tmpEntry->FindBestCandidate ().GetRoutingCost ();
		      query_definitive = true;
		      query_fixed = tmpEntry->IsFixed ();
		      query_lease = tmpEntry->FindBestCandidate ().GetExpireTime ();
		      NS_LOG_INFO ("We are connected to the queried 3N name, distance to return: " << query_distance);
		      NS_LOG_INFO ("Query lease: " << query_lease);
		    }
		}

	      ohr_o->SetDefiniteQueryInfo (query_definitive);
	      ohr_o->SetQueryName (query_name);
	      ohr_o->SetQueryDistance (query_distance);
	      ohr_o->SetQueryFixed (query_fixed);
	      ohr_o->SetQueryLease(query_lease);

	      // Roughly pick the next hop that would bring us closer to endDest
	      std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

	      outFace = tmp.first;
	      destAddr = tmp.second;

	      if (!outFace)
		{
		  NS_LOG_WARN ("Obtained Face is NULL, please check for errors!");
		}
	      else
		{
		  // Finally add the PoA name to what is being returned
		  ohr_o->SetPoa (outFace->GetAddress ());
		  NS_LOG_INFO ("Sending OHR to: " << endDest << " PoA: " << destAddr << " out Face: " << *outFace);
		  // Push out the PDU
		  ok = outFace->SendOHR (ohr_o, destAddr);

		  if (ok)
		    m_outOHRs (ohr_o, outFace);
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("The RHR was not directed at this node, forwarding RHR");

	      redirect3N = m_nnpt->foundOldName (dst_name);
	      searchDest = m_nnpt->findPairedNamePtr (dst_name);
	      endDest = searchDest->getName ();

	      if (redirect3N)
		NS_LOG_INFO ("We are redirecting " << *dst_name << " to " << *searchDest);
	      else
		{
		  NS_LOG_DEBUG ("Didn't find any redirection for " << *dst_name);
		}

	      rhr_p->SetDstName (searchDest);

	      // Roughly pick the next hop that would bring us closer to endDest
	      std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

	      outFace = tmp.first;
	      destAddr = tmp.second;

	      if (!outFace)
		{
		  NS_LOG_WARN ("Obtained Face is NULL, please check for errors!");
		}
	      else
		{
		  // Finally add the PoA name to what is being returned
		  rhr_p->SetPoa (outFace->GetAddress ());
		  NS_LOG_INFO ("Sending RHR to: " << endDest << " PoA: " << destAddr << " out Face: " << *outFace);
		  // Push out the PDU
		  if (m_useQueues)
		    ok = EnqueueRHR(outFace, destAddr, rhr_p);
		  else
		    ok = outFace->SendRHR (rhr_p, destAddr);

		  if (ok)
		    m_outRHRs (rhr_p, outFace);
		}
	    }
	}
      else
	{
	  NS_LOG_INFO ("Received a one hop request");

	  // Change redirection
	  ohr_o->SetDstName (searchDest);

	  if (GoesBy3NName (query_name))
	    {
	      query_distance = 0;
	      query_definitive = true;
	      query_fixed = m_node_names->isFixed (query_name);
	      query_lease = m_node_names->findNameExpireTime(query_name);
	      NS_LOG_INFO ("We are the node looked by the RHR, distance to return: " << query_distance);
	      NS_LOG_INFO ("Query lease: " << query_lease);
	    }
	  else
	    {
	      NS_LOG_INFO ("Searching NNST for " << *query_name);
	      Ptr<nnst::Entry> tmpEntry = m_nnst->Find (query_name);
	      if (!tmpEntry)
		{
		  // We don't have a clear path, so we calculate the distance from the
		  // us to the nearest intermediate node
		  tmpEntry = m_nnst->ClosestSector (query_name);

		  query_distance = tmpEntry->FindBestCandidate ().GetRoutingCost ();
		  query_lease = m_node_names->findNewestExpireTime () / 8;
		  NS_LOG_INFO ("We do not have a direct path, calculating distance: " << query_distance);
		  NS_LOG_INFO ("Query lease: " << query_lease);
		}
	      else
		{
		  // We have seen this address, hand over the information
		  query_distance = tmpEntry->FindBestCandidate ().GetRoutingCost ();
		  query_definitive = true;
		  query_fixed = tmpEntry->IsFixed ();
		  query_lease = tmpEntry->FindBestCandidate ().GetExpireTime ();
		  NS_LOG_INFO ("We are connected to the queried 3N name, distance to return: " << query_distance);
		}
	    }

	  ohr_o->SetDefiniteQueryInfo (query_definitive);
	  ohr_o->SetQueryName (query_name);
	  ohr_o->SetQueryDistance (query_distance);
	  ohr_o->SetQueryFixed (query_fixed);
	  ohr_o->SetQueryLease(query_lease);

	  NS_LOG_INFO ("No search for Face, returning the way it was received via Face: " << face->GetId());
	  // Roughly pick the next hop that would bring us closer to endDest
	  std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

	  outFace = tmp.first;
	  destAddr = tmp.second;

	  if (!face)
	    {
	      NS_LOG_WARN ("Obtained Face is NULL, please check for errors!");
	    }
	  else
	    {
	      // Finally add the PoA name to what is being returned
	      ohr_o->SetPoa (face->GetAddress ());
	      NS_LOG_INFO ("Sending OHR to: " << endDest << " PoA: " << destAddr << " out Face: " << *face);
	      // Push out the PDU
	      if (m_useQueues)
		ok = EnqueueOHR(face, destAddr, ohr_o);
	      else
		ok = face->SendOHR (ohr_o, destAddr);

	      if (ok)
		m_outOHRs (ohr_o, face);
	    }
	}
    }

    void
    ForwardingStrategy::OnOHR (Ptr<Face> face, Ptr<OHR> ohr_p)
    {
      NS_LOG_FUNCTION (this);

      Ptr<const NNNAddress> query_name = ohr_p->GetQueryNamePtr ();

      if (!query_name)
	{
	  NS_LOG_DEBUG ("Query name is NULL, something is off!");
	  return;
	}

      NS_LOG_INFO ("Received query for " << *query_name);
      bool hasSrc = ohr_p->HasSource ();
      bool isDirected = ohr_p->HasDestination ();
      bool hasDefiniteQuery = ohr_p->HasDefiniteQueryInfo ();

      bool src_fixed = false;
      Ptr<const NNNAddress> src_name;
      Time src_lease = Seconds (0);
      uint32_t distance_to_src = 0;
      Address poa = ohr_p->GetPoa ();

      uint16_t face_cost = face->GetMetric ();

      NS_LOG_INFO ("Calculated cost for face is: " << face_cost);

      // Obtain where this came from
      if (hasSrc)
	{
	  src_name = ohr_p->GetSrcNamePtr ();
	  src_fixed = ohr_p->IsSrcFixed();
	  src_lease = ohr_p->GetSrcLease ();
	  distance_to_src = ohr_p->GetDestDistance () + face_cost;

	  NS_LOG_INFO ("Query was sent from " << *src_name);
	}

      bool query_fixed = ohr_p->IsQueryFixed();
      Time query_lease = ohr_p->GetQueryLease ();
      // Distance is the distance calculated by the other nodes plus the metric
      // for the Face through which the PDU arrived
      uint32_t distance_to_query = ohr_p->GetQueryDistance () + face_cost;

      Ptr<const NNNAddress> dst_name;

      // Now we find out who the destination was
      if (isDirected)
	{
	  dst_name = ohr_p->GetDstNamePtr ();
	  NS_LOG_INFO ("Query response is directed to " << *dst_name);
	}
      else
	{
	  NS_LOG_INFO ("If forwarded, response will be sent knowing only interface");
	}

      // If we definite query information, then we add it as is
      if (hasDefiniteQuery)
	{
	  NS_LOG_INFO ("Obtained definitive query response to " << *query_name);
	  NS_LOG_INFO ("Cost to query is: " << distance_to_query);

	  m_nnst->Add (query_name, face, poa, query_lease, distance_to_query, query_fixed);

	  NS_LOG_INFO ("Adding information to not to ask for " << *query_name << " until " << query_lease);
	  m_handshaken_names->addEntry (query_name, query_lease, false);
	}
      else
	{
	  NS_LOG_INFO ("Did not obtain a definitive query response to " << *query_name);
	  NS_LOG_INFO ("Will not include this information in NNST");
	}

      if (hasSrc)
	{
	  NS_LOG_INFO ("Aggregating information for source " << *src_name);
	  NS_LOG_INFO ("Cost to source is: " << distance_to_src);

	  m_nnst->Add (src_name, face, poa, src_lease, distance_to_src, src_fixed);

	  NS_LOG_INFO ("Adding information to not to ask for " << *src_name << " until " << src_lease);
	  m_handshaken_names->addEntry (src_name, src_lease, false);
	}
      else
	{
	  NS_LOG_INFO ("Will not include " << *src_name << " data in NNST");
	}

      // Now we have to find out how far to continue forwarding the OHR
      if (isDirected)
	{
	  if (GoesBy3NName (dst_name))
	    {
	      NS_LOG_INFO ("We initially attempted to handshake for " << *query_name);
	      NS_LOG_INFO ("No more routing for now");
	      NS_LOG_DEBUG ("Modified NNST:" << std::endl << *m_nnst);
	    }
	  else
	    {
	      NS_LOG_INFO ("Obtained an OHR PDU from " << *src_name << " that is headed to " << *dst_name);

	      bool redirect3N = false;
	      Ptr<Face> outFace;
	      Address destAddr;
	      NNNAddress endDest;
	      Ptr<const NNNAddress> searchDest;
	      bool ok = false;

	      // Modify the distance to update information using the Face metric we have
	      // This applies to both the query and the src
	      ohr_p->SetDestDistance (distance_to_src);
	      ohr_p->SetQueryDistance (distance_to_query);

	      redirect3N = m_nnpt->foundOldName (dst_name);
	      searchDest = m_nnpt->findPairedNamePtr (dst_name);
	      endDest = searchDest->getName ();

	      if (redirect3N)
		NS_LOG_INFO ("We are redirecting " << *dst_name << " to " << endDest);
	      else
		{
		  NS_LOG_DEBUG ("Didn't find any redirection for " << *dst_name);
		}

	      // Roughly pick the next hop that would bring us closer to endDest
	      std::pair<Ptr<Face>, Address> tmp = m_nnst->ClosestSectorFaceInfo (endDest, 0);

	      outFace = tmp.first;
	      destAddr = tmp.second;

	      if (!outFace)
		{
		  NS_LOG_WARN ("Obtained Face is NULL, please check for errors!");
		}
	      else
		{
		  NS_LOG_INFO ("Sending OHR to: " << endDest << " PoA: " << destAddr << " out Face: " << *outFace);
		  if (m_useQueues)
		    ok = EnqueueOHR(face, destAddr, ohr_p);
		  else
		    ok = outFace->SendOHR (ohr_p, destAddr);

		  if (ok)
		    m_outOHRs (ohr_p, outFace);
		}
	    }
	}
      else
	{
	  // Assume that the OHR is a single hop, use the information provided
	  NS_LOG_INFO ("Obtained OHR with no destination, halting retransmission");
	  NS_LOG_DEBUG ("Modified NNST:" << std::endl << *m_nnst);
	}
    }

    void
    ForwardingStrategy:: OnACKP (Ptr<Face> face, Ptr<ACKP> ack_p)
    {
      NS_LOG_FUNCTION (face->GetId () << ack_p->GetFlowid () << ack_p->GetSequence() << std::boolalpha << ack_p->IsOutofOrder());
      m_inACKPs(ack_p, face);

      std::map<uint32_t, uint32_t> flowid_map = ack_p->GetACKFlowids();
      std::map<uint32_t, uint32_t>::iterator it = flowid_map.begin ();

      // Remove the PDU from the buffer
      m_node_rxbuffer_container->AcknowledgePDU(face, face->GetBroadcastAddress(), ack_p);

      uint32_t face_id = face->GetId ();
      uint16_t dst_id = face->GetBroadcastId ();
      uint32_t flowid = 0;
      uint32_t sequence = 0;
      bool out_of_order = false;

      while (it != flowid_map.end ())
	{
	  flowid = it->first;
	  sequence = it->second;

	  out_of_order = IsACKPOutOfOrder(face_id, dst_id, flowid, sequence);

	  NS_LOG_DEBUG ("ACKP Acknowledging PDUs up to Flowid: " << flowid << " Sequence: " << it->second <<
			" In order: " << std::boolalpha << (!out_of_order));

	  CancelFlowidWindowWait(face_id, dst_id, flowid);
	  UpdateLastSeenACKP(face_id, dst_id, flowid, sequence, out_of_order);
	  TransmitNext3NWindow(face, face->GetBroadcastAddress(), flowid, sequence);
	  ++it;
	}
    }

    void
    ForwardingStrategy::OnLayeredACKP (Ptr<Face> face, Ptr<ACKP> ack_p, const Address& from, const Address& to)
    {
      NS_LOG_FUNCTION (face->GetId () << ack_p->GetFlowid () << ack_p->GetSequence() << std::boolalpha << ack_p->IsOutofOrder() << from);
      m_inACKPs(ack_p, face);

      std::map<uint32_t, uint32_t> flowid_map = ack_p->GetACKFlowids();
      std::map<uint32_t, uint32_t>::iterator it = flowid_map.begin ();

      uint32_t face_id = face->GetId ();
      uint16_t dst_id = face->GetAddressId(from);

      // Remove the PDU from the buffer
      m_node_rxbuffer_container->AcknowledgePDU (face, from, ack_p);

      uint32_t flowid = 0;
      uint32_t sequence = 0;
      bool out_of_order = false;

      while (it != flowid_map.end ())
	{
	  flowid = it->first;
	  sequence = it->second;

	  out_of_order = IsACKPOutOfOrder(face_id, dst_id, flowid, sequence);

	  NS_LOG_DEBUG ("ACKP Acknowledging PDUs up to Flowid: " << flowid << " Sequence: " << it->second <<
			" In order: " << std::boolalpha << (!out_of_order));

	  CancelFlowidWindowWait(face_id, dst_id, flowid);
	  UpdateLastSeenACKP (face_id, dst_id, flowid, sequence, out_of_order);
	  TransmitNext3NWindow(face, from, flowid, sequence);
	  ++it;
	}
    }

    void
    ForwardingStrategy::AddFace (Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this << face->GetId ());

      m_faces->Add (face);

      uint32_t faceid = face->GetId ();
      m_face_pdu_counter.insert(std::make_pair (faceid, 0));

      if (!m_useQueues)
	return;

      NS_LOG_INFO ("Will use queues for Face: " << faceid);
      Time next = Seconds (1 / ceil((m_3n_default_rate * 500) / 1024));
      NS_LOG_DEBUG ("For Face: " << faceid << " accepting transmission every: " << next.GetSeconds());

      if ((face->isAppFace() && face->IsApp3NEnabled()) || (face->IsNminus3NEnabled()))
	{
	  m_face_3n_address_queue[faceid] = std::queue<Address> ();
	  m_face_3n_pdu_queue[faceid] = Create<PDUQueue> ();
	  m_face_3n_tx_timer[faceid] = next;
	  m_face_3n_tx_event[faceid] = Simulator::Schedule(next, &ForwardingStrategy::Forward3NPDU, this, face);
	}

      if ((face->isAppFace() && face->IsAppICNEnabled()) || (face->IsNminusICNEnabled()))
	{
	  m_face_icn_address_queue[faceid] = std::queue<Address> ();
	  m_face_icn_pdu_queue[faceid] = Create<PDUQueue> ();
	  m_face_icn_tx_timer[faceid] = next;
	  m_face_icn_tx_event[faceid] = Simulator::Schedule(next, &ForwardingStrategy::ForwardICNPDU, this, face);
	}
    }

    void
    ForwardingStrategy::Forward3NPDU(Ptr<Face> face)
    {
      uint32_t faceid = face->GetId ();

      Ptr<PDUQueue> pdu_queue = m_face_3n_pdu_queue[faceid];

      if (!pdu_queue)
	{
	  NS_LOG_WARN ("PDU queue is null, verify");
	  return;
	}

      std::queue<Address> addr_queue = m_face_3n_address_queue[faceid];
      Time next = m_face_3n_tx_timer[faceid];
      Time now = Simulator::Now();

      if (!m_face_3n_tx_event[faceid].IsRunning())
	{
	  if (!pdu_queue->isEmpty())
	    {
	      NS_ASSERT_MSG ((pdu_queue->size() == m_face_3n_address_queue[faceid].size()), "PDU size and address size is different, check code for issues!");

	      Ptr<Packet> pdu = pdu_queue->pop();
	      Address addr = m_face_3n_address_queue[faceid].front();
	      m_face_3n_address_queue[faceid].pop();

	      if (face->isAppFace())
		{
		  NS_LOG_DEBUG ("Queue for Face: " << faceid << " send one 3N PDU to App, scheduling next transmission for " << (now + next).GetSeconds());
		  face->SendToApp(pdu);
		}
	      else
		{
		  NS_LOG_DEBUG ("Queue for Face: " << faceid << " send one 3N PDU to " <<  addr << ", scheduling next transmission for " << (now + next).GetSeconds());
		  face->SendToWire(pdu, addr);
		}

	      if (pdu_queue->isEmpty())
		{
		  NS_LOG_DEBUG ("Queue for Face: " << faceid << " has been emptied");
		}
	    }

	  m_face_3n_tx_event[faceid] = Simulator::Schedule(next, &ForwardingStrategy::Forward3NPDU, this, face);
	}
    }

    void
    ForwardingStrategy::ForwardICNPDU(Ptr<Face> face)
    {
      uint32_t faceid = face->GetId ();

      Ptr<PDUQueue> pdu_queue = m_face_icn_pdu_queue[faceid];

      if (!pdu_queue)
	{
	  NS_LOG_WARN ("PDU queue is null, verify");
	  return;
	}

      std::queue<Address> addr_queue = m_face_icn_address_queue[faceid];
      Time next = m_face_icn_tx_timer[faceid];
      Time now = Simulator::Now();

      if (!m_face_icn_tx_event[faceid].IsRunning())
	{
	  if (!pdu_queue->isEmpty())
	    {
	      NS_ASSERT_MSG ((pdu_queue->size() == m_face_icn_address_queue[faceid].size()), "PDU size and address size is different, check code for issues!");

	      Ptr<Packet> pdu = pdu_queue->pop();
	      Address addr = m_face_icn_address_queue[faceid].front();
	      m_face_icn_address_queue[faceid].pop();

	      if (face->isAppFace())
		{
		  NS_LOG_DEBUG ("Queue for Face: " << faceid << " send one ICN PDU to App, scheduling next transmission for " << (now + next).GetSeconds());
		  face->SendToApp(pdu);
		}
	      else
		{
		  NS_LOG_DEBUG ("Queue for Face: " << faceid << " send one ICN PDU to " <<  addr << ", scheduling next transmission for " << (now + next).GetSeconds());
		  face->SendToWire(pdu, addr);
		}

	      if (pdu_queue->isEmpty())
		{
		  NS_LOG_DEBUG ("Queue for Face: " << faceid << " has been emptied");
		}
	    }

	  m_face_icn_tx_event[faceid] = Simulator::Schedule(next, &ForwardingStrategy::ForwardICNPDU, this, face);
	}
    }

    void
    ForwardingStrategy::RemoveFace (Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this << face->GetId ());
      uint32_t faceid = face->GetId ();
      m_face_pdu_counter.erase(faceid);
      m_face_3n_tx_event[faceid].Cancel();
      m_face_icn_tx_event[faceid].Cancel();
      m_faces->Remove (face);
    }

    std::vector<Address>
    ForwardingStrategy::GetAllPoANames (Ptr<Face> face)
    {
      NS_LOG_FUNCTION (this);
      // Vector to save the PoA names
      std::vector<Address> poanames1;
      std::vector<Address> poanames2;
      // Get the total number of addresses we have
      int totalFaces = m_faces->GetN ();
      Ptr<Face> tmp;
      Address tmpaddr;

      // Go through the Faces attached to the ForwardingStrategy
      for (int i = 0; i < totalFaces; i++)
	{
	  // Get a Face
	  tmp = m_faces->Get (i);

	  // Check that the Face is not of type APPLICATION
	  if (!tmp->isAppFace ())
	    {
	      if (face == tmp)
		{
		  tmpaddr = tmp->GetAddress ();
		  poanames1.push_back (tmpaddr);
		}
	      else
		poanames2.push_back (tmp->GetAddress ());
	    }
	}

      for (size_t i = 0; i < poanames2.size (); i++)
	{
	  poanames1.push_back (poanames2[i]);
	}

      return poanames1;
    }

    void
    ForwardingStrategy::PoAModification (std::string context, const Mac48Address mac)
    {
      NS_LOG_FUNCTION (this);

      // Temporary container for context string
      std::vector<std::string> context_parts;

      // Use boost library to split the string
      boost::split(context_parts, context, boost::is_any_of ("//"));

      // Get the interface ID
      int interfaceID = std::atoi (context_parts[4].c_str());

      // Convert MAC to Address
      Address nowAddr = mac.operator ns3::Address();

      last_seen_mac_it = last_seen_mac.find(interfaceID);

      if (last_seen_mac_it != last_seen_mac.end ())
	{
	  // We know that we have connected somewhere before
	  if (last_seen_mac[interfaceID] == nowAddr)
	    {
	      NS_LOG_INFO ("Face " << interfaceID << " was already connected to PoA using " << nowAddr);
	      if (m_use3NName)
		{
		  if (!Has3NName())
		    {
		      NS_LOG_INFO ("Still don't have a 3N name, reattempting 3N enroll");
		      Simulator::Schedule(m_3n_wireless_wait, &ForwardingStrategy::Enroll, this);
		    }
		}
	      else
		{
		  Simulator::Schedule(m_3n_wireless_wait, &ForwardingStrategy::m_PoAAssociation, this);
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Face " << interfaceID << " has changed PoA to " << nowAddr);
	      last_seen_mac[interfaceID] = nowAddr;
	      if (m_use3NName)
		{
		  NS_LOG_INFO ("Initiating 3N reenroll");
		  Simulator::Schedule(m_3n_wireless_wait, &ForwardingStrategy::Reenroll, this);
		}
	      else
		{
		  Simulator::Schedule(m_3n_wireless_wait, &ForwardingStrategy::m_PoAAssociation, this);
		}
	    }
	}
      else
	{
	  NS_LOG_INFO ("First time Face " << interfaceID << " sees an address, saving " << nowAddr);
	  // Save the Address
	  last_seen_mac[interfaceID] = nowAddr;
	  if (m_use3NName)
	    {
	      NS_LOG_INFO ("Initiating 3N enroll");
	      Simulator::Schedule(m_3n_wireless_wait, &ForwardingStrategy::Enroll, this);
	    }
	  else
	    {
	      Simulator::Schedule(m_3n_wireless_wait, &ForwardingStrategy::m_PoAAssociation, this);
	    }
	}
    }

    void
    ForwardingStrategy::Enroll ()
    {
      NS_LOG_FUNCTION (this);
      // Check whether this node has a 3N name
      if (!Has3NName ())
	{
	  // Temporary test - an enroll means we have no name, fire the traced callback
	  m_no3Nname ();
	  bool ok = false;

	  Ptr<Face> tmp;
	  // Now transmit the EN through all Faces that are not of type APPLICATION
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      // Get a Face
	      tmp = m_faces->Get (i);
	      // Check that the Face is not of type APPLICATION
	      if (!tmp->isAppFace ())
		{
		  NS_LOG_INFO ("Sending out Face " << boost::cref(*tmp) << " with " << tmp->GetAddress ());

		  // Obtain all the node's PoA names
		  std::vector<Address> poanames = GetAllPoANames (tmp);

		  // Create the EN PDU to transmit
		  Ptr<EN> en_o = Create<EN> ();
		  // Set the lifetime for the EN PDU
		  en_o->SetLifetime (m_3n_lifetime);
		  // Set if we are asking for a fixed 3N name
		  en_o->SetFixed (m_AskFixedName);

		  // Add all the PoA names we found
		  for (size_t i = 0; i < poanames.size (); i++)
		    {
		      NS_LOG_INFO ("Adding PoA name: " << poanames[i]);
		      en_o->AddPoa (poanames[i]);
		    }

		  // Send the EN throughout the Faces
		  if (m_useQueues)
		    ok = EnqueueEN(tmp, tmp->GetBroadcastAddress(), en_o);
		  else
		    ok = tmp->SendEN (en_o);

		  if (ok)
		    m_outENs (en_o, tmp);
		}
	    }

	  NS_LOG_INFO ("Scheduling an enroll should things go south");
	  // Schedule the another enroll, should things go bad
	  m_en_timer = Simulator::Schedule (m_3n_retransmit_time, &ForwardingStrategy::Enroll, this);
	}
      else
	{
	  NS_LOG_INFO ("Node has name " << GetNode3NName () << ", no more need for enrollment for now");
	}
    }

    void
    ForwardingStrategy::Reenroll ()
    {
      NS_LOG_FUNCTION (this);
      std::set <Ptr<Face>, PtrFaceComp>::iterator it;

      if (m_AskFixedName)
	{
	  NS_LOG_INFO ("The node has been asking for a fixed node, but is reenrolling. Check debug and scenario file!");
	  return;
	}

      // Check whether this node has a 3N name
      if (!Has3NName ())
	{
	  NS_LOG_INFO ("Worst case scenario. Our reenrollment was called but we have no 3N names, schedule normal enroll");
	  Simulator::ScheduleNow (&ForwardingStrategy::Enroll, this);
	}
      else
	{
	  if (m_on_ren_oen)
	    {
	      NS_LOG_INFO ("On " << GetNode3NName () << ", we seem to have reenrolled, cancelling retransmission");
	      // Reset everything
	      m_sent_ren = false;
	      m_on_ren_oen = false;
	      return;
	    }

	  bool ok = false;
	  Ptr<Face> tmp;
	  // Now transmit the REN through all Faces that are not of type APPLICATION
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      // Get a Face
	      tmp = m_faces->Get (i);
	      // Check to see
	      it = m_returnEN_faces.find (tmp);

	      if (it != m_returnEN_faces.end ())
		{
		  NS_LOG_INFO ("Face " << *tmp << " has been used to answer EN, skipping");
		  continue;
		}

	      // Check that the Face is not of type APPLICATION
	      if (!tmp->isAppFace ())
		{
		  std::vector<Address> poanames = GetAllPoANames (tmp);

		  // Create the REN PDU to transmit
		  Ptr<REN> ren_o = Create<REN> ();
		  Ptr<const NNNAddress> addr = GetNode3NNamePtr ();

		  // Set the lifetime for the REN PDU
		  ren_o->SetLifetime (m_3n_lifetime);
		  // Set the 3N name for the REN
		  ren_o->SetName (*addr);
		  // Write the expire time for the 3N name (Time within the simulator is absolute)
		  ren_o->SetRemainLease (m_node_names->findNameExpireTime (addr));
		  // Add all the PoA names we found
		  for (size_t i = 0; i < poanames.size (); i++)
		    {
		      NS_LOG_INFO ("Adding PoA name: " << poanames[i]);
		      ren_o->AddPoa (poanames[i]);
		    }

		  NS_LOG_INFO ("Sending out REN via " << *tmp << " with " << GetNode3NName ());


		  // Send the REN throughout the Faces
		  if (m_useQueues)
		    ok = EnqueueREN(tmp, tmp->GetBroadcastAddress(), ren_o);
		  else
		    ok = tmp->SendREN (ren_o);

		  if (ok)
		    {
		      m_outRENs (ren_o, tmp);
		      m_sent_ren = true;
		    }
		}
	    }
	  NS_LOG_INFO ("Scheduling an reenroll should things go south");
	  // Schedule the another enroll, should things go bad
	  m_ren_timer = Simulator::Schedule (m_3n_retransmit_time, &ForwardingStrategy::Reenroll, this);
	}
    }

    void
    ForwardingStrategy::Disenroll ()
    {
      NS_LOG_FUNCTION (this);
      // Check whether this node has a 3N name
      NNNAddress myAddr = GetNode3NName ();

      if (Has3NName ())
	{
	  NS_LOG_INFO ("On " << myAddr << " attempting disenroll");
	  bool ok = false;

	  // Add the new information into the Awaiting DEN Response NNST type structure
	  // Create a complete timeout of 3*(Retransmit + Ack_Generate + Lifetime) - remember absolute time
	  Time now = Simulator::Now () + 3*(m_3n_retransmit_time + m_3n_ackgenerate_time + m_3n_lifetime);

	  Ptr<const NNNAddress> addr = GetNode3NNamePtr ();

	  Ptr<Face> tmp;
	  // Now transmit the DEN through all Faces that are not of type APPLICATION
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      // Get a Face
	      tmp = m_faces->Get (i);
	      // Check that the Face is not of type APPLICATION
	      if (!tmp->isAppFace ())
		{
		  std::vector<Address> poanames = GetAllPoANames (tmp);

		  // Create the DEN PDU to transmit
		  Ptr<DEN> den_o = Create<DEN> ();

		  // Set the lifetime for the DEN PDU
		  den_o->SetLifetime (m_3n_lifetime);
		  // Set the 3N name for the DEN
		  den_o->SetName (*addr);
		  // Set the 3N name expire time
		  den_o->SetExpireTime(m_node_names->findNameExpireTime (addr));
		  // Add all the PoA names we found
		  for (size_t i = 0; i < poanames.size (); i++)
		    {
		      den_o->AddPoa (poanames[i]);
		    }

		  // Send the DEN throughout the Faces
		  if (m_useQueues)
		    ok = EnqueueDEN(tmp, tmp->GetBroadcastAddress(), den_o);
		  else
		    ok = tmp->SendDEN (den_o);

		  if (ok)
		    {
		      m_outDENs (den_o, tmp);
		      // At this point, we should at least reset the DEN flags
		      m_sent_ren = false;
		      m_on_ren_oen = false;
		    }

		  // The first PoA is given higher priority
		  m_awaiting_den_response->Add (addr, tmp, poanames.front (), now, m_standardMetric -1, false);

		  // Eliminate the first one, since it has already been added
		  poanames.erase (poanames.begin ());

		  // Insert the remaining PoAs
		  m_awaiting_den_response->Add (addr, tmp, poanames, now, m_standardMetric, false);
		}
	    }
	  // Schedule the another dis-enroll, should things go bad
	  NS_LOG_DEBUG ("On " << myAddr << " scheduling an disenroll should things go south");
	  m_den_timer = Simulator::Schedule (m_3n_retransmit_time, &ForwardingStrategy::RetransmitDEN, this, addr);
	}
    }

    void
    ForwardingStrategy::DisenrollName (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this);
      // Check whether this node has a 3N name

      if (GoesBy3NName (addr))
	{
	  NNNAddress myAddr = GetNode3NName ();

	  NS_LOG_INFO ("On " << myAddr << " attempting disenroll of " << *addr);
	  bool ok = false;

	  // Add the new information into the Awaiting DEN Response NNST type structure
	  // Create a complete timeout of 3*(Retransmit + Ack_Generate + Lifetime) - remember absolute time
	  Time now = Simulator::Now () + 3*(m_3n_retransmit_time + m_3n_ackgenerate_time + m_3n_lifetime);

	  Ptr<Face> tmp;
	  // Now transmit the DEN through all Faces that are not of type APPLICATION
	  for (uint32_t i = 0; i < m_faces->GetN (); i++)
	    {
	      // Get a Face
	      tmp = m_faces->Get (i);
	      // Check that the Face is not of type APPLICATION
	      if (!tmp->isAppFace ())
		{
		  std::vector<Address> poanames = GetAllPoANames (tmp);

		  // Create the DEN PDU to transmit
		  Ptr<DEN> den_o = Create<DEN> ();

		  // Set the lifetime for the DEN PDU
		  den_o->SetLifetime (m_3n_lifetime);
		  // Set the 3N name for the DEN
		  den_o->SetName (*addr);
		  // Set the 3N name expire time
		  den_o->SetExpireTime(m_node_names->findNameExpireTime (addr));
		  // Add all the PoA names we found
		  for (size_t i = 0; i < poanames.size (); i++)
		    {
		      den_o->AddPoa (poanames[i]);
		    }

		  // Send the DEN throughout the Faces
		  if (m_useQueues)
		    ok = EnqueueDEN(tmp, tmp->GetBroadcastAddress(), den_o);
		  else
		    ok = tmp->SendDEN (den_o);

		  if (ok)
		    {
		      m_outDENs (den_o, tmp);
		      // At this point, we should at least reset the DEN flags
		      m_sent_ren = false;
		      m_on_ren_oen = false;
		    }

		  // The first PoA is given higher priority
		  m_awaiting_den_response->Add (addr, tmp, poanames.front (), now, m_standardMetric -1, false);

		  // Eliminate the first one, since it has already been added
		  poanames.erase (poanames.begin ());

		  // Insert the remaining PoAs
		  m_awaiting_den_response->Add (addr, tmp, poanames, now, m_standardMetric, false);

		  m_aden_from_en = true;
		}
	    }
	  // Schedule the another dis-enroll, should things go bad
	  NS_LOG_DEBUG ("On " << myAddr << " scheduling an disenroll should things go south");
	  m_den_timer = Simulator::Schedule (m_3n_retransmit_time, &ForwardingStrategy::RetransmitDEN, this, addr);
	}
      else
	{
	  NS_LOG_WARN ("Attempted disenroll of " << *addr << " which doesn't belong to node");
	}
    }

    void
    ForwardingStrategy::AskHandshake (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this);

      if (!addr)
	{
	  NS_LOG_DEBUG ("3N name asked for is NULL, please check!");
	  return;
	}
      else
	{
	  NS_LOG_DEBUG ("Will attempt to ask for " << *addr);
	}

      if (addr->isEmpty ())
	{
	  NS_LOG_DEBUG ("We received an empty query, returning");
	  return;
	}

      // Check if we are still waiting for a response
      if (m_waiting_handshake_timeout->foundName (addr))
	{
	  NS_LOG_INFO ("We have sent a RHR PDU, still have time before need to attempt again");
	  return;
	}
      else
	{
	  NS_LOG_INFO ("No retransmission timer found for " << *addr << " proceeding");
	}

      // Check if we have handshaken for particular 3N name
      if (m_handshaken_names->foundName (addr))
	{
	  NS_LOG_INFO ("We have handshaken for " << *addr << ", no need to repeat for now");
	  return;
	}
      else
	{
	  NS_LOG_DEBUG ("We have no record of handshaking for " << *addr);
	}

      bool hasName = Has3NName ();
      Ptr<const NNNAddress> tmpaddr;
      bool isFixed = false;
      Time lease = Seconds (0);
      bool ok = false;
      bool oneOk = false;

      if (hasName)
	{
	  tmpaddr = GetNode3NNamePtr ();
	  isFixed = m_node_names->isFixed (tmpaddr);
	  lease = m_node_names->findNameExpireTime (tmpaddr);
	}

      Ptr<Face> tmp;
      uint32_t rhr_sent = 0;
      // Go through our faces and begin sending out handshake requests
      for (uint32_t i = 0; i < m_faces->GetN (); i++)
	{
	  tmp = m_faces->Get(i);

	  if (!tmp)
	    {
	      NS_LOG_DEBUG ("Something terribly wrong with Face " << i << " returning");
	      continue;
	    }

	  // Verify that we aren't using an App Face
	  if (!(tmp->isAppFace()))
	    {
	      Ptr<RHR> rhr_o = Create<RHR> ();
	      rhr_o->SetLifetime (m_3n_lifetime);
	      if (hasName)
		{
		  rhr_o->SetSrcName (tmpaddr);
		  rhr_o->SetSrcFixed (isFixed);
		  rhr_o->SetSrcLease (lease);
		}
	      Address addrtmp = tmp->GetAddress ();
	      rhr_o->SetPoa (addrtmp);
	      rhr_o->SetQueryName (addr);

	      NS_LOG_INFO ("Sending out address: " <<  addrtmp);

	      // Send the RHR throughout the Faces
	      if (m_useQueues)
		ok = EnqueueRHR(tmp, tmp->GetBroadcastAddress(), rhr_o);
	      else
		ok = tmp->SendRHR (rhr_o);

	      if (ok)
		{
		  m_outRHRs (rhr_o, tmp);
		  oneOk |= ok;
		  rhr_sent++;
		}
	    }
	}

      // If we have been able to send at least one RHR PDU, wait for timeouts
      if (oneOk)
	{
	  Time shift = m_3n_retransmit_time + 2*m_3n_ackgenerate_time;
	  Time now = Simulator::Now () + shift;
	  NS_LOG_INFO ("Sent " << rhr_sent << " pdus for "<< *addr << ", waiting for timeout at " << now);
	  m_waiting_handshake_timeout->addEntry (addr, now, false);
	}
    }

    void
    ForwardingStrategy::RetransmitOEN(Ptr<Face> face, Ptr<const NNNAddress> addr, Address dest_addr, Ptr<OEN> oen_p)
    {
      NS_LOG_FUNCTION (this << *addr);

      // Check if we have not yet received an AEN for this particular 3N name
      if (!m_awaiting_oen_response->FoundName(*addr))
	{
	  NS_LOG_INFO (*addr << " has been acked, cancelling retransmission");
	  return;
	}

      NS_LOG_INFO ("Still waiting for AEN from " << *addr << ", attempting retransmission");

      if (m_useQueues)
	EnqueueOEN(face, dest_addr, oen_p);
      else
	face->SendOEN (oen_p, dest_addr);

      m_outOENs (oen_p, face);

      // Schedule retransmission of OEN if we haven't received an AEN
      m_oen_timer = Simulator::Schedule(m_3n_retransmit_time, &ForwardingStrategy::RetransmitOEN, this, face, addr, dest_addr, oen_p);
    }

    void
    ForwardingStrategy::RetransmitDEN (Ptr<const NNNAddress> addr)
    {
      NS_LOG_FUNCTION (this << *addr);

      // Check if we have not yet received an ADEN for this particular 3N name
      if (!m_awaiting_den_response->FoundName(*addr))
	{
	  NS_LOG_INFO ("Disassociation for " << *addr << " has been acked, cancelling retransmission");
	  return;
	}

      NS_LOG_INFO ("Still waiting for ADEN for " << *addr << ", attempting retransmission");

      bool ok = false;

      Ptr<Face> tmp;
      // Now transmit the DEN through all Faces that are not of type APPLICATION
      for (uint32_t i = 0; i < m_faces->GetN (); i++)
	{
	  // Get a Face
	  tmp = m_faces->Get (i);
	  // Check that the Face is not of type APPLICATION
	  if (!tmp->isAppFace ())
	    {
	      std::vector<Address> poanames = GetAllPoANames (tmp);

	      // Create the DEN PDU to transmit
	      Ptr<DEN> den_o = Create<DEN> ();

	      // Set the lifetime for the DEN PDU
	      den_o->SetLifetime (m_3n_lifetime);
	      // Set the 3N name for the DEN
	      den_o->SetName (*addr);
	      // Set the 3N name expire time
	      den_o->SetExpireTime(m_node_names->findNameExpireTime (addr));
	      // Add all the PoA names we found
	      for (size_t i = 0; i < poanames.size (); i++)
		{
		  den_o->AddPoa (poanames[i]);
		}
	      // Send the DEN throughout the Faces
	      if (m_useQueues)
		ok = EnqueueDEN(tmp, tmp->GetBroadcastAddress(), den_o);
	      else
		ok = tmp->SendDEN (den_o);

	      if (ok)
		{
		  m_outDENs (den_o, tmp);
		  // At this point, we should at least reset the DEN flags
		  m_sent_ren = false;
		  m_on_ren_oen = false;
		}
	    }
	}

      NS_LOG_INFO ("Scheduling an disenroll should things go south");
      // Schedule retransmission of DEN if we haven't received an ADEN
      m_den_timer = Simulator::Schedule(m_3n_retransmit_time, &ForwardingStrategy::RetransmitDEN, this, addr);
    }

    void
    ForwardingStrategy::DidAddNNSTEntry (Ptr<nnst::Entry> NNSTEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ForwardingStrategy::WillRemoveNNSTEntry (Ptr<nnst::Entry> NNSTEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ForwardingStrategy::DidAddNNPTEntry (Ptr<nnpt::Entry> NNPTEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ForwardingStrategy::WillRemoveNNPTEntry (Ptr<nnpt::Entry> NNPTEntry)
    {
      NS_LOG_FUNCTION (this);
    }

    void
    ForwardingStrategy::NotifyNewAggregate ()
    {
      NS_LOG_FUNCTION(this);

      if (m_nnpt == 0)
	{
	  m_nnpt = GetObject<NNPT> ();
	}
      if (m_nnst == 0)
	{
	  m_nnst = GetObject<NNST> ();
	}
      Object::NotifyNewAggregate ();
    }

    void
    ForwardingStrategy::DoDispose ()
    {
      m_nnpt = 0;
      m_nnst = 0;
      m_awaiting_oen_response = 0;
      m_awaiting_den_response    = 0;
      m_waiting_aden_timeout   = 0;
      m_faces                  = 0;
      m_node_names            = 0;
      m_leased_names          = 0;
      m_handshaken_names      = 0;
      m_waiting_handshake_timeout  = 0;
      m_node_pdu_buffer        = 0;
      m_node_den_buffer       = 0;
      m_node_pdu_queue         = 0;
      m_node_rxbuffer_container  = 0;
      m_node_txbuffer_container  = 0;
      uniRandom  = 0;

      m_node_lease_times.clear ();
      m_fixed_name_pending.clear ();
      m_returnEN_faces.clear ();

      last_seen_mac.clear ();
      tx_list.clear ();

      Object::DoDispose ();
    }

    void
    ForwardingStrategy::AskACKedPDUs (bool value)
    {
      NS_LOG_FUNCTION(this << std::boolalpha << value);

      m_ask_ACKP = value;
    }

    bool
    ForwardingStrategy::IsAskingACKedPDUs () const
    {
      return m_ask_ACKP;
    }

    void
    ForwardingStrategy::ReturnsACKedPDUs (bool value)
    {
      NS_LOG_FUNCTION(this << std::boolalpha << value);

      m_return_ACKP = value;
    }

    bool
    ForwardingStrategy::IsReturningACKedPDUs () const
    {
      return m_return_ACKP;
    }

    uint32_t
    ForwardingStrategy::GetMin3NWindow () const
    {
      return m_3n_min_pdu_window;
    }

    void
    ForwardingStrategy::SetMin3NWindow (uint32_t value)
    {
      NS_ASSERT (value > 0);

      m_3n_min_pdu_window = value;
    }

    uint32_t
    ForwardingStrategy::GetMax3NWindow () const
    {
      return m_3n_max_pdu_window;
    }

    void
    ForwardingStrategy::SetMax3NWindow (uint32_t value)
    {
      NS_ASSERT (value > 0);
      m_3n_max_pdu_window = value;

      m_3n_min_pdu_window = m_3n_max_pdu_window / 4;
    }

    //  2 - Sending has been stopped due to out of window margin
    //  1 - Sending has been stopped by 3N ACK wait
    //  0 - Sent without issue
    // -1 - Error sending
    // -2 - Node Rx threshold surpassed
    // -3 - Error with PDU or data
    int8_t
    ForwardingStrategy::txPreparation (Ptr<DATAPDU> pdu, Ptr<Face> outFace, Address dst_poa, bool retx)
    {
      NS_LOG_FUNCTION (outFace->GetId () << dst_poa << std::boolalpha << retx);
      if (!pdu)
	{
	  NS_LOG_WARN ("Cannot use ACKP without 3N PDUs, check code");
	  UpdatePDUCounters(pdu, outFace, -3, retx);
	  return -3;
	}
      else
	{
	  if (!(outFace->isAppFace()))
	    {
	      Ptr<DATAPDU> pdu_o = pdu->Copy ();

	      // Verify that the Flowid, sequence and others are fine
	      SetupFlowidSeq(pdu_o, outFace, dst_poa, retx);

	      Address internal;

	      RxPDU data = RxPDU ();

	      if (dst_poa.IsInvalid ())
		{
		  internal = outFace->GetBroadcastAddress ();
		}
	      else
		{
		  internal = dst_poa;
		}

	      data.m_face_id = outFace->GetId ();
	      data.m_dst_id = outFace->GetAddressId (internal);
	      data.m_flowid = pdu_o->GetFlowid ();
	      data.m_sequence = pdu_o->GetSequence ();

	      if (data.m_flowid == 0)
		{
		  NS_LOG_WARN ("Received Flowid 0, cannot propagate. CHECK CODE!");
		  return -2;
		}

	      NS_ASSERT_MSG ((data.m_dst_id != 0), "PoA ID for " << internal << " returned 0");

	      NS_LOG_DEBUG ("Attempting to send Flowid: " << pdu_o->GetFlowid() << " Sequence: " << pdu_o->GetSequence() << " out Face: " << outFace->GetId () << " PoA: " << internal << " PoA ID: " << data.m_dst_id);

	      UpdateSentPDUs (pdu_o, outFace, internal, retx);

	      // Buffer the PDU
	      NS_LOG_DEBUG ("Buffering PDU Face: " << data.m_face_id << " PoA: " << internal <<
			    " PoA ID: " << data.m_dst_id << " Flowid: " << data.m_flowid <<
			    " Sequence: " << data.m_sequence);
	      m_node_rxbuffer_container->InsertPDU (outFace, internal, pdu_o, Seconds (0));

	      // Enter window checking loop
	      int8_t ok = CheckSendDATAPDU (data, pdu_o, retx);

	      UpdatePDUCounters(pdu_o, outFace, ok, retx);

	      return ok;
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Out Face is an Application face, no need to buffer");
	      int8_t ok = SendDATAPDUToApp (outFace, pdu);

	      UpdatePDUCounters(pdu, outFace, ok, retx);

	      return ok;
	    }
	}
    }

    void
    ForwardingStrategy::txReset (Ptr<DATAPDU> pdu)
    {
      if (pdu->GetFlowid() != 0)
	{
//	  NS_LOG_DEBUG ("Zeroing Flowid and turning off RF for Flowid: " << pdu->GetFlowid()
//			<< " Sequence: " << pdu->GetSequence());
	  pdu->SetFlowid(0);
	  pdu->SetPDURF(false);
	}
    }

    void
    ForwardingStrategy::TransmitACKP (Ptr<Face> outFace, Address dst)
    {
      if (outFace->isAppFace ())
	{
	  return;
	}

      std::map<uint32_t, uint32_t> ret = VerifyReceivedFlowidMaxSeq (outFace, dst);
      std::map<uint32_t, uint32_t>::iterator it = ret.begin ();

      if (ret.size() == 0)
	{
	  /*
	  if (dst != outFace->GetBroadcastAddress())
	    {
	      ret = ObtainFlowidsSeqReceived (outFace, outFace->GetBroadcastAddress());

	      if (ret.size () == 0)
		{
		  NS_LOG_DEBUG ("Have absolutely no information for Face: " << outFace->GetId () << " PoA: " << dst << " no ACKP sent");
		  return;
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("A priori exhausted search, absolutely no ACKP sent for Face: " << outFace->GetId () << " no ACKP sent");
	      return;
	    }
	   */
	  NS_LOG_DEBUG ("Have no information for Face: " << outFace->GetId () << " PoA: " << dst << " no ACKP sent");
	  return;
	}

      bool info_to_send = false;

      Ptr<ACKP> ack_p = Create<ACKP> ();
      ack_p->SetLifetime (m_3n_lifetime);
      while (it != ret.end ())
	{
	  if(!HasSentACKReceivedFlowid(outFace, dst, it->first))
	    {
	      NS_LOG_DEBUG ("Creating ACKP with Flowid: " << it->first << " Sequence: " << it->second);
	      ack_p->AddACKFlowidSeq(it->first, it->second);
	      info_to_send = true;
	    }
	  else
	    {
	      SetACKSentReceivedFlowid (outFace, dst, it->first, false);
	    }
	  ++it;
	}

      bool ok = false;

      if (info_to_send)
	{
	  if (m_useQueues)
	    ok = EnqueueACKP(outFace, dst, ack_p);
	  else
	    ok = outFace->SendACKP (ack_p, dst);
	}
      else
	{
	  NS_LOG_WARN ("No updated Flowid Sequences to acknowledge, aborting ACK");
	  return;
	}

      if (ok)
	{
	  NS_LOG_INFO ("Returned ACKP out Face: " << outFace->GetId () << " PoA: " << dst);
	  m_outACKPs (ack_p, outFace);

	  it = ret.begin ();
	  while (it != ret.end ())
	    {
	      SetACKSentReceivedFlowid (outFace, dst, it->first);
	      ++it;
	    }
	}
      else
	{
	  NS_LOG_WARN ("Could not send ACKP out Face: " << outFace->GetId ());
	  m_dropACKPs (ack_p, outFace);
	}
    }

    void
    ForwardingStrategy::TransmitFlowidACKP (Ptr<Face> face, Address dst, uint32_t flowid)
    {
      NS_LOG_FUNCTION (face->GetId () << dst << flowid);
      if (face->isAppFace ())
	{
	  return;
	}

      std::map<uint32_t, uint32_t> ret = VerifyReceivedFlowidMaxSeq (face, dst);
      std::map<uint32_t, uint32_t>::iterator it = ret.find(flowid);

      if (it != ret.end ())
	{
	  if(HasSentACKReceivedFlowid(face, dst, flowid))
	    {
	      NS_LOG_DEBUG ("Face: " << face->GetId () << " PoA: " << dst <<
			    " have already sent ACK for Flowid: " << flowid << " Sequence: " << it->second <<
			    " aborting ACK");
	      SetACKSentReceivedFlowid (face, dst, flowid, false);
	      return;
	    }

	  Ptr<ACKP> ack_p = Create<ACKP> ();
	  ack_p->SetLifetime (m_3n_lifetime);

	  NS_LOG_DEBUG ("Creating ACKP with Flowid: " << flowid << " Sequence: " << it->second);
	  ack_p->AddACKFlowidSeq(flowid, it->second);

	  bool ok;
	  if (m_useQueues)
	    ok = EnqueueACKP(face, dst, ack_p);
	  else
	    ok = face->SendACKP (ack_p, dst);

	  if (ok)
	    {
	      NS_LOG_INFO ("Returned ACKP for Flowid: " << flowid <<
			   " out Face: " << face->GetId () << " PoA: " << dst);
	      m_outACKPs (ack_p, face);
	      SetACKSentReceivedFlowid (face, dst, flowid);
	    }
	  else
	    {
	      NS_LOG_WARN ("Could not send ACKP for Flowid: " << flowid <<
			   " out Face: " << face->GetId ());
	      m_dropACKPs (ack_p, face);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Have no information for Face: " << face->GetId () << " PoA: " << dst <<
			" Flowid: " << flowid << " no ACKP sent");
	}
    }

    void
    ForwardingStrategy::TransmitLossACKP (Ptr<Face> face, Address dst, uint32_t flowid)
    {
      NS_LOG_FUNCTION (face->GetId () << dst << flowid);
      if (face->isAppFace ())
	{
	  return;
	}

      std::map<uint32_t, uint32_t> ret = VerifyReceivedFlowidMaxSeq (face, dst);
      std::map<uint32_t, uint32_t>::iterator it = ret.find(flowid);

      if (it != ret.end ())
	{
	  if(HasSentACKReceivedFlowid(face, dst, flowid))
	    {
	      NS_LOG_DEBUG ("Face: " << face->GetId () << " PoA: " << dst <<
			    " have already sent ACK for Flowid: " << flowid << " Sequence: " << it->second <<
			    " aborting ACK");
	      SetACKSentReceivedFlowid (face, dst, flowid, false);
	      return;
	    }

	  Ptr<ACKP> ack_p = Create<ACKP> ();
	  ack_p->SetLifetime (m_3n_lifetime);

	  NS_LOG_DEBUG ("Creating ACKP with Flowid: " << flowid << " Sequence: " << it->second);
	  ack_p->AddACKFlowidSeq(flowid, it->second);
	  bool ok;
	  if (m_useQueues)
	    ok = EnqueueACKP(face, dst, ack_p);
	  else
	    ok = face->SendACKP (ack_p, dst);

	  if (ok)
	    {
	      NS_LOG_INFO ("Returned ACKP for Flowid: " << flowid <<
			   " out Face: " << face->GetId () << " PoA: " << dst);
	      m_outACKPs (ack_p, face);
	      SetACKSentReceivedFlowid (face, dst, flowid);
	    }
	  else
	    {
	      NS_LOG_WARN ("Could not send ACKP for Flowid: " << flowid <<
			   " out Face: " << face->GetId ());
	      m_dropACKPs (ack_p, face);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Have no information for Face: " << face->GetId () << " PoA: " << dst <<
		       " Flowid: " << flowid << " no ACKP sent");
	}
    }

    uint32_t
    ForwardingStrategy::GenerateAppFlowid ()
    {
      bool generate = true;
      uint32_t flow = 0;

      while (generate)
	{
	  flow = uniRandom->GetInteger();

	  if (m_flowids_received.find (flow) == m_flowids_received.end ())
	    {
	      if (m_flowids_sent.find (flow) == m_flowids_sent.end ())
		{
		  generate = false;
		}
	    }
	}

      NS_LOG_DEBUG ("Generated Flowid: " << flow);
      return flow;
    }

    bool
    ForwardingStrategy::HasSentACKReceivedFlowid (Ptr<Face> outFace, Address dst, uint32_t flowid)
    {
      uint32_t face_id = outFace->GetId ();
      uint16_t dst_id = outFace->GetAddressId(dst);

      FlowidReceivedContainer_uniq& face_index = m_flowids_accepted.get<i_fid_rx> ();
      FlowidReceivedContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT_MSG(itface != face_index.end (), "Could not find received Face: " << face_id << " PoA ID: " << dst_id <<
		    " Flowid: " << flowid);

      Flowid_Receiver tmp = *itface;

      return tmp.m_rx_ack;
    }

    void
    ForwardingStrategy::SetACKSentReceivedFlowid (Ptr<Face> outFace, Address dst, uint32_t flowid, bool value)
    {
      uint32_t face_id = outFace->GetId ();
      uint16_t dst_id = outFace->GetAddressId(dst);

      FlowidReceivedContainer_uniq& face_index = m_flowids_accepted.get<i_fid_rx> ();
      FlowidReceivedContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT_MSG(itface != face_index.end (), "Could not find received Face: " << face_id << " PoA ID: " << dst_id <<
		    " Flowid: " << flowid);

      Flowid_Receiver tmp = *itface;

      tmp.m_rx_ack = value;

      face_index.replace (itface, tmp);
    }

    std::map<uint32_t, uint32_t>
    ForwardingStrategy::VerifyReceivedFlowidMaxSeq (Ptr<Face> outFace, Address dst)
    {
      //      NS_LOG_FUNCTION(outFace->GetId() << dst);

      FlowidReceivedContainer_uniq& face_index = m_flowids_accepted.get<i_fid_rx> ();
      FlowidReceivedContainer_uniq::iterator itface = face_index.begin ();

      std::map<uint32_t, uint32_t> ret;
      while (itface != face_index.end ())
	{
	  if (itface->m_face == outFace->GetId ())
	    {
	      if (itface->m_dst_id == outFace->GetAddressId(dst))
		{
		  Flowid_Receiver old_entry = *itface;
		  if (!itface->m_rx_sequence.empty ())
		    {
		      std::set<uint32_t> tmp = old_entry.m_rx_sequence;
		      std::set<uint32_t>::iterator it;

		      uint32_t start = *(tmp.begin());

		      it = tmp.begin();

		      if (it != tmp.end ())
			it++;

		      while (it != tmp.end ())
			{
			  if (start + 1 == *it)
			    {
			      start = *it;
			      it++;
			    }
			  else
			    {
			      break;
			    }
			}

		      uint32_t max = old_entry.m_rx_max_acked_sequence > start ? old_entry.m_rx_max_acked_sequence : start;

		      NS_LOG_DEBUG ("Adding ACK for Flowid: " << old_entry.m_flowid << " Sequence: " << max <<
				    " previous Max ACKed Sequence: " << old_entry.m_rx_max_acked_sequence);
		      ret.insert(std::make_pair(old_entry.m_flowid, max));

		      if (max != old_entry.m_rx_max_acked_sequence)
			{
			  old_entry.m_rx_max_acked_sequence = max;
			  old_entry.m_rx_ack = false;
			  m_flowids_accepted.replace(itface, old_entry);
			}
		    }
		  else
		    {
		      ret.insert(std::make_pair(old_entry.m_flowid, old_entry.m_rx_max_acked_sequence));
		    }
		}
	    }
	  ++itface;
	}
      return ret;
    }

    Flowid_Sender
    ForwardingStrategy::ObtainSentFlowidInfo (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT (itface != face_index.end ());

      Flowid_Sender tmp = *itface;

      return tmp;
    }

    std::tuple<uint32_t, uint32_t>
    ForwardingStrategy::ObtainFlowidSeqSent (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      //      NS_LOG_FUNCTION (face_id << dst_id << flowid);

      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT (itface != face_index.end ());

      Flowid_Sender tmp = *itface;

      return std::make_tuple (tmp.m_tx_sequence, tmp.m_tx_initial_sequence);
    }

    uint32_t
    ForwardingStrategy::ObtainFlowidWindowSent (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      //      NS_LOG_FUNCTION (face_id << dst_id << flowid);

      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT (itface != face_index.end ());

      Flowid_Sender tmp = *itface;

      return tmp.m_tx_window;
    }

    bool
    ForwardingStrategy::VerifyFlowid (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      //      nNS_LOG_FUNCTION (face_id << dst_id << flowid);

      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      return (itface != face_index.end ());
    }

    uint32_t
    ForwardingStrategy::ObtainFlowidRateSent (Ptr<Face> face, Address dst, uint32_t flowid)
    {
      //      NS_LOG_FUNCTION (face->GetId () << dst << flowid);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face->GetId (), face->GetAddressId(dst)));

      NS_ASSERT (flowid != 0);

      if (itface != face_index.end ())
	{
	  Flowid_Sender tmp = *itface;
	  NS_LOG_DEBUG ("Face " << face->GetId () << " PoA: " << dst << " Flowid: " << flowid << " return rate: " << tmp.m_tx_rate);
	  return tmp.m_tx_rate;
	}
      else
	{
	  NS_LOG_WARN ("Flowid: " << flowid << " was not declared, check code!");
	  return 0;
	}
    }

    Time
    ForwardingStrategy::ObtainNextPDUSentWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT (flowid != 0);

      if (itface != face_index.end ())
	{
	  Flowid_Sender tmp = *itface;
	  return (m_3n_MPL / 2) + Seconds (8.192 / tmp.m_tx_rate);
	}
      else
	{
	  NS_LOG_WARN ("Flowid: " << flowid << " was not declared, check code!");
	  return Seconds (0);
	}
    }

    Time
    ForwardingStrategy::ObtainRetransmissionInterval (RxPDU data)
    {
      NS_ASSERT (data.m_flowid != 0);
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(data.m_flowid, data.m_face_id, data.m_dst_id));

      NS_ASSERT (itface != face_index.end ());
      Flowid_Sender tmp = *itface;

      NS_LOG_DEBUG ("Set ReTx timer of: " << tmp.m_ack_interval.GetSeconds ());

      return tmp.m_ack_interval;
    }

    void
    ForwardingStrategy::UpdatePDUBeforeACKP (RxPDU data)
    {
      NS_LOG_FUNCTION (data.m_flowid << data.m_face_id << data.m_dst_id << data.m_sequence << Simulator::Now ().GetSeconds());
      NS_ASSERT (data.m_flowid != 0);
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(data.m_flowid, data.m_face_id, data.m_dst_id));

      NS_ASSERT (itface != face_index.end ());
      Flowid_Sender tmp = *itface;

      tmp.m_pdu_before_ackp = Simulator::Now ();

      face_index.replace(itface, tmp);
    }

    void
    ForwardingStrategy::VerifyAndMigrateBroadcast (uint32_t flowid, Ptr<Face> inFace, Address src)
    {
      //      NS_LOG_FUNCTION (inFace->GetId () << src << flowid);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.begin ();

      NS_ASSERT (flowid != 0);

      bool broadcast_m = false;
      Flowid_Sender tmp;
      while (itface != face_index.end ())
	{
	  tmp = *itface;
	  if (flowid == tmp.m_flowid)
	    {
	      broadcast_m = true;
	      break;
	    }
	  ++itface;
	}

      if (broadcast_m)
	{
	  if (tmp.m_dst_id == inFace->GetBroadcastId())
	    {
	      NS_LOG_DEBUG ("Found a broadcast based sent information for Flowid: " << flowid << ", updating");

	      uint16_t dst_new = inFace->GetAddressId (src);

	      if (dst_new != 0)
		{
		  Time now = Simulator::Now ();
		  tmp.m_dst_id = dst_new;

		  if (tmp.m_flowid_erasure.IsRunning())
		    {
		      tmp.m_flowid_erasure.Cancel ();
		      // If IsRunning, then we have time to reschedule
		      Time force_del = tmp.m_flowid_erasure_time - now;
		      NS_LOG_DEBUG ("Migrating erasure time to " << force_del.GetSeconds ());
		      tmp.m_flowid_erasure = Simulator::Schedule(force_del, &ForwardingStrategy::EliminateSentFlowid, this, tmp.m_face, tmp.m_dst_id, flowid);
		    }

		  /*
		  if (tmp.m_force_retransmit.IsRunning ())
		    {
		      tmp.m_force_retransmit.Cancel ();
		      Time force_rx = tmp.m_force_retransmit_time - now;
		      NS_LOG_DEBUG ("Migrating retransmit time to " << force_rx.GetSeconds ());
		      tmp.m_force_retransmit = Simulator::Schedule(force_rx, &ForwardingStrategy::TransmitLossWindow, this, tmp.m_face, tmp.m_dst_id, flowid);
		    }
		   */

		  NS_LOG_DEBUG ("Migrated broadcast for Flowid: " << flowid << " to PoA: " << src << " PoA ID: " << dst_new);
		  m_flowids_forwarded.replace(itface, tmp);
		}
	      else
		{
		  NS_LOG_WARN ("Serious PoA issue, verify code");
		}
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Flowid " << flowid << " not using broadcast, continue");
	    }
	}
      else
	{
	  NS_LOG_WARN ("Have no knowledge of Flowid: " << flowid << " prepare for failure");

	  itface = face_index.begin ();

	  while (itface != face_index.end ())
	    {
	      tmp = *itface;
	      NS_LOG_DEBUG ("Have Flowid: " << tmp.m_flowid << " Face: " << tmp.m_face << " PoA ID: " << tmp.m_dst_id << " Last updated: " << tmp.m_tx_last_ackp.GetSeconds());
	      ++itface;
	    }
	}
    }

    bool
    ForwardingStrategy::UpdateReceivedPDUs (Ptr<DATAPDU> pdu, Ptr<Face> inFace, Address src)
    {
      //      if (pdu)
      //	{
      //	  NS_LOG_FUNCTION(pdu->GetFlowid() << pdu->GetSequence() << inFace->GetId () << src);
      //	}
      //      else
      //	{
      //	  NS_LOG_WARN ("Given a null PDU. Check code");
      //	  return;
      //	}

      bool ret = true;

      if (inFace->isAppFace ())
	{
	  NS_LOG_DEBUG ("Out Face is App Face, returning");
	  return ret;
	}

      uint32_t flowid = pdu->GetFlowid ();

      if (flowid == 0)
	{
	  NS_LOG_ERROR ("Received PDU with Flowid 0, cannot keep track of PDUs");
	  return ret;
	}

      uint32_t faceid = inFace->GetId ();
      uint16_t dstid = inFace->GetAddressId (src);
      uint32_t window = pdu->GetPDUWindow ();
      uint32_t rate = pdu->GetRate ();
      uint32_t seq = pdu->GetSequence ();
      uint32_t ini_seq = pdu->GetInitialWindowSeq ();

      FlowidReceivedContainer_uniq& face_index = m_flowids_accepted.get<i_fid_rx> ();
      FlowidReceivedContainer_uniq::iterator itface = face_index.find (boost::make_tuple (flowid, faceid, dstid));

      Time now = Simulator::Now ();

      if (itface != face_index.end ())
	{
	  Flowid_Receiver old_entry = *itface;

	  if (pdu->hasRF ())
	    {
	      NS_ASSERT (window != 0);
	      NS_ASSERT (rate != 0);

//	      if ((!old_entry.m_rx_first_ack) || (old_entry.m_rx_first_ack && seq > old_entry.m_rx_max_acked_sequence))
//		{
		  bool buffer_reset = false;

		  NS_LOG_DEBUG ("RF modification Flowid: " << flowid << " Sequence: " << seq << " arrived with Initial Sequence: " << ini_seq <<
				" End Sequence: " << (ini_seq + window -1) << " Window: " << window << " Rate: " << rate);

		  if (old_entry.m_rx_window != window)
		    {
		      NS_LOG_DEBUG ("RF modification of window for Flowid: " << flowid <<
				    " from " << old_entry.m_rx_window << " to " << window);
		      old_entry.m_rx_window = window;
		      buffer_reset = true;
		    }

		  if (old_entry.m_rx_initial_sequence != ini_seq)
		    {
		      if (ini_seq < old_entry.m_rx_max_acked_sequence)
			{
			  NS_LOG_WARN ("RF modification of initial sequence for Flowid: " << flowid <<
				       " seq from " << old_entry.m_rx_initial_sequence << " to " << ini_seq <<
				       " Max ACKed Sequence: " << old_entry.m_rx_max_acked_sequence << " retransmit in process");
			}
		      else
			{
			  NS_LOG_DEBUG ("RF modification of initial sequence for Flowid: " << flowid <<
					" seq from " << old_entry.m_rx_initial_sequence << " to " << ini_seq <<
					" Max ACKed Sequence: " << old_entry.m_rx_max_acked_sequence);
			}

		      old_entry.m_rx_initial_sequence = ini_seq;
		      buffer_reset = true;
		    }

		  if (old_entry.m_rx_rate != rate)
		    {
		      NS_LOG_DEBUG ("RF modification of rate for Flowid: " << flowid << " from " << old_entry.m_rx_rate << " to " << rate);
		      old_entry.m_rx_rate = rate;
		    }

		  PrintPDUSets(flowid, old_entry.m_rx_sequence, old_entry.m_rx_initial_sequence,
			       old_entry.m_rx_initial_sequence + old_entry.m_rx_window, "RF modification");

		  if (buffer_reset)
		    {
		      NS_LOG_DEBUG ("RF modification for Flowid: " << flowid << " Sequence: " <<  seq << " activated buffer clean");
		      old_entry.m_rx_sequence.clear ();
		      /*
		      if (!old_entry.m_rx_sequence.empty ())
			{
			  std::set<uint32_t>::iterator it = old_entry.m_rx_sequence.lower_bound (old_entry.m_rx_initial_sequence);

			  if (it != old_entry.m_rx_sequence.end ())
			    {
			      if (it != old_entry.m_rx_sequence.begin ())
				{
				  NS_LOG_WARN ("RF modification having to clear Flowid: " << flowid <<
					       " buffer of sequences lower: " << old_entry.m_rx_initial_sequence);
				  old_entry.m_rx_sequence.erase (old_entry.m_rx_sequence.begin (), it);
				}
			    }
			  else
			    {
			      NS_LOG_WARN ("RF modification having to clear Flowid: " << flowid << " buffer completely");
			      old_entry.m_rx_sequence.clear ();
			    }

			  uint32_t max_seq = old_entry.m_rx_initial_sequence + old_entry.m_rx_window -1;
			  it = old_entry.m_rx_sequence.upper_bound (max_seq);

			  if (it != old_entry.m_rx_sequence.end ())
			    {
			      NS_LOG_WARN ("RF modification having to clear Flowid: " << flowid <<
					   " of sequences higher than : " << max_seq);
			      old_entry.m_rx_sequence.erase (it, old_entry.m_rx_sequence.end ());
			    }
			}
			*/
		    }
//		}
//	      else
//		{
//		  NS_LOG_WARN ("PDU with RF will be ignored because Flowid: " << flowid << " has been ACKed up to Sequence: " <<
//			       old_entry.m_rx_max_acked_sequence << " and received a Sequence: " << seq << " retransmitting ACKP");
//		  TransmitACKP (inFace, src);
//		}
	    }

	  uint32_t window_compare = old_entry.m_rx_window;
	  uint32_t ini_seq_compare = old_entry.m_rx_initial_sequence;
	  uint32_t max_sequence = ini_seq_compare + window_compare;

	  if (ini_seq_compare <= seq && seq < max_sequence)
	    {
	      std::set<uint32_t>::iterator it;
	      std::tie (it, ret) = old_entry.m_rx_sequence.insert (seq);
	      if (old_entry.m_rx_first_ack && seq < old_entry.m_rx_max_acked_sequence)
		{
		  NS_LOG_WARN ("IGNORED Flowid: " << flowid << " Sequence: " << seq << ". We have ACKed up to Sequence: " <<
			       old_entry.m_rx_max_acked_sequence);
		  ret = false;
		}
	      else
		{
		  NS_LOG_DEBUG ("Inserting Flowid: " << flowid << " Sequence: " << seq <<
				" which is " << ini_seq_compare << " <= x < " << max_sequence <<
				" Max ACKed Sequence: " << old_entry.m_rx_max_acked_sequence);
		}
	    }
	  else
	    {
	      NS_LOG_ERROR ("REJECTED Flowid: " << flowid << " Sequence: " << seq <<
			    " looking between " << ini_seq_compare << " <= x < " << max_sequence <<
			    " Maximum seen Sequence: " << old_entry.m_rx_max_acked_sequence);
	      PrintPDUSets(flowid, old_entry.m_rx_sequence, ini_seq_compare,
			   max_sequence, "Pre-rejection buffer", true);

	      //	      NS_ASSERT ((seq <= total_max_seq) || (ini_seq_compare <= seq && seq < max_sequence));
	      ret = false;
	    }

	  Time sch = 2*m_3n_MPL;

	  Time next_pdu = Seconds (8.192 / old_entry.m_rx_rate);
	  Time A = Seconds (8.192 / old_entry.m_rx_rate) * old_entry.m_rx_window;
	  Time R = m_retransmission_attempts*m_3n_pdu_window_retx;

	  // Cancel the previous scheduled event, make a new one
	  old_entry.m_flowid_erasure.Cancel ();

	  // Update Time
	  sch += 2*(A + R);

	  old_entry.m_flowid_erasure = Simulator::Schedule(sch, &ForwardingStrategy::EliminateReceivedFlowid, this, faceid, dstid, flowid);
	  old_entry.m_rx_last_pdu = Simulator::Now ();

	  old_entry.m_force_ack.Cancel ();
	  old_entry.m_force_ack = Simulator::Schedule((2*(m_3n_MPL + next_pdu)), &ForwardingStrategy::TransmitLossACKP, this, inFace, src, flowid);
	  NS_LOG_DEBUG ("Rescheduling Received Flowid: " << flowid << " Sequence: " << seq << " -> elimination at: " << (now + sch).GetSeconds () <<
			" forced ACK at: " << (now +(2*(m_3n_MPL + next_pdu))).GetSeconds ());

	  if (CanTransmitACKP(old_entry.m_rx_sequence, old_entry.m_flowid, window_compare, ini_seq_compare, max_sequence))
	    {
	      NS_LOG_DEBUG ("Received Face: " << faceid << " PoA: " << dstid << " Flowid: " << flowid <<
			    " Sequence: " << ini_seq_compare << " <--> " << (ini_seq_compare + window_compare));

	      // Force an update, particularly of the m_rx_sequence
	      m_flowids_accepted.replace(itface, old_entry);

	      TransmitFlowidACKP (inFace, src, flowid);

	      // Because of TransmitACKP, we need to recall old_entry
	      itface = face_index.find (boost::make_tuple (flowid, faceid, dstid));

	      NS_ASSERT_MSG (itface != face_index.end (), "UpdateReceivedPDU did not find the position again, check code");

	      old_entry = *itface;
	      // Now clear the sequence
	      old_entry.m_rx_sequence.clear ();
	      // Signal that we have at least one ACK
	      old_entry.m_rx_first_ack = true;
//	      // Update the maximum ACK seen
//	      old_entry.m_rx_max_acked_sequence = seq;
	    }

	  m_flowids_accepted.replace(itface, old_entry);
	}
      else
	{
	  if (!pdu->hasRF ())
	    {
	      NS_LOG_ERROR ("REJECTED Flowid: " << flowid << " Sequence: " << seq << " first time seeing this flowid and have no RF");
	      return false;
	    }
	  NS_ASSERT (window != 0);
	  NS_ASSERT (rate != 0);

	  Time next_pdu = Seconds (8.192 / rate);
	  Time A = Seconds ((8.192 * window) / rate);
	  Time R = m_retransmission_attempts*m_3n_pdu_window_retx;

	  Flowid_Receiver tmp;
	  tmp.m_flowid = flowid;
	  tmp.m_face = faceid;
	  tmp.m_dst_id = dstid;
	  tmp.m_rx_initial_sequence = ini_seq;
	  tmp.m_rx_sequence = std::set<uint32_t> ();
	  tmp.m_rx_window = window;
	  tmp.m_rx_rate = rate;
	  tmp.m_rx_ack = false;
	  tmp.m_rx_first_ack = false;
	  tmp.m_rx_max_acked_sequence = 0;
	  tmp.m_rx_last_pdu = Simulator::Now ();
	  tmp.m_flowid_erasure = Simulator::Schedule((2*(m_3n_MPL + R + A)), &ForwardingStrategy::EliminateReceivedFlowid, this, faceid, dstid, flowid);
	  tmp.m_force_ack = Simulator::Schedule((2*(m_3n_MPL + next_pdu)), &ForwardingStrategy::TransmitLossACKP, this, inFace, src, flowid);

	  uint32_t max_sequence = ini_seq + window;

	  if (tmp.m_rx_initial_sequence <= seq && seq < max_sequence)
	    {
	      NS_LOG_DEBUG ("Flowid: " << flowid << " inserting Sequence: " << seq <<
			    " which is between " << ini_seq << " and " << max_sequence);
	      tmp.m_rx_sequence.insert (seq);
	    }
	  else
	    {
	      NS_LOG_WARN ("Flowid: " << flowid << " Sequence: " << seq <<
			   " looking between " << ini_seq << " <= x < " << max_sequence);
	    }

	  FlowidReceivedContainer::iterator it;
	  bool insert = false;

	  std::tie (it, insert) = m_flowids_accepted.insert (tmp);

	  if (insert)
	    {
	      NS_LOG_DEBUG ("Insertion Received Flowid: " << tmp.m_flowid << " Sequence: " << seq << " Face: " <<  tmp.m_face <<
			    " PoA: " << src << " PoA ID: " << tmp.m_dst_id << " Initial Sequence: " << tmp.m_rx_initial_sequence  <<
			    " Max Sequence seen: " << seq <<
			    " Window: " << tmp.m_rx_window << " Rate (kbit/s): " << tmp.m_rx_rate  <<
			    " ACK Sent: " << std::boolalpha << tmp.m_rx_ack <<
			   // " Force ACK: " << (now + (2*(m_3n_MPL + next_pdu))).GetSeconds () <<
			    " Elimination: " << (now + 2*(m_3n_MPL + R + A)).GetSeconds ());

	      if (CanTransmitACKP(tmp.m_rx_sequence, tmp.m_flowid, tmp.m_rx_window, ini_seq, max_sequence))
		{
		  TransmitFlowidACKP (inFace, src, flowid);

		  tmp.m_rx_sequence.clear ();

		  m_flowids_accepted.replace(it, tmp);
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("Error in insertion of received Flowid: " << tmp.m_flowid << " Sequence: " << seq << " Face: " <<  tmp.m_face <<
			   " PoA: " << src << " PoA ID: " << tmp.m_dst_id << " Initial Sequence: " << tmp.m_rx_initial_sequence  <<
			   " Window: " << tmp.m_rx_window << " Rate (kbit/s): " << tmp.m_rx_rate  <<
			   " ACK Sent: " << std::boolalpha << tmp.m_rx_ack <<
			   " Force ACK: " << (now + (2*(m_3n_MPL + next_pdu))).GetSeconds () <<
			   " Elimination: " << (now + 2*(m_3n_MPL + R + A)).GetSeconds ());
	    }
	}

      NS_LOG_DEBUG ("Flowid: " << flowid << " Sequence: " << seq << " Forwarding: " << std::boolalpha << ret);
      return ret;
    }

    void
    ForwardingStrategy::UpdateSentPDUs (Ptr<DATAPDU> pdu, Ptr<Face> outFace, Address dst, bool retx)
    {
      //      if (pdu)
      //	{
      //	  NS_LOG_FUNCTION(pdu->GetFlowid() << pdu->GetSequence() << outFace->GetId () << dst);
      //	}
      //      else
      //	{
      //	  NS_LOG_WARN ("Given a null PDU. Check code");
      //	  return;
      //	}

      if (outFace->isAppFace())
	{
	  NS_LOG_DEBUG ("Out Face is App Face, returning");
	  return;
	}

      std::string rtstr;

      if (retx)
	rtstr = "ReTx ";
      else
	rtstr = "";

      uint32_t flowid = pdu->GetFlowid ();

      NS_ASSERT (flowid != 0);

      uint32_t faceid = outFace->GetId ();
      uint16_t dstid = outFace->GetAddressId (dst);
      uint32_t window = pdu->GetPDUWindow ();
      uint32_t rate = pdu->GetRate ();
      uint32_t seq = pdu->GetSequence ();
      uint32_t ini_seq = pdu->GetInitialWindowSeq ();

      Time now = Simulator::Now ();

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, faceid, dstid));

      std::string str_ini = "Flowid: " + std::to_string(flowid) + " Sequence: " + std::to_string(seq);

      if (itface != face_index.end ())
	{
	  Flowid_Sender old_entry = *itface;

	  uint32_t window_compare = old_entry.m_tx_window;
	  uint32_t ini_seq_compare = old_entry.m_tx_initial_sequence;
	  uint32_t max_sequence = ini_seq_compare + window_compare;

	  if (pdu->hasRF ())
	    {
	      NS_ASSERT (window != 0);
	      NS_ASSERT (rate != 0);

	      std::string modification_str = "";

	      if (old_entry.m_tx_window != window)
		{
		  modification_str = " Window from " + std::to_string(old_entry.m_tx_window) + " to " + std::to_string(window);
		  old_entry.m_tx_window = window;
		}

	      if (old_entry.m_tx_initial_sequence != ini_seq)
		{
		  modification_str += " Initial sequence from " + std::to_string(old_entry.m_tx_initial_sequence) +
		      " to " + std::to_string(ini_seq);
		  old_entry.m_tx_initial_sequence = ini_seq;
		}

	      if (old_entry.m_tx_rate != rate)
		{
		  modification_str += " Rate from " + std::to_string(old_entry.m_tx_rate) +
		      " to " + std::to_string (rate);
		  old_entry.m_tx_rate = rate;
		}

	      NS_LOG_DEBUG ("RF modification " << str_ini << modification_str);

	      PrintPDUSets(flowid, old_entry.m_tx_sequence_window, ini_seq_compare, max_sequence, "RF modification");

	      if (!old_entry.m_tx_sequence_window.empty ())
		{
		  std::set<uint32_t>::iterator it = old_entry.m_tx_sequence_window.lower_bound (old_entry.m_tx_initial_sequence);

		  if (it != old_entry.m_tx_sequence_window.end ())
		    {
		      if (it != old_entry.m_tx_sequence_window.begin ())
			{
			  NS_LOG_WARN (rtstr << "RF modification having to clear Flowid: " << flowid <<
				       " buffer of sequences lower: " << old_entry.m_tx_initial_sequence);
			  old_entry.m_tx_sequence_window.erase (old_entry.m_tx_sequence_window.begin (), it);
			}
		    }
		  else
		    {
		      NS_LOG_WARN (rtstr << "RF modification having to clear Flowid: " << flowid << " buffer completely");
		      old_entry.m_tx_sequence_window.clear ();
		    }

		  uint32_t max_seq = old_entry.m_tx_initial_sequence + old_entry.m_tx_window -1;
		  it = old_entry.m_tx_sequence_window.upper_bound (max_seq);

		  if (it != old_entry.m_tx_sequence_window.end ())
		    {
		      NS_LOG_WARN (rtstr << "RF modification having to clear Flowid: " << flowid <<
				   " of sequences higher than : " << max_seq);
		      old_entry.m_tx_sequence_window.erase (it, old_entry.m_tx_sequence_window.end ());
		    }
		}
	    }

	  // We are sending something, so increase the tx_sequence anyway
	  if (!retx)
	    {
	      old_entry.m_tx_sequence++;
	      NS_LOG_DEBUG (rtstr << "Flowid: " << flowid << " increasing seen Sequence to " << old_entry.m_tx_sequence);
	    }

	  // Whether we add it to the queue, is a different issue
	  if (ini_seq_compare <= seq && seq < max_sequence)
	    {

	      NS_LOG_DEBUG (rtstr << "Inserting Flowid: " << flowid << " Sequence: " << seq <<
			    " which is " << ini_seq_compare << " <= x < " << max_sequence);
	      old_entry.m_tx_sequence_window.insert (seq);
	    }
	  else
	    {
	      NS_LOG_WARN (rtstr << " REJECTED Flowid: " << flowid << " Sequence: " << seq <<
			   " looking between " << ini_seq_compare << " <= x < " << max_sequence);
	    }

	  Time sch = 3*m_3n_MPL;

	  Time A = Seconds ((8.192 * old_entry.m_tx_window) / old_entry.m_tx_rate);
	  Time R = m_retransmission_attempts*m_3n_pdu_window_retx;

	  old_entry.m_flowid_erasure.Cancel ();
	  /*
	      old_entry.m_force_retransmit.Cancel ();
	   */

	  // Update Time
	  sch += 3*(A + R);

	  old_entry.m_flowid_erasure_time = now + sch;
	  old_entry.m_flowid_erasure = Simulator::Schedule(sch,
							   &ForwardingStrategy::EliminateSentFlowid, this, faceid, dstid, flowid);
	  NS_LOG_DEBUG (rtstr << "Rescheduling Sent Flowid: " << flowid << " Sequence: " << seq << " elimination at " << (now + sch).GetSeconds ());

	  /*
	      old_entry.m_force_retransmit_time = now + (m_3n_MPL + (A/2));
	      old_entry.m_force_retransmit = Simulator::Schedule((m_3n_MPL + (A/2)),
								 &ForwardingStrategy::TransmitLossWindow, this, faceid, dstid, flowid);
	      NS_LOG_DEBUG ("Rescheduling forced Retransmit Flowid: " << flowid << " Sequence: " << seq << " elimination at " << (now +(m_3n_MPL + (A/2))).GetSeconds ());
	   */

	  m_flowids_forwarded.replace(itface, old_entry);
	}
      else
	{
	  NS_ASSERT (pdu->hasRF ());
	  NS_ASSERT (window != 0);
	  NS_ASSERT (rate != 0);

	  Time A = Seconds ((8.192 * m_3n_min_pdu_window) / rate);
	  Time R = m_retransmission_attempts*m_3n_pdu_window_retx;
	  Time now = Simulator::Now ();
	  Flowid_Sender tmp;
	  tmp.m_flowid = flowid;
	  tmp.m_face = faceid;
	  tmp.m_dst_id = dstid;
	  tmp.m_tx_sequence = seq;
	  tmp.m_tx_sequence_window = std::set<uint32_t> ();
	  tmp.m_tx_initial_sequence = ini_seq;
	  tmp.m_tx_first_window = m_3n_max_pdu_window;
	  tmp.m_tx_window = m_3n_min_pdu_window;
	  tmp.m_tx_rate = rate;
	  tmp.m_tx_initial_rate = rate;
	  tmp.m_tx_window_retx = 0;
	  tmp.m_tx_last_ackp = now;
	  tmp.m_ackp_update = false;
	  tmp.m_forcing_retransmit = false;
	  tmp.m_ack_interval = m_3n_MPL;
	  tmp.m_pdu_before_ackp = now;
	  /*
	  tmp.m_force_retransmit_time = now + (m_3n_MPL + (A/2));
	   */
	  tmp.m_flowid_erasure_time = now + (3*(m_3n_MPL + R + A));

	  tmp.m_flowid_erasure = Simulator::Schedule((3*(m_3n_MPL + R + A)), &ForwardingStrategy::EliminateSentFlowid, this, faceid, dstid, flowid);
	  /*
	  tmp.m_force_retransmit = Simulator::Schedule((m_3n_MPL + (A/2)), &ForwardingStrategy::TransmitLossWindow, this, faceid, dstid, flowid);
	   */

	  uint32_t max_sequence = ini_seq + m_3n_min_pdu_window;

	  if (tmp.m_tx_initial_sequence <= seq && seq < max_sequence)
	    {
	      NS_LOG_DEBUG (rtstr << "New Flowid: " << flowid << " inserting Sequence: " << seq <<
			    " which is between " << ini_seq << " and " << max_sequence);
	      tmp.m_tx_sequence_window.insert (seq);
	    }
	  else
	    {
	      NS_LOG_WARN (rtstr << " REJECTED Flowid: " << flowid << " Sequence: " << seq <<
			   " looking between " << ini_seq << " <= x < " << max_sequence);
	    }

	  NS_LOG_DEBUG (rtstr << "Insertion Sent Flowid: " << tmp.m_flowid << " Sequence: " << seq << " Face: " <<  tmp.m_face <<
			" PoA: " << dst << " PoA ID: " << tmp.m_dst_id << " Initial Sequence: " << tmp.m_tx_initial_sequence  <<
			" Window: " << tmp.m_tx_window << " First Window: " << tmp.m_tx_first_window << " Rate (kbit/s): " << tmp.m_tx_rate <<
			/*" Force Retransmit: " << (now + m_3n_MPL + (A/2)).GetSeconds () <<*/
			" Elimination: " << (now + 3*(m_3n_MPL + R + A)).GetSeconds ());

	  m_flowids_forwarded.insert (tmp);
	}
    }

    void
    ForwardingStrategy::SetupFlowidSeq (Ptr<DATAPDU> out_pdu, Ptr<Face> outFace, Address dst, bool retx)
    {
      //      // Log for the trace
      //      if (in_pdu)
      //	NS_LOG_FUNCTION (in_pdu->GetFlowid () << in_pdu->GetSequence() << outFace->GetId () << dst);
      //      else
      //	NS_LOG_FUNCTION ("No reference" << outFace->GetId () << dst);

      // Get the information from the selected destination
      uint32_t out_faceid = outFace->GetId ();
      uint16_t out_dstid = outFace->GetAddressId (dst);
      uint32_t out_flowid = 0;

      if (out_dstid == 0)
	{
	  outFace->InsertAddress (dst);
	  out_dstid = outFace->GetAddressId (dst);
	  NS_LOG_DEBUG ("PoA: " << dst << " on Face: " << out_faceid << " returned 0, inserting for PoA ID: " <<
			out_dstid);
	}

      bool found_map = false;

      std::string retxstr;

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface;

      if (retx)
	{
	  out_flowid = out_pdu->GetFlowid();
	  NS_LOG_DEBUG ("Searching for Flowid: " << out_pdu->GetFlowid() << " Sequence: " << out_pdu->GetSequence ());
	  itface = face_index.find ((boost::make_tuple(out_flowid, out_faceid, out_dstid)));

	  found_map = (itface != face_index.end ());
	  retxstr = "ReTx ";
	}
      else
	{
	  itface = face_index.begin ();

	  while (itface != face_index.end ())
	    {
	      if (itface->m_face == out_faceid)
		{
		  if (itface->m_dst_id == out_dstid)
		    {
		      found_map = true;
		      break;
		    }
		}

	      ++itface;
	    }

	  retxstr = "";
	}

      Time R = m_retransmission_attempts*m_3n_pdu_window_retx;

      if (found_map)
	{
	  NS_ASSERT_MSG((itface != face_index.end ()), "Did not find information for Face: " << out_faceid << " PoA ID: " << out_dstid);

	  Flowid_Sender tmp = *itface;

	  out_flowid = tmp.m_flowid;
	  uint32_t out_ini_seq = tmp.m_tx_initial_sequence;
	  uint32_t out_sequence = tmp.m_tx_sequence +1;
	  uint32_t out_rate = tmp.m_tx_rate;
	  uint32_t out_window = 0;

	  NS_LOG_DEBUG ("Current state of Face: " << out_faceid << " PoA ID: " << out_dstid << " Flowid: " << out_flowid <<
			" Initial Sequence: " << tmp.m_tx_initial_sequence << " Window: " << tmp.m_tx_window <<
			" Rate: " << out_rate << " Last transmitted Sequence: " << tmp.m_tx_sequence);

	  out_window = tmp.m_tx_window;

	  if (retx)
	    {
	      NS_ASSERT_MSG((out_flowid == out_pdu->GetFlowid()), "Error in obtaining information for Face: " << out_faceid << " PoA ID: " << out_dstid << " Flowid: " << out_flowid);
	      out_sequence = out_pdu->GetSequence();
	    }
	  else
	    {
	      out_pdu->SetFlowid (out_flowid);
	      out_pdu->SetSequence (out_sequence);
	    }


	  if (out_sequence == tmp.m_tx_initial_sequence || tmp.m_ackp_update)
	    {
	      NS_ASSERT (out_window != 0);
	      NS_ASSERT (out_rate != 0);

	      out_pdu->SetPDURF (true);
	      out_pdu->SetPDUWindow (out_window);
	      out_pdu->SetRate (out_rate);
	      out_pdu->SetInitialWindowSeq (out_ini_seq);

	      if (tmp.m_ackp_update)
		{
		  tmp.m_ackp_update = false;
		  face_index.replace (itface, tmp);
		}
	      NS_LOG_DEBUG (retxstr << "Using found Flowid: " << out_flowid << " Sequence: " << out_sequence <<
			    " pushing RF setting Window: " << out_window << " Rate: " << out_rate <<
			    " initial seq: " << out_ini_seq
	      );
	    }
	  else
	    {
	      // Turn off RF
	      out_pdu->SetPDURF(false);
	      NS_LOG_DEBUG ("Using found Flowid: " << out_flowid << " Sequence: " << out_sequence << " no RF");
	    }
	}
      else
	{
	  // Search the broadcast range
	  itface = face_index.begin ();
	  bool broadcast_m = false;

	  while (itface != face_index.end ())
	    {
	      if (itface->m_face == out_faceid)
		{
		  if (itface->m_dst_id == outFace->GetBroadcastId ())
		    {
		      broadcast_m = true;
		      break;
		    }
		}
	      ++itface;
	    }

	  if (broadcast_m)
	    {
	      uint16_t m_poa = outFace->GetAddressId (dst);

	      if (m_poa == 0)
		{
		  outFace->InsertAddress (dst);

		  m_poa = outFace->GetAddressId (dst);
		}

	      Flowid_Sender tmp = *itface;
	      uint16_t old_poa = tmp.m_dst_id;
	      tmp.m_dst_id = m_poa;
	      out_flowid = tmp.m_flowid;

	      NS_LOG_DEBUG ("Current state of broadcast Flowid: " << out_flowid << " Initial Sequence: " << tmp.m_tx_initial_sequence <<
			    " Window: " << tmp.m_tx_window << " Last transmitted Sequence: " << tmp.m_tx_sequence <<
			    " Migration of from PoA ID: " << old_poa << " to PoA ID: " << m_poa);
	      m_flowids_forwarded.replace (itface, tmp);

	      if (old_poa != m_poa)
		{
		  // Have to check for retransmissions, as these will also be affected
		  FaceEventContainer_by_unique& face_index = m_flowid_event_keeper.get<i_nf> ();
		  FaceEventContainer_by_unique::iterator itface = face_index.find (boost::make_tuple(out_flowid, out_faceid, old_poa));

		  if (itface != face_index.end ())
		    {
		      Face_Flowid_Event tmp = *itface;

		      Time now = Simulator::Now ();

		      if (tmp.m_next_retransmit > now)
			{
			  NS_LOG_DEBUG ("Broadcast Face: " << out_faceid << " PoA ID: " << old_poa << " Flowid: " << out_flowid <<
					" migrating to PoA ID: " << m_poa);
			  tmp.m_evt.Cancel ();
			  RxPDU data = RxPDU (out_faceid, m_poa, out_flowid, tmp.m_sequence);
			  tmp.m_evt = Simulator::Schedule (tmp.m_next_retransmit - now, &ForwardingStrategy::Retransmit3NWindow, this, data);

			  face_index.replace(itface, tmp);
			}
		      else
			{
			  NS_LOG_DEBUG ("Broadcast Face: " << out_faceid << " PoA ID: " << old_poa << " Flowid: " << out_flowid <<
					" no PoA modification");
			}
		    }
		  else
		    {
		      NS_LOG_DEBUG ("No events found for Face: " << out_faceid << " PoA ID: " << out_dstid << " Flowid: " << out_flowid << " skipping");
		    }

		}

	      uint32_t out_ini_seq = tmp.m_tx_initial_sequence;
	      uint32_t out_sequence = tmp.m_tx_sequence +1;
	      uint32_t out_rate = tmp.m_tx_rate;
	      uint32_t out_window = 0;

	      out_window = tmp.m_tx_window;

	      if (!retx)
		{
		  NS_LOG_DEBUG ("Assigning PDU broadcast Flowid: " << out_flowid << " Sequence: " << out_sequence);
		  out_pdu->SetFlowid (out_flowid);
		  out_pdu->SetSequence (out_sequence);
		}
	      else
		{
		  out_flowid = out_pdu->GetFlowid();
		  NS_LOG_DEBUG ("ReTx PDU has broadcast Flowid: " << out_flowid << " Sequence: " << out_pdu->GetSequence());
		}

	      if (out_sequence == tmp.m_tx_initial_sequence || tmp.m_ackp_update)
		{
		  NS_ASSERT (out_window != 0);
		  NS_ASSERT (out_rate != 0);
		  NS_LOG_DEBUG ("Using broadcast Flowid: " << out_flowid << " setting Sequence: " << out_sequence <<
				" pushing RF setting Window: " << out_window << " Rate: " << out_rate <<
				" Initial sequence: " << out_ini_seq);
		  out_pdu->SetPDURF (true);
		  out_pdu->SetPDUWindow (out_window);
		  out_pdu->SetRate (out_rate);
		  out_pdu->SetInitialWindowSeq(out_sequence);

		  if (tmp.m_ackp_update)
		    {
		      tmp.m_ackp_update = false;
		      face_index.replace (itface, tmp);
		    }
		}
	      else
		{
		  out_pdu->SetPDURF(false);
		  NS_LOG_DEBUG ("Using broadcast Flowid: " << out_flowid << " setting Sequence: " << out_sequence <<
				" no RF");
		}
	    }
	  else
	    {
	      // Obtain a new flowid
	      out_flowid = GenerateAppFlowid();

	      NS_ASSERT (m_3n_default_rate != 0);

	      // Now ensure that the outgoing PDU has the updated information
	      out_pdu->SetPDURF (true);
	      NS_ASSERT (m_3n_min_pdu_window != 0);
	      out_pdu->SetPDUWindow (m_3n_min_pdu_window);
	      NS_LOG_DEBUG ("Generated new Flowid: " << out_flowid <<
			    " setting Sequence: 0 with RF Window: " << m_3n_min_pdu_window <<
			    " Rate: " << m_3n_default_rate);
	      out_pdu->SetRate (m_3n_default_rate);
	      out_pdu->SetInitialWindowSeq (0);
	      out_pdu->SetFlowid (out_flowid);
	      out_pdu->SetSequence (0);
	    }
	}
    }

    bool
    ForwardingStrategy::IsWaitingForNextWindowACK (RxPDU data)
    {
      FaceEventContainer_by_flowid& face_index = m_flowid_event_keeper.get<i_nf_fid> ();
      FaceEventContainer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = face_index.equal_range(data.m_flowid);

      if (it0 != it1)
	{
	  while (it0 != it1)
	    {
	      Face_Flowid_Event tmp = *it0;
	      NS_LOG_DEBUG ("Waiting for next ACKP for Face: " << tmp.m_face <<
			    " PoA ID: " << tmp.m_dst_id << " Flowid: " << tmp.m_flowid <<
			    " Sequence: " << tmp.m_sequence << " trigger Execution: " << tmp.m_next_retransmit.GetSeconds ());
	      ++it0;
	    }
	  return true;
	}
      else
	{
	  return false;
	}
    }

    bool
    ForwardingStrategy::IsWaitingForPrevWindowACK (RxPDU data)
    {
      FaceEventContainer_by_flowid& face_index = m_flowid_event_keeper.get<i_nf_fid> ();
      FaceEventContainer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = face_index.equal_range(data.m_flowid);

      if (it0 != it1)
	{
	  while (it0 != it1)
	    {
	      Face_Flowid_Event tmp = *it0;
	      NS_LOG_DEBUG ("Waiting for previous ACKP for Face: " << tmp.m_face <<
			    " PoA ID: " << tmp.m_dst_id << " Flowid: " << tmp.m_flowid <<
			    " Sequence: " << tmp.m_sequence << " trigger Execution: " << tmp.m_next_retransmit.GetSeconds () <<
			    " Initial Sequence: " << tmp.m_ini_sequence << " Window: " << tmp.m_window <<
			    " Rate: " << tmp.m_rate);
	      ++it0;
	    }
	  return true;
	}
      else
	{
	  return false;
	}
    }

    bool
    ForwardingStrategy::IsACKPOutOfOrder (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t sequence)
    {
      FaceEventContainer_by_unique& face_index = m_flowid_event_keeper.get<i_nf> ();
      FaceEventContainer_by_unique::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      if (itface != face_index.end ())
	{
	  Face_Flowid_Event tmp = *itface;

	  if (sequence != tmp.m_sequence)
	    {
	      NS_LOG_DEBUG ("Window ACK out of order wait for received Flowid: " << flowid << " Sequence: " << sequence <<
			    " Face: " << face_id << " PoA ID: " << dst_id << " trigger sequence was: " << tmp.m_sequence <<
			    " Execution: " << tmp.m_next_retransmit.GetSeconds ());
	      return true;
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Window ACK in order wait for received Flowid: " << flowid << " Sequence: " << sequence <<
			    " Face: " << face_id << " PoA ID: " << dst_id << " trigger sequence is: " << tmp.m_sequence <<
			    " Execution: " << tmp.m_next_retransmit.GetSeconds ());
	      return false;
	    }
	}
      else
	{
	  NS_LOG_WARN ("No Window ACK waiting for Flowid: " << flowid << " Sequence: " << sequence <<
		       " Face: " << face_id << "PoA ID: " << dst_id);
	  return true;
	}
    }

    bool
    ForwardingStrategy::CancelWindowWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t sequence)
    {
      //      NS_LOG_FUNCTION(face_id << dst_id << flowid << sequence);
      FaceEventContainer_by_unique& face_index = m_flowid_event_keeper.get<i_nf> ();
      FaceEventContainer_by_unique::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      if (itface != face_index.end ())
	{
	  Face_Flowid_Event tmp = *itface;

	  if (sequence >= tmp.m_sequence)
	    {
	      NS_LOG_DEBUG ("Canceling Window wait for received Flowid: " << flowid << " Sequence: " << sequence <<
			    " Face: " << face_id << " PoA ID: " << dst_id << " trigger sequence was: " << tmp.m_sequence <<
			    " Execution: " << tmp.m_next_retransmit.GetSeconds ());
	      tmp.m_evt.Cancel ();

	      face_index.erase (itface);
	      return true;
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Cannot cancel Window wait for received Flowid: " << flowid << " Sequence: " << sequence <<
			    " Face: " << face_id << " PoA ID: " << dst_id << " trigger sequence is: " << tmp.m_sequence <<
			    " Execution: " << tmp.m_next_retransmit.GetSeconds ());
	      return false;
	    }

	}
      else
	{
	  FaceEventContainer_by_unique::iterator itface2 = face_index.find (boost::make_tuple(flowid, face_id, m_faces->Get(face_id)->GetBroadcastId ()));

	  if (itface2 != face_index.end ())
	    {
	      Face_Flowid_Event tmp = *itface2;
	      if (sequence >= tmp.m_sequence)
		{
		  NS_LOG_DEBUG ("Canceling Window wait for received Flowid: " << flowid << " Sequence: " << sequence <<
				" Face: " << face_id << " PoA ID: " << m_faces->Get(face_id)->GetBroadcastId () <<
				" trigger sequence was: " << tmp.m_sequence << " Execution: " << tmp.m_next_retransmit.GetSeconds ());
		  tmp.m_evt.Cancel ();

		  face_index.erase (itface2);
		  return true;
		}
	      else
		{
		  NS_LOG_DEBUG ("Cannot cancel Window wait for received Flowid: " << flowid << " Sequence: " << sequence <<
				" Face: " << face_id << " PoA ID: " << m_faces->Get(face_id)->GetBroadcastId () << " trigger sequence is: " << tmp.m_sequence <<
				" Execution: " << tmp.m_next_retransmit.GetSeconds ());
		  return false;
		}
	    }
	  else
	    {
	      NS_LOG_WARN ("Absolutely no Window wait for Flowid: " << flowid << " Sequence: " << sequence << " Face: " << face_id << " PoA ID: " << dst_id << " or Broadcast");

	      if (face_index.empty())
		{
		  NS_LOG_WARN ("Flowid Event container is empty!");
		}
	      else
		{
		  itface = face_index.begin ();

		  while (itface != face_index.end ())
		    {
		      Face_Flowid_Event tmp = *itface;
		      NS_LOG_WARN ("Available Window wait for Flowid: " << tmp.m_flowid << " Sequence: " << tmp.m_sequence << " Face: " << tmp.m_face << " PoA ID: " << tmp.m_dst_id <<
				   " Execution: " << tmp.m_next_retransmit.GetSeconds ()
		      );
		      ++itface;
		    }
		}
	      return false;
	    }
	}
    }

    bool
    ForwardingStrategy::CancelFlowidWindowWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      FaceEventContainer_by_unique& face_index = m_flowid_event_keeper.get<i_nf> ();
      FaceEventContainer_by_unique::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      if (itface != face_index.end ())
	{
	  Face_Flowid_Event tmp = *itface;

	  NS_LOG_DEBUG ("Canceling Window wait for received Flowid: " << flowid <<
			" Face: " << face_id << " PoA ID: " << dst_id << " trigger sequence was: " << tmp.m_sequence <<
			" Execution: " << tmp.m_next_retransmit.GetSeconds ());
	  tmp.m_evt.Cancel ();

	  face_index.erase (itface);
	  return true;
	}
      else
	{
	  FaceEventContainer_by_unique::iterator itface2 = face_index.find (boost::make_tuple(flowid, face_id, m_faces->Get(face_id)->GetBroadcastId ()));

	  if (itface2 != face_index.end ())
	    {
	      Face_Flowid_Event tmp = *itface2;

	      NS_LOG_DEBUG ("Canceling Window wait for received Flowid: " << flowid <<
			    " Face: " << face_id << " PoA ID: " << m_faces->Get(face_id)->GetBroadcastId () <<
			    " trigger sequence was: " << tmp.m_sequence << " Execution: " << tmp.m_next_retransmit.GetSeconds ());
	      tmp.m_evt.Cancel ();

	      face_index.erase (itface2);
	      return true;
	    }
	  else
	    {
	      NS_LOG_WARN ("Absolutely no Window wait for Flowid: " << flowid << " Face: " << face_id << " PoA ID: " << dst_id << " or Broadcast");

	      if (face_index.empty())
		{
		  NS_LOG_WARN ("Flowid Event container is empty!");
		}
	      else
		{
		  itface = face_index.begin ();

		  while (itface != face_index.end ())
		    {
		      Face_Flowid_Event tmp = *itface;
		      NS_LOG_WARN ("Available Window wait for Flowid: " << tmp.m_flowid << " Sequence: " << tmp.m_sequence << " Face: " << tmp.m_face << " PoA ID: " << tmp.m_dst_id <<
				   " Execution: " << tmp.m_next_retransmit.GetSeconds ()
		      );
		      ++itface;
		    }
		}
	      return false;
	    }
	}
    }

    bool
    ForwardingStrategy::UpdateLastSeenACKP (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t seq, bool out_of_order)
    {
      //      NS_LOG_FUNCTION (face_id << dst_id << flowid << seq << std::boolalpha << out_of_order << canceledACKP);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      if (itface != face_index.end ())
	{
	  Flowid_Sender old_entry = *itface;

	  uint32_t out_rate = old_entry.m_tx_rate;
	  uint32_t out_window = old_entry.m_tx_window;
	  Time last_seen = old_entry.m_tx_last_ackp;

	  Time A = Seconds ((8.192 * out_window) / out_rate);
	  Time R = m_retransmission_attempts*m_3n_pdu_window_retx;

	  Time now = Simulator::Now ();
	  Time interval = now - last_seen;

	  if (out_of_order)
	    {
	      NS_LOG_DEBUG ("ACKed Out of Order Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
			    " Sequence: " << old_entry.m_tx_initial_sequence << " <--> " << (seq +1));

	      NS_LOG_DEBUG ("Acknowledging Out of Order ACKP Flowid: " << flowid << " up to Sequence: " << seq <<
			    " current Rate: " << out_rate << " Window: " << out_window);

	      uint32_t seen_window = seq - old_entry.m_tx_initial_sequence +1;
	      uint32_t n_out_rate = (8.192 * seen_window) / interval.GetSeconds ();
	      if (n_out_rate <= 1)
		{
		  NS_LOG_DEBUG ("Have not seen in order ACK for Flowid: " << flowid <<
				" reached minimum Rate: 1  Initial Sequence: " << seq+1 <<
				" PDUs seen: " << seen_window << " interval: " << interval.GetSeconds () <<
				" calculated Rate: " << n_out_rate);
		  n_out_rate = 1;

		}
	      else
		{
		  n_out_rate /= 4;
		  if (n_out_rate > m_3n_default_rate)
		    {
		      n_out_rate = m_3n_default_rate;
		    }

		  if (n_out_rate <= 1)
		    {
		      n_out_rate = 1;
		    }

		  NS_LOG_DEBUG ("Have not seen in order ACK for Flowid: " << flowid <<
				" reducing Rate: " << n_out_rate << " Initial Sequence: " << seq+1 <<
				" PDUs seen: " << seen_window << " interval: " << interval.GetSeconds ());
		}

	      old_entry.m_tx_rate = n_out_rate;

	      out_window = n_out_rate * interval.GetSeconds();

	      if (out_window > old_entry.m_tx_window)
		{
		  out_window = old_entry.m_tx_window / 2;

		  if (out_window < m_3n_min_pdu_window)
		    {
		      out_window = m_3n_min_pdu_window;
		    }
		}
	      else
		{
		  if (out_window < m_3n_min_pdu_window)
		    {
		      out_window = m_3n_min_pdu_window;
		    }
		}

	      NS_LOG_DEBUG ("Normal have not seen in order ACK for Flowid: " << flowid <<
			    " modifying Window from: " << old_entry.m_tx_window <<
			    " to Window: " << out_window << " Initial Sequence: " << seq+1);
	      /*
	      if (out_window > m_3n_min_pdu_window)
		{
		  uint32_t curr_max_window =  m_3n_max_pdu_window > old_entry.m_tx_first_window ? m_3n_max_pdu_window : old_entry.m_tx_first_window;

		  if (out_window < curr_max_window)
		    {
		      NS_LOG_DEBUG ("Normal have not seen in order ACK for Flowid: " << flowid <<
				    " modifying Window from: " << old_entry.m_tx_window <<
				    " to Window: " << out_window << " Initial Sequence: " << seq+1);
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Normal have not seen in order ACK for Flowid: " << flowid <<
				    " modifying Window from: " << old_entry.m_tx_window <<
				    " to Window: " << curr_max_window << " Initial Sequence: " << seq+1 <<
				    " calculated Window: " << out_window);
		      out_window = curr_max_window;
		    }
		}
	      else
		{
		  NS_LOG_DEBUG ("Normal have not seen in order ACK for Flowid: " << flowid <<
				" reached minimum Window: " << m_3n_min_pdu_window << " Initial Sequence: " << seq+1 <<
				" calculated Window: " << out_window);
		  out_window = m_3n_min_pdu_window;
		}
		*/

	      old_entry.m_tx_window = out_window;

	      uint32_t n_ini = seq + 1;

	      if (n_ini > old_entry.m_tx_initial_sequence)
		{
		  NS_LOG_DEBUG ("Updating Flowid: " << flowid << " Initial Sequence: " << n_ini << " from " << old_entry.m_tx_initial_sequence);
		  old_entry.m_tx_initial_sequence = seq + 1;
		}
	      else
		{
		  NS_LOG_WARN ("Flowid: " << flowid << " Initial Sequence: " << old_entry.m_tx_initial_sequence << " > (less than) received ACK: " << n_ini);
		}

	      if (!old_entry.m_tx_sequence_window.empty())
		{
		  /*
		  std::set<uint32_t>::iterator it = old_entry.m_tx_sequence_window.lower_bound (old_entry.m_tx_initial_sequence);

		  if (it != old_entry.m_tx_sequence_window.end ())
		    {
		      if (it != old_entry.m_tx_sequence_window.begin ())
			{
			  NS_LOG_WARN ("Updating out of order Flowid: " << flowid <<
				       " clearing buffer of sequence lower than Initial Sequence: " <<
				       old_entry.m_tx_initial_sequence);
			  old_entry.m_tx_sequence_window.erase (old_entry.m_tx_sequence_window.begin (), it);
			}
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Updating out of order Flowid: " << flowid << " Initial Sequence: " << old_entry.m_tx_initial_sequence <<
				    " clearing complete sequence buffer");
		      old_entry.m_tx_sequence_window.clear ();
		    }

		  uint32_t max_seq = old_entry.m_tx_initial_sequence + old_entry.m_tx_window -1;
		  it = old_entry.m_tx_sequence_window.upper_bound (max_seq);

		  if (it != old_entry.m_tx_sequence_window.end ())
		    {
		      NS_LOG_WARN ("Updating out of order Flowid: " << flowid <<
				   " clearing buffer of sequences higher than : " << max_seq);
		      old_entry.m_tx_sequence_window.erase (it, old_entry.m_tx_sequence_window.end ());
		    }
		    */
		  old_entry.m_tx_sequence_window.clear();
		}

	      old_entry.m_ackp_update = true;

	      m_flowids_forwarded.replace(itface, old_entry);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("ACKed In Order Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
			    " Sequence: " << old_entry.m_tx_initial_sequence << " <--> " << (seq +1));
	      NS_LOG_DEBUG ("Acknowledging in order ACKP Flowid: " << flowid << " Sequence: " << seq << " Interval: " <<
			    interval.GetSeconds () << " Rate: " << out_rate << " Window: " << out_window);

	      uint32_t n_out_rate = (8.192 * out_window) / interval.GetSeconds ();

	      uint32_t curr_max_rate = old_entry.m_tx_initial_rate > m_3n_default_rate ? old_entry.m_tx_initial_rate : m_3n_default_rate;

	      if (n_out_rate > curr_max_rate)
		{
		  old_entry.m_tx_rate = curr_max_rate;
		  NS_LOG_DEBUG ("Have seen ACK for Flowid: " << flowid << " in " <<
				interval.GetSeconds () << " giving calculate Rate: " << n_out_rate <<
				" but already using original Rate: " << curr_max_rate);
		}
	      else
		{
		  if (n_out_rate <= 1)
		    {
		      n_out_rate = 1;
		    }

		  old_entry.m_tx_rate = n_out_rate;
		  NS_LOG_DEBUG ("Have seen ACK for Flowid: " << flowid << " window of " << out_window << " in "
				<< interval.GetSeconds () << " increasing Rate: " << n_out_rate);
		}

	      uint32_t curr_max_window =  m_3n_max_pdu_window > old_entry.m_tx_first_window ? m_3n_max_pdu_window : old_entry.m_tx_first_window;

	      out_window = n_out_rate * interval.GetSeconds();

	      if (out_window < curr_max_window)
		{
		  NS_LOG_DEBUG ("Have seen ACK for Flowid: " << flowid << " in " << interval.GetSeconds () <<
				" increasing Window from: " << old_entry.m_tx_window <<
				" to " << out_window);
		  old_entry.m_tx_window = out_window;
		}
	      else
		{
		  if (old_entry.m_tx_window != m_3n_max_pdu_window)
		    {
		      NS_LOG_DEBUG ("Have seen ACK for Flowid: " << flowid << " in " <<
				    interval.GetSeconds () << " increasing Window from: " <<
				    old_entry.m_tx_window << " to " << m_3n_max_pdu_window <<
				    " calculated Window: " << out_window);
		      old_entry.m_tx_window = m_3n_max_pdu_window;
		      old_entry.m_tx_first_window = m_3n_max_pdu_window;
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Have seen ACK for Flowid: " << flowid << " in " <<
				    interval.GetSeconds () << " but already using max window: " <<
				    m_3n_max_pdu_window << " calculated Window: " << out_window);
		    }
		}

	      uint32_t n_ini = seq + 1;

	      if (n_ini > old_entry.m_tx_initial_sequence)
		{
		  NS_LOG_DEBUG ("Updating Flowid: " << flowid << " last ACKP to " << now.GetSeconds () <<
				" Initial Sequence: " << n_ini << " from " << old_entry.m_tx_initial_sequence);
		  old_entry.m_tx_initial_sequence = n_ini;
		}
	      else
		{
		  NS_LOG_WARN ("Flowid: " << flowid << " Initial Sequence: " << old_entry.m_tx_initial_sequence <<
			       " received ACK: " << n_ini << " updating only last ACKP to " << now.GetSeconds ());
		}

	      if (!old_entry.m_tx_sequence_window.empty())
		{
		  std::set<uint32_t>::iterator it = old_entry.m_tx_sequence_window.lower_bound (old_entry.m_tx_initial_sequence);

		  if (it != old_entry.m_tx_sequence_window.end ())
		    {
		      if (it != old_entry.m_tx_sequence_window.begin ())
			{
			  NS_LOG_WARN ("Updating in order Flowid: " << flowid <<
				       " clearing buffer of sequence lower than Initial Sequence: " <<
				       old_entry.m_tx_initial_sequence);
			  old_entry.m_tx_sequence_window.erase (old_entry.m_tx_sequence_window.begin (), it);
			}
		    }
		  else
		    {
		      NS_LOG_DEBUG ("Updating in order Flowid: " << flowid << " Initial Sequence: " << old_entry.m_tx_initial_sequence <<
				    " clearing complete sequence buffer");
		      old_entry.m_tx_sequence_window.clear ();
		    }

		  uint32_t max_seq = old_entry.m_tx_initial_sequence + old_entry.m_tx_window -1;
		  it = old_entry.m_tx_sequence_window.upper_bound (max_seq);

		  if (it != old_entry.m_tx_sequence_window.end ())
		    {
		      NS_LOG_WARN ("Updating in order Flowid: " << flowid <<
				   "clearing buffer of sequences higher than : " << max_seq);
		      old_entry.m_tx_sequence_window.erase (it, old_entry.m_tx_sequence_window.end ());
		    }
		}

	      old_entry.m_tx_last_ackp = now;

	      Time n_ack = now - old_entry.m_pdu_before_ackp;
	      Time n_interval = (old_entry.m_ack_interval + n_ack) / 2;

	      NS_LOG_DEBUG ("Saw an ACKP for Flowid: " << flowid << " Sequence: " << seq <<
			    " take: " << n_ack.GetSeconds () << ", modifying Window retransmission interval from " <<
			    old_entry.m_ack_interval.GetSeconds () << " to " << n_interval.GetSeconds());

	      old_entry.m_ack_interval = n_interval;

	      old_entry.m_ackp_update = true;

	      m_flowids_forwarded.replace(itface, old_entry);
	    }
	  return true;
	}
      else
	{
	  NS_LOG_WARN ("Found no sent information for Flowid: " << flowid << " Sequence: " << seq << " Face: " << face_id << " PoA ID: " << dst_id);
	  return false;
	}
    }

    void
    ForwardingStrategy::CancelAndEliminateWindowWait (uint32_t face_id, uint16_t dst_id, uint32_t flowid)
    {
      FaceEventContainer_by_flowid& face_index = m_flowid_event_keeper.get<i_nf_fid> ();
      FaceEventContainer_by_flowid::iterator it0, it1;
      std::tie(it0, it1) = face_index.equal_range(flowid);

      while (it0 != it1)
	{
	  Face_Flowid_Event tmp_evt = *it0;
	  if (tmp_evt.m_face == face_id)
	    {
	      if (tmp_evt.m_dst_id == dst_id || tmp_evt.m_dst_id == m_faces->Get(face_id)->GetBroadcastId ())
		{
		  tmp_evt.m_evt.Cancel();
		  NS_LOG_DEBUG ("Eliminating retransmissions ACK for Face: " << face_id <<
				" PoA: " << m_faces->Get(face_id)->GetIdAddress(dst_id) <<
				" Flowid: " << flowid << " Sequence: " << tmp_evt.m_sequence <<
				" trigger Execution: " << tmp_evt.m_next_retransmit.GetSeconds ());
		  it0 = face_index.erase (it0);
		}
	      else
		{
		  ++it0;
		}
	    }
	  else
	    {
	      ++it0;
	    }
	}
    }

    void
    ForwardingStrategy::CancelAndEliminateReceivedFlowids (Ptr<Face> face, Address dst)
    {
      //      NS_LOG_FUNCTION(face->GetId () << dst);

      FlowidReceivedContainer_uniq& face_index = m_flowids_accepted.get<i_fid_rx> ();
      FlowidReceivedContainer_uniq::iterator itface = face_index.begin ();

      uint32_t faceid = face->GetId ();
      uint16_t dst_id = face->GetAddressId(dst);

      while (itface != face_index.end ())
	{
	  Flowid_Receiver tmp = *itface;
	  if (tmp.m_face == faceid)
	    {
	      if (tmp.m_dst_id == dst_id)
		{
		  uint32_t flowid = tmp.m_flowid;
		  NS_LOG_DEBUG ("Eliminating received information from Face: " << faceid << " PoA: " << dst << " Flowid: " << flowid);
		  tmp.m_flowid_erasure.Cancel();
		  tmp.m_force_ack.Cancel ();

		  itface = m_flowids_accepted.erase(itface);

		  m_FlowIdElimination(flowid);
		}
	      else
		{
		  ++itface;
		}
	    }
	  else
	    {
	      ++itface;
	    }
	}
    }

    void
    ForwardingStrategy::CancelAndEliminateAllSentFlowids (Ptr<Face> face)
    {
      //      NS_LOG_FUNCTION(face->GetId ());

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.begin ();

      uint32_t faceid = face->GetId ();

      while (itface != face_index.end ())
	{
	  Flowid_Sender tmp = *itface;
	  if (tmp.m_face == faceid)
	    {
	      uint32_t flowid = tmp.m_flowid;
	      uint16_t dst_id = tmp.m_dst_id;

	      Address dst = face->GetIdAddress(dst_id);

	      CancelAndEliminateWindowWait (faceid, dst_id, flowid);

	      // Flush Buffer
	      m_node_rxbuffer_container->RemoveFlowid(face, dst, flowid);

	      NS_LOG_DEBUG ("Eliminating sent information for Face: " << faceid << " PoA: " << dst << " Flowid: " << flowid);
	      /*
	      tmp.m_force_retransmit.Cancel ();
	       */
	      tmp.m_flowid_erasure.Cancel();

	      itface = m_flowids_forwarded.erase(itface);

	      m_FlowIdElimination(flowid);
	    }
	  else
	    {
	      ++itface;
	    }
	}
    }

    void
    ForwardingStrategy::MigrateAllSentFlowids (Ptr<Face> face)
    {
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.begin ();

      uint32_t faceid = face->GetId ();

      std::vector<Flowid_Sender> old_flows;
      std::vector<Flowid_Sender>::iterator it;

      while (itface != face_index.end ())
	{
	  Flowid_Sender tmp = *itface;
	  if (tmp.m_face == faceid)
	    {
	      if (tmp.m_pdu_before_ackp >= m_3n_last_oen_time)
		{
		  NS_LOG_DEBUG ("PoA ID: " << tmp.m_dst_id << " Flowid: " << tmp.m_flowid << " last PDU was sent at "
				<< tmp.m_pdu_before_ackp.GetSeconds () << " > ADEN time: " << m_3n_last_oen_time.GetSeconds ()
				<< " skipping");
		  ++itface;
		  continue;
		}
	      NS_LOG_DEBUG ("Attempting migration of Face: " << faceid << " PoA ID: " << tmp.m_dst_id <<
			    " Flowid: " << tmp.m_flowid << " last PDU was sent at " << tmp.m_pdu_before_ackp.GetSeconds () <<
			    " > ADEN time: " << m_3n_last_oen_time.GetSeconds ());
	      old_flows.push_back(tmp);
	    }

	  ++itface;
	}

      for (it = old_flows.begin(); it != old_flows.end(); it++)
	{
	  Flowid_Sender tmp = *it;

	  uint32_t curr_flowid = tmp.m_flowid;
	  uint32_t curr_dst = tmp.m_dst_id;
	  uint32_t curr_seq = tmp.m_tx_sequence;

	  NS_LOG_DEBUG ("Eliminate all wait windows for Face: " << faceid << " PoA ID: " << curr_dst <<
			" Flowid: " << curr_flowid);
	  CancelAndEliminateWindowWait (faceid, curr_dst, curr_flowid);

	  RxPDU data = RxPDU (faceid, curr_dst, curr_flowid, curr_seq);

	  NS_LOG_DEBUG ("Attempting final retransmit for Face: " << faceid <<
			" PoA ID: " << curr_dst << " Flowid: " << curr_flowid <<
			" Window: " << tmp.m_tx_window << " Buffer size: " << m_node_rxbuffer_container->Size(face, face->GetIdAddress(curr_dst), curr_flowid));

	  Migrate3NWindow(data);

	  NS_LOG_DEBUG ("Eliminate Face: " << faceid << " PoA ID: " << curr_dst << " Flowid: " << curr_flowid);
	  EliminateSentFlowid (faceid, curr_dst, curr_flowid);
	}
    }

    int8_t
    ForwardingStrategy::SendDATAPDU (RxPDU data, Ptr<DATAPDU> pdu)
    {
      //      NS_LOG_FUNCTION(data.m_flowid << data.m_face_id << data.m_dst_id << pdu);
      Ptr<Face> outFace = m_faces->Get (data.m_face_id);
      Address internal = outFace->GetIdAddress(data.m_dst_id);

      // If not a window PDU just send it out
      Ptr<NULLp> nullp_i;
      Ptr<SO> so_i;
      Ptr<DO> do_i;
      Ptr<DU> du_i;

      if (internal.IsInvalid())
	{
	  internal = outFace->GetBroadcastAddress();
	}

      uint32_t pduid = pdu->GetPacketId();
      switch(pduid)
      {
	case NULL_NNN:
	  // Convert pointer to NULLp PDU
	  nullp_i = DynamicCast<NULLp> (pdu);
	  NS_LOG_DEBUG ("Attempting transmission of NULL Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence() << " to PoA: " <<internal);
	  if (m_useQueues)
	    return (EnqueueNULLp(outFace, internal, nullp_i) ? 0 : -1);
	  else
	    return (outFace->SendNULLp(nullp_i, internal) ? 0 : -1);
	case SO_NNN:
	  // Convert pointer to SO PDU
	  so_i = DynamicCast<SO> (pdu);
	  NS_LOG_DEBUG ("Attempting transmission of SO Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence() << " to PoA: " <<internal);
	  if (m_useQueues)
	    return (EnqueueSO(outFace, internal, so_i) ? 0 : -1);
	  else
	    return (outFace->SendSO(so_i, internal) ? 0 : -1);
	case DO_NNN:
	  // Convert pointer to DO PDU
	  do_i = DynamicCast<DO> (pdu);
	  NS_LOG_DEBUG ("Attempting transmission of DO Flowid: " <<  pdu->GetFlowid() << " Sequence: " << pdu->GetSequence() << " to PoA: " <<internal);
	  if (m_useQueues)
	    return (EnqueueDO(outFace, internal, do_i) ? 0 : -1);
	  else
	    return (outFace->SendDO(do_i, internal) ? 0 : -1);
	case DU_NNN:
	  // Convert pointer to DU PDU
	  du_i = DynamicCast<DU> (pdu);
	  NS_LOG_DEBUG ("Attempting transmission of DU Flowid: " << pdu->GetFlowid() << " Sequence: " << pdu->GetSequence() << " to PoA: " <<internal);
	  if (m_useQueues)
	    return (EnqueueDU(outFace, internal, du_i) ? 0 : -1);
	  else
	    return (outFace->SendDU(du_i, internal) ? 0 : -1);
	default:
	  NS_LOG_WARN ("Received unknown PDU type! Check code");
	  return -3;
      }
    }

    int8_t
    ForwardingStrategy::SendDATAPDUToApp (Ptr<Face> face, Ptr<DATAPDU> pdu)
    {
      if (pdu)
	NS_LOG_FUNCTION(face << pdu->GetFlowid () << pdu->GetSequence ());
      else
	{
	  NS_LOG_WARN ("Given a null PDU. Check code");
	  return -3;
	}
      Ptr<NULLp> nullp_i;
      Ptr<SO> so_i;
      Ptr<DO> do_i;
      Ptr<DU> du_i;

      uint32_t pduid = pdu->GetPacketId();
      switch(pduid)
      {
	case NULL_NNN:
	  // Convert pointer to NULLp PDU
	  nullp_i = DynamicCast<NULLp> (pdu);
	  if (m_useQueues)
	    return (EnqueueNULLp(face, Address (), nullp_i) ? 0 : -1);
	  else
	    return (face->SendNULLp(nullp_i) ? 0 : -1);
	case SO_NNN:
	  // Convert pointer to SO PDU
	  so_i = DynamicCast<SO> (pdu);
	  if (m_useQueues)
	    return (EnqueueSO(face, Address (), so_i) ? 0 : -1);
	 else
	   return (face->SendSO(so_i) ? 0 : -1);
	case DO_NNN:
	  // Convert pointer to DO PDU
	  do_i = DynamicCast<DO> (pdu);
	  if (m_useQueues)
	    return (EnqueueDO(face, Address (), do_i) ? 0 : -1);
	  else
	    return (face->SendDO(do_i) ? 0 : -1);
	case DU_NNN:
	  // Convert pointer to DU PDU
	  du_i = DynamicCast<DU> (pdu);
	  if (m_useQueues)
	    return (EnqueueDU(face, Address (), du_i ) ? 0 : -1);
	  else
	    return (face->SendDU(du_i) ? 0 : -1);
	default:
	  NS_LOG_WARN ("Received unknown PDU type! Check code");
	  return -3;
      }
    }

    void
    ForwardingStrategy::UpdatePDUCountersB (Ptr<DATAPDU> pdu, Ptr<Face> face, bool ok, bool retx)
    {
      if (ok)
	UpdatePDUCounters (pdu, face, 0, retx);
      else
	UpdatePDUCounters (pdu, face, -1, retx);
    }

    bool
    ForwardingStrategy::CanTransmitACKP (std::set<uint32_t> seen, uint32_t flowid, uint32_t window, uint32_t start, uint32_t end)
    {
      if (!seen.empty())
	{
	  PrintPDUSets(flowid, seen, start, end, "In CanTransmitACKP");

	  uint32_t i_max = *(seen.rbegin ());
	  uint32_t i_min = *(seen.begin ());

	  NS_ASSERT_MSG(((start <= i_min) && (i_max <= end)),
			"Flowid: " << flowid << " Sequences include more than " <<
			start << " (" << i_min << ") and " << end << " (" << i_max << ")");

	  uint32_t res = i_max - i_min + 1;
	  bool ret = false;

	  if (res == window)
	    {
	      if (seen.size() == window)
		{
		  ret = true;
		}
	    }

	  if (ret)
	    {
	      NS_LOG_DEBUG ("Oking ACKP for Flowid: " << flowid << " between Sequence: " << i_min << " and " << i_max);
	    }
	  else
	    {
	      NS_LOG_DEBUG ("Have only seen Flowid: " << flowid <<  " " << res << " PDUs of Window: " << window <<
			    " between Sequence: " << start << " and " << end);
	    }
	  return ret;
	}
      else
	{
	  NS_LOG_DEBUG ("Have not seen PDUs for Flowid: " << flowid);
	  return false;
	}
    }

    bool
    ForwardingStrategy::CanWaitForACKP (RxPDU data)
    {
      uint32_t flowid = data.m_flowid;
      uint32_t face_id = data.m_face_id;
      uint16_t dst_id = data.m_dst_id;

      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT (itface != face_index.end ());

      std::set<uint32_t> seen = itface->m_tx_sequence_window;

      uint32_t window = itface->m_tx_window;
      uint32_t ini = itface->m_tx_initial_sequence;
      uint32_t fini = ini + window;

      if (!seen.empty())
	{
	  PrintPDUSets(flowid, seen, ini, fini, "In CanWaitForACKP");

	  uint32_t i_max = *(seen.rbegin ());
	  uint32_t i_min = *(seen.begin ());

	  NS_ASSERT_MSG(((ini <= i_min) && (i_max <= fini)),
			"Flowid: " << flowid << " Sequences include more than " <<
			ini << " (" << i_min << ") and " << fini << " (" << i_max << ")");

	  uint32_t res = i_max - i_min + 1;
	  bool ret = false;

	  if (res == window)
	    {
	      if (seen.size() == window)
		{
		  ret = true;
		}
	    }

	  if (ret)
	    {
	      NS_LOG_DEBUG ("Oking ACKP for Flowid: " << flowid << " between Sequence: " << i_min << " and " << i_max);

	    }
	  else
	    {
	      NS_LOG_DEBUG ("Have only seen Flowid: " << flowid <<  " " << res << " PDUs of Window: " << window <<
			    " between Sequence: " << ini << " and " << fini);
	    }
	  return ret;
	}
      else
	{
	  NS_LOG_DEBUG ("Have not seen PDUs for Flowid: " << flowid);
	  return false;
	}
    }

    void
    ForwardingStrategy::ClearTxSequences (RxPDU data)
    {

      uint32_t flowid = data.m_flowid;
      uint32_t face_id = data.m_face_id;
      uint16_t dst_id = data.m_dst_id;

      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT (itface != face_index.end ());

      Flowid_Sender tmp = *itface;
      NS_LOG_DEBUG("Removing Face: " << data.m_face_id << " PoA ID: "<< data.m_dst_id << " Flowid: " << data.m_flowid << " " << tmp.m_tx_sequence_window.size() << " PDUs");
      tmp.m_tx_sequence_window.clear ();
      tmp.m_forcing_retransmit = false;

      face_index.replace (itface, tmp);
    }

    std::set<Ptr<Face> >
    ForwardingStrategy::GetAllFaceSet ()
    {
      std::set<Ptr<Face> > all;

      for (uint32_t i = 0; i < m_faces->GetN(); i++)
	{
	  all.insert(m_faces->Get (i));
	}

      return all;
    }

    void
    ForwardingStrategy::UpdatePDUCounters (Ptr<DATAPDU> pdu, Ptr<Face> face, int8_t ok, bool retx)
    {
      Ptr<NULLp> nullp_i;
      Ptr<SO> so_i;
      Ptr<DO> do_i;
      Ptr<DU> du_i;

      uint32_t pduid = pdu->GetPacketId();

      if (pduid == NULL_NNN)
	{
	  nullp_i = DynamicCast<NULLp> (pdu);

	  if (ok >= 0)
	    {
	      if (ok == 0)
		{
		  m_outNULLps (nullp_i, face);
		  if (retx)
		    {
		      NS_LOG_INFO ("Retransmitted NULL Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " via Face: " << face->GetId ());
		      m_rxNULLps (nullp_i, face);
		    }
		  else
		    {
		      NS_LOG_INFO ("Transmitted NULL Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " via Face: " << face->GetId ());
		    }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Dropping NULL Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " via Face: " << face->GetId ());
	      m_dropNULLps (nullp_i, face);
	    }
	}
      else if (pduid == SO_NNN)
	{
	  so_i = DynamicCast<SO> (pdu);

	  if (ok >= 0)
	    {
	      if (ok == 0)
		{
		  m_outSOs (so_i, face);
		  if (retx)
		    {
		      NS_LOG_INFO ("Retransmitted SO Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " from " << pdu->GetSrcName () << " via Face: " << face->GetId ());
		      m_rxSOs (so_i, face);
		    }
		  else
		    {
		      NS_LOG_INFO ("Transmitted with SO Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " from " << pdu->GetSrcName () << " via Face: " << face->GetId ());
		    }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Dropping with SO Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " from " << pdu->GetSrcName () << " via Face: " << face->GetId ());
	      m_dropSOs (so_i, face);
	    }
	}
      else if (pduid == DO_NNN)
	{
	  do_i = DynamicCast<DO> (pdu);

	  if (ok >= 0)
	    {
	      if (ok == 0)
		{
		  m_outDOs (do_i, face);
		  if (retx)
		    {
		      NS_LOG_INFO ("Retransmitted with DO Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " to " << pdu->GetDstName () << " via Face: " << face->GetId ());
		      m_rxDOs (do_i, face);
		    }
		  else
		    {
		      NS_LOG_INFO ("Transmitted with DO Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " to " << pdu->GetDstName () << " via Face: " << face->GetId ());
		    }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Dropping with DO Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " to " << pdu->GetDstName () << " via Face: " << face->GetId ());
	      m_dropDOs (do_i, face);
	    }
	}
      else if (pduid == DU_NNN)
	{
	  du_i = DynamicCast<DU> (pdu);

	  if (ok >= 0)
	    {
	      if (ok == 0)
		{
		  m_outDUs (du_i, face);
		  if (retx)
		    {
		      NS_LOG_INFO ("Retransmitted with DU Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " from " << pdu->GetSrcName () << " to " << pdu->GetDstName () << " via Face: " << face->GetId ());
		      m_rxDUs (du_i, face);
		    }
		  else
		    {
		      NS_LOG_INFO ("Transmitted with DU Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " from " << pdu->GetSrcName () << " to " << pdu->GetDstName () << " via Face: " << face->GetId ());
		    }
		}
	    }
	  else
	    {
	      NS_LOG_INFO ("Dropping with DU Flowid: " << pdu->GetFlowid () << " Sequence: " << pdu->GetSequence () << " from " << pdu->GetSrcName () << " to " << pdu->GetDstName () << " via Face: " << face->GetId ());
	      m_dropDUs (du_i, face);
	    }
	}
      else
	{
	  NS_LOG_WARN ("Received unknown PDU type! Check code");
	}
    }

    void
    ForwardingStrategy::ResetFlowidSenderInfo (uint32_t flowid, uint32_t face_id, uint16_t dst_id, uint32_t ini_sequence, uint32_t window)
    {
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT_MSG(itface != face_index.end (), "Found no entry for Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid);

      Flowid_Sender tmp = *itface;

      NS_LOG_DEBUG ("Resetting Flowid sender for Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
		    " Initial Sequence: " << tmp.m_tx_initial_sequence << " -> " << ini_sequence <<
		    " Current Sequence: " << tmp.m_tx_sequence << " -> " << (ini_sequence -1) <<
		    " Window: " << tmp.m_tx_window << " -> " << window);

      tmp.m_tx_initial_sequence = ini_sequence;
      tmp.m_tx_sequence_window.clear();
      tmp.m_tx_window = window;
      face_index.replace(itface, tmp);
    }

    void
    ForwardingStrategy::ResetReTxNumSenderInfo (uint32_t flowid, uint32_t face_id, uint16_t dst_id)
    {
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT_MSG(itface != face_index.end (), "Found no entry for Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid);

      Flowid_Sender tmp = *itface;

      NS_LOG_DEBUG ("Reset Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
		    " Sequence: " << tmp.m_tx_initial_sequence << " <--> " << (tmp.m_tx_initial_sequence + tmp.m_tx_window) <<
		    " ReTx times: " << tmp.m_tx_window_retx << " to 0");

      tmp.m_tx_window_retx = 0;
      face_index.replace(itface, tmp);
    }

    void
    ForwardingStrategy::IncreaseReTxNumSenderInfo (uint32_t flowid, uint32_t face_id, uint16_t dst_id)
    {
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT_MSG(itface != face_index.end (), "Found no entry for Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid);

      Flowid_Sender tmp = *itface;

      NS_LOG_DEBUG ("Retransmitted Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
		    " Sequence: " << tmp.m_tx_initial_sequence << " <--> " << (tmp.m_tx_initial_sequence + tmp.m_tx_window) <<
		    " ReTx times: " << (tmp.m_tx_window_retx+1));

      tmp.m_tx_window_retx++;
      face_index.replace(itface, tmp);
    }

    bool
    ForwardingStrategy::CanReTxFlowidWindow (uint32_t flowid, uint32_t face_id, uint16_t dst_id)
    {
      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face_id, dst_id));

      NS_ASSERT_MSG(itface != face_index.end (), "Found no entry for Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid);

      Flowid_Sender tmp = *itface;

      if (m_infinite_retransmission)
	{
//	  NS_LOG_DEBUG ("Infinite ReTx Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
//			" Sequence: " << tmp.m_tx_initial_sequence << " <--> " << (tmp.m_tx_initial_sequence + tmp.m_tx_window) <<
//			" ReTx times: " << tmp.m_tx_window_retx);
	  return true;
	}
      else
	{
//	  NS_LOG_DEBUG ("Face: " << face_id << " PoA: " << dst_id << " Flowid: " << flowid <<
//			" Sequence: " << tmp.m_tx_initial_sequence << " <--> " << (tmp.m_tx_initial_sequence + tmp.m_tx_window) <<
//			" verify ReTx times: " << tmp.m_tx_window_retx << " < " << m_retransmission_attempts);
	  return (tmp.m_tx_window_retx < m_retransmission_attempts);
	}
    }

    void
    ForwardingStrategy::Retransmit3NWindow (RxPDU data)
    {
      NS_LOG_FUNCTION(data.m_flowid << data.m_sequence << data.m_face_id << data.m_dst_id);

      Ptr<Face> outFace = m_faces->Get (data.m_face_id);
      Address internal = outFace->GetIdAddress(data.m_dst_id);
      uint32_t flowid = data.m_flowid;
      uint32_t retx_trigger_sequence = data.m_sequence;
      uint32_t flow_ini_sequence = 0;
      uint32_t flow_window = 0;
      uint32_t flow_rate = 0;
      bool waited_for_ack = false;

      FaceEventContainer_by_unique& face_index = m_flowid_event_keeper.get<i_nf> ();
      FaceEventContainer_by_unique::iterator itface = face_index.find (boost::make_tuple(flowid, data.m_face_id, data.m_dst_id));

      std::string logstr = " trigger Flowid: " + std::to_string(flowid) + " Sequence: " + std::to_string(retx_trigger_sequence);

      if (itface != face_index.end ())
	{
	  Face_Flowid_Event tmp = *itface;

	  flow_ini_sequence = tmp.m_ini_sequence;
	  flow_window = tmp.m_window;
	  flow_rate = tmp.m_rate;

	  waited_for_ack = true;

	  NS_LOG_LOGIC (" Face: " << data.m_face_id << " PoA: " << internal << logstr <<
			" found ACKP wait event with Initial Sequence: " << flow_ini_sequence <<
			" Window: " << flow_window << " Rate: " << flow_rate);
	}
      else
	{
	  NS_LOG_LOGIC ("Retransmit called for Face:  " << data.m_face_id << " PoA: " << internal <<
			logstr  << " but didn't send enough PDUs to trigger ACK");
	}

      // Obtain the PDUs below the given threshold
      std::list<Ptr<DATAPDU> > rx_pdu;
      if (waited_for_ack)
	{
	  rx_pdu = m_node_rxbuffer_container->RetrieveDATAPDUByFlowidLessThanSeq (outFace, internal, flowid, flow_ini_sequence + flow_window);
	}
      else
	{
	  rx_pdu = m_node_rxbuffer_container->RetrieveDATAPDUByFlowidLessThanSeq (outFace, internal, flowid, retx_trigger_sequence +1);
	}

      size_t list_size = rx_pdu.size ();

      if (list_size > 0)
	{
	  NS_LOG_DEBUG ("Face: " << data.m_face_id << " PoA: " << internal << logstr <<
			" found " << rx_pdu.size() << " PDUs");

	  std::list<Ptr<DATAPDU> >::iterator pdu_it = rx_pdu.begin ();
	  std::list<Ptr<DATAPDU> >::reverse_iterator pdu_it2 = rx_pdu.rbegin ();
	  Ptr<DATAPDU> t_pdu = *pdu_it;

	  uint32_t loop_ini_sequence = t_pdu->GetSequence ();

	  t_pdu = *pdu_it2;
	  uint32_t loop_final_sequence = t_pdu->GetSequence ();

	  uint32_t loop_window = 0;
	  if (waited_for_ack)
	    {
	      loop_window = flow_ini_sequence + flow_window - loop_ini_sequence;
	    }
	  else
	    {
	      loop_window = list_size;
	    }

	  if (loop_window > m_3n_max_pdu_window)
	    {
	      loop_window = m_3n_max_pdu_window;
	    }

	  if (CanReTxFlowidWindow(flowid, data.m_face_id, data.m_dst_id))
	    {
	      // Reset the Flowid_Sender information
	      ResetFlowidSenderInfo(flowid, data.m_face_id, data.m_dst_id, loop_ini_sequence, loop_window);

	      // Cancel the current ACK wait
	      CancelWindowWait(data.m_face_id, data.m_dst_id, flowid, retx_trigger_sequence);

	      NS_LOG_DEBUG ("Attempting to retransmit Face: " << data.m_face_id << " PoA: " << internal <<
			    logstr << " PDU Sequences from: " << loop_ini_sequence << " to " <<
			    loop_final_sequence);
	      // Start processing the PDU list obtained
	      while (pdu_it != rx_pdu.end ())
		{
		  t_pdu = *pdu_it;

		  txPreparation(t_pdu, outFace, internal, true);
		  ++pdu_it;
		}

	      IncreaseReTxNumSenderInfo(flowid, data.m_face_id, data.m_dst_id);
	    }
	  else
	    {
	      /*
	      uint32_t int_seq = 0;
	      NS_LOG_DEBUG ("Exceeded retransmission attempts for Face: " << data.m_face_id << " PoA: " << internal <<
			    logstr << " PDU Sequences from: " << loop_ini_sequence << " to " <<
			    loop_final_sequence << " dropping");
	      while (pdu_it != rx_pdu.end ())
		{
		  t_pdu = *pdu_it;

		  int_seq = t_pdu->GetSequence ();

		  m_node_rxbuffer_container->RemovePDUExactFlowidSequence(outFace, internal, flowid, int_seq);

		  UpdatePDUCounters(t_pdu, outFace, -2, false);
		  ++pdu_it;
		}

	      ResetReTxNumSenderInfo(flowid, data.m_face_id, data.m_dst_id);
	      */
	      NS_ASSERT_MSG (false, "Exceeded retransmission attempts for Face: " << data.m_face_id << " PoA: " << internal <<
			     logstr << " PDU Sequences from: " << loop_ini_sequence << " to " <<
			     loop_final_sequence << " dropping");
	    }
	}
      else
	{
	  NS_LOG_DEBUG ("Face: " << data.m_face_id << " PoA: " << internal << logstr <<
			" no PDUs found in buffer, stopping retransmission");
	}
    }

    void
    ForwardingStrategy::Migrate3NWindow (RxPDU data)
    {
      NS_LOG_FUNCTION(data.m_flowid << data.m_sequence << data.m_face_id << data.m_dst_id);
      Ptr<Face> outFace = m_faces->Get (data.m_face_id);
      Ptr<Face> newFace;
      Address internal = outFace->GetIdAddress(data.m_dst_id);
      Address newAddr;
      uint32_t face_id = outFace->GetId ();
      uint32_t flowid = data.m_flowid;
//      uint32_t sequence = data.m_sequence + 1;
      uint32_t curr_sequence = 0;
      uint32_t n_flowid = 0;

      std::list<Ptr<DATAPDU> > rx_pdu = m_node_rxbuffer_container->RetrieveDATAPDUByFlowid (outFace, internal, flowid);

      std::list<Ptr<DATAPDU> >::iterator pdu_it = rx_pdu.begin ();

      if (rx_pdu.size () > 0)
	{
	  NS_LOG_DEBUG ("Migrating for Face " << data.m_face_id << " PoA: " << internal <<
			" Flowid: " << flowid << " Initial buffer: " << rx_pdu.size () << " PDUs"
	  );

	  NS_LOG_DEBUG (*m_nnst);

	  uint32_t disenrollment = 0;
	  uint32_t dst_migration = 0;
	  uint32_t src_migration = 0;
	  uint32_t poa_migration = 0;
	  uint32_t no_interface = 0;
	  uint32_t no_modification = 0;
	  uint32_t no_face = 0;
	  uint32_t hopeless_migration = 0;

	  std::map<uint32_t, TxInfo> flowid_txinfo;
	  std::map<uint32_t, TxInfo>::iterator ittx;

	  // Check if there are any nodes connected to where we are. If not, retransmitting will be in vain
	  std::set<Address> interfaces = m_nnst->OneHopPoAInfo(GetNode3NNamePtr(), outFace);

	  // First count what the size of the window would actually be
	  uint32_t total_count = 0;
	  std::string log_str;

	  while (pdu_it != rx_pdu.end ())
	    {
	      Ptr<const NNNAddress> m_new_dst = 0;
	      Ptr<DATAPDU> t_pdu = *pdu_it;
	      n_flowid = t_pdu->GetFlowid ();
	      curr_sequence = t_pdu->GetSequence ();

	      log_str = " Migrating PDU Flowid: " + std::to_string(n_flowid) + " Sequence: " + std::to_string(curr_sequence);

	      if (t_pdu->has3NSrc())
		{
		  Ptr<const NNNAddress> pdu_name = t_pdu->GetSrcNamePtr();

		  if (GoesBy3NName (pdu_name))
		    {
		      NNNAddress curr_name = GetNode3NName();
		      if (*pdu_name != curr_name)
			{
			  t_pdu->SetSrcName(GetNode3NNamePtr());

			  log_str += " 3N SRC " + pdu_name->toDotHex() + " changed to " + curr_name.toDotHex();
			  src_migration++;
			}
		    }
		}

	      if (t_pdu->has3NDst())
		{
		  Ptr<const NNNAddress> m_dst = t_pdu->GetDstNamePtr();
		  // Check if there was a disenrollment
		  if (m_node_pdu_buffer->DestinationExists(m_dst))
		    {
		      NS_LOG_WARN ("Destination " << *m_dst << " for Face: " << data.m_face_id <<
				   " Flowid: " << data.m_flowid << " Sequence: " << curr_sequence <<
				   " is moving, cancelling migration, removing from buffer and introducing to DEN buffer"
		      );
		      m_node_rxbuffer_container->RemovePDUExactFlowidSequence(outFace, internal, flowid, curr_sequence);
		      m_node_pdu_buffer->PushDATAPDU(*m_dst, *pdu_it);
		      pdu_it = rx_pdu.erase (pdu_it);
		      disenrollment++;
		      continue;
		    }

		  // Verify if the destination has another 3N name
		  if (m_nnpt->foundOldName(m_dst))
		    {
		      m_new_dst = m_nnpt->findPairedNamePtr (m_dst);
		      t_pdu->SetDstName(m_new_dst);
		      log_str += " 3N DST " + m_dst->toDotHex() + " changed to " + m_new_dst->toDotHex();
		      dst_migration++;
		    }

		  if (m_new_dst)
		    {
		      std::tie(newFace, newAddr) = m_nnst->ClosestSectorFaceInfo (m_new_dst, 0);
		    }
		  else
		    {
		      std::tie(newFace, newAddr) = m_nnst->ClosestSectorFaceInfo (m_dst, 0);
		    }

		  if (newFace)
		    {
		      NS_LOG_DEBUG (log_str << " Face: " << face_id << " PoA: " << internal <<
				    " changed to Face: " << newFace->GetId () << " PoA: " << newAddr);

		      txPreparation(t_pdu, newFace, newAddr, false);
		      total_count++;
		      ++pdu_it;
		      continue;
		    }
		  else
		    {
		      NS_LOG_WARN ("Didn't find viable Face for migration of " << log_str);
		      no_face++;
		      pdu_it = rx_pdu.erase (pdu_it);
		      continue;
		    }
		}

	      // Have no 3N DST, check PoA information
	      if (interfaces.empty ())
		{
		  NS_LOG_WARN ("There are no known nodes connected to this node. Cancelling migration Face: " << data.m_face_id <<
			       " Flowid: " << flowid << " Sequence: " << curr_sequence
		  );
		  m_node_rxbuffer_container->RemovePDUExactFlowidSequence (outFace, internal, flowid, curr_sequence);
		  no_interface++;
		  pdu_it = rx_pdu.erase (pdu_it);
		  continue;
		}
	      else
		{
		  if (internal == outFace->GetBroadcastAddress())
		    {
		      NS_LOG_DEBUG (log_str << " utilizing broadcast PoA, no change to Face: " <<
				    face_id << " PoA: " << internal);
		      txPreparation(t_pdu, outFace, outFace->GetBroadcastAddress(), false);
		      total_count++;
		      no_modification++;
		      ++pdu_it;
		      continue;
		    }
		  else
		    {
		      if (interfaces.find (internal) != interfaces.end ())
			{
			  NS_LOG_DEBUG (log_str << " utilizing found PoA, no change to Face: " <<
					face_id << " PoA: " << internal);
			  txPreparation(t_pdu, outFace, internal, false);
			  total_count++;
			  no_modification++;
			  ++pdu_it;
			  continue;
			}
		      else
			{
			  NS_LOG_DEBUG (log_str << " didn't find Face: " << face_id <<  " PoA: " << internal <<
					" utilizing Face: " << face_id << " PoA: " << outFace->GetBroadcastAddress());
			  NS_LOG_DEBUG (log_str);
			  txPreparation(t_pdu, outFace, outFace->GetBroadcastAddress(), false);
			  total_count++;
			  poa_migration++;
			  ++pdu_it;
			  continue;
			}
		    }
		}

	      log_str += " found no way to transmit";
	      NS_LOG_WARN (log_str);
	      m_node_rxbuffer_container->RemovePDUExactFlowidSequence (outFace, internal, flowid, curr_sequence);
	      hopeless_migration++;
	      pdu_it = rx_pdu.erase (pdu_it);
	    }

	  NS_LOG_DEBUG ("Migration modifications -> Disenrollment: " << disenrollment <<
			" Source: " << src_migration << " Destination: " << dst_migration <<
			" PoA: " << poa_migration << " No modifications: " << no_modification);
	  NS_LOG_DEBUG ("Migration transmission -> Total transmissions: " << total_count <<
			" No Face: " << no_face << " Hopeless: " << hopeless_migration);
	}
      else
	{
	  NS_LOG_DEBUG ("No PDUs found in buffer for Flowid: " << data.m_flowid << " lower than Sequence: "
			<< data.m_sequence + 1 << " stopping migration");
	}
    }

    void
    ForwardingStrategy::TransmitNext3NWindow (Ptr<Face> face, Address dst, uint32_t flowid, uint32_t sequence)
    {
      NS_LOG_FUNCTION (face->GetId () << dst << flowid << sequence);

      VerifyAndMigrateBroadcast (flowid, face, dst);

      if (!VerifyFlowid(face->GetId (), face->GetAddressId (dst), flowid))
	{
	  NS_LOG_WARN ("No information for Flowid: " << flowid << " Face: " << face->GetId () << "PoA: " << dst << " PoA ID: " << face->GetAddressId (dst) << ", ignoring");
	  return;
	}

      uint32_t window = ObtainFlowidWindowSent(face->GetId (), face->GetAddressId (dst), flowid);

      NS_LOG_DEBUG ("Transmitting all for Face " << face->GetId () << " PoA: " << dst << " Flowid: " << flowid << " with Sequence greater than " << sequence);
      std::list<Ptr<DATAPDU> > rx_pdu = m_node_rxbuffer_container->RetrieveDATAPDUByFlowidGreaterThanSeq (face, dst, flowid, sequence, window);

      std::list<Ptr<DATAPDU> >::iterator pdu_it = rx_pdu.begin ();

      if (rx_pdu.size() > 0)
	{
	  NS_LOG_DEBUG ("Pushing " << rx_pdu.size () << " PDUs for Face: " << face->GetId () << " PoA: " << dst << " Flowid: " << flowid << " with Sequence larger than " << sequence);
	}
      else
	{
	  NS_LOG_DEBUG ("No PDUs stopped waiting for ACKP");
	  return;
	}

      uint32_t count = 1;

      // Reset the retransmission attempts
      ResetReTxNumSenderInfo(flowid, face->GetId (), face->GetAddressId(dst));

      while (pdu_it != rx_pdu.end ())
	{
	  NS_LOG_DEBUG ("Forwarding held back Flowid: " << flowid << " Sequence: " << (*pdu_it)->GetSequence() << " - " << count << " of " << rx_pdu.size ());

	  txPreparation (*pdu_it, face, dst, true);
	  count++;
	  ++pdu_it;
	}

      NS_LOG_DEBUG ("Finalized transmitting next window of Face: " << face->GetId () << "PoA: " << dst <<
		    " PoA ID: " <<face->GetAddressId (dst) << " Flowid: " << flowid);
    }

    int8_t
    ForwardingStrategy::CheckSendDATAPDU (RxPDU data, Ptr<DATAPDU> pdu, bool retx)
    {
      //      NS_LOG_FUNCTION(data.m_flowid << data.m_sequence << data.m_face_id << data.m_dst_id << pdu << std::boolalpha << rt);

      std::string rtstr;
      if (retx)
	rtstr = "ReTx ";
      else
	rtstr = "";

      // We must check if we have received the previous ACK
      if (IsWaitingForPrevWindowACK(data))
	{
	  NS_LOG_DEBUG (rtstr << "Waiting for ACK, buffering PDU for Face: " << data.m_face_id <<
			" PoA ID: " << data.m_dst_id << " Flowid: " << data.m_flowid <<
			" Sequence: " << data.m_sequence);
	  return 1;
	}

      // We are not waiting for an ACK, check if we have to set a retransmission
      Flowid_Sender tmp = ObtainSentFlowidInfo (data.m_face_id, data.m_dst_id, data.m_flowid);

      uint32_t initial_seq = tmp.m_tx_initial_sequence;
      uint32_t curr_window = tmp.m_tx_window;
      uint32_t curr_rate = tmp.m_tx_rate;
      uint32_t window_seq = initial_seq + curr_window -1;
      uint32_t retx_times = tmp.m_tx_window_retx;
      uint32_t threshold = 1;
      if (window_seq > 4)
	threshold = window_seq / 4;

      if (!(initial_seq <= data.m_sequence && data.m_sequence < (window_seq + threshold)))
	{
	  NS_LOG_WARN (rtstr << "Buffering PDU for Face: " << data.m_face_id <<
		       " PoA ID: " << data.m_dst_id << " Flowid: " << data.m_flowid <<
		       " Sequence: " << data.m_sequence << " due to sequence not being " <<
		       initial_seq << " <= x < " << (window_seq + threshold));
	  return 2;
	}

      bool insert = false;

      if (CanWaitForACKP(data))
	{
	  Time now = Simulator::Now ();
	  // Set up a timer to wait for the ACK
	  // Reintegrate the exponential backoff
	  uint32_t num = backoffRandom->GetInteger(0, ((2 << retx_times)-1));
	  Time interval = ObtainRetransmissionInterval(data);
	  Ptr<Face> outFace = m_faces->Get(data.m_face_id);
	  Time sch;
	  if (outFace->IsWiFi ())
	    {
	      sch = 2*m_3n_MPL+(num*m_3n_MPL);
	    }
	  else
	    {
	      sch = 2*m_3n_MPL+(num*m_3n_MPL);
	    }
	  EventId evt = Simulator::Schedule(sch, &ForwardingStrategy::Retransmit3NWindow, this, data);

	  // Introduce timer
	  Face_Flowid_Event tmp_flowid_evt;
	  tmp_flowid_evt.m_face = data.m_face_id;
	  tmp_flowid_evt.m_dst_id = data.m_dst_id;
	  tmp_flowid_evt.m_flowid = data.m_flowid;
	  tmp_flowid_evt.m_sequence = window_seq;
	  tmp_flowid_evt.m_ini_sequence = initial_seq;
	  tmp_flowid_evt.m_window = curr_window;
	  tmp_flowid_evt.m_rate = curr_rate;
	  tmp_flowid_evt.m_evt = evt;
	  tmp_flowid_evt.m_next_retransmit = now + sch;

	  FaceEventContainer::iterator it;

	  std::tie (it, insert) = m_flowid_event_keeper.insert(tmp_flowid_evt);

	  if (insert)
	    {
	      NS_LOG_DEBUG (rtstr << "Setting up ACK timer for Flowid: " << data.m_flowid << " origin Sequence: " << initial_seq <<
			    " Window: " << curr_window << " Max Sequence: " << window_seq <<
			    " Face: " << data.m_face_id << " PoA ID: " << data.m_dst_id << " retransmit at " <<
			    (now+ sch).GetSeconds() << " using backoff: " << num << " due to retransmission: " << retx_times <<
			    " and interval: " << interval.GetSeconds());
	      NS_LOG_DEBUG ("Transmitted Face: " << data.m_face_id << " PoA: " << data.m_dst_id << " Flowid: " << data.m_flowid <<
			    " Sequence: " << initial_seq << " <--> " << (initial_seq + curr_window));
	      ClearTxSequences (data);
	    }
	  else
	    {
	      NS_LOG_WARN (rtstr << "Error setting up ACK timer for Flowid: " << data.m_flowid << " origin Sequence: " << initial_seq <<
			   " Window: " << curr_window << " Max Sequence: " << window_seq <<
			   " Face: " << data.m_face_id << " PoA ID: " << data.m_dst_id << " retransmit at " <<
			   (now+ sch).GetSeconds());
	    }
	}
      else
	{
	  NS_LOG_DEBUG (rtstr << "No action taken for ACK timer for Flowid: " << data.m_flowid <<
			" Sequence: " << data.m_sequence << " with initial seq: " << initial_seq <<
			" Window: " << curr_window << " Max seq: " << window_seq);
	}

      // Increase the number of PDUs sent out this Face
      m_face_pdu_counter[data.m_face_id] += 1;

      // Finally send out the current PDU
      int8_t ret = SendDATAPDU(data, pdu);

      if (ret == 0)
	{
	  if (insert)
	    {
	      UpdatePDUBeforeACKP (data);
	    }
	}

      return ret;
    }

    void
    ForwardingStrategy::EliminateSentFlowid (uint32_t face, uint16_t dst,
					     uint32_t flowid)
    {
      NS_LOG_FUNCTION(face << dst << flowid);

      NS_ASSERT (flowid != 0);

      FlowidSentContainer_uniq& face_index = m_flowids_forwarded.get<i_fid_tx> ();
      FlowidSentContainer_uniq::iterator itface = face_index.find (boost::make_tuple(flowid, face, dst));

      if (itface != face_index.end ())
	{
	  // Cancel events just in case
	  Flowid_Sender tmp = *itface;
	  Time last_ack = tmp.m_tx_last_ackp;
	  /*
	  tmp.m_force_retransmit.Cancel ();
	   */
	  tmp.m_flowid_erasure.Cancel();

	  m_flowids_forwarded.erase (itface);

	  CancelAndEliminateWindowWait(face, dst, flowid);

	  // Flush Buffer
	  uint32_t ret = m_node_rxbuffer_container->RemoveFlowid(m_faces->Get(face), m_faces->Get(face)->GetIdAddress(dst), flowid);

	  NS_LOG_DEBUG ("Eliminating sending Flowid: " << flowid <<
			" Face: " << face << " PoA: " << dst <<
			" Last ACKP: " << last_ack.GetSeconds () <<
			" PDUs eliminated: " << ret);

	  m_FlowIdElimination(flowid);
	}
      else
	{
	  NS_LOG_WARN ("Attempted to erase undefined Face: " << face << " PoA: " << dst << " Flowid: " << flowid);
	}
    }

    void
    ForwardingStrategy::EliminateReceivedFlowid (uint32_t face, uint16_t dst,
						 uint32_t flowid)
    {
      NS_LOG_FUNCTION(face << dst << flowid);

      NS_ASSERT (flowid != 0);

      FlowidReceivedContainer_uniq& face_index = m_flowids_accepted.get<i_fid_rx> ();
      FlowidReceivedContainer_uniq::iterator itface = face_index.find (boost::make_tuple (flowid, face, dst));

      Ptr<Face> outFace = m_faces->Get (face);
      Address internal = outFace->GetIdAddress (dst);

      if (itface != face_index.end ())
	{
	  Flowid_Receiver tmp = *itface;
	  NS_LOG_DEBUG ("Eliminating receiving Flowid: " << flowid << " Face: " << face << " PoA: " << internal << " Window: " << tmp.m_rx_window << " Rate " << tmp.m_rx_rate << " Last PDU seen at: " << tmp.m_rx_last_pdu.GetSeconds ());
	  tmp.m_flowid_erasure.Cancel ();
	  tmp.m_force_ack.Cancel ();

	  m_flowids_accepted.erase (itface);
	  m_FlowIdElimination(flowid);
	}
      else
	{
	  NS_LOG_WARN ("Attempted to erase undefined Flowid: " << flowid << " Face: " << face << " PoA: " << internal);
	}
    }

    void
    ForwardingStrategy::PrintPDUSets (uint32_t flowid, std::set<uint32_t> t, uint32_t start, uint32_t end, std::string msg, bool error)
    {
      if (!t.empty())
	{
	  std::set<uint32_t>::iterator it = t.begin ();

	  std::string dbseq = "";

	  while (it != t.end ())
	    {
	      dbseq += " " + std::to_string(*it);
	      ++it;
	    }

	  if (error)
	    NS_LOG_ERROR (msg << " Flowid: " << flowid << " Sequences seen between " << start << " and " << end << ":" << dbseq);
	  else
	    NS_LOG_DEBUG (msg << " Flowid: " << flowid << " Sequences seen between " << start << " and " << end << ":" << dbseq);
	}
      else
	{
	  if (error)
	    NS_LOG_ERROR (msg << " Flowid: " << flowid << " has no seen Sequences");
	  else
	    NS_LOG_DEBUG (msg << " Flowid: " << flowid << " has no seen Sequences");
	}
    }

    bool
    ForwardingStrategy::  EnqueueNULLp (Ptr<Face> face, Address dst, Ptr<NULLp> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing NULLp out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushNULLp(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::   EnqueueSO (Ptr<Face> face, Address dst, Ptr<SO> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing SO out Face: " << faceid << " Dst: " << dst << " from: " << pdu->GetSrcName());
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushSO(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy:: EnqueueDO (Ptr<Face> face, Address dst, Ptr<DO> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing DO out Face: " << faceid << " Dst: " << dst << " to: " << pdu->GetDstName());
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushDO(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueDU (Ptr<Face> face, Address dst, Ptr<DU> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing DU out Face: " << faceid << " Dst: " << dst << " from: " << pdu->GetSrcName() << " to: " << pdu->GetDstName());
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushDU(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueEN (Ptr<Face> face, Address dst, Ptr<EN> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing EN out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushEN(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueAEN (Ptr<Face> face, Address dst,Ptr<AEN> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing AEN out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushAEN(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueOEN (Ptr<Face> face, Address dst,Ptr<OEN> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing OEN out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushOEN(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueREN (Ptr<Face> face, Address dst,Ptr<REN> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing REN out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushREN(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueDEN (Ptr<Face> face, Address dst,Ptr<DEN> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing DEN out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushDEN(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueADEN (Ptr<Face> face, Address dst,Ptr<ADEN> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing ADEN out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushADEN(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueINF (Ptr<Face> face, Address dst,Ptr<INF> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing INF out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushINF(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueRHR (Ptr<Face> face, Address dst,Ptr<RHR> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing RHR out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushRHR(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueOHR (Ptr<Face> face, Address dst, Ptr<OHR> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing OHR out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushOHR(pdu, Simulator::Now());
      return true;
    }

    bool
    ForwardingStrategy::EnqueueACKP (Ptr<Face> face, Address dst, Ptr<ACKP> pdu)
    {
      uint32_t faceid = face->GetId ();
      NS_LOG_DEBUG ("Enqueuing ACKP out Face: " << faceid << " Dst: " << dst);
      m_face_3n_address_queue[faceid].push(dst);
      m_face_3n_pdu_queue[faceid]->pushACKP(pdu, Simulator::Now());
      return true;
    }
  } // namespace nnn
} // namespace ns3
