/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnnsim-ackp.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnnsim-ackp.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnnsim-ackp.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnnsim-ackp.h"

namespace ns3
{
  namespace nnn
  {
    NS_LOG_COMPONENT_DEFINE ("nnn.wire.nnnSIM.ACKP");

    namespace wire
    {
      namespace nnnSIM
      {
	NS_OBJECT_ENSURE_REGISTERED (ACKP);

	ACKP::ACKP ()
	: CommonHeader<nnn::ACKP> ()
	{
	}

	ACKP::ACKP(Ptr<nnn::ACKP> ack_p)
	: CommonHeader<nnn::ACKP> (ack_p)
	{
	}

	TypeId
	ACKP::GetTypeId (void)
	{
	  static TypeId tid = TypeId ("ns3::nnn::ACKP::nnnSIM")
		      .SetGroupName ("Nnn")
		      .SetParent<Header> ()
		      .AddConstructor<ACKP> ()
		      ;
	  return tid;
	}

	TypeId
	ACKP::GetInstanceTypeId (void) const
	{
	  return GetTypeId ();
	}

	Ptr<Packet>
	ACKP::ToWire (Ptr<const nnn::ACKP> ack_p)
	{
	  Ptr<const Packet> p = ack_p->GetWire ();
	  if (!p)
	    {
	      Ptr<Packet> packet = Create<Packet> ();
	      ACKP wireEncoding (ConstCast<nnn::ACKP> (ack_p));
	      packet->AddHeader (wireEncoding);
	      ack_p->SetWire (packet);

	      p = packet;
	    }
	  return p->Copy ();
	}

	Ptr<nnn::ACKP>
	ACKP::FromWire (Ptr<Packet> packet)
	{
	  Ptr<nnn::ACKP> ack_p = Create<nnn::ACKP> ();
	  Ptr<Packet> wire = packet->Copy ();

	  ACKP wireEncoding (ack_p);
	  packet->RemoveHeader (wireEncoding);

	  ack_p->SetWire (wire);

	  return ack_p;
	}

	uint32_t
	ACKP::GetSerializedSize (void) const
	{
	  size_t size = CommonGetSerializedSize()                   /* Common header */
	      ;
	  size += 1;                                                /* ACKP type */
	  size_t flowids = m_ptr->GetACKNumFlowids ();
	  size += 4 + (flowids)*(4*2);                              /* Sequence numbers for nonces */

	  return size;
	}

	void
	ACKP::Serialize (Buffer::Iterator start) const
	{
	  // Serialize the header
	  CommonSerialize(start);

	  // Remember that CommonSerialize doesn't write the Packet length
	  // Move the iterator forward
	  start.Next(CommonGetSerializedSize() -2);

	  NS_LOG_INFO ("Serialize -> PktID = " << m_ptr->GetPacketId());
	  NS_LOG_INFO ("Serialize -> TTL = " << Seconds(static_cast<uint16_t> (m_ptr->GetLifetime ().ToInteger (Time::S))));
	  NS_LOG_INFO ("Serialize -> Version = " << m_ptr->GetVersion ());
	  NS_LOG_INFO ("Serialize -> Flowid = " << m_ptr->GetFlowid ());
	  NS_LOG_INFO ("Serialize -> Sequence = " << m_ptr->GetSequence ());
	  NS_LOG_INFO ("Serialize -> Pkt Len = " << GetSerializedSize());

	  // Serialize the packet size
	  start.WriteU16(GetSerializedSize());

	  NS_LOG_INFO ("Serialize -> Type: " << m_ptr->GetType ());
	  start.WriteU8(m_ptr->GetType ());
	  size_t flowids = m_ptr->GetACKNumFlowids ();
	  NS_LOG_INFO ("Serialize -> Flowids = " << flowids);
	  start.WriteU32 (flowids);

	  std::map<uint32_t, uint32_t> flowid_map = m_ptr->GetACKFlowids();
	  std::map<uint32_t, uint32_t>::iterator it = flowid_map.begin ();

	  while (it != flowid_map.end ())
	    {
	      start.WriteU32 (it->first);
	      start.WriteU32 (it->second);
	      ++it;
	    }

	  NS_LOG_INFO("Finished serialization");
	}

	uint32_t
	ACKP::Deserialize (Buffer::Iterator start)
	{
	  Buffer::Iterator i = start;

	  // Deserialize the header
	  uint32_t skip = CommonDeserialize (i);

	  NS_LOG_INFO ("Deserialize -> PktID = " << m_ptr->GetPacketId());
	  NS_LOG_INFO ("Deserialize -> TTL = " << Seconds(static_cast<uint16_t> (m_ptr->GetLifetime ().ToInteger (Time::S))));
	  NS_LOG_INFO ("Deserialize -> Version = " << m_ptr->GetVersion ());
	  NS_LOG_INFO ("Deserialize -> Flowid = " << m_ptr->GetFlowid ());
	  NS_LOG_INFO ("Deserialize -> Sequence = " << m_ptr->GetSequence ());
	  NS_LOG_INFO ("Deserialize -> Pkt len = " << m_packet_len);

	  // Check packet ID
	  if (m_ptr->GetPacketId() != nnn::ACKP_NNN)
	    throw new ACKPException ();

	  // Move the iterator forward
	  i.Next(skip);

	  uint8_t type = i.ReadU8 ();
	  NS_LOG_INFO ("Deserialize -> Type: " << type);
	  m_ptr->SetType(type);
	  size_t flowids = i.ReadU32 ();

	  size_t tmp;
	  uint32_t non;
	  uint32_t seq;
	  for (tmp = 0; tmp < flowids; tmp++)
	    {
	      non = i.ReadU32 ();
	      seq = i.ReadU32 ();
	      m_ptr->AddACKFlowidSeq(non, seq);
	    }

	  NS_ASSERT (GetSerializedSize () == (i.GetDistanceFrom (start)));

	  return i.GetDistanceFrom (start);
	}
      } /* namespace nnnSIM */
    } /* namespace wire */
  } /* namespace nnn */
} /* namespace ns3 */
