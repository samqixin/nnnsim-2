/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pit-entry-outgoing-face.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pit-entry-outgoing-face.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pit-entry-outgoing-face.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-pit-entry-outgoing-face.h"

#include "ns3/simulator.h"

namespace ns3
{
  namespace nnn
  {
    namespace pit
    {
      OutgoingFace::OutgoingFace (Ptr<Face> face)
      : m_face (face)
      , m_sendTime (Simulator::Now ())
      , m_retxCount (0)
      , m_waitingInVain (false)
      {
      }

      OutgoingFace::OutgoingFace (Ptr<Face> face, Address addr, bool raw_icn)
      : m_face (face)
      , m_sendTime (Simulator::Now ())
      , m_retxCount (0)
      , m_waitingInVain (false)
      {
	AddPoADestination(addr);
	if (raw_icn)
	  EnableRawICNonPoA(addr);

      }

      OutgoingFace::OutgoingFace ()
      : m_face (0)
      , m_sendTime (0)
      , m_retxCount (0)
      , m_waitingInVain (false)
      {
      }

      void
      OutgoingFace::AddPoADestination (Address addr)
      {
	m_poa_addrs[addr] = PoAOutInfo ();
      }

      void
      OutgoingFace::RemovePoADestination (Address addr)
      {
	m_poa_addrs.erase(addr);
      }

      std::map<Address,PoAOutInfo>
      OutgoingFace::GetPoADestinations () const
      {
	return m_poa_addrs;
      }

      void
      OutgoingFace::EnableRawICNonPoA (Address addr)
      {
	std::map<Address,PoAOutInfo>::iterator it = m_poa_addrs.find (addr);

	if (it != m_poa_addrs.end ())
	  {
	    (it->second).m_raw_icn = true;
	  }
      }

      bool
      OutgoingFace::SawRawICNonPoA (Address addr)
      {
	std::map<Address,PoAOutInfo>::iterator it = m_poa_addrs.find (addr);

	if (it != m_poa_addrs.end ())
	  {
	    return (it->second).m_raw_icn;
	  }
	else
	  return false;
      }

      OutgoingFace &
      OutgoingFace::operator = (const OutgoingFace &other)
      {
	m_face = other.m_face;
	m_poa_addrs = other.m_poa_addrs;
	m_sendTime = other.m_sendTime;
	m_retxCount = other.m_retxCount;
	m_waitingInVain = other.m_waitingInVain;

	return *this;
      }

      void
      OutgoingFace::UpdateOnRetransmit ()
      {
	m_sendTime = Simulator::Now ();
	m_retxCount++;
	m_waitingInVain = false;
      }

      void
      OutgoingFace::UpdateOnRetransmit (Address addr, bool raw_icn)
      {
	std::map<Address,PoAOutInfo>::iterator it = m_poa_addrs.find (addr);

	if (it != m_poa_addrs.end ())
	  {
	    (it->second).m_sendTime = Simulator::Now ();
	    (it->second).m_retxCount++;
	    (it->second).m_waitingInVain = false;
	    (it->second).m_raw_icn = raw_icn;
	  }
	else
	  {
	    PoAOutInfo tmp = PoAOutInfo ();
	    tmp.m_sendTime = Simulator::Now ();
	    tmp.m_retxCount++;
	    tmp.m_waitingInVain = false;
	    tmp.m_raw_icn = raw_icn;
	    m_poa_addrs[addr] = tmp;
	  }
      }

      void
      OutgoingFace::Add3NDestination (Ptr<const NNNAddress> addr)
      {
	m_dst_addrs.insert(NamesSeen(addr, Simulator::Now(), Simulator::Now()));
      }

      void
      OutgoingFace::Remove3NDestination (Ptr<const NNNAddress> addr)
      {
	names_seen_by_name& names_index = m_dst_addrs.get<i_name> ();
	names_seen_by_name::iterator it = names_index.find (addr);

	if (it != names_index.end ())
	  {
	    names_index.erase (it);
	  }
      }

      bool
      OutgoingFace::Has3NDestination (Ptr<const NNNAddress> addr)
      {
	names_seen_by_name& names_index = m_dst_addrs.get<i_name> ();
	names_seen_by_name::iterator it = names_index.find (addr);

	if (it != names_index.end ())
	  {
	    return true;
	  }
	else
	  {
	    return false;
	  }
      }

      bool
      OutgoingFace::NoAddresses ()
      {
	return (m_dst_addrs.size() == 0);
      }
    } // namespace pit
  } // namespace nnn
} // namespace ns3
