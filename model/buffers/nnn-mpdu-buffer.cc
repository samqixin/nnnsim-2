/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/*
 * Copyright (c) 2015 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-mpdu-buffer.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-mpdu-buffer.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-mpdu-buffer.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#include <algorithm>

#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/core.hpp>
#include <boost/ref.hpp>

#include "ns3/log.h"

namespace ll = boost::lambda;

#include "nnn-mpdu-buffer.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.MPDUBuffer");

  namespace nnn
  {

    TypeId
    MPDUBuffer::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::MPDUBuffer")
            	    .SetParent<Object> ()
		    .AddConstructor<MPDUBuffer> ()
		    ;
      return tid;
    }

    MPDUBuffer::MPDUBuffer ()
    : m_retx (MilliSeconds (50))
    {
    }

    MPDUBuffer::MPDUBuffer (Time retx)
    : m_retx (retx)
    {
    }

    MPDUBuffer::~MPDUBuffer ()
    {
    }

    void
    MPDUBuffer::AddDestination (const NNNAddress &addr, Time lease_expire)
    {
      NS_LOG_FUNCTION(this << addr);

      NS_LOG_INFO("Inserting (" << addr << ") will expire at " << lease_expire);

      Time now = Simulator::Now ();
      Time relativeExpireTime = lease_expire - now;

      if (relativeExpireTime.IsStrictlyPositive ())
	{
	  std::pair< super::iterator, bool> result = super::insert(addr, 0);

	  if (result.first != super::end ())
	    {
	      if (result.second)
		{
		  NS_LOG_INFO("New buffer for : (" << addr << ")");

		  result.first->set_payload(Create<PDUQueue> (lease_expire));

		  Simulator::Schedule(relativeExpireTime, &MPDUBuffer::CleanExpired, this, addr);
		}
	    }
	}
    }

    void
    MPDUBuffer::AddDestination (Ptr<NNNAddress> addr, Time lease_expire)
    {
      AddDestination (*addr, lease_expire);
    }

    void
    MPDUBuffer::RemoveDestination (const NNNAddress &addr)
    {
      NS_LOG_FUNCTION(this << addr);

      NS_LOG_INFO("Deleting (" << addr << ")");

      super::iterator item = super::find_exact(addr);

      if (item != super::end ())
	{
	  super::erase(item);
	}
    }

    void
    MPDUBuffer::RemoveDestination (Ptr<NNNAddress> addr)
    {
      RemoveDestination (*addr);
    }

    bool
    MPDUBuffer::DestinationExists (const NNNAddress &addr)
    {
      NS_LOG_FUNCTION(this << addr);
      super::iterator item = super::find_exact(addr);

      return (item != super::end());
    }

    bool
    MPDUBuffer::DestinationExists (Ptr<NNNAddress> addr)
    {
      return DestinationExists (*addr);
    }

    void
    MPDUBuffer::PushDEN (const NNNAddress &addr, Ptr<const DEN> den_p)
    {
      NS_LOG_FUNCTION(this << addr);

      NS_LOG_INFO ("DEN Looking for (" << addr << ")");

      super::iterator item = super::find_exact(addr);

      if (item != super::end ())
	{
	  NS_LOG_INFO("DEN, found (" << addr << ") inserting");
	  Ptr<PDUQueue> tmp = item->payload();

	  tmp->pushDEN(den_p, m_retx);
	}
    }

    void
    MPDUBuffer::PushDEN (Ptr<NNNAddress> addr, Ptr<const DEN> den_p)
    {
      PushDEN (*addr, den_p);
    }

    std::queue<Ptr<Packet> >
    MPDUBuffer::PopQueue (const NNNAddress &addr)
    {
      NS_LOG_FUNCTION(this << addr);

      NS_LOG_INFO ("Looking for (" << addr << ")");

      super::iterator item = super::find_exact(addr);

      std::queue<Ptr<Packet> > pdu_queue;

      if (item != super::end ())
	{
	  NS_LOG_INFO("Found 3N name (" << addr << ")");
	  if (item->payload() == 0)
	    {
	      NS_LOG_INFO("No info found");
	    }
	  else
	    {
	      NS_LOG_INFO("Found info, obtaining queue");
	      std::queue<std::pair<Time, Ptr<Packet> > > queue_with_time = item->payload()->popQueue();

	      Time now = Simulator::Now ();

	      // Go through the queue and get those PDUs that haven't yet hit the retransmission time
	      while (!queue_with_time.empty ())
		{
		  // Get the PDU at the front of the queue
		  std::pair<Time,Ptr<Packet> > queue_pair = queue_with_time.front ();

		  Time expiry = queue_pair.first;

		  pdu_queue.push (queue_pair.second);

		  // Pop the queue and continue
		  queue_with_time.pop ();
		}
	    }
	}

      NS_LOG_DEBUG ("Buffer for (" << addr << ") being returned");

      return pdu_queue;
    }

    std::queue<Ptr<Packet> >
    MPDUBuffer::PopQueue (Ptr<NNNAddress> addr)
    {
      return PopQueue (*addr);
    }

    uint
    MPDUBuffer::QueueSize (const NNNAddress &addr)
    {
      NS_LOG_FUNCTION(this << addr);

      NS_LOG_INFO ("Looking for " << addr);

      super::iterator item = super::find_exact(addr);

      if (item == super::end ())
	return 0;
      else
	{
	  NS_LOG_INFO("Found 3N name " << addr);
	  if (item->payload() == 0)
	    {
	      NS_LOG_INFO("No info found");
	      return 0;
	    }
	  else
	    {
	      NS_LOG_INFO("Found info, obtaining queue");
	      return item->payload()->size ();
	    }
	}
    }

    uint
    MPDUBuffer::QueueSize (Ptr<NNNAddress> addr)
    {
      return QueueSize (*addr);
    }

    void
    MPDUBuffer::SetReTX (Time rtx)
    {
      m_retx = rtx;
    }

    Time
    MPDUBuffer::GetReTX () const
    {
      return m_retx;
    }

    void
    MPDUBuffer::UpdateLeaseTime (const NNNAddress &addr, Time lease_expire)
    {
      NS_LOG_FUNCTION(this << addr << lease_expire);

      Time now = Simulator::Now ();
      Time relativeExpireTime = lease_expire - now;

      NS_LOG_INFO ("Checking remaining lease time " << relativeExpireTime << " for (" << addr << ") at " << now);

      // If the relative expire time is above 0, we can save it
      if (relativeExpireTime.IsStrictlyPositive())
	{
	  super::iterator item = super::find_exact (addr);

	  if (item != super::end ())
	    {
	      bool ok = super::modify (&(*item), ll::bind (&PDUQueue::SetLeaseExpire, ll::_1, lease_expire));

	      if (ok)
		{
		  Simulator::Schedule(relativeExpireTime, &MPDUBuffer::CleanExpired, this, addr);
		}
	    }
	}
    }

    void
    MPDUBuffer::UpdateLeaseTime (Ptr<NNNAddress> addr, Time lease_expire)
    {
      UpdateLeaseTime (*addr, lease_expire);
    }

    void
    MPDUBuffer::CleanExpired (const NNNAddress &addr)
    {
      NS_LOG_FUNCTION(this << addr);

      Time now = Simulator::Now ();
      super::iterator item = super::find_exact (addr);

      if (item != super::end ())
	{
	  Time expiry = item->payload()->GetLeaseExpire ();

	  if (now >= expiry)
	    {
	      RemoveDestination(addr);
	    }
	}
    }
  } /* namespace nnn */
} /* namespace ns3 */
