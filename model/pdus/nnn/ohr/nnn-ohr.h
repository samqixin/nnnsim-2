/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-orhr.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-orhr.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-orhr.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_ORHR_H_
#define NNN_ORHR_H_

#include "ns3/nnn-rhr-pdus.h"

namespace ns3
{
  namespace nnn
  {
    class OHR : public RHRPDU
    {
    public:
      OHR ();

      OHR (const OHR &ohr_p);

      virtual
      ~OHR ();

      bool
      HasDefiniteQueryInfo () const;

      void
      SetDefiniteQueryInfo (bool enable);

      uint32_t
      GetDestDistance () const;

      void
      SetDestDistance (uint32_t dist);

      bool
      IsQueryFixed () const;

      void
      SetQueryFixed (bool enable);

      Time
      GetQueryLease () const;

      void
      SetQueryLease (Time lease);

      uint32_t
      GetQueryDistance () const;

      void
      SetQueryDistance (uint32_t dist);

      void
      Print (std::ostream &os) const;

    private:
      // NO_ASSIGN
      OHR &
      operator = (const OHR &other) { return *this; }

      uint32_t m_dst_distance;              // The distance from the destination to the current node sending the OHR
      bool m_definite_query_info;           // Definite query info
      bool m_isQueryFixed;                  // Is the queried 3N name fixed
      Time m_query_lease;                   // The lease on the query
      uint32_t m_query_distance;            // The distance from the destination to the queried distance
    };

    inline std::ostream &
    operator << (std::ostream &os, const OHR &i)
    {
      i.Print (os);
      return os;
    }
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_ORHR_H_ */
