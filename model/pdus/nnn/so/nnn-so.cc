/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-so.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-so.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-so.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"

#include "nnn-so.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.SO");

  namespace nnn
  {
    SO::SO ()
    : NNNPDU (SO_NNN, Seconds (0))
    , DATAPDU ()
    {
    }

    SO::SO (Ptr<const NNNAddress> name, Ptr<Packet> payload)
    : NNNPDU (SO_NNN, Seconds (0))
    , DATAPDU ()
    {
      if (m_payload == 0)
	m_payload = Create<Packet> ();
      else
	m_payload = payload;

      SetSrcName (name);
    }

    SO::SO (const SO &so_p)
    {
      NS_LOG_FUNCTION("SO correct copy constructor");
      m_packetid = so_p.GetPacketId ();
      m_version = so_p.GetVersion ();
      m_ttl = so_p.GetLifetime ();
      m_PDUdatatype = so_p.GetPDUPayloadType ();
      if (so_p.GetPayload ())
	m_payload = so_p.GetPayload ()->Copy ();
      m_src_name = so_p.GetSrcNamePtr();
      m_pdurf = so_p.GetPDURF ();
      m_window = so_p.GetPDUWindow ();
      m_rate = so_p.GetRate ();
      m_ini_seq = so_p.GetInitialWindowSeq ();
      m_flowid = so_p.GetFlowid ();
      m_sequence = so_p.GetSequence ();
      if (so_p.GetWire ())
	m_wire = so_p.GetWire()->Copy();
    }

    SO::~SO ()
    {
    }

    const NNNAddress&
    SO::GetName () const
    {
      if (m_src_name == 0) throw SOException ();
      return *m_src_name;
    }

    Ptr<const NNNAddress>
    SO::GetNamePtr () const
    {
      return m_src_name;
    }

    void
    SO::SetName (Ptr<const NNNAddress> name)
    {
      m_src_name = name;
      m_wire = 0;
    }

    void
    SO::Print (std::ostream &os) const
    {
      os << "<SO>" << std::endl;
      NNNPDU::Print (os);
      os << "  <Name>" << GetName () << "</Name>" << std::endl;
      DATAPDU::Print (os);
      os << "</SO>" << std::endl;
    }

    bool
    SO::operator < (const Ptr<SO>& so_p) const
    {
      if (GetFlowid () == so_p->GetFlowid ())
	{
	  return false;
	}

      if (GetSequence () == so_p->GetSequence () )
	{
	  return (GetName () < so_p->GetName ());
	}
      else
	{
	  return (GetSequence () < so_p->GetSequence ());
	}
    }

    Ptr<DATAPDU>
    SO::Copy () const
    {
      return Ptr<DATAPDU> (new SO(*this), false);
    }
  } // namespace nnn
} // namespace ns3


