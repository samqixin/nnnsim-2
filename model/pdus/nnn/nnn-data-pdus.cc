/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-data-pdus.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-data-pdus.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-data-pdus.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/nnn-data-pdus.h"

namespace ns3
{
  namespace nnn
  {
    DATAPDU::DATAPDU ()
    : m_PDUdatatype (ICN_NNN)
    , m_src_name    (0)
    , m_dst_name    (0)
    , m_pdurf       (0)
    , m_window      (0)
    , m_rate        (0)
    , m_ini_seq     (0)
    {
    }

    DATAPDU::~DATAPDU ()
    {
      m_payload = 0;
      m_src_name = 0;
      m_dst_name = 0;
    }

    const NNNAddress&
    DATAPDU::GetSrcName () const
    {
      if (m_src_name == 0) throw DATAPDUException ();
      return *m_src_name;
    }

    const NNNAddress&
    DATAPDU::GetDstName () const
    {
      if (m_dst_name == 0) throw DATAPDUException ();
      return *m_dst_name;
    }

    Ptr<const NNNAddress>
    DATAPDU::GetSrcNamePtr () const
    {
      return m_src_name;
    }

    Ptr<const NNNAddress>
    DATAPDU::GetDstNamePtr () const
    {
      return m_dst_name;
    }

    void
    DATAPDU::SetSrcName (Ptr<const NNNAddress> src)
    {
      m_src_name = src;
      SetWire(0);
    }

    void
    DATAPDU::SetDstName (Ptr<const NNNAddress> dst)
    {
      m_dst_name = dst;
      SetWire(0);
    }

    uint16_t
    DATAPDU::GetPDUPayloadType() const
    {
      return m_PDUdatatype;
    }

    void
    DATAPDU::SetPDUPayloadType (uint16_t pdu_type)
    {
      m_PDUdatatype = pdu_type;
      SetWire(0);
    }

    void
    DATAPDU::SetPayload (Ptr<Packet> payload)
    {
      m_payload = payload;
      SetWire(0);
    }

    Ptr<const Packet>
    DATAPDU::GetPayload () const
    {
      return m_payload;
    }

    bool
    DATAPDU::has3NSrc () const
    {
      return (m_src_name);
    }

    bool
    DATAPDU::has3NDst () const
    {
      return (m_dst_name);
    }

    void
    DATAPDU::SetPDURF (bool value)
    {
      if (value)
	m_pdurf |= 1;
      else
	m_pdurf &= 0;
      SetWire(0);
    }

    void
    DATAPDU::SetPDURF (uint8_t value)
    {
      m_pdurf = value;
      SetWire(0);
    }

    uint8_t
    DATAPDU::GetPDURF () const
    {
      return m_pdurf;
    }

    bool
    DATAPDU::hasRF () const
    {
      return (m_pdurf && 1);
    }

    void
    DATAPDU::SetPDUWindow (uint32_t value)
    {
      m_window = value;
      SetWire(0);
    }

    uint32_t
    DATAPDU::GetPDUWindow () const
    {
      return m_window;
    }

    void
    DATAPDU::SetRate (uint32_t rate)
    {
      m_rate = rate;
      SetWire(0);
    }

    uint32_t
    DATAPDU::GetRate () const
    {
      return m_rate;
    }

    void
    DATAPDU::SetInitialWindowSeq (uint32_t seq)
    {
      m_ini_seq = seq;
      SetWire (0);
    }

    uint32_t
    DATAPDU::GetInitialWindowSeq () const
    {
      return m_ini_seq;
    }

    void
    DATAPDU::Print(std::ostream &os) const
    {
      os << "  <PDU Type>" << GetPDUPayloadType() << "</PDU Type>" << std::endl;
      if (m_payload != 0)
	os << "  <Payload>Yes</Payload>" << std::endl;
      else
	os << "  <Payload>No</Payload>" << std::endl;
    }

  } /* namespace nnn */
} /* namespace ns3 */


