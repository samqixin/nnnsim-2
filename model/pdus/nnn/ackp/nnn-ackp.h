/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-ackp.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-ackp.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-ackp.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_ACKP_NNN_ACKP_H_
#define NNN_ACKP_NNN_ACKP_H_

#include "ns3/nnn-pdu.h"
#include "ns3/nnn-data-pdus.h"
#include "ns3/nnn-naming.h"

namespace ns3
{
  namespace nnn
  {

    class ACKP :public NNNPDU
    {
    public:
      ACKP (
      );

      ACKP (
	  uint32_t packet_id,
	  uint32_t flowid,
	  uint32_t sequence
      );

      ACKP (
	  const ACKP &ack_p
      );

      virtual
      ~ACKP (
      );

      void
      SetOutofOrder (bool value);

      bool
      IsOutofOrder () const;

      uint8_t
      GetType () const;

      void
      SetType (uint8_t type);

      const std::map<uint32_t, uint32_t>
      GetACKFlowids () const;

      void
      SetACKFlowids (std::map<uint32_t, uint32_t> other);

      void
      AddACKFlowidSeq (uint32_t flowid, uint32_t sequence);

      size_t
      GetACKNumFlowids () const;

    private:
      // 0 - Normal
      // 1 - Out of Order
      uint8_t m_type;
      std::map<uint32_t, uint32_t> m_flowid_seq;
    };

    /**
     * @brief Class for ACKP parsing exception
     */
    class ACKPException {};

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_ACKP_NNN_ACKP_H_ */
