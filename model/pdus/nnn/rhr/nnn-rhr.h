/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rhr.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rhr.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rhr.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_RHR_H_
#define NNN_RHR_H_

#include "ns3/nnn-rhr-pdus.h"

namespace ns3
{
  namespace nnn
  {
    class RHR : public RHRPDU
    {
    public:
      RHR ();

      RHR (const RHR &rhr_p);

      virtual
      ~RHR ();

      void
      Print (std::ostream &os) const;

    private:
      // NO_ASSIGN
      RHR &
      operator = (const RHR &other) { return *this; }
    };

    inline std::ostream &
    operator << (std::ostream &os, const RHR &i)
    {
      i.Print (os);
      return os;
    }
  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_RHR_H_ */
