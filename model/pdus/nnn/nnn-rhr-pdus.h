/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-rhr-pdus.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-rhr-pdus.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-rhr-pdus.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_RHR_PDUS_H_
#define NNN_RHR_PDUS_H_

#include "nnn-pdu.h"
#include "ns3/nstime.h"
#include "ns3/nnn-naming.h"

namespace ns3
{
  namespace nnn
  {
    class RHRPDU : public virtual NNNPDU
    {
    public:
      RHRPDU ();
      virtual
      ~RHRPDU ();

      bool
      IsSrcFixed () const;

      void
      SetSrcFixed (bool fixed);

      bool
      HasSource () const;

      void
      SourceSet (bool enable);

      bool
      HasDestination () const;

      void
      DestinationSet (bool enable);

      const NNNAddress&
      GetSrcName () const;

      Ptr<const NNNAddress>
      GetSrcNamePtr () const;

      void
      SetSrcName (Ptr<const NNNAddress> name);

      void
      SetSrcName (const NNNAddress &name);

      const NNNAddress&
      GetDstName () const;

      Ptr<const NNNAddress>
      GetDstNamePtr () const;

      void
      SetDstName (Ptr<const NNNAddress> name);

      void
      SetDstName (const NNNAddress &name);

      const NNNAddress&
      GetQueryName () const;

      Ptr<const NNNAddress>
      GetQueryNamePtr () const;

      void
      SetQueryName (Ptr<const NNNAddress> name);

      void
      SetQueryName (const NNNAddress &name);

      Address
      GetPoa () const;

      void
      SetPoa (Address poa);

      Time
      GetSrcLease() const;

      void
      SetSrcLease (Time lease);

      virtual void
      Print (std::ostream &os) const;

    private:
      bool m_hasSource;                     // Whether this PDU has a Src name
      Ptr<const NNNAddress> m_src_name;     // 3N name of sender of this PDU
      Address m_src_poa;                    // PoA through which this PDU was sent
      bool m_isSrcFixed;                    // Signals whether m_src_name is fixed
      Time m_src_lease;                     // Lease time of the responding node (in absolute time)
      bool m_hasDestination;                // Whether the PDU has a specific destination
      Ptr<const NNNAddress> m_dst_name;     // 3N name of the destination
      Ptr<const NNNAddress> m_query_name;   // 3N name of query
    };

    class RHRException {};

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_RHR_PDUS_H_ */
