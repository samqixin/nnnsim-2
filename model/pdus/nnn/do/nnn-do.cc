/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-do.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-do.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-do.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"

#include "nnn-do.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.DO");

  namespace nnn
  {
    DO::DO ()
    : NNNPDU (DO_NNN, Seconds (0))
    , DATAPDU ()
    {
    }

    DO::DO (Ptr<const NNNAddress> name, Ptr<Packet> payload)
    : NNNPDU (DO_NNN, Seconds (0))
    , DATAPDU ()
    {
      if (m_payload == 0)
	m_payload = Create<Packet> ();
      else
	m_payload = payload;

      SetDstName (name);
    }

    DO::DO (const DO &do_p)
    {
      NS_LOG_FUNCTION("DO correct copy constructor");
      m_packetid = do_p.GetPacketId ();
      m_version = do_p.GetVersion ();
      m_ttl = do_p.GetLifetime ();
      m_PDUdatatype = do_p.GetPDUPayloadType ();
      if (do_p.GetPayload ())
	m_payload = do_p.GetPayload ()->Copy ();
      m_dst_name = do_p.GetDstNamePtr ();
      m_pdurf = do_p.GetPDURF ();
      m_window = do_p.GetPDUWindow ();
      m_rate = do_p.GetRate ();
      m_ini_seq = do_p.GetInitialWindowSeq ();
      m_flowid = do_p.GetFlowid ();
      m_sequence = do_p.GetSequence ();
      if (do_p.GetWire ())
	m_wire = do_p.GetWire()->Copy();
    }

    DO::~DO ()
    {
    }

    const NNNAddress&
    DO::GetName () const
    {
      if (m_dst_name == 0) throw DOException ();
      return *m_dst_name;
    }

    Ptr<const NNNAddress>
    DO::GetNamePtr () const
    {
      return m_dst_name;
    }

    void
    DO::SetName (Ptr<const NNNAddress> name)
    {
      m_dst_name = name;
      SetWire(0);
    }

    void
    DO::Print (std::ostream &os) const
    {
      os << "<DO>" << std::endl;
      NNNPDU::Print (os);
      os << "  <Name>" << GetName () << "</Name>" << std::endl;
      DATAPDU::Print (os);
      os << "</DO>" << std::endl;
    }

    bool
    DO::operator < (const Ptr<DO>& do_p) const
    {
      if (GetFlowid () == do_p->GetFlowid ())
	{
	  return false;
	}

      if (GetSequence () == do_p->GetSequence () )
	{
	  return (GetName () < do_p->GetName ());
	}
      else
	{
	  return (GetSequence () < do_p->GetSequence ());
	}
    }

    Ptr<DATAPDU>
    DO::Copy () const
    {
      return Ptr<DATAPDU> (new DO(*this), false);
    }

  } // namespace nnn
} // namespace ns3
