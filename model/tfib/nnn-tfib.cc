/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-tfib.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-tfib.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-tfib.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "nnn-tfib.h"

#include "ns3/names.h"
#include "ns3/node.h"


namespace ns3
{
  namespace nnn
  {
    TypeId
    TFib::GetTypeId (void)
    {
      static TypeId tid = TypeId ("ns3::nnn::TFib") // cheating ns3 object system
	.SetParent<Object> ()
	.SetGroupName ("Nnn")
	;
      return tid;
    }

    std::ostream&
    operator<< (std::ostream& os, const TFib &tfib)
    {
      os << "TFIB:" << std::endl;
      os << "///////////////////////////////////////////////////////////////////////////////" << std::endl;
      os << boost::format(tfib_formatter) % "DST PRFX" % "FC" % "PoA" % "Sequence" % "Part" % "RTx" % "3N Name";
      os << "-------------------------------------------------------------------------------" << std::endl;
      tfib.Print (os);
      os << "///////////////////////////////////////////////////////////////////////////////" << std::endl;
      return os;
    }

  } // namespace nnn
} // namespace ns3
