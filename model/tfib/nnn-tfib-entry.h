/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-tfib-entry.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-tfib-entry.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-tfib-entry.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */
#ifndef _NNN_TFIB_ENTRY_H_
#define	_NNN_TFIB_ENTRY_H_

#include <ostream>

#include "ns3/ptr.h"
#include "ns3/nstime.h"
#include "ns3/traced-value.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include "ns3/nnn-face.h"
#include "ns3/nnn-icn-interest.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-icn-naming.h"
#include "ns3/nnn-limits.h"

namespace ns3
{
  namespace icn
  {
    class Name;
  }
  namespace nnn
  {
    class TFib;

    /**
     * @ingroup ndn-tfib
     * @brief Namespace for TFIB operations
     */
    namespace tfib
    {

      /**
       * @ingroup nnn-tfib
       * \brief Structure holding various parameters associated with a (FibEntry, Face) tuple
       */
      class FaceMetric
      {
	/**
	 * @brief Color codes for TFIB face status
	 */
	public:

	struct PtrFaceComp
	{
	  bool operator () (const Ptr<const Face> &lhs , const Ptr<const Face>  &rhs) const  {
	    return *lhs < *rhs;
	  }
	};

	FaceMetric (Ptr<Face> face, uint32_t seq)
	: m_face                (face)
	, m_seq                 (seq)
	, m_part                (std::numeric_limits<uint32_t>::max ())
	, m_poa                 (Address ())
	, m_addr                (0)
	, m_retransmit_ui_timer (Seconds (0))
	{
	}

	/**
	 * \brief Metric constructor
	 *
	 * \param face Face for which metric
	 * \param seq Initial sequence number seen
	 */
	FaceMetric (Ptr<Face> face, uint32_t seq, Time retransmission)
	: m_face (face)
	, m_seq (seq)
	, m_part (std::numeric_limits<uint32_t>::max ())
	, m_retransmit_ui_timer (retransmission)
	{
	  m_poa = Address ();
	  m_addr = 0;
	}

	FaceMetric (Ptr<Face> face, Address poa, uint32_t seq, Time retransmission)
	: m_face (face)
	, m_seq (seq)
	, m_part (std::numeric_limits<uint32_t>::max ())
	, m_poa (poa)
	, m_retransmit_ui_timer (retransmission)
	{
	  m_addr = 0;
	}

	FaceMetric (Ptr<Face> face, Address poa, uint32_t seq, Time retransmission, Ptr<const NNNAddress> addr)
	: m_face (face)
	, m_seq (seq)
	, m_part (std::numeric_limits<uint32_t>::max ())
	, m_poa (poa)
	, m_addr (addr)
	, m_retransmit_ui_timer (retransmission)
	{
	}

	/**
	 * \brief Comparison operator used by boost::multi_index::identity<>
	 */
	bool
	operator< (const FaceMetric &fm) const { return m_seq > fm.m_seq; }

	Ptr<Face>
	GetFace () const
	{
	  return m_face;
	}

	/**
	 * @brief Get current sequence number
	 */
	uint32_t
	GetSequence () const
	{
	  return m_seq;
	}

	uint32_t
	GetPart () const
	{
	  return m_part;
	}

	Time
	GetRetransmissionTime () const
	{
	  return m_retransmit_ui_timer;
	}

	Address
	GetPoA () const
	{
	  return m_poa;
	}

	Ptr<const NNNAddress>
	Get3NNamePtr () const
	{
	  return m_addr;
	}

	void
	UpdateFaceInfo (Ptr<Face> face)
	{
	  m_face = face;
	}

	void
	UpdatePoAInfo (Address poa)
	{
	  m_poa = poa;
	}

	void
	UpdateRxInfo (Time retx)
	{
	  m_retransmit_ui_timer = retx;
	}

	void
	UpdateSrcInfo (Ptr<const NNNAddress> addr)
	{
	  m_addr = addr;
	}

	void
	UpdatePartInfo (uint32_t part)
	{
	  m_part = part;
	}

	void
	UpdateInfo (Ptr<Face> face, Time retransmission)
	{
	  m_face = face;
	  m_retransmit_ui_timer = retransmission;
	}

      private:
	friend std::ostream& operator<< (std::ostream& os, const FaceMetric &metric);

      private:
	Ptr<Face> m_face; ///< \brief Face
	uint32_t m_seq; ///< \brief Sequence number
	uint32_t m_part;///< \brief Part number
	Address m_poa; ///< \brief Obtained Point of Attachment name
	Ptr<const NNNAddress> m_addr; ///< \brief Obtained 3N name
	Time m_retransmit_ui_timer; ///< \brief Time for when to retransmit Interest IU for MapMe
      };

      /// @cond include_hidden
      class i_seq {};
      class i_metric {};
      class i_face {};
      class i_timer {};
      class i_poa {};
      /// @endcond

      /**
       * @ingroup nnn-tfib
       * @brief Typedef for sequence face container of Entry
       *
       */
      typedef boost::multi_index::multi_index_container<
	  FaceMetric,
	  boost::multi_index::indexed_by<
	  // Order by FaceMetric
	    boost::multi_index::ordered_unique<
	      boost::multi_index::tag<i_metric>,
	      boost::multi_index::identity<FaceMetric>
	    >,

	    // Order by sequence
	    boost::multi_index::ordered_unique<
	      boost::multi_index::tag<i_seq>,
	      boost::multi_index::const_mem_fun<FaceMetric,uint32_t,&FaceMetric::GetSequence>
	    >,

          // List of available faces ordered by Face name
	    boost::multi_index::ordered_non_unique<
	      boost::multi_index::tag<i_face>,
	      boost::multi_index::const_mem_fun<FaceMetric,Ptr<Face>,&FaceMetric::GetFace>,
	      FaceMetric::PtrFaceComp
      	    >,

	  // List of available faces ordered by Face name
	    boost::multi_index::ordered_non_unique<
	      boost::multi_index::tag<i_poa>,
	      boost::multi_index::const_mem_fun<FaceMetric,Address,&FaceMetric::GetPoA>
	    >,

	  // List of available faces ordered by retransmission time
      	    boost::multi_index::ordered_non_unique<
      	      boost::multi_index::tag<i_timer>,
      	      boost::multi_index::const_mem_fun<FaceMetric,Time,&FaceMetric::GetRetransmissionTime>
            >
          >
	> seq_list;

      typedef seq_list::index<i_metric>::type seq_list_by_metric;
      typedef seq_list::index<i_seq>::type seq_list_by_seq;

      /**
       * @ingroup nnn-tfib
       * \brief Structure for TFIB table entry, holding indexed list of
       *        available faces and their respective metrics
       */
      class Entry : public Object
      {
      public:
	typedef Entry base_type;

      public:
	class NoFaces {}; ///< @brief Exception class for the case when TFIB entry is not found

	/**
	 * \brief Constructor
	 * \param prefix smart pointer to the prefix for the TFIB entry
	 */
	Entry (Ptr<TFib> tfib, const Ptr<const icn::Name> &prefix)
	: m_tfib (tfib)
	, m_prefix (prefix)
	{
	}

	/**
	 * \brief Get prefix for the TFIB entry
	 */
	const icn::Name&
	GetPrefix () const { return *m_prefix; }

	bool
	HasSequence (uint32_t seq);

	Ptr<Face>
	GetFace (uint32_t seq);

	uint32_t
	GetPart (uint32_t seq);

	std::pair<Ptr<Face>, Address>
	GetForwardFace (uint32_t seq);

	Address
	GetPoA (uint32_t seq);

	Ptr<const NNNAddress>
	Get3NNamePtr (uint32_t seq);

	/**
	 *  \brief Get the Face stored for the newest sequence and the associated ICN name
	 */
	Ptr<Face>
	GetFace ();

	uint32_t
	GetPart ();

	std::pair<Ptr<Face>, Address>
	GetForwardFace ();

	Address
	GetPoA ();

	Ptr<const NNNAddress>
	Get3NNamePtr ();

	/**
	 * \brief Find the newest sequence for a particular ICN name
	 */
	uint32_t
	GetNewestSequence ();

	/**
	 * \brief Add or update the Face - Sequence number combination
	 */
	void
	AddOrUpdateSequence (Ptr<Face> face, uint32_t seq, Time retransmission);

	void
	AddOrUpdateSequenceName (Ptr<Face> face, uint32_t seq, Time retransmission, Ptr<const NNNAddress> addr);

	void
	AddOrUpdatePoASequence (Ptr<Face> face, Address poa, uint32_t seq, Time retransmission);

	void
	AddOrUpdatePoASequenceName (Ptr<Face> face, Address poa, uint32_t seq, Time retransmission, Ptr<const NNNAddress> addr);

	void
	AddOrUpdate (Ptr<Face> face, Address poa, Ptr<const Interest> interest, Ptr<const NNNAddress> addr);
	/**
	 * @brief Get pointer to access TFIB, to which this entry is added
	 */
	Ptr<TFib>
	GetTFib ()
	{
	  return m_tfib;
	}

	void
	Print (std::ostream &os) const;

      private:
	friend std::ostream& operator<< (std::ostream& os, const Entry &entry);

      public:
	Ptr<TFib> m_tfib; ///< \brief TFIB to which entry is added
	Ptr<const icn::Name> m_prefix; ///< \brief Prefix of the TFIB entry
	seq_list m_faces; ///< \brief Indexed list of faces
      };

      std::ostream& operator<< (std::ostream& os, const Entry &entry);
      std::ostream& operator<< (std::ostream& os, const FaceMetric &metric);

    } // namespace tfib
  } // namespace nnn
} // namespace ns3

#endif // _NNN_TFIB_ENTRY_H_
