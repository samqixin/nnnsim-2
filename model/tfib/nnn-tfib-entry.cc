/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-tfib-entry.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-tfib-entry.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-tfib-entry.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/nnn-icn-naming.h"

#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/ref.hpp>

namespace ll = boost::lambda;

#include "nnn-tfib.h"
#include "nnn-tfib-entry.h"

namespace ns3
{
  NS_LOG_COMPONENT_DEFINE ("nnn.tfib.Entry");

  namespace nnn
  {
    namespace tfib
    {
      bool
      Entry::HasSequence (uint32_t seq)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();
	seq_list_by_seq::iterator it = seq_index.find (seq);

	return (it != seq_index.end ());
      }

      Ptr<Face>
      Entry::GetFace (uint32_t seq)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();
	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end ())
	  {
	    return it->GetFace ();
	  }
	else
	  {
	    return 0;
	  }
      }

      uint32_t
      Entry::GetPart (uint32_t seq)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();
	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end ())
	  {
	    return it->GetPart ();
	  }
	else
	  {
	    return 0;
	  }
      }

      std::pair<Ptr<Face>, Address>
      Entry::GetForwardFace (uint32_t seq)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();
	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end())
	  return std::make_pair(it->GetFace (), it->GetPoA());
	else
	  return std::make_pair(Ptr<Face> (), Address ());
      }

      Address
      Entry::GetPoA (uint32_t seq)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();
	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end ())
	  {
	    return it->GetPoA();
	  }
	else
	  {
	    return Address ();
	  }
      }

      Ptr<const NNNAddress>
      Entry::Get3NNamePtr (uint32_t seq)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();
	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end ())
	  {
	    return it->Get3NNamePtr ();
	  }
	else
	  {
	    return 0;
	  }
      }


      uint32_t
      Entry::GetPart ()
      {
	seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	seq_list_by_metric::iterator it = seq_index.begin ();

	if (it != seq_index.end())
	  return it->GetPart ();
	else
	  return 0;
      }

      uint32_t
      Entry::GetNewestSequence ()
      {
	seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	seq_list_by_metric::iterator it = seq_index.begin ();

	if (it != seq_index.end())
	  return it->GetSequence();
	else
	  return 0;
      }

      Ptr<Face>
      Entry::GetFace ()
      {
	seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	seq_list_by_metric::iterator it = seq_index.begin ();

	if (it != seq_index.end())
	  return it->GetFace ();
	else
	  return 0;
      }


      std::pair<Ptr<Face>, Address>
      Entry::GetForwardFace ()
      {
	seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	seq_list_by_metric::iterator it = seq_index.begin ();

	if (it != seq_index.end())
	  return std::make_pair(it->GetFace (), it->GetPoA());
	else
	  return std::make_pair(Ptr<Face> (), Address ());
      }

      Address
      Entry::GetPoA ()
      {
	seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	seq_list_by_metric::iterator it = seq_index.begin ();

	if (it != seq_index.end())
	  return it->GetPoA();
	else
	  return Address ();
      }

      Ptr<const NNNAddress>
      Entry::Get3NNamePtr ()
      {
	seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	seq_list_by_metric::iterator it = seq_index.begin ();

	if (it != seq_index.end())
	  return it->Get3NNamePtr ();
	else
	  return 0;
      }

      void
      Entry::AddOrUpdateSequence (Ptr<Face> face, uint32_t seq, Time retransmission)
      {
	NS_LOG_FUNCTION (this);
	NS_ASSERT_MSG (face != NULL, "Trying to Add or Update NULL face");

	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();

	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end())
	  {
	    FaceMetric tmp = *it;

	    tmp.UpdateInfo (face, retransmission);

	    if (seq_index.replace (it, tmp))
	      {
		NS_LOG_DEBUG ("Have updated known Seq: " << seq << " with Face " << face->GetId () << " Time: " << retransmission);
	      }
	  }
	else
	  {
	    m_faces.insert (FaceMetric (face, seq, retransmission));
	  }
      }

      void
      Entry::AddOrUpdateSequenceName (Ptr<Face> face, uint32_t seq, Time retransmission, Ptr<const NNNAddress> addr)
      {
	NS_LOG_FUNCTION (this);
	NS_ASSERT_MSG (face != NULL, "Trying to Add or Update NULL face");

	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();

	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end())
	  {
	    FaceMetric tmp = *it;

	    tmp.UpdateInfo (face, retransmission);
	    tmp.UpdateSrcInfo(addr);

	    if (seq_index.replace (it, tmp))
	      {
		NS_LOG_DEBUG ("Have updated known Seq: " << seq << " with Face " << face->GetId () << " Time: " << retransmission);
	      }
	  }
	else
	  {
	    m_faces.insert (FaceMetric (face, seq, retransmission));
	  }
      }

      void
      Entry::AddOrUpdatePoASequence (Ptr<Face> face, Address poa, uint32_t seq, Time retransmission)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();

	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end())
	  {
	    FaceMetric tmp = *it;

	    tmp.UpdateInfo (face, retransmission);
	    tmp.UpdatePoAInfo(poa);

	    if (seq_index.replace (it, tmp))
	      {
		NS_LOG_DEBUG ("Have updated known Seq: " << seq << " with Face " << face->GetId () << " PoA: " << poa << " Time: " << retransmission);
	      }
	  }
	else
	  {
	    m_faces.insert (FaceMetric (face, poa, seq, retransmission));
	  }
      }

      void
      Entry::AddOrUpdatePoASequenceName (Ptr<Face> face, Address poa, uint32_t seq, Time retransmission, Ptr<const NNNAddress> addr)
      {
	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();

	seq_list_by_seq::iterator it = seq_index.find (seq);

	if (it != seq_index.end())
	  {
	    FaceMetric tmp = *it;

	    tmp.UpdateInfo (face, retransmission);
	    tmp.UpdatePoAInfo(poa);
	    tmp.UpdateSrcInfo(addr);

	    if (seq_index.replace (it, tmp))
	      {
		NS_LOG_DEBUG ("Have updated known Seq: " << seq << " with Face " << face->GetId () << " PoA: " << poa << " Time: " << retransmission);
	      }
	  }
	else
	  {
	    m_faces.insert (FaceMetric (face, poa, seq, retransmission, addr));
	  }
      }

      void
      Entry::AddOrUpdate (Ptr<Face> face, Address poa, Ptr<const Interest> interest, Ptr<const NNNAddress> addr)
      {
	NS_LOG_FUNCTION (this);

	NS_ASSERT_MSG (interest != NULL, "Trying to Add or update with no information!");

	seq_list_by_seq& seq_index = m_faces.get<i_seq> ();

	seq_list_by_seq::iterator it = seq_index.find (interest->GetSequenceNumber ());

	bool mod_poa = poa.IsInvalid() ? false : true;
	bool mod_addr = addr ? true : false;

	if (it != seq_index.end())
	  {
	    FaceMetric tmp = *it;

	    tmp.UpdateInfo (face, interest->GetRetransmissionTime ());
	    tmp.UpdatePartInfo (interest->GetPartNumber ());
	    if (mod_poa)
	      tmp.UpdatePoAInfo(poa);
	    if (mod_addr)
	      tmp.UpdateSrcInfo(addr);

	    if (seq_index.replace (it, tmp))
	      {
		NS_LOG_DEBUG ("Have updated known Seq: " << interest->GetSequenceNumber () << " Part: " << interest->GetPartNumber() << " with Face " << face->GetId () << " PoA: " << poa << " Time: " << interest->GetRetransmissionTime ());
	      }
	  }
	else
	  {
	    FaceMetric tmp = FaceMetric (face, interest->GetSequenceNumber ());

	    tmp.UpdateInfo (face, interest->GetRetransmissionTime ());
	    tmp.UpdatePartInfo (interest->GetPartNumber ());
	    if (mod_poa)
	      tmp.UpdatePoAInfo(poa);
	    if (mod_addr)
	      tmp.UpdateSrcInfo(addr);

	    m_faces.insert (tmp);
	  }
      }

      void
      Entry::Print (std::ostream &os) const
      {
	const seq_list_by_metric& seq_index = m_faces.get<i_metric> ();

	std::copy(seq_index.begin (), seq_index.end (), std::ostream_iterator<seq_list_by_metric::value_type> (os));
      }

      std::ostream& operator<< (std::ostream& os, const Entry &entry)
      {
	entry.Print (os);
	return os;
      }

      std::ostream& operator<< (std::ostream& os, const FaceMetric &metric)
      {
	if (!(metric.Get3NNamePtr ()))
	  {
	    os << boost::format(tfib_formatter) % " " % metric.GetFace ()->GetId () %
		metric.GetPoA () % metric.GetSequence () % metric.GetPart() %
		metric.GetRetransmissionTime().GetMilliSeconds() % "-";
	  }
	else
	  {
	    os << boost::format(tfib_formatter) % " " % metric.GetFace ()->GetId () %
		metric.GetPoA () % metric.GetSequence () % metric.GetPart() %
		metric.GetRetransmissionTime().GetMilliSeconds() % *(metric.Get3NNamePtr ());
	  }
	return os;
      }
    } // namespace tfib
  } // namespace nnn
} // namespace ns3
