/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-tfib.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-tfib.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-tfib.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_TFIB_H_
#define	_NNN_TFIB_H_

#include "ns3/node.h"
#include "ns3/simple-ref-count.h"

#include <boost/format.hpp>

#include "nnn-tfib-entry.h"

namespace ns3
{
  namespace nnn
  {
    class Interest;
    typedef Interest InterestHeader;

    /**
     * @ingroup nnn
     * @defgroup nnn-tfib TFIB
     */

    const boost::format tfib_formatter("%-8s|%-2s %-24s %-8x %-8x %-4s %-20s\n");
    /**
     * @ingroup nnn-tfib
     * @brief Class implementing TFIB functionality
     */
    class TFib : public Object
    {
      public:

      /**
       * \brief Interface ID
       *
       * \return interface ID
       */
      static TypeId GetTypeId ();
      /**
       * @brief Default constructor
       */
      TFib () {};

      /**
       * @brief Virtual destructor
       */
      virtual ~TFib () { };

      /**
       * \brief Perform longest prefix match
       *
       * \todo Implement exclude filters
       *
       * \param interest Interest packet header
       * \returns If entry found a valid iterator (Ptr<tfib::Entry>) will be returned, otherwise End () (==0)
       */
      virtual Ptr<tfib::Entry>
      LongestPrefixMatch (const Interest &interest) = 0;

      /**
       * @brief Get TFIB entry for the prefix (exact match)
       *
       * @param prefix Name for TFIB entry
       * @returns If entry is found, a valid iterator (Ptr<tfib::Entry>) will be returned. Otherwise End () (==0)
       */
      virtual Ptr<tfib::Entry>
      Find (const icn::Name &prefix) = 0;

      /**
       * \brief Add or update TFIB entry using smart pointer to prefix
       *
       * If the entry exists, metric will be updated. Otherwise, new entry will be created
       *
       * @param face		Forwarding Face
       * @param from		Forwarding PoA
       * @param interest	MapMe Interest
       * @param addr		Source 3N Name
       */
      virtual Ptr<tfib::Entry>
      Add (Ptr<Face> face, const Address& from, const Ptr<const Interest> &interest, Ptr<const NNNAddress> addr) = 0;

      /**
       * @brief Remove TFIB entry
       *
       * @param name	Smart pointer to prefix
       */
      virtual void
      Remove (const Ptr<const icn::Name> &prefix) = 0;

      /**
       * @brief Print out entries in TFIB
       */
      virtual void
      Print (std::ostream &os) const = 0;

      /**
       * @brief Get number of entries in TFIB
       */
      virtual uint32_t
      GetSize () const = 0;

      /**
       * @brief Return first element of TFIB (no order guaranteed)
       */
      virtual Ptr<const tfib::Entry>
      Begin () const = 0;

      /**
       * @brief Return first element of TFIB (no order guaranteed)
       */
      virtual Ptr<tfib::Entry>
      Begin () = 0;

      /**
       * @brief Return item next after last (no order guaranteed)
       */
      virtual Ptr<const tfib::Entry>
      End () const = 0;

      /**
       * @brief Return item next after last (no order guaranteed)
       */
      virtual Ptr<tfib::Entry>
      End () = 0;

      /**
       * @brief Advance the iterator
       */
      virtual Ptr<const tfib::Entry>
      Next (Ptr<const tfib::Entry>) const = 0;

      /**
       * @brief Advance the iterator
       */
      virtual Ptr<tfib::Entry>
      Next (Ptr<tfib::Entry>) = 0;

      /**
       * @brief Static call to cheat python bindings
       */
      static inline Ptr<TFib>
      GetTFib (Ptr<Object> node);

    private:
      TFib (const TFib&) {} ; ///< \brief copy constructor is disabled
    };

    std::ostream& operator<< (std::ostream& os, const TFib &fib);

    Ptr<TFib>
    TFib::GetTFib (Ptr<Object> node)
    {
      return node->GetObject<TFib> ();
    }
  } // namespace nnn
} // namespace ns3

#endif // _NNN_TFIB_H_
