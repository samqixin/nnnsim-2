/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-tfib-impl.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-tfib-impl.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-tfib-impl.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef _NNN_TFIB_IMPL_H_
#define	_NNN_TFIB_IMPL_H_

#include <boost/format.hpp>

#include "nnn-tfib.h"
#include "ns3/nnn-icn-interest.h"
#include "ns3/nnn-icn-naming.h"
#include "ns3/nnn-trie-with-policy.h"
#include "ns3/nnn-counting-policy.h"

namespace ns3
{
  namespace nnn
  {

    namespace tfib
    {
      /**
       * @ingroup ndn-tfib
       * @brief TFIB entry implementation with with additional references to the base container
       */
      class EntryImpl : public Entry
      {
      public:
	typedef nnn::nnnSIM::trie_with_policy<
	    ns3::icn::Name,
	    nnn::nnnSIM::smart_pointer_payload_traits<EntryImpl>,
	    nnn::nnnSIM::counting_policy_traits
	    > trie;

	EntryImpl (Ptr<TFib> tfib, const Ptr<const icn::Name> &prefix)
	: Entry (tfib, prefix)
	, item_ (0)
	{
	}

	void
	SetTrie (trie::iterator item)
	{
	  item_ = item;
	}

	trie::iterator to_iterator () { return item_; }
	trie::const_iterator to_iterator () const { return item_; }

      private:
	trie::iterator item_;
      };

      /**
       * @ingroup nnn-tfib
       * \brief Class implementing TFIB functionality
       */
      class TFibImpl : public TFib,
      protected nnn::nnnSIM::trie_with_policy<icn::Name,
      nnn::nnnSIM::smart_pointer_payload_traits< EntryImpl >,
      nnn::nnnSIM::counting_policy_traits >
      {
      public:
	typedef nnn::nnnSIM::trie_with_policy<icn::Name,
	    nnn::nnnSIM::smart_pointer_payload_traits<EntryImpl>,
	    nnn::nnnSIM::counting_policy_traits > super;

	/**
	 * \brief Interface ID
	 *
	 * \return interface ID
	 */
	static TypeId GetTypeId ();

	/**
	 * \brief Constructor
	 */
	TFibImpl ();

	virtual Ptr<Entry>
	LongestPrefixMatch (const Interest &interest);

	virtual Ptr<Entry>
	Find (const icn::Name &prefix);

	virtual Ptr<Entry>
	Add (Ptr<Face> face, const Address& from, const Ptr<const Interest> &interest, Ptr<const NNNAddress> addr);

	virtual void
	Remove (const Ptr<const icn::Name> &prefix);

	virtual void
	Print (std::ostream &os) const;

	uint32_t
	GetSize () const;

	virtual Ptr<const Entry>
	Begin () const;

	virtual Ptr<Entry>
	Begin ();

	virtual Ptr<const Entry>
	End () const;

	virtual Ptr<Entry>
	End ();

	virtual Ptr<const Entry>
	Next (Ptr<const Entry> item) const;

	virtual Ptr<Entry>
	Next (Ptr<Entry> item);

      protected:
	// inherited from Object class
	virtual void NotifyNewAggregate (); ///< @brief Notify when object is aggregated
	virtual void DoDispose (); ///< @brief Perform cleanup
      };
    } // namespace tfib
  } // namespace nnn
} // namespace ns3

#endif	/* _NNN_TFIB_IMPL_H_ */
