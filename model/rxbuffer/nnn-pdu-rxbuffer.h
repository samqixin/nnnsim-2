/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2017 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  nnn-pdu-rxbuffer.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-pdu-rxbuffer.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-pdu-rxbuffer.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 */

#ifndef NNN_PDU_RXBUFFER_H_
#define NNN_PDU_RXBUFFER_H_

#include "ns3/address.h"
#include "ns3/ptr.h"
#include "ns3/node.h"
#include "ns3/object.h"
#include "ns3/simulator.h"
#include "ns3/simple-ref-count.h"

#include <boost/format.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include "ns3/nnn-face.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-pdus.h"

using boost::multi_index_container;
using namespace ::boost::multi_index;

namespace ns3
{
  namespace nnn
  {
    // These structures allow searches by find using std::make_tuple(Seq, 3N src, 3N dst) when available
    struct seq_datapdu{};
    struct dst_datapdu{};
    struct src_datapdu{};
    struct time_datapdu{};
    struct flowid_datapdu{};

    const NNNAddress empty = NNNAddress ();

    struct Comp3N
    {
      bool
      operator () (const NNNAddress& a, const NNNAddress& b) const
      {
	return (a < b);
      }
    };

    struct DATAPDU_buffer_entry
    {
      Ptr<const DATAPDU> m_pdu;
      Time m_process_time;

      DATAPDU_buffer_entry (Ptr<const DATAPDU> pdu, Time now)
      : m_pdu (pdu)
      , m_process_time (now)
      {
      }

      uint32_t
      GetPDUId () const
      {
	return m_pdu->GetPacketId ();
      }

      uint32_t
      GetFlowid () const
      {
	return m_pdu->GetFlowid ();
      }

      uint32_t
      GetSequence () const
      {
	return m_pdu->GetSequence ();
      }

      Time
      GetProcessTime () const
      {
	return m_process_time;
      }

      const NNNAddress&
      GetSrcName () const
      {
	if (m_pdu->has3NSrc ())
	  return m_pdu->GetSrcName ();
	else
	  return empty;
      }

      const NNNAddress&
      GetDstName () const
      {
	if (m_pdu->has3NDst ())
	  return m_pdu->GetDstName ();
	else
	  return empty;
      }

      Ptr<const NNNAddress>
      GetSrcNamePtr () const
      {
	return m_pdu->GetSrcNamePtr ();
      }

      Ptr<const NNNAddress>
      GetDstNamePtr () const
      {
	return m_pdu->GetDstNamePtr ();
      }

      bool
      has3NSrc () const
      {
	return m_pdu->has3NSrc ();
      }

      bool
      has3NDst () const
      {
	return m_pdu->has3NDst ();
      }

      bool
      operator < (const DATAPDU_buffer_entry& b) const
      {
	if (GetPDUId () == b.GetPDUId())
	  {
	    if (GetFlowid () == b.GetFlowid ())
	      {
		if (GetSequence () == b.GetSequence ())
		  {
		    return (GetProcessTime () < b.GetProcessTime());
		  }
		else
		  {
		    return (GetSequence () < b.GetSequence ());
		  }
	      }
	    else
	      {
		return (GetFlowid () < b.GetFlowid ());
	      }
	  }
	else
	  {
	    return (GetPDUId () < b.GetPDUId());
	  }
      }
    };

    typedef multi_index_container<
	DATAPDU_buffer_entry,
	indexed_by<
	    ordered_unique<
	        tag<seq_datapdu>,
		composite_key<
		    DATAPDU_buffer_entry,
		    const_mem_fun<DATAPDU_buffer_entry, uint32_t, &DATAPDU_buffer_entry::GetPDUId>,
		    const_mem_fun<DATAPDU_buffer_entry, uint32_t, &DATAPDU_buffer_entry::GetFlowid>,
		    const_mem_fun<DATAPDU_buffer_entry, uint32_t, &DATAPDU_buffer_entry::GetSequence>
                >
            >,
	    ordered_non_unique<
	        tag<flowid_datapdu>,
		const_mem_fun<DATAPDU_buffer_entry, uint32_t, &DATAPDU_buffer_entry::GetFlowid>
            >,
	    ordered_non_unique<
	        tag<src_datapdu>,
		const_mem_fun<DATAPDU_buffer_entry, const NNNAddress&, &DATAPDU_buffer_entry::GetSrcName>,
		Comp3N
            >,
	    ordered_non_unique<
	        tag<dst_datapdu>,
		const_mem_fun<DATAPDU_buffer_entry, const NNNAddress&, &DATAPDU_buffer_entry::GetDstName>,
		Comp3N
            >,
	    ordered_non_unique<
	        tag<time_datapdu>,
		const_mem_fun<DATAPDU_buffer_entry, Time, &DATAPDU_buffer_entry::GetProcessTime>
            >
        >
    > datapdu_buffer;

    typedef datapdu_buffer::index<seq_datapdu>::type datapdu_buffer_by_seq;
    typedef datapdu_buffer::index<src_datapdu>::type datapdu_buffer_by_src;
    typedef datapdu_buffer::index<dst_datapdu>::type datapdu_buffer_by_dst;
    typedef datapdu_buffer::index<time_datapdu>::type datapdu_buffer_by_time;
    typedef datapdu_buffer::index<flowid_datapdu>::type datapdu_buffer_by_flowid;

    struct i_nf {};
    struct i_nf_fid {};
    struct i_nf_face {};
    struct i_nf_dst {};

    struct i_fid_rx {};
    struct i_fid_tx {};

    struct Flowid_Receiver
    {
      uint32_t m_flowid;
      uint32_t m_face;
      uint16_t m_dst_id;
      bool     m_rx_first_ack;
      uint32_t m_rx_max_acked_sequence;
      uint32_t m_rx_initial_sequence;
      std::set<uint32_t> m_rx_sequence;
      uint32_t m_rx_window;
      uint32_t m_rx_rate;
      bool     m_rx_ack;
      Time     m_rx_last_pdu;
      EventId  m_force_ack;
      EventId  m_flowid_erasure;

      bool
      operator < (const Flowid_Receiver b) const
      {
	if (m_flowid == b.m_flowid)
	  {
	    if (m_face == b.m_face)
	      {
		return (m_dst_id < b.m_dst_id);
	      }
	    else
	      {
		return (m_face < b.m_face);
	      }
	  }
	else
	  {
	    return (m_flowid < b.m_flowid);
	  }
      }
    };

    typedef multi_index_container<
	Flowid_Receiver,
	    indexed_by<
	        ordered_unique<
	            tag<i_fid_rx>,
		    composite_key<
		        Flowid_Receiver,
			member<Flowid_Receiver, uint32_t, &Flowid_Receiver::m_flowid>,
			member<Flowid_Receiver, uint32_t, &Flowid_Receiver::m_face>,
			member<Flowid_Receiver, uint16_t, &Flowid_Receiver::m_dst_id>
                    >
                >
           >
    > FlowidReceivedContainer;

    typedef FlowidReceivedContainer::index<i_fid_rx>::type FlowidReceivedContainer_uniq;

    struct Flowid_Sender
    {
      uint32_t m_flowid;
      uint32_t m_face;
      uint16_t m_dst_id;
      uint32_t m_tx_initial_sequence;
      uint32_t m_tx_sequence;
      std::set<uint32_t> m_tx_sequence_window;
      uint32_t m_tx_first_window;
      uint32_t m_tx_window;
      uint32_t m_tx_rate;
      uint32_t m_tx_initial_rate;
      uint32_t m_tx_window_retx;
      Time     m_pdu_before_ackp;
      Time     m_tx_last_ackp;
      Time     m_flowid_erasure_time;
      Time     m_ack_interval;
      EventId  m_flowid_erasure;
      bool     m_ackp_update;
      bool     m_forcing_retransmit;

      bool
      operator < (const Flowid_Sender& b) const
      {
	if (m_flowid == b.m_flowid)
	  {
	    if (m_face == b.m_face)
	      {
		return (m_dst_id < b.m_dst_id);
	      }
	    else
	      {
		return (m_face < b.m_face);
	      }
	  }
	else
	  {
	    return (m_flowid < b.m_flowid);
	  }
      }
    };

    typedef multi_index_container<
	Flowid_Sender,
	indexed_by<
	    ordered_unique<
	        tag<i_fid_tx>,
	        composite_key<
	            Flowid_Sender,
		    member<Flowid_Sender, uint32_t, &Flowid_Sender::m_flowid>,
		    member<Flowid_Sender, uint32_t, &Flowid_Sender::m_face>,
		    member<Flowid_Sender, uint16_t, &Flowid_Sender::m_dst_id>
                >
           >
        >
    > FlowidSentContainer;

    typedef FlowidSentContainer::index<i_fid_tx>::type FlowidSentContainer_uniq;

    struct Face_Flowid_Seq
    {
      uint32_t m_flowid;
      uint32_t m_face;
      uint16_t m_dst_id;
      uint32_t m_sequence;
      uint32_t m_first_window;
      uint32_t m_window;
      uint32_t m_rate;
      uint32_t m_pdu_count;
      Time     m_3n_a;
      Time     m_3n_r;
      Time     m_last_seen_ackp;
      bool     m_ack_tx;
      EventId  m_flowid_erasure;

      bool
      operator < (const Face_Flowid_Seq& b) const
      {
	if (m_flowid == b.m_flowid)
	  {
	    if (m_face == b.m_face)
	      {
		return (m_dst_id < b.m_dst_id);
	      }
	    else
	      {
		return (m_face < b.m_face);
	      }
	  }
	else
	  {
	    return (m_flowid < b.m_flowid);
	  }
      }
    };

    typedef multi_index_container<
	Face_Flowid_Seq,
	indexed_by<
	    ordered_unique<
	        tag<i_nf>,
		composite_key<
		    Face_Flowid_Seq,
		    member<Face_Flowid_Seq, uint32_t, &Face_Flowid_Seq::m_flowid>,
		    member<Face_Flowid_Seq, uint32_t, &Face_Flowid_Seq::m_face>,
		    member<Face_Flowid_Seq, uint16_t, &Face_Flowid_Seq::m_dst_id>
                >
            >,
	    ordered_non_unique<
	        tag<i_nf_fid>,
		member<Face_Flowid_Seq, uint32_t, &Face_Flowid_Seq::m_flowid>
            >,
	    ordered_non_unique<
	        tag<i_nf_face>,
		member<Face_Flowid_Seq, uint32_t, &Face_Flowid_Seq::m_face>
            >,
	    ordered_non_unique<
	        tag<i_nf_dst>,
		member<Face_Flowid_Seq, uint16_t, &Face_Flowid_Seq::m_dst_id>
            >
        >
    > FaceSequenceContainer;

    typedef FaceSequenceContainer::index<i_nf>::type FaceSequenceContainer_by_unique;
    typedef FaceSequenceContainer::index<i_nf_fid>::type FaceSequenceContainer_by_fid;
    typedef FaceSequenceContainer::index<i_nf_face>::type FaceSequenceContainer_by_face;
    typedef FaceSequenceContainer::index<i_nf_dst>::type FaceSequenceContainer_by_dst;

    struct Face_Flowid_Event
    {
      uint32_t m_flowid;
      uint32_t m_face;
      uint16_t m_dst_id;
      uint32_t m_sequence;
      uint32_t m_ini_sequence;
      uint32_t m_window;
      uint32_t m_rate;
      Time m_next_retransmit;
      EventId m_evt;

      bool
      operator < (const Face_Flowid_Event& b) const
      {
	if (m_flowid == b.m_flowid)
	  {
	    if (m_face == b.m_face)
	      {
		return (m_dst_id < b.m_dst_id);
	      }
	    else
	      {
		return (m_face < b.m_face);
	      }
	  }
	else
	  {
	    return (m_flowid < b.m_flowid);
	  }
      }
    };

    typedef multi_index_container<
	Face_Flowid_Event,
	indexed_by<
	    ordered_unique<
	        tag<i_nf>,
		composite_key<
		    Face_Flowid_Event,
		    member<Face_Flowid_Event, uint32_t, &Face_Flowid_Event::m_flowid>,
		    member<Face_Flowid_Event, uint32_t, &Face_Flowid_Event::m_face>,
		    member<Face_Flowid_Event, uint16_t, &Face_Flowid_Event::m_dst_id>
                >
            >,
	    ordered_non_unique<
	        tag<i_nf_fid>,
		member<Face_Flowid_Event, uint32_t, &Face_Flowid_Event::m_flowid>
            >
        >
    > FaceEventContainer;

    typedef FaceEventContainer::index<i_nf>::type FaceEventContainer_by_unique;
    typedef FaceEventContainer::index<i_nf_fid>::type FaceEventContainer_by_flowid;

    struct RxPDU
    {
      uint32_t m_face_id;
      uint16_t m_dst_id;
      uint32_t m_flowid;
      uint32_t m_sequence;
      uint32_t m_rxtimes;

      RxPDU ()
      : m_face_id  (0)
      , m_dst_id   (0)
      , m_flowid    (0)
      , m_sequence (0)
      , m_rxtimes  (0)
      {
      }

      RxPDU (uint32_t face_id, uint16_t dst_id, uint32_t flowid, uint32_t seq)
      : m_face_id  (face_id)
      , m_dst_id   (dst_id)
      , m_flowid   (flowid)
      , m_sequence (seq)
      , m_rxtimes  (0)
      {
      }

      uint32_t
      GetFaceId () const
      {
	return m_face_id;
      }

      uint16_t
      DstId () const
      {
	return m_dst_id;
      }

      uint32_t
      GetFlowid () const
      {
	return m_flowid;
      }

      uint32_t
      GetSequence () const
      {
	return m_sequence;
      }

      uint32_t
      GetTransmissions () const
      {
	return m_rxtimes;
      }

      bool
      operator < (const RxPDU e) const
      {
	if (m_flowid == e.m_flowid)
	  {
	    if (m_sequence == e.m_sequence)
	      {
		if (m_face_id == e.m_face_id)
		  {
		    return (m_dst_id < e.m_dst_id);
		  }
		else
		  {
		    return (m_face_id < e.m_face_id);
		  }
	      }
	    else
	      {
		return (m_sequence < e.m_sequence);
	      }
	  }
	else
	  {
	    return (m_flowid < e.m_flowid);
	  }
      }
    };

    struct pdu_rx{};
    struct pdu_rx_flowid {};

    typedef multi_index_container<
	RxPDU,
	indexed_by<
	    ordered_unique<
	        tag<pdu_rx>,
		composite_key<
		    RxPDU,
		    const_mem_fun<RxPDU, uint32_t, &RxPDU::GetFaceId>,
		    const_mem_fun<RxPDU, uint16_t, &RxPDU::DstId>,
		    const_mem_fun<RxPDU, uint32_t, &RxPDU::GetFlowid>,
		    const_mem_fun<RxPDU, uint32_t, &RxPDU::GetSequence>
                >
            >,
	    ordered_non_unique<
	        tag<pdu_rx_flowid>,
		member<RxPDU, uint32_t, &RxPDU::m_flowid>
            >
        >
    > pdu_rx_tracker;

    typedef pdu_rx_tracker::index<pdu_rx>::type pdu_rx_track;
    typedef pdu_rx_tracker::index<pdu_rx_flowid>::type pdu_rx_track_flowid;

    struct TxInfo
    {
      uint32_t m_face_id;
      uint16_t m_dst_id;
      uint32_t m_window;
      uint32_t m_initial_seq;
      bool     m_first;
      bool     m_new_flowid;

      TxInfo ()
      : m_face_id     (0)
      , m_dst_id      (0)
      , m_window      (0)
      , m_initial_seq (0)
      , m_first       (true)
      , m_new_flowid  (false)
      {
      }

      TxInfo (uint32_t face_id, uint16_t dst_id, uint32_t window, uint32_t sequence, bool new_flowid)
      : m_face_id     (face_id)
      , m_dst_id      (dst_id)
      , m_window      (window)
      , m_initial_seq (sequence)
      , m_first       (true)
      , m_new_flowid  (new_flowid)
      {
      }
    };

  } /* namespace nnn */
} /* namespace ns3 */

#endif /* NNN_PDU_RXBUFFER_H_ */
