/* -*- Mode:C++; c-file-style:"gnu" -*- */
/*
 * Copyright (c) 2016 Jairo Eduardo Lopez
 *
 *   This file is part of nnnsim.
 *
 *  string-center.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  string-center.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with string-center.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Kevin Ballard - StackoverFlow forums
 *  http://stackoverflow.com/questions/14861018/center-text-in-fixed-width-field-with-stream-manipulators-in-c
 *  Introduced into nnnsim by Jairo Eduardo Lopez
 */

#ifndef UTILS_STRING_CENTER_H_
#define UTILS_STRING_CENTER_H_

#include <string>
#include <iostream>
#include <iomanip>

template<typename charT, typename traits>
class help_to_center {
    std::basic_string<charT, traits> str_;
public:
    help_to_center(std::basic_string<charT, traits> str) : str_(str) {}
    template<typename a, typename b>
    friend std::basic_ostream<a, b>& operator<<(std::basic_ostream<a, b>& s, const help_to_center<a, b>& c);
};

template<typename charT, typename traits>
help_to_center<charT, traits> centered(std::basic_string<charT, traits> str) {
    return help_to_center<charT, traits>(str);
}

// redeclare for std::string directly so we can support anything that implicitly converts to std::string
help_to_center<std::string::value_type, std::string::traits_type> centered(const std::string& str) {
    return help_to_center<std::string::value_type, std::string::traits_type>(str);
}

template<typename charT, typename traits>
std::basic_ostream<charT, traits>& operator<<(std::basic_ostream<charT, traits>& s, const help_to_center<charT, traits>& c) {
    std::streamsize w = s.width();
    if ((uint32_t)w > (uint32_t)c.str_.length()) {
        std::streamsize left = (w + c.str_.length()) / 2;
        s.width(left);
        s << c.str_;
        s.width(w - left);
        s << "";
    } else {
        s << c.str_;
    }
    return s;
}

#endif /* UTILS_STRING_CENTER_H_ */
